//
//  TempScroll.m
//  ERAAPP
//
//  Created by kushna Mr. kothapali on 08/06/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "TempScroll.h"

#import "DetailViewController.h"

@implementation TempScroll

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
       
    NSLog(@"Touch Called!");
    
    DetailViewController *dObj=[[DetailViewController alloc]init];
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if(touch.view == self)
    {
    
    // Post a notification to hide the views from ScrollView...
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"hideComplete" object:dObj]; 
    
    NSLog(@"Notification Called :");
    
    }
    
    
}


@end
