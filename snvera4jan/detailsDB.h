//
//  detailsDB.h
//  mine
//
//  Created by Krishna Mr. kothapali on 27/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface detailsDB : NSObject

{

   //eSignDashboardAppDelegate *appdelegate;
}

-(NSString *)copyIfDBNeeded;
-(NSMutableArray *) selectAllFromDb;
-(NSMutableArray *)selectPlayerstFromDb;

-(void) insertIntoDb:(NSMutableDictionary *)mDict;

-(void) deleteFromDatabase;
-(void) deleteMatchDB:(NSString *)Str ;
-(void) updatedDB:(NSString *)Str dictionary:(NSMutableDictionary *)mDict;
-(void)insertPlayerIntoDb:(NSMutableDictionary *)mDict;
-(void) deletePlayerFromDatabase :(NSString *)teamName;
-(void) deletehomePlayerFromDatabase :(NSString *)teamName;
//-(void) deletePlayerFromDatabase :(NSString *)teamName :(NSString *)PlayerName :(NSString *)JerseyNumnber;
-(NSMutableArray *)selectHomeTeamPlayerstFromDb :(NSString *)TeamName;
-(void)insertHomePlayerIntoDb:(NSMutableDictionary *)mDict;
-(NSMutableArray *)selectTeamPlayerstFromDb :(NSString *)TeamName;
-(void) insertIntoDbforMatches:(NSMutableDictionary *)mDict;

-(NSMutableDictionary *)selectTotalInningsFromDb :(NSString *)Date :(NSString *)TeamName;
-(void)insertMatchSummary:(NSMutableDictionary *)macthDic;
-(NSMutableArray *)selectAllTotalInningsFromDb ;



@end
