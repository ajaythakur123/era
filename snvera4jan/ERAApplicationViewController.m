//
//  ERAApplicationViewController.m
//  ERAApplication
//
//  Created by ajay thakur on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ERAApplicationViewController.h"
#import "SelectTeamViewController.h"
#import "ADDRoasterViewControler.h"
#import "HomeTeamViewController.h"
#import "TestViewController.h"
#import "RecallMatchesViewController.h"

@implementation ERAApplicationViewController
@synthesize buttonNewGame,buttonRoaster,buttonLoadGame,ButtonSettings,buttonOurPitcher,buttonOurCatcher;
-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    NSMutableString
//    *str =[[NSMutableString stringWithFormat:@"%f",] substringToIndex:5];
//    
//    NSLog(@"......%@",str);
//    for (int index=0; index<=89; (index +=10)) {
//       
//        
//        NSLog(@"......%d",index);
//        
//        
//        
//        
//        
//        
//        if(index==80)
//        {
//            index=1;
//            NSLog(@"......%d",index);
//
//            
//        }
//        if(index==81)
//        {
//            index=2;
//              NSLog(@"......%d",index);
//        }
//
//        if(index==82)
//        {
//            index=3;
//              NSLog(@"......%d",index);
//        }
//
//        if(index==83)
//        {
//            index=4;
//              NSLog(@"......%d",index);
//        }
//        if(index==84)
//        {
//            index=5;
//              NSLog(@"......%d",index);
//        }
//        if(index==85)
//        {
//            index=6;
//              NSLog(@"......%d",index);
//        }
//        if(index==86)
//        {
//            index=7;
//            
//        }
//        if(index==87)
//        {
//            index=8;
//            
//        }
//        if(index==88)
//        {
//            index=9;
//            
//        }
//       
//
//        if(index==1)
//        {
//        
//            break;
//        }
//        
//        
//    }
    
    
    
 

// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}
-(IBAction)SelecTeam

{
    ADDRoasterViewControler *aDDRoasterViewControler = [[ADDRoasterViewControler alloc]initWithNibName:@"ADDRoasterViewControler" bundle:nil];
    [self.navigationController pushViewController:aDDRoasterViewControler animated:YES];
}


-(IBAction)setttings{
    
    UIAlertView  *  alert=[[UIAlertView alloc] initWithTitle:@"Under Progress" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
    
    [alert show];
}
-(IBAction)LoadGame{
    

    RecallMatchesViewController *recallMatchesViewController = [[RecallMatchesViewController alloc]initWithNibName:@"RecallMatchesViewController" bundle:nil];
    [self.navigationController pushViewController:recallMatchesViewController animated:YES];
}
-(IBAction)NewGame
{
    
    TestViewController *newGameViewController = [[TestViewController alloc]initWithNibName:@"TestViewController" bundle:nil];
    [self.navigationController pushViewController:newGameViewController animated:YES];


}




-(IBAction)Pitcher
{
    NSLog(@"%s",__func__);
    HomeTeamViewController *homeTeamViewController=[[HomeTeamViewController alloc]initWithNibName:@"HomeTeamViewController" bundle:nil];
    
    homeTeamViewController.type=@"Pitcher";
    [self.navigationController pushViewController:homeTeamViewController animated:YES];
}

-(IBAction)Catcher
{
    NSLog(@"%s",__func__);
    HomeTeamViewController *homeTeamViewController=[[HomeTeamViewController alloc]initWithNibName:@"HomeTeamViewController" bundle:nil];
    homeTeamViewController.type=@"Catcher";
    [self.navigationController pushViewController:homeTeamViewController animated:YES];
}


//-(void)didRotateFromInterfaceOrientation: (UIInterfaceOrientation) fromInterfaceOrientation
//{
//    
//    if(fromInterfaceOrientation != UIInterfaceOrientationPortrait &&
//       fromInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown)
//    {
//        // portait
//        
//     
//        buttonNewGame.frame=CGRectMake(294, 288, 180,37);
//        buttonLoadGame.frame=CGRectMake(294, 350, 180,37);
//        buttonRoaster.frame=CGRectMake(294, 412, 180,37);
//        ButtonSettings.frame=CGRectMake(294, 473, 180,37);
//        
//        
//    }
//    else
//    {  
//        // landscape
//        buttonNewGame.frame=CGRectMake(320, 288, 180,37);
//        buttonLoadGame.frame=CGRectMake(320, 350, 180,37);
//        buttonRoaster.frame=CGRectMake(320, 412, 180,37);
//        ButtonSettings.frame=CGRectMake(320, 473, 180,37);
//              
//        
//    }
//    
//}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
 
    
    
    if(interfaceOrientation != UIInterfaceOrientationPortrait &&
       interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown)
    {
        // landscape
        
        buttonNewGame.frame=CGRectMake(400, 200, 180,37);
        buttonLoadGame.frame=CGRectMake(400, 260, 180,37);
        
        buttonRoaster.frame=CGRectMake(400, 320, 180,37);
        buttonOurPitcher.frame=CGRectMake(400, 380, 180,37);
        buttonOurCatcher.frame=CGRectMake(400,440, 180,37);
        ButtonSettings.frame=CGRectMake(400, 500, 180,37);
      
        
        
    }
    else
    {  
        // portrait
       
        buttonNewGame.frame=CGRectMake(279, 290, 180,37);
        buttonLoadGame.frame=CGRectMake(279, 350, 180,37);
      
        buttonRoaster.frame=CGRectMake(279, 410, 180,37);
        buttonOurPitcher.frame=CGRectMake(279, 470, 180,37);
        buttonOurCatcher.frame=CGRectMake(279,530, 180,37);
        ButtonSettings.frame=CGRectMake(279, 590, 180,37);
        
    }
   return YES;
    
}

@end
