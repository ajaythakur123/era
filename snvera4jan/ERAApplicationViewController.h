//
//  ERAApplicationViewController.h
//  ERAApplication
//
//  Created by ajay thakur on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ERAApplicationViewController : UIViewController

{
    UIButton *buttonNewGame;
    UIButton *buttonLoadGame;
    UIButton *buttonRoaster;
    UIButton *ButtonSettings;
    UIButton *buttonOurPitcher;
    UIButton *buttonOurCatcher;
}
@property(nonatomic,strong)IBOutlet UIButton *buttonOurCatcher;
@property(nonatomic,strong)IBOutlet UIButton *buttonOurPitcher;
@property(nonatomic,strong)IBOutlet UIButton *buttonNewGame;
@property(nonatomic,strong)IBOutlet UIButton *buttonLoadGame;
@property(nonatomic,strong)IBOutlet UIButton *buttonRoaster;
@property(nonatomic,strong)IBOutlet UIButton *ButtonSettings;



-(IBAction)SelecTeam;

-(IBAction)LoadGame;
-(IBAction)NewGame;
-(IBAction)Pitcher;
-(IBAction)Catcher;
-(IBAction)setttings;
@end
