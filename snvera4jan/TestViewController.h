//
//  TestViewController.h
//  ERAAPP
//
//  Created by Uday Kumar on 8/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "detailsDB.h"
@class RootViewController;
@class DetailViewController;
@class ViewController;
@interface TestViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
    
    //---ambadas---
    NSMutableArray *teamNames_Array;
    NSMutableArray *arrayPlayers;
     NSMutableArray *array_pitcher;
    NSMutableArray * array_catcher;
    
    IBOutlet UITableView  *tblw_alldatatable;
    IBOutlet UILabel  *lbl_teamName;
    IBOutlet UILabel  *lbl_opponentName;
    IBOutlet UILabel  *lbl_catcher;
    IBOutlet UILabel * lbl_pitcher;
    IBOutlet UILabel *lbl_titleMatchdata;
    IBOutlet UIView  *viewAddplayer;
     IBOutlet UIView  *viewAddplayerCP;
     IBOutlet   UITextField *myTextField;
    
    IBOutlet   UITextField *playerNameTextField;
    IBOutlet   UITextField *JerseyNumberTextField;
    IBOutlet UIButton *btn_batterside;
       IBOutlet UIButton *btnPitcher;
     IBOutlet UIButton *btnNewGame;
    IBOutlet UIView  *vw_newgameoriantation;
    UIAlertView *alert;
    UITextField  *textAlert;
    
    IBOutlet UILabel *lbl_datepicker;
    UIDatePicker *date_picker;
    IBOutlet   UINavigationBar *nvgn_datepicker;
    
    IBOutlet UIView *vw_matchinfoData;
    BOOL  bl_DefensivePosiPopups;
    
    NSString *type;
    NSString *typeBatter;
    
    detailsDB *DBObj;
    NSMutableArray   *matchDataArray;
    NSMutableDictionary *matchDic;
    NSString *homeTeamName;
    NSMutableDictionary *mdict;
   
}
@property (nonatomic, strong) UIButton *btnPitcher;
@property (nonatomic, strong) UIButton *btnNewGame;
@property (nonatomic, strong) UITextField *playerNameTextField;
@property (nonatomic, strong) UITextField *JerseyNumberTextField;
@property (nonatomic, strong) UIButton *btn_batterside;


@property (nonatomic, strong)   UIView  *viewAddplayer;
@property (nonatomic, strong)   UIView   *viewAddplayerCP;
@property (nonatomic, strong) IBOutlet UISplitViewController *splitViewController;

@property (nonatomic, strong) IBOutlet RootViewController *rootViewController;

@property (nonatomic, strong) IBOutlet DetailViewController *detailViewController;
@property (nonatomic, strong) IBOutlet ViewController *viewController;
@property (nonatomic, strong) NSString *homeTeamName;
//----Ambadas---

@property(nonatomic,strong) IBOutlet UIView  *vw_newgameoriantation;
@property(nonatomic,strong)IBOutlet UILabel *lbl_datepicker;
@property(nonatomic,strong)IBOutlet UIView *vw_matchinfoData;
@property(nonatomic,strong) IBOutlet   UINavigationBar *nvgn_datepicker;
@property(nonatomic,strong) UIDatePicker *date_picker;



-(BOOL)validateTextField:(NSString *)candidate;
-(void)CatcherPitcher:(id)sender;
-(IBAction)Cancel;
-(IBAction)sumbmitPlayerToDabase;
-(IBAction)AddPlayer:(id)sender;
-(IBAction)Datepickerbutton:(id)sender;
-(IBAction)DatepickerCancel:(id)sender;
-(IBAction)DatepickerClicked:(id)sender;
-(IBAction)MatchinfoData:(UIButton *)sender;
-(IBAction)NewGame;
-(IBAction)addNew:(id)sender;
-(void)getTeamDb;
-(void)getOutEntry;
-(IBAction)addNew:(id)sender;
//-------------
-(IBAction)push:(id)sender;
@end
