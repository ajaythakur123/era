//
//  detailsDB.m
//  mine
//
//  Created by Krishna Mr. kothapali on 27/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "detailsDB.h"

@implementation detailsDB



//   Getting the Document Directory path And Copy DataBase file inot DocumentsDirectory.......


-(NSString *)copyIfDBNeeded
{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    
    
    // to know the Path for documents directory
    
    NSString *documentsDir=[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSLog(@"%@",documentsDir);
    NSString *writableDBPath=[documentsDir stringByAppendingPathComponent:@"ERA_Database.sqlite"];
    
    
    BOOL success=[fileManager fileExistsAtPath:writableDBPath];
    
    if(!success)
        
    {
        
        
        NSString *defaultDBPath=[[NSBundle mainBundle]pathForResource:@"ERA_Database" ofType:@"sqlite"];
        
        
        success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
        
        if (!success)
            NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
        
    }
    
    
    return writableDBPath;
}




//  Insert values into  DataBase..................

#pragma mark Team name insertion



-(void) insertIntoDb:(NSMutableDictionary *)mDict
{
    
 


    NSString *projectPath=[self copyIfDBNeeded];
    
    sqlite3_stmt *insertStmt = nil;
    sqlite3 *cuisineDB = nil;

    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    
    // Open the database. The database was prepared outside the application.
    
    
    if (sqlite3_open([projectPath UTF8String], &cuisineDB) == SQLITE_OK)
    {
        if(insertStmt == nil) 
        
        {
           
            NSString *strValue;
         
               strValue = [NSString stringWithFormat:@"insert into TeamName(TeamName,Location) Values(?,?)"];
                NSLog(@"value of the string = %@",strValue);
                   
            
            const char *sql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(cuisineDB, sql, -1, &insertStmt, NULL) != SQLITE_OK) {
                NSLog(@"Error while creating InsertStmt  %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);
                
           
            }
        
        }
        
        
        
        NSString *Name;
      

        Name=[mDict objectForKey:@"TeamName"];
      NSString *Location =[mDict objectForKey:@"Location"];
      
        
        sqlite3_bind_text(insertStmt, 1, [Name UTF8String], -1, SQLITE_TRANSIENT);
         sqlite3_bind_text(insertStmt, 2, [Location UTF8String], -1, SQLITE_TRANSIENT);
        
      //sqlite3_bind_blob(insertStmt, 2, [dataImage bytes], [dataImage length], SQLITE_TRANSIENT);
   
       //  sqlite3_finalize(insertStmt);
        if(SQLITE_DONE != sqlite3_step(insertStmt)) {           
            NSLog(@"Error while Executing InsertStmt  %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);           
            
        }
        
        sqlite3_reset(insertStmt);
        
        if (insertStmt) 
        
        {
            sqlite3_finalize(insertStmt);
            insertStmt = nil;
        
        }       
    }
   // sqlite3_finalize(insertStmt);
    
    sqlite3_close(cuisineDB);   
    
    
    
}
#pragma mark insert matches
-(void) insertIntoDbforMatches:(NSMutableDictionary *)mDict
{
    
    
    
    
    NSString *projectPath=[self copyIfDBNeeded];
    
    sqlite3_stmt *insertStmt = nil;
    sqlite3 *cuisineDB = nil;
    
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    
    // Open the database. The database was prepared outside the application.
    
    
    if (sqlite3_open([projectPath UTF8String], &cuisineDB) == SQLITE_OK)
    {
        if(insertStmt == nil) 
            
        {
            
            NSString *strValue;
            
            strValue = [NSString stringWithFormat:@"insert into MatchTable(TeamName,OppentTeamName,Date,Catcher,Pitcher) Values(?,?,?,?,?)"];
            NSLog(@"value of the string = %@",strValue);
            
            
            const char *sql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(cuisineDB, sql, -1, &insertStmt, NULL) != SQLITE_OK) {
                NSLog(@"Error while creating InsertStmt  %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);
                
                
            }
            
        }
        
        
                
        
        NSString *TeamName =[mDict objectForKey:@"TeamName"];
        NSString *OppentTeamName =[mDict objectForKey:@"OppentTeamName"];
        NSString *Date =[mDict objectForKey:@"Date"];
        NSString *Catcher =[mDict objectForKey:@"Catcher"];
        NSString *Pitcher =[mDict objectForKey:@"Pitcher"];
        
        sqlite3_bind_text(insertStmt, 1, [TeamName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 2, [OppentTeamName UTF8String], -1, SQLITE_TRANSIENT);
        
        sqlite3_bind_text(insertStmt, 3, [Date UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 4, [Catcher UTF8String], -1, SQLITE_TRANSIENT);
          sqlite3_bind_text(insertStmt, 5, [Pitcher UTF8String], -1, SQLITE_TRANSIENT);

        //sqlite3_bind_blob(insertStmt, 2, [dataImage bytes], [dataImage length], SQLITE_TRANSIENT);
        
        //  sqlite3_finalize(insertStmt);
        if(SQLITE_DONE != sqlite3_step(insertStmt)) {           
            NSLog(@"Error while Executing InsertStmt  %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);           
            
        }
        
        sqlite3_reset(insertStmt);
        
        if (insertStmt) 
            
        {
            sqlite3_finalize(insertStmt);
            insertStmt = nil;
            
        }       
    }
    // sqlite3_finalize(insertStmt);
    
    sqlite3_close(cuisineDB);   
    
    
    
}

#pragma mark insert match Summmary


-(void)insertMatchSummary:(NSMutableDictionary *)macthDic{
    
    NSString *projectPath=[self copyIfDBNeeded];
    
    sqlite3_stmt *insertStmt = nil;
    sqlite3 *cuisineDB = nil;
    
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    
    // Open the database. The database was prepared outside the application.
    
    
    if (sqlite3_open([projectPath UTF8String], &cuisineDB) == SQLITE_OK)
    {
        if(insertStmt == nil) 
            
        {
            
            NSString *strValue;
            
            strValue = [NSString stringWithFormat:@"insert into MatchData(Date,TeamName,TotalInnings) Values(?,?,?)"];
            NSLog(@"value of the string = %@",strValue);
            
            
            const char *sql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(cuisineDB, sql, -1, &insertStmt, NULL) != SQLITE_OK) {
                NSLog(@"Error while creating InsertStmt  %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);
                
                
            }
            
        }
        
        NSString *Date =[macthDic objectForKey:@"Date"];
    
        NSString *TeamName =[macthDic objectForKey:@"TeamName"];
        
            
        NSData *TotalInnings =[macthDic objectForKey:@"TotalInnings"];
              
        sqlite3_bind_text(insertStmt, 1, [Date UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 2, [TeamName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_blob(insertStmt, 3, [TotalInnings bytes], [TotalInnings length], SQLITE_TRANSIENT);
    
     
        //sqlite3_bind_blob(insertStmt, 2, [dataImage bytes], [dataImage length], SQLITE_TRANSIENT);
        
        //  sqlite3_finalize(insertStmt);
        if(SQLITE_DONE != sqlite3_step(insertStmt)) {           
            NSLog(@"Error while Executing InsertStmt  %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);           
            
        }
        
        sqlite3_reset(insertStmt);
        
        if (insertStmt) 
            
        {
            sqlite3_finalize(insertStmt);
            insertStmt = nil;
            
        }       
    }
    // sqlite3_finalize(insertStmt);
    
    sqlite3_close(cuisineDB);   

}


#pragma mark Select Players data
-(NSMutableArray *)selectPlayerstFromDb
{
    
    NSString *projectPath=[self copyIfDBNeeded];
    
    sqlite3_stmt *selectAllStmt = nil;
    sqlite3 *cuisineDB = nil;
    
    NSMutableArray *PlayerArr=[[NSMutableArray alloc]init];
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
        
    {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
        
    }
    
    // Open the database
    
    
    if (sqlite3_open([projectPath UTF8String], &cuisineDB) == SQLITE_OK)
    {
        NSString *strValue = [NSString stringWithFormat:@"SELECT * FROM TeamName"];
        const char *sql = [strValue UTF8String];
        
        if(sqlite3_prepare_v2(cuisineDB, sql, -1, &selectAllStmt, NULL) == SQLITE_OK)
            
        {
            
            while(sqlite3_step(selectAllStmt) == SQLITE_ROW)
                
            {
                NSMutableDictionary *mDict=[[NSMutableDictionary alloc]init];
                
              
                
//                 int Id=sqlite3_column_int(selectAllStmt, 0);
//                
//                NSString *TeamId =[NSString stringWithFormat:@"%d",Id];
                  NSString *TeamName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllStmt, 1)];
                  NSString *Location=[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllStmt, 2)];
               
                 // NSData *dataForImage = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAllStmt, 1) length: 
                
                  [mDict setObject:TeamName forKey:@"TeamName"];
                  [mDict setObject:Location forKey:@"Location"];
            
                
                
                [PlayerArr addObject:mDict];
                
            }
            
        }
        
        sqlite3_reset(selectAllStmt);
        
        if (selectAllStmt) 
            
        {           
            sqlite3_finalize(selectAllStmt);
            selectAllStmt = nil;
        }
        
    }
    
    sqlite3_close(cuisineDB);
    
    return PlayerArr;
    
}
#pragma mark Select Players data
-(NSMutableArray *)selectTeamPlayerstFromDb :(NSString *)TeamName
{
    
    NSString *projectPath=[self copyIfDBNeeded];
    
    sqlite3_stmt *selectAllStmt = nil;
    sqlite3 *cuisineDB = nil;
    
    NSMutableArray *PlayerArr=[[NSMutableArray alloc]init];
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
        
    {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
        
    }
    
    // Open the database
    
    
    if (sqlite3_open([projectPath UTF8String], &cuisineDB) == SQLITE_OK)
    {
        NSString *strValue = [NSString stringWithFormat:@"SELECT * FROM PlayerData WHERE TeamName ='%@'",TeamName];
        const char *sql = [strValue UTF8String];
        
        if(sqlite3_prepare_v2(cuisineDB, sql, -1, &selectAllStmt, NULL) == SQLITE_OK)
            
        {
            
            while(sqlite3_step(selectAllStmt) == SQLITE_ROW)
                
            {
                NSMutableDictionary *mDict=[[NSMutableDictionary alloc]init];
                
                
                
                //                 int Id=sqlite3_column_int(selectAllStmt, 0);
                //                
                //                NSString *TeamId =[NSString stringWithFormat:@"%d",Id];
                NSString *TeamName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllStmt, 0)];
                NSString *PlayerName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllStmt, 1)];
                
                NSString *JerseyNumber=[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllStmt, 2)];
                NSString *Handling=[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllStmt, 3)];
                
                
                [mDict setObject:TeamName forKey:@"TeamName"];
                [mDict setObject:PlayerName forKey:@"PlayerName"];
                [mDict setObject:JerseyNumber forKey:@"JerseyNumber"];
                [mDict setObject:Handling forKey:@"Handling"];
                
                
                [PlayerArr addObject:mDict];
                
            }
            
        }
        
        sqlite3_reset(selectAllStmt);
        
        if (selectAllStmt) 
            
        {           
            sqlite3_finalize(selectAllStmt);
            selectAllStmt = nil;
        }
        
    }
    
    sqlite3_close(cuisineDB);
    
    return PlayerArr;
    
}
#pragma mark select HomeTeamPlayerstFromDb
-(NSMutableArray *)selectHomeTeamPlayerstFromDb :(NSString *)TeamName
{
    
    NSString *projectPath=[self copyIfDBNeeded];
    
    sqlite3_stmt *selectAllStmt = nil;
    sqlite3 *cuisineDB = nil;
    
    NSMutableArray *PlayerArr=[[NSMutableArray alloc]init];
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
        
    {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
        
    }
    
    // Open the database
    
    
    if (sqlite3_open([projectPath UTF8String], &cuisineDB) == SQLITE_OK)
    {
        NSString *strValue = [NSString stringWithFormat:@"SELECT * FROM HomeTeam WHERE Status ='%@'",TeamName];
        const char *sql = [strValue UTF8String];
        
        if(sqlite3_prepare_v2(cuisineDB, sql, -1, &selectAllStmt, NULL) == SQLITE_OK)
            
        {
            
            while(sqlite3_step(selectAllStmt) == SQLITE_ROW)
                
            {
                NSMutableDictionary *mDict=[[NSMutableDictionary alloc]init];
                
                
                
                //                 int Id=sqlite3_column_int(selectAllStmt, 0);
                //                
                //                NSString *TeamId =[NSString stringWithFormat:@"%d",Id];
                NSString *TeamName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllStmt, 0)];
                
                 NSString *Status=[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllStmt, 1)];
                
                NSString *PlayerName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllStmt, 2)];
                
                NSString *JerseyNumber=[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllStmt, 3)];
                NSString *Handling=[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllStmt, 4)];
                
                
                [mDict setObject:TeamName forKey:@"TeamName"];
                [mDict setObject:Status forKey:@"Status"];
                [mDict setObject:PlayerName forKey:@"PlayerName"];
                [mDict setObject:JerseyNumber forKey:@"JerseyNumber"];
                [mDict setObject:Handling forKey:@"Handling"];
                
                
                [PlayerArr addObject:mDict];
                
            }
            
        }
        
        sqlite3_reset(selectAllStmt);
        
        if (selectAllStmt) 
            
        {           
            sqlite3_finalize(selectAllStmt);
            selectAllStmt = nil;
        }
        
    }
    
    sqlite3_close(cuisineDB);
    
    return PlayerArr;
    
}


#pragma mark store player name

-(void)insertPlayerIntoDb:(NSMutableDictionary *)mDict
{
    
    
    
    
    NSString *projectPath=[self copyIfDBNeeded];
    
    sqlite3_stmt *insertStmt = nil;
    sqlite3 *cuisineDB = nil;
    
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    
    // Open the database. The database was prepared outside the application.
    
    
    if (sqlite3_open([projectPath UTF8String], &cuisineDB) == SQLITE_OK)
    {
        if(insertStmt == nil) 
            
        {
            
            NSString *strValue = [NSString stringWithFormat:@"insert into PlayerData(TeamName,PlayerName,jerseyNumber,Handled) Values(?,?,?,?)"];
            NSLog(@"value of the string = %@",strValue);
            
            const char *sql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(cuisineDB, sql, -1, &insertStmt, NULL) != SQLITE_OK) {
                NSLog(@"Error while creating InsertStmt  %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);
                
                
            }
            
        }
        
        
        
       
          NSString *TeamName=[mDict objectForKey:@"TeamName"];
        
         NSString *PlayerName=[mDict objectForKey:@"PlayerName"];
        
         NSString *JerseyNumber=[mDict objectForKey:@"JerseyNumber"]; 
        NSString *Handling=[mDict objectForKey:@"Handling"];
        // NSString *TeamId=[mDict objectForKey:@"TeamId"];
        
        
         sqlite3_bind_text(insertStmt, 1, [TeamName UTF8String], -1, SQLITE_TRANSIENT);
         sqlite3_bind_text(insertStmt, 2, [PlayerName UTF8String], -1, SQLITE_TRANSIENT);
         sqlite3_bind_text(insertStmt, 3, [JerseyNumber UTF8String], -1, SQLITE_TRANSIENT);
         sqlite3_bind_text(insertStmt, 4, [Handling UTF8String], -1, SQLITE_TRANSIENT);
 
        
        //  sqlite3_finalize(insertStmt);
        if(SQLITE_DONE != sqlite3_step(insertStmt)) {           
            NSLog(@"Error while Executing InsertStmt  %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);           
            
        }
        
        sqlite3_reset(insertStmt);
        
        if (insertStmt) 
            
        {
         
            
            sqlite3_finalize(insertStmt);
            insertStmt = nil;
            
        }       
    }
    // sqlite3_finalize(insertStmt);
    
    sqlite3_close(cuisineDB);   
    
    
    
}
#pragma mark insert Home player

-(void)insertHomePlayerIntoDb:(NSMutableDictionary *)mDict
{
    
    

    NSString *projectPath=[self copyIfDBNeeded];
    
    sqlite3_stmt *insertStmt = nil;
    sqlite3 *cuisineDB = nil;
    
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    
    // Open the database. The database was prepared outside the application.
    
    
    if (sqlite3_open([projectPath UTF8String], &cuisineDB) == SQLITE_OK)
    {
        if(insertStmt == nil) 
            
        {
            
            NSString *strValue = [NSString stringWithFormat:@"insert into HomeTeam(TeamName,Status,PlayerName,jerseyNumber,Handled) Values(?,?,?,?,?)"];
            NSLog(@"value of the string = %@",strValue);
            
            const char *sql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(cuisineDB, sql, -1, &insertStmt, NULL) != SQLITE_OK) {
                NSLog(@"Error while creating InsertStmt  %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);
                
                
            }
            
        }
        
        
        
        
        NSString *TeamName=[mDict objectForKey:@"TeamName"];
        
        NSString *PlayerName=[mDict objectForKey:@"PlayerName"];
        
        NSString *JerseyNumber=[mDict objectForKey:@"JerseyNumber"]; 
        NSString *Handling=[mDict objectForKey:@"Handling"];
        NSString *Status=[mDict objectForKey:@"Status"];
        
        // NSString *TeamId=[mDict objectForKey:@"TeamId"];
        
        
        sqlite3_bind_text(insertStmt, 1, [TeamName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 2, [Status UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 3, [PlayerName UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 4, [JerseyNumber UTF8String], -1, SQLITE_TRANSIENT);
        sqlite3_bind_text(insertStmt, 5, [Handling UTF8String], -1, SQLITE_TRANSIENT);
        
        
        //  sqlite3_finalize(insertStmt);
        if(SQLITE_DONE != sqlite3_step(insertStmt)) {           
            NSLog(@"Error while Executing InsertStmt  %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);           
            
        }
        
        sqlite3_reset(insertStmt);
        
        if (insertStmt) 
            
        {
            
            
            sqlite3_finalize(insertStmt);
            insertStmt = nil;
            
        }       
    }
    // sqlite3_finalize(insertStmt);
    
    sqlite3_close(cuisineDB);   
    
    
    
}


//  Retrieve from DataBase..................


-(NSMutableArray *) selectAllFromDb
{
    
    NSString *projectPath=[self copyIfDBNeeded];
    
    sqlite3_stmt *selectAllStmt = nil;
    sqlite3 *cuisineDB = nil;
    
    NSMutableArray *dataArr=[[NSMutableArray alloc]init];
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
    
    {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    
    }
    
    // Open the database
    
    
    if (sqlite3_open([projectPath UTF8String], &cuisineDB) == SQLITE_OK)
    {
        
       
        
        NSString *strValue;
       
            
            strValue = [NSString stringWithFormat:@"SELECT * FROM UserSign"];

        
        
        
    
        const char *sql = [strValue UTF8String];
        
        if(sqlite3_prepare_v2(cuisineDB, sql, -1, &selectAllStmt, NULL) == SQLITE_OK)
            
        {
            
            while(sqlite3_step(selectAllStmt) == SQLITE_ROW)
            
            {
                NSMutableDictionary *mDict=[[NSMutableDictionary alloc]init];
                
                NSString *Name;
             
                
                Name=[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllStmt, 0)];
               
                NSData *dataForImage = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAllStmt, 1) length: sqlite3_column_bytes(selectAllStmt, 1)];                
                [mDict setObject:Name forKey:@"Name"];
                [mDict setObject:dataForImage forKey:@"signImage"];
           
                
                [dataArr addObject:mDict];
                
            }
      
        }
        
        sqlite3_reset(selectAllStmt);
        
        if (selectAllStmt) 
        
        {           
            sqlite3_finalize(selectAllStmt);
            selectAllStmt = nil;
        }
        
    }
    
    sqlite3_close(cuisineDB);
    
    return dataArr;
    
}



//  Delete Row From DataBase ..................

#pragma mark Delete from database

-(void) deletePlayerFromDatabase :(NSString *)teamName
{
    
    NSString *projectPath=[self copyIfDBNeeded];
    
    sqlite3_stmt *deleteStmt = nil;
    sqlite3 *cuisineDB = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) 
    
    {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    
    }
    
    // Open the database. The database was prepared outside the application.
    
    if (sqlite3_open([projectPath UTF8String], &cuisineDB) == SQLITE_OK)
    {
        if(deleteStmt == nil) 
        
        {
           
            
            NSString *strValue;
                            
               strValue = [NSString stringWithFormat:@"DELETE  FROM PlayerData WHERE TeamName='%@'",teamName];
      
            
           
            const char *sql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(cuisineDB, sql, -1, &deleteStmt, NULL) != SQLITE_OK) {
                NSLog(@"Error while creating Delete Stmt %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);
                
            }
        
        }    
        
        
        if(SQLITE_DONE != sqlite3_step(deleteStmt))
        
        {           
            NSLog(@"Error while Executing Delete Stmt %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);           
       
        }
        
        sqlite3_reset(deleteStmt);
        
        if (deleteStmt) 
        
        {
            sqlite3_finalize(deleteStmt);
            deleteStmt = nil;
        } 
        
    }
    
    sqlite3_close(cuisineDB);
    
    
    
}

#pragma mark delete homePlayerFromDatabase
-(void) deletehomePlayerFromDatabase :(NSString *)teamName
{
    
    NSString *projectPath=[self copyIfDBNeeded];
    
    sqlite3_stmt *deleteStmt = nil;
    sqlite3 *cuisineDB = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) 
        
    {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
        
    }
    
    // Open the database. The database was prepared outside the application.
    
    if (sqlite3_open([projectPath UTF8String], &cuisineDB) == SQLITE_OK)
    {
        if(deleteStmt == nil) 
            
        {
            
            
            NSString *strValue;
            
            strValue = [NSString stringWithFormat:@"DELETE  FROM HomeTeam WHERE Status='%@'",teamName];
            
            
            
            const char *sql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(cuisineDB, sql, -1, &deleteStmt, NULL) != SQLITE_OK) {
                NSLog(@"Error while creating Delete Stmt %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);
                
            }
            
        }    
        
        
        if(SQLITE_DONE != sqlite3_step(deleteStmt))
            
        {           
            NSLog(@"Error while Executing Delete Stmt %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);           
            
        }
        
        sqlite3_reset(deleteStmt);
        
        if (deleteStmt) 
            
        {
            sqlite3_finalize(deleteStmt);
            deleteStmt = nil;
        } 
        
    }
    
    sqlite3_close(cuisineDB);
    
    
    
}




//  Update DataBase ..................

-(void) updatedDB:(NSString *)Str dictionary:(NSMutableDictionary *)mDict 
{
    
    NSString *projectPath=[self copyIfDBNeeded];
    sqlite3_stmt *insertStmt = nil;
    sqlite3 *cuisineDB = nil;
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK) 
        
    {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    // Open the database. The database was prepared outside the application.
    
    if (sqlite3_open([projectPath UTF8String], &cuisineDB) == SQLITE_OK)
    {
        if(insertStmt == nil)
            
        {
            // PlayerData(TeamName,PlayerName,jerseyNumber,Handled) Values(?,?,?,?)"];
            
            NSString *strValue = [NSString stringWithFormat:@"update PlayerData set PlayerName='%@', Handled='%@' where  TeamName='%@' AND jerseyNumber='%@'",[mDict objectForKey:@"PlayerName"],[mDict objectForKey:@"Handling"],Str,[mDict objectForKey:@"JerseyNumber"]];
            NSLog(@"updated1");
            
            const char *sql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(cuisineDB, sql, -1, &insertStmt, NULL) != SQLITE_OK)
                
            {
                NSLog(@"Error while creating UpdateStmt  %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);
            }
            
        }
        
        if(SQLITE_DONE != sqlite3_step(insertStmt)) 
            
        {           
            NSLog(@"Error while Executing updatedStmt  %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);           
            
        }
        
        sqlite3_reset(insertStmt);
        
        if (insertStmt) 
            
        {
            sqlite3_finalize(insertStmt);
            insertStmt = nil;
        }       
        
    }
    sqlite3_close(cuisineDB);   
    
    
}

#pragma mark Select Match data
-(NSMutableDictionary *)selectTotalInningsFromDb :(NSString *)Date :(NSString *)TeamName
{
    
    NSString *projectPath=[self copyIfDBNeeded];
    
    sqlite3_stmt *selectAllStmt = nil;
    sqlite3 *cuisineDB = nil;
    
  
      NSMutableDictionary *mDict=[[NSMutableDictionary alloc]init];
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
        
    {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
        
    }
    
    // Open the database
    
    
    if (sqlite3_open([projectPath UTF8String], &cuisineDB) == SQLITE_OK)
    {
        NSString *strValue = [NSString stringWithFormat:@"SELECT * FROM MatchData WHERE TeamName ='%@' AND Date='%@' ",TeamName,Date];
        const char *sql = [strValue UTF8String];
        
        if(sqlite3_prepare_v2(cuisineDB, sql, -1, &selectAllStmt, NULL) == SQLITE_OK)
            
        {
            
            while(sqlite3_step(selectAllStmt) == SQLITE_ROW)
                
            {
              
                
                NSData *dataForImage = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAllStmt, 3) length: sqlite3_column_bytes(selectAllStmt, 3)];
             
                [mDict setObject:dataForImage forKey:@"TotalInnings"];
                
                
            
                
            }
            
        }
        
        sqlite3_reset(selectAllStmt);
        
        if (selectAllStmt)
            
        {
            sqlite3_finalize(selectAllStmt);
            selectAllStmt = nil;
        }
        
    }
    
    sqlite3_close(cuisineDB);
    
    return mDict;
    
}

#pragma mark Select Match data
-(NSMutableArray *)selectAllTotalInningsFromDb 
{
    
    NSString *projectPath=[self copyIfDBNeeded];
    
    sqlite3_stmt *selectAllStmt = nil;
    sqlite3 *cuisineDB = nil;
    
      NSMutableArray *dataArr=[[NSMutableArray alloc]init];
    NSMutableDictionary *mDict=[[NSMutableDictionary alloc]init];
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
        
    {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
        
    }
    
    // Open the database
    
    
    if (sqlite3_open([projectPath UTF8String], &cuisineDB) == SQLITE_OK)
    {
        NSString *strValue = [NSString stringWithFormat:@"SELECT * FROM MatchData"];
        const char *sql = [strValue UTF8String];
        
        if(sqlite3_prepare_v2(cuisineDB, sql, -1, &selectAllStmt, NULL) == SQLITE_OK)
            
        {
            
            while(sqlite3_step(selectAllStmt) == SQLITE_ROW)
                
            {
                
              
                
                
                  NSString *Id=[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllStmt, 0)];
                NSString *Date=[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllStmt, 1)];
                  NSString *TeamName=[NSString stringWithUTF8String:(char *)sqlite3_column_text(selectAllStmt, 2)];
               // NSData *dataForImage = [[NSData alloc] initWithBytes:sqlite3_column_blob(selectAllStmt, 1) length: sqlite3_column_bytes(selectAllStmt, 1)];
                [mDict setObject:Id forKey:@"Id"];
                [mDict setObject:Date forKey:@"Date"];
                [mDict setObject:TeamName forKey:@"TeamName"];
                
                
                [dataArr addObject:mDict];
                
                
                
            }
            
        }
        
        sqlite3_reset(selectAllStmt);
        
        if (selectAllStmt)
            
        {
            sqlite3_finalize(selectAllStmt);
            selectAllStmt = nil;
        }
        
    }
    
    sqlite3_close(cuisineDB);
    
    return dataArr;
    
}

-(void) deleteMatchDB:(NSString *)Str 
{
    
    NSString *projectPath=[self copyIfDBNeeded];
    sqlite3_stmt *insertStmt = nil;
    sqlite3 *cuisineDB = nil;
    int matchId =[Str intValue];
    
    if (sqlite3_config(SQLITE_CONFIG_SERIALIZED) == SQLITE_OK)
        
    {
        NSLog(@"Can now use sqlite on multiple threads, using the same connection");
    }
    
    // Open the database. The database was prepared outside the application.
    
    if (sqlite3_open([projectPath UTF8String], &cuisineDB) == SQLITE_OK)
    {
        if(insertStmt == nil)
            
        {
            // PlayerData(TeamName,PlayerName,jerseyNumber,Handled) Values(?,?,?,?)"];
            
            NSString *strValue = [NSString stringWithFormat:@"DELETE  FROM MatchData WHERE matchId='%d'",matchId];
            NSLog(@"updated1");
            
            const char *sql = [strValue UTF8String];
            
            if(sqlite3_prepare_v2(cuisineDB, sql, -1, &insertStmt, NULL) != SQLITE_OK)
                
            {
                NSLog(@"Error while creating UpdateStmt  %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);
            }
            
        }
        
        if(SQLITE_DONE != sqlite3_step(insertStmt))
            
        {
            NSLog(@"Error while Executing updatedStmt  %@", [NSString stringWithUTF8String:(char *)sqlite3_errmsg(cuisineDB)]);
            
        }
        
        sqlite3_reset(insertStmt);
        
        if (insertStmt)
            
        {
            sqlite3_finalize(insertStmt);
            insertStmt = nil;
        }
        
    }
    sqlite3_close(cuisineDB);
    
    
}

@end
