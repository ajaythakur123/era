//
//  ExistRoasterViewController.m
//  ERAApplication
//
//  Created by ajay thakur on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ExistRoasterViewController.h"
#import "detailsDB.h"
#import "CustomCellPlayer.h"
#import "ADDRoasterViewControler.h"

@implementation ExistRoasterViewController
@synthesize PlayerTableView,playerNameTextField,JerseyNumberTextField,handlingTextField,addplayerView,teamID,teamName,TeamTableView,buttonSave,buttonAddPlayer,labelHanded,labelPlayerName,labelJerseyNumeber;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
//    NSLog(@"--touch up calling--");
//    addplayerView.hidden=YES;
//    
//  hidenViewTable.hidden=YES;
      [tableviewList removeFromSuperview];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    arrayPlayerdataArray =[[NSMutableArray alloc]init];
    addplayerView.hidden=YES;
    hidenViewTable.hidden=YES;
    DBObj =[[detailsDB alloc]init];
    mdict=[[NSMutableDictionary alloc]init];
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.backBarButtonItem=nil;
    
    //PlayerTableView.frame =CGRectMake(0, 0, 500, 600);
    
    UIBarButtonItem *navi_back = [[UIBarButtonItem alloc] 
                                  initWithTitle:@"Back"                                            
                                  style:UIBarButtonItemStyleBordered 
                                  target:self 
                                  action:@selector(BackTopreviousMethod:)];
    self.navigationItem.leftBarButtonItem = navi_back;

     //teamNames_Array = [[NSMutableArray alloc]initWithObjects:@"Team",@"Team2",@"Team3",@"Team4", nil];

    // Do any additional setup after loading the view from its nib.
}

-(void)BackTopreviousMethod:(id)sender
{ 
    
    
    if ([arrayPlayerdataArray count]!=0)
    {
        str_AddPlayercheck =@"saveAddplayerData";
        str_message=@"Do you want to save this data";
        
        [self alertMessageMethod:str_message];
        return;
    }
    
    
    
    
    ADDRoasterViewControler *AddRoasterViewControler = 
    [[ADDRoasterViewControler alloc] initWithNibName: 
     @"ADDRoasterViewControler" bundle:nil];
    
    // self.navigationController.navigationBar.topItem.title=nil;
    //  [oAuthLoginView dismissModalViewControllerAnimated:YES];
    [self.navigationController popViewControllerAnimated:NO];
    [self.navigationController pushViewController:AddRoasterViewControler animated:NO];    
        
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
   // [self getPlayerDb];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    // Return the number of rows in the section.
    
    
    if (type==@"teamName") 
    {
        	return [teamNames_Array count];
    }
    else
    {
        	return [arrayPlayerdataArray count];
    }
    

}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    
    
    if (type==@"teamName") 
    {
        
        
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier] ;
        //if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        //	}
        
        UILabel *defensiveposn = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
        
        if (type==@"teamName") 
        {
            
            
            defensiveposn.text=[[teamNames_Array objectAtIndex:indexPath.row]objectForKey:@"TeamName"];
            
        }
        defensiveposn.textAlignment = UITextAlignmentLeft;
        defensiveposn.textColor = [UIColor blackColor];
        defensiveposn.backgroundColor = [UIColor clearColor];
        defensiveposn.font = [UIFont fontWithName:@"Arial" size:12];
        [cell addSubview:defensiveposn];
        
        cell.selectionStyle=UITableViewCellSelectionStyleBlue;
        tableView.separatorColor=[UIColor clearColor];
        
        return cell;
        
    }
else
{
    

    
    CustomCellPlayer *cell;
    cell=[[[NSBundle mainBundle]loadNibNamed:@"CustomCellPlayer" owner:self options:nil]objectAtIndex:0];
    
    
    cell.Handled.text =[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"Handling"];
    cell.playerName.text =[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"PlayerName"];
    cell.JerseyNumber.text =[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"JerseyNumber"];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell ;
    
}
    
    
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (type==@"teamName") 
    {
        
        
    }
    else
    {
        
    
    [tableView beginUpdates];
    if (editingStyle == UITableViewCellEditingStyleDelete) {
                
        
        
//        NSString *PlayerName =[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"PlayerName"];
//        NSString *JerseyNumber =[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"JerseyNumber"];
//        NSString *TeamName =[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"TeamName"];
//         
//        
//        [DBObj deletePlayerFromDatabase:TeamName :PlayerName :JerseyNumber];
        
        
        [arrayPlayerdataArray removeObjectAtIndex:indexPath.row]; 
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [PlayerTableView reloadData];

        
    }       
    [tableView endUpdates];
    
    }
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableviewList removeFromSuperview];
    
    if (type==@"teamName") 
    {
        
         
        selectTeam.text=[[teamNames_Array objectAtIndex:indexPath.row]objectForKey:@"TeamName"]; 
         arrayPlayerdataArray =[DBObj selectTeamPlayerstFromDb:selectTeam.text];
        type =@"PlayerData";
        [PlayerTableView reloadData];
        
          hidenViewTable.hidden=YES;
        return;
        
    }
    if (type==@"PlayerData") 
    {
        
    addplayerView.hidden=NO;
    btn_batterside.titleLabel.text =[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"Handling"];
        
        [btn_batterside setTitle:btn_batterside.titleLabel.text forState:UIControlStateNormal];
        NSLog(@"%@",btn_batterside.titleLabel.text);
    playerNameTextField.text =[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"PlayerName"];
    JerseyNumberTextField.text =[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"JerseyNumber"];
    MODE =@"edit";
    //    JerseyNumberTextField.userInteractionEnabled=NO;
        
        buttonEdit.enabled=NO;
        buttonSave.enabled=NO;
        buttonAddPlayer.enabled=NO;
        buttonTeamSelect.enabled=NO;
    replaceObject =indexPath.row;
        
           return;
        }

    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (BOOL)tableView:(UITableView *)tableView
canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void) tableView:(UITableView *)tableView
moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath
       toIndexPath:(NSIndexPath *)targetIndexPath
{
    sourceIndex = [sourceIndexPath row];
    targetIndex = [targetIndexPath row];
    
    
    id object = [arrayPlayerdataArray objectAtIndex:sourceIndex];
    [arrayPlayerdataArray removeObjectAtIndex:sourceIndex];
    [arrayPlayerdataArray insertObject:object atIndex:targetIndex];
    
    NSLog(@"%@",arrayPlayerdataArray);
    
}


-(IBAction)Edit:(id)sender{
    
    
    
    if(self.editing)
    {
        [super setEditing:NO animated:NO];
        [PlayerTableView setEditing:NO animated:NO];
        
        [buttonEdit setTitle:@"Edit" forState:UIControlStateNormal];
        buttonSave.enabled=YES;
        buttonTeamSelect.enabled=YES;
        buttonAddPlayer.enabled=YES;
    }
    else
    {
        [buttonEdit setTitle:@"Close" forState:UIControlStateNormal];
        
        //buttonEdit.titleLabel.text=@"Close";
        
        buttonSave.enabled=NO;
        buttonTeamSelect.enabled=NO;
        buttonAddPlayer.enabled=NO;
        
        [super setEditing:YES animated:YES];
        [PlayerTableView setEditing:YES animated:YES];
        
    }
}

-(IBAction)AddPlayerOnRoaster
{
    NSLog(@"---%s--",__func__);
    MODE =@"nonedit";
    
    buttonEdit.enabled=NO;
    buttonSave.enabled=NO;
    buttonAddPlayer.enabled=NO;
    buttonTeamSelect.enabled=NO;
    
   //   JerseyNumberTextField.userInteractionEnabled=YES;
    addplayerView.hidden=NO;
    playerNameTextField.text=@"";
    JerseyNumberTextField.text=@"";
    btn_batterside.titleLabel.text=nil;
    
    if (selectTeam.text.length==0) 
    {
        str_message =@"Please Select Team";
           addplayerView.hidden=YES;
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:str_message delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alrt_message show];
        return;
}
      

    
}
-(IBAction)sumbmitPlayerToDabase{
    
    
      if([JerseyNumberTextField.text length]==0||[btn_batterside.titleLabel.text length]==0)
    {
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please add Jersey Number and Handed" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alrt_message show];
        return;
    }
    else
    {  
        
        addplayerView.hidden=YES;
        [playerNameTextField resignFirstResponder];
        [JerseyNumberTextField resignFirstResponder];
        [handlingTextField resignFirstResponder];
        if([arrayPlayerdataArray count]>=50)
        {
            str_message =@"User Cant store more then 50 Players!";
            [self alertMessageMethod:str_message];
            
        }
        else
        {
            //NSMutableDictionary *mdict=[[NSMutableDictionary alloc]init];
            //   [mdict setObject:textInputView.text forKey:@"Text"]; 
            if ([self validateTextField:JerseyNumberTextField.text]==NO)
            {
                str_message=@"Please Enter Jersy number Numeric Value";
                [self alertMessageMethod:str_message];
                return; 
            }
            else if ([self PlayerVadiation:playerNameTextField.text]==NO)
            {
                str_message=@"Please Enter valid name";
                [self alertMessageMethod:str_message];
                return; 
            }
            

            
            
            else
            {

                    if([MODE isEqualToString:@"edit"])//editMode
                    {
                        
                        
                        for (int i=0; i<[arrayPlayerdataArray count]; i++) {
                            
                            
                            NSString *jerseyrnumber =[[arrayPlayerdataArray objectAtIndex:i]objectForKey:@"JerseyNumber"];
                            
                            if ([JerseyNumberTextField.text isEqualToString:jerseyrnumber]&&replaceObject!=i) {
                                
                                UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number already exist" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                                [alrt_message show];
                                addplayerView.hidden=NO;
                                
                                return;
                                
                                
                            }
                            
                        }

                        
//                        if ([[[arrayPlayerdataArray objectAtIndex:replaceObject]objectForKey:@"JerseyNumber"] isEqualToString:JerseyNumberTextField.text]) {
                            
                            NSLog(@"%@",arrayPlayerdataArray);
                            
                            NSMutableDictionary *dict =[[NSMutableDictionary alloc]init];
                            
                            
                            [dict setObject:selectTeam.text forKey:@"TeamName"];
                            [dict setObject:playerNameTextField.text forKey:@"PlayerName"];
                            [dict setObject:JerseyNumberTextField.text forKey:@"JerseyNumber"];
                            [dict setObject:btn_batterside.titleLabel.text forKey:@"Handling"];
                            
                            NSLog(@"%d",replaceObject);
                         [arrayPlayerdataArray replaceObjectAtIndex:replaceObject withObject:dict];
                              
                          //  JerseyNumberTextField.userInteractionEnabled=YES;
                            
                             NSLog(@"%@",arrayPlayerdataArray);
                            [PlayerTableView reloadData];
                            
                            buttonEdit.enabled=YES;
                            buttonSave.enabled=YES;
                            buttonAddPlayer.enabled=YES;
                            buttonTeamSelect.enabled=YES;
                            return;
                            
                            
//                        }
//                        else
//                            
//                        {
//                            UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number already exist" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
//                            [alrt_message show];
//                            [alrt_message release];
//                             addplayerView.hidden=NO; 
//                            return;
//                            
//                        }
                    }

                
                                   
                for (int i=0; i<[arrayPlayerdataArray count]; i++) {
                    
                    NSString *jerseyrnumber =[[arrayPlayerdataArray objectAtIndex:i]objectForKey:@"JerseyNumber"];
                    
                    if ([JerseyNumberTextField.text isEqualToString:jerseyrnumber]) {
                        
                        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number already exist" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                        [alrt_message show];
                        
                        addplayerView.hidden=NO;
                        return;
                        
                        
                    }
                }
            
          NSMutableDictionary *madict=[[NSMutableDictionary alloc]init];
            
            [madict setObject:selectTeam.text forKey:@"TeamName"];
            [madict setObject:playerNameTextField.text forKey:@"PlayerName"];
            [madict setObject:JerseyNumberTextField.text forKey:@"JerseyNumber"];
            [madict setObject:btn_batterside.titleLabel.text forKey:@"Handling"];
            
            [arrayPlayerdataArray addObject:madict];
            [PlayerTableView reloadData];
                
                
                buttonEdit.enabled=YES;
                buttonSave.enabled=YES;
                buttonAddPlayer.enabled=YES;
                buttonTeamSelect.enabled=YES;
            }
        }
        
    }

}
-(IBAction)CancelPlayerView{
    
    buttonEdit.enabled=YES;
    buttonSave.enabled=YES;
    buttonAddPlayer.enabled=YES;
    buttonTeamSelect.enabled=YES;
    
  //  JerseyNumberTextField.userInteractionEnabled=YES;
    addplayerView.hidden=YES;
    [playerNameTextField resignFirstResponder];
    [JerseyNumberTextField resignFirstResponder];
}
-(IBAction)FinalSubmitPlayer
{
    if ([arrayPlayerdataArray count]==0) {
        
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please ADD Players" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alrt_message show];
        
    }
    else
    {
    
    
    [DBObj deletePlayerFromDatabase: selectTeam.text];
    
    
    for (int i=0 ;i <[arrayPlayerdataArray count];i++){
        
        mdict=[arrayPlayerdataArray objectAtIndex:i];
        [DBObj insertPlayerIntoDb:mdict];
    }
    

      
        
        ADDRoasterViewControler *AddRoasterViewControler = 
        [[ADDRoasterViewControler alloc] initWithNibName: 
         @"ADDRoasterViewControler" bundle:nil];
        
        // self.navigationController.navigationBar.topItem.title=nil;
        //  [oAuthLoginView dismissModalViewControllerAnimated:YES];
        [self.navigationController popViewControllerAnimated:NO];
        [self.navigationController pushViewController:AddRoasterViewControler animated:NO];    
      //[self.navigationController popToRootViewControllerAnimated:YES];
    }
    
}

-(BOOL)PlayerVadiation :(NSString *)PlayerName
{
    unichar c;
    
    if ([PlayerName length]>0)
    {
        c = [PlayerName characterAtIndex:0];
    }
    else
    {
        return YES;
    }
    
    if ([[NSCharacterSet letterCharacterSet] characterIsMember:c])
    {
        return YES;
    }
    return NO;
    
}
-(IBAction)selectTeam
{
    NSLog(@"-->teamName_ButtonAction: Method Called<--");
    
    type=@"teamName";
    
    [self getPlayerDb];
    
    [TeamTableView reloadData];
    [tableviewList setFrame:CGRectMake(205,113,218, 166)];
    
    
       hidenViewTable.hidden=NO;
    [self.view addSubview:tableviewList];
}
//--------Ambadas -----
-(IBAction)BatterSideMethod:(id)sender
{
    
    str_message =@"Please Select the batter side";
    [self alertMessageMethod:str_message];
    
}
-(BOOL)validateTextField:(NSString *)candidate 
{
    
    NSLog(@"validateTextField: Called");
    NSString *emailRegex=@"^(?:|0|[0-9]\\d*)(?:\\.\\d*)?$"; 
    NSLog(@"-all string value %@-",emailRegex);
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex]; 
    return [emailTest evaluateWithObject:candidate];
    
    
}

-(void)alertMessageMethod:(NSString *)message
{
    
    if ([str_message isEqualToString:@"Please Enter Jersy number Numeric Value"]||[str_message isEqualToString:@"Please Select the all Value!"]||[str_message isEqualToString:@"User Cant store more then 50 Players!"]||[str_message isEqualToString:@"Please Enter valid name"]) 
    {   
        addplayerView.hidden=NO;
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:str_message delegate:self cancelButtonTitle:nil otherButtonTitles:@"Okey", nil];
        [alrt_message show];
        return;
        
    }
    if ([str_message isEqualToString:@"Please Select the batter side"])
    {
         str_batterside =@"batterside";
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:str_message delegate:self cancelButtonTitle:@"Left" otherButtonTitles:@"Right", nil];
        [alrt_message show];
        return;
    }
    
    
    if ([str_message isEqualToString:@"You are not added any players! Do you want to go back"]||[str_message isEqualToString:@"Do you want to save this data"]||[str_message isEqualToString:@"You are not Saved Data! Do you want to go back"])
    {
        
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:str_message delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [alrt_message show];
        return;
    }
    
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"----%s",__func__);
    if (buttonIndex==0)
    {
        if ( str_batterside ==@"batterside")
        {
            [btn_batterside setTitle:@"L" forState:UIControlStateNormal];
            [btn_batterside setTintColor:[UIColor blackColor]];
            NSLog(@"---%@---",btn_batterside.titleLabel.text);
        }
        
        if (str_AddPlayercheck ==@"saveAddplayerData")
        {
            str_AddPlayercheck=@"EditPlayerBackbtn";
            str_message=@"You are not Saved Data! Do you want to go back";
            //[self alertMessageMethod:str_message];
        }
    }
    else if (buttonIndex==1)
    {
        if ( str_batterside ==@"batterside")
        {
            [btn_batterside setTitle:@"R" forState:UIControlStateNormal];
            NSLog(@"---%@---",btn_batterside.titleLabel.text);
        }
        if (str_AddPlayercheck==@"EditPlayerBackbtn") 
        {
            [[self navigationController]popViewControllerAnimated:YES];
        }
        
        if (str_AddPlayercheck ==@"saveAddplayerData")
        {
            
            [ self FinalSubmitPlayer];
        }
        
    }
}
//-------------------


#pragma mark get team Name 
-(void)getPlayerDb{
    
    
    
      type=@"teamName";
    teamNames_Array =[DBObj selectPlayerstFromDb];
    [TeamTableView reloadData];
 
    
    
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if(interfaceOrientation != UIInterfaceOrientationPortrait &&
       interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown)
    {
        // landscape
        addplayerView.frame=CGRectMake(110,-270, 900, 796);
        
        
        PlayerTableView.frame =CGRectMake(200, 331, 680,300);
        labelPlayerName.frame=CGRectMake(207, 305, 206, 21);
        labelJerseyNumeber.frame=CGRectMake(421, 305, 230, 21);
        labelHanded.frame=CGRectMake(659,305,216, 21 );
        buttonAddPlayer.frame =CGRectMake(750, 250, 121, 37);
        buttonSave.frame =CGRectMake(450, 650, 75, 37);
         buttonEdit.frame =CGRectMake(200, 250, 75, 37);
    }
    else
    {  
        
        
        addplayerView.frame=CGRectMake(0, 0, 728,796);
        PlayerTableView.frame =CGRectMake(52, 331, 680, 574);
        labelPlayerName.frame=CGRectMake(60, 305, 206, 21);
        labelJerseyNumeber.frame=CGRectMake(274, 305, 230, 21);
        labelHanded.frame=CGRectMake(512,305,216, 21 );
        
        buttonAddPlayer.frame =CGRectMake(611, 252, 121, 37);
        buttonSave.frame =CGRectMake(346, 913, 75, 37);
         buttonEdit.frame =CGRectMake(52, 250, 72, 37);
        // NSLog(@"%@",viewAddPlayerBox.frame);
        // portrait
        // addplayerViewBox.frame=CGRectMake(0, 0,379,269);
        
        
    }
	return YES;
}

@end
