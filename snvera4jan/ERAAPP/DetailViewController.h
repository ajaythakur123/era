//
//  DetailViewController.h
//  ERAAPP
//
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "sqlite3.h"
#import "ERAAPPAppDelegate.h"

@class RootViewController;
@interface DetailViewController : UIViewController <UIPopoverControllerDelegate, UISplitViewControllerDelegate,UITextFieldDelegate>
{
     
   IBOutlet UIScrollView     *scrv_scoreboard;
    int in_circleoutcount;
    int in_baseCircleout;
    int batterCount;
    int runsCount;
    int in_cummulativepitchcountValue;
    NSMutableArray            *array_defnsiveposidata;
    IBOutlet  UIView   *vw_atBatAnimation;
//------checkbox buttons------
    IBOutlet UIButton         *btn_checkboxmainmenu;
    IBOutlet UIButton         *btn_submenucheckbox;
    IBOutlet UIButton         *btn_strikeTypeofPitcbox;
    IBOutlet UIButton         *btn_strikecheckboxOutPitch;
    IBOutlet UIButton         *btn_strikeoutCheckbox;
    IBOutlet UIButton         *btn_hitfaircheckbox;
    IBOutlet UIButton         *btn_exit;
    IBOutlet UIButton         *ball_typeBtn;
//----assign value-----
UILabel   *matchid;
UILabel   *lbl_currpitchcount;
UILabel   *lbl_imagedata;
UILabel   *lbl_batteronbase;
UILabel   *lbl_scoreboard1;
UILabel   *lbl_scoreboard2;
    
    
    
UIAlertView               *alert1;
//----score board buttons-------
    UIButton                   *btn_baseRunnerDispalyValue;
    UIButton                    *btn_save;
    IBOutlet UIButton          *btn_baserunner;
    IBOutlet UIButton          *btn_teamName;
    IBOutlet UIButton          *btn_OpponentTeam;
    IBOutlet UIButton          *btn_Catcher;
    IBOutlet UIButton          *btn_umpire;
    IBOutlet UITableView       *tblw_alldatatable;
    NSString                   *str_TeamName;
    NSString                   *str_OpponentTeam;
    NSString                   *str_Catcher;
    NSString                   *str_type1;
    NSString                   *str_Umpire;
    UIDatePicker              *date_picker;
    NSMutableArray            *array_redrowbtns;
    
    IBOutlet UINavigationBar  *nvgn_datepicker;
    IBOutlet UISegmentedControl  *segmnt_controll;
    NSString                  *Str_LengthString;
    
    BOOL                      bl_hideimage;
    BOOL                      ballpopup;
    BOOL                      mainbaserunnerpopup;
    BOOL                      bl_DefensivePosiPopups;
    BOOL                      bl_alertPopup;
    BOOL                      bl_ballbox1, bl_ballbox2,bl_ballbox3,bl_ballbox4,bl_ballbox5,bl_ballbox6,bl_ballbox7,bl_unselected;  
    BOOL                  bl_check_data,bl_save_data;
   
    UIButton                  *btn_loadSenderidbtn;
    UIButton                  *btn_loadstrikesenderid;
    
    IBOutlet UIView           *vw_typeOFpitchselect;
    IBOutlet UIView           *vw_StrikeView;
    IBOutlet UIView           *vw_OnBaseview;
    IBOutlet UIView           *vw_onBaseOptionHitFair;
    IBOutlet UIView           *vw_BallPopMainMenu;
    IBOutlet UIView           *vw_BallPopUpSubMenu;
    IBOutlet UIView           *vw_Mainbaserunnerview,*vw_defensivePositions;
    IBOutlet UIView           *vw_LocationOfHitView;
   
    IBOutlet UIButton         *btn_strike_out;
    IBOutlet UIButton         *btn_typeofpitch; 
    IBOutlet UIButton         *btn_BallMainMenu;
    IBOutlet UITextField      *txtf_gb1,*txtf_gb2,*txtf_db1,*txtf_db2,*txtf_agb1,*txtf_agb2;
    
    IBOutlet UILabel    *lbl_onbasepopups,*lbl_onbasepopups1,*lbl_onbasepopups3,*lbl_datepicker;
    IBOutlet UIButton        *btn_scordyes,*btn_scordno,*btn_runerndYes,*btn_runArndNo,*btn_EoiYes,*btn_EoiNo;
    IBOutlet UILabel     *lbl_batRuns;
    IBOutlet UIImageView  *imgv_Fielderchoice;
    IBOutlet UILabel  *lbl_circleout;
    UIImageView       *imgv_hitEoi;
    UIImageView       *imgv_eoi;
    
    int outComeAtbat_Tag;
    int inTag_baseRunnerDEfensivepos;
    
    //----Database stuff-----
    
//    NSString         *databasePath;
//    sqlite3          *contactDB;
 //   NSString  *strm_issaved;
   // NSString  *strm_sqlprevData;
    //...........................
    NSString *type,*strBtn;
    NSMutableArray  *opponents_Array,*catchers_Array,*pitchers_Array,*teamNames_Array;
    IBOutlet UIButton   *add_Button;
    IBOutlet UILabel    *title_Label;
    IBOutlet UIAlertView  *alert;
     UITextField       *textAlert;
    IBOutlet UILabel   *catcher_lbl;
    IBOutlet UILabel   *pitcher_lbl;
    IBOutlet UILabel   *teamName_lbl;
    IBOutlet UILabel   *opponent_lbl;
    IBOutlet UILabel   *lbl_jerseyNumber;
    IBOutlet UILabel   *lbl_batterName;
    IBOutlet UILabel   *lbl_RorL;
    IBOutlet UILabel   *lbl_hitfairLineDrive;
    IBOutlet UILabel   *lbl_hitfairLineDriveto;
    IBOutlet UILabel   *lbl_batterCount;
    IBOutlet UILabel   *lbl_pitchCount;
    IBOutlet UILabel   *teamName_Scoreboard;
    IBOutlet UILabel   *opponent_Scoreboard;
    IBOutlet UILabel   *batter_lbl;
    
    NSString *countStr;
    NSMutableArray *ArrayOfButtons;
    NSMutableArray *ArrayOfLabels;
    
    //---------pitcher buttons----
    IBOutlet UIButton  *btn_ballbox1;
    IBOutlet UIButton  *btn_ballbox2;
    IBOutlet UIButton  *btn_ballbox3;
    IBOutlet UIButton  *btn_ballbox4;
    IBOutlet UIButton  *btn_ballbox5;
    IBOutlet UIButton  *btn_ballbox6;
    IBOutlet UIButton  *btn_ballbox7;
    IBOutlet UIButton  *btn_strikebox8;
    IBOutlet UIButton  *btn_strikebox9;
    IBOutlet UIButton  *btn_strikebox10;
    IBOutlet UIButton  *btn_strikebox11;
    IBOutlet UIButton  *btn_strikebox12;
    IBOutlet UIButton  *btn_strikebox13;
    IBOutlet UIButton  *btn_strikebox14;
    
    //---ambadas---
    NSString * str_strikeboxinfo;
    NSMutableString * str_ballboxinfo ;
    
    
}


@property (nonatomic, strong)  IBOutlet UIImageView  *imgv_Fielderchoice;

@property (nonatomic, strong) IBOutlet UIToolbar *toolbar;
@property (nonatomic, strong)IBOutlet UIScrollView     *scrv_scoreboard;
@property(nonatomic,strong)IBOutlet UIView           *vw_BallPopMainMenu;
@property(nonatomic,strong)IBOutlet UINavigationBar        *nvgn_datepicker;
@property(nonatomic,strong)IBOutlet UIView           *vw_BallPopUpSubMenu;
@property(nonatomic,strong)IBOutlet UIView           *vw_LocationOfHitView;
@property(nonatomic,strong)IBOutlet UIView           *vw_typeOFpitchselect;
@property(nonatomic,strong)IBOutlet UIView           *vw_StrikeView;
@property(nonatomic,strong) IBOutlet UIView           *vw_OnBaseview;
@property(nonatomic,strong)IBOutlet UIView           *vw_Mainbaserunnerview,*vw_defensivePositions;
@property(nonatomic,strong)IBOutlet UIView           *vw_onBaseOptionHitFair;
@property(nonatomic,strong)IBOutlet UITextField      *txtf_gb1,*txtf_gb2,*txtf_db1,*txtf_db2,*txtf_agb1,*txtf_agb2;
@property(nonatomic,strong)IBOutlet UIButton         *btn_typeofpitch; 
@property (nonatomic, strong) id detailItem;

@property (nonatomic, strong) IBOutlet UILabel *detailDescriptionLabel;
@property(nonatomic,strong)IBOutlet UIButton * btn_loadstrikesenderid;

-(IBAction)Datepickerbutton:(id)sender;
-(IBAction)DatepickerCancel:(id)sender;
-(IBAction)ERAExitMethod:(id)sender;
-(IBAction)DatepickerClicked:(id)sender;
-(IBAction)TypeOfPitchMethod:(id)sender;
-(IBAction)SrikeOptionMethod:(id)sender;
-(IBAction)Onbaseoptionmethod:(id)sender;
-(IBAction)OnbaseoptionHitFairMethod:(id)sender;
-(IBAction)BallpopMainMenuMethod:(id)sender;
-(IBAction)BallpopSubMainMethod:(id)sender;
-(IBAction)ScoreBoardMethod:(id)sender;
-(IBAction)BacktoBallTypeofpitchppopup:(id)sender;
-(IBAction)BacktostrikeTypeofpitchpopup:(id)sender;
-(IBAction)BacktostrikebasepopupFrmstrikeout:(id)sender;
-(IBAction)BacktostrikebasepopupFrmHitfair:(id)sender;
-(IBAction)BallpopupMethod:(UIButton *)sender;
-(IBAction)StrikepopupMethod:(UIButton *)sender;
//-------save data----
-(void)insertIntoMatchMainInfo;
-(void)insertIntoMatchSummary;
-(void)SaveButtonMethod:(id)sender;

-(void)NewGameMethod:(id)sender;
-(void)NewBatterMethod:(id)sender;
//-----baserunner methods-------
//-(IBAction)mainBaseRunnerview:(id)sender;
-(IBAction)DefensivePositionMethod:(id)sender;

-(IBAction)BaserunnerTagCallMethod:(UIButton *)sender;

//-------hit fair method-------
-(IBAction)HomeRunControllermethod:(id)sender;
-(IBAction)EditableBatterinfo:(id)sender;
- (BOOL)validateTextField:(NSString *)candidate ;

-(IBAction)OnbasisPopups:(id)sender;
//  Location Of Hit View Method.....
-(IBAction)locationOfHitView:(id)sender;
-(IBAction) teamName_ButtonAction: (id) sender;
-(IBAction) opponent_ButtonAction:(id) sender;
-(IBAction) catcher_ButtonAction:(id) sender;
-(IBAction) umpire_ButtonAction:(id) sender;
-(IBAction)addNew:(id)sender;


@end
