//
//  HomeTeamViewController.h
//  ERAAPP
//
//  Created by Krishna Kothapalli on 08/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "detailsDB.h"

@interface HomeTeamViewController : UIViewController<UITextFieldDelegate>
{
    UITableView *PlayerTableView;
    
    detailsDB *DBObj;
    UITextField *textField;
    
    NSMutableArray *textArray;
    NSMutableArray *teamNames_Array;
    NSMutableDictionary *mdict;
    NSMutableArray *arrayPlayerdataArray;
    NSMutableArray *arrayPitcher;
    NSMutableArray *arraycatcher;
    
    IBOutlet   UIView *addplayerView;
    IBOutlet   UIView *viewAddPlayerBox;
    IBOutlet   UITextField *playerNameTextField;
    IBOutlet   UITextField *JerseyNumberTextField;
    IBOutlet   UITextField *handlingTextField;
    IBOutlet   UITextField *homeTeamNameTextField;
    IBOutlet   UITextField *homeTeamLocationTextField;
    IBOutlet   UILabel *labelPlayerName;
    IBOutlet   UILabel *labelJerseyNumeber;
    IBOutlet   UILabel *labelHanded;
    IBOutlet   UIButton *buttonAddPlayer;
    IBOutlet   UIButton *buttonSave;
    IBOutlet   UIButton *buttonPitcher;
    IBOutlet   UIButton *buttonCatcher;
    NSMutableArray *finalTextArray;
    
    NSArray *TextArrayCopy ;
    NSMutableArray *array1;
    NSMutableArray *  ArraySortName ;
    NSArray *sortedArray ;
    int teamID;
    NSString *teamName;
     NSString *type;
    NSString   *str_message;
    NSString   *str_batterside;
    NSString   *str_AddPlayercheck;
   
    
    NSString * MODE;
    int replaceObject;
    
    IBOutlet UIButton *btn_batterside;
    
    UIAlertView *alert;
}
@property (nonatomic, strong)  NSString   *type;
@property (nonatomic, strong) UIButton *buttonPitcher;
@property (nonatomic, strong) UIButton *buttonCatcher;
@property (nonatomic, strong) NSString *teamName;
@property(nonatomic,assign) int teamID;
@property (nonatomic, strong) IBOutlet   UIButton *buttonAddPlayer;
@property (nonatomic, strong) IBOutlet   UIButton *buttonSave;
@property (nonatomic, strong) IBOutlet UITableView *PlayerTableView;
@property (nonatomic, strong)  UITextField *playerNameTextField;
@property (nonatomic, strong)  UITextField *JerseyNumberTextField;
@property (nonatomic, strong)  UITextField *handlingTextField;
@property (nonatomic, strong) IBOutlet UITextField *homeTeamNameTextField;
@property (nonatomic, strong) IBOutlet UITextField *homeTeamLocationTextField;
@property (nonatomic, strong) IBOutlet UILabel *homeTeamNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *homeTeamLocationlabel;
@property (nonatomic, strong)  IBOutlet   UIView *addplayerView;
@property (nonatomic, strong)  UIView *viewAddPlayerBox;
@property (nonatomic, strong)  UILabel *labelPlayerName;
@property (nonatomic, strong)   UILabel *labelJerseyNumeber;
@property (nonatomic, strong)   UILabel *labelHanded;

-(IBAction)AddPlayerOnRoaster;
-(IBAction)sumbmitPlayerToDabase;
-(void)getPlayerDb;
-(IBAction)CancelPlayerView;
-(IBAction)FinalSubmitPlayer;
-(IBAction)Pitcher:(id)sender;
-(IBAction)Catcher:(id)sender;


-(BOOL)validateTextField:(NSString *)candidate ;
-(void)alertMessageMethod:(NSString *)message;
-(IBAction)BatterSideMethod:(id)sender;
-(BOOL)PlayerVadiation :(NSString *)PlayerName;

@end
