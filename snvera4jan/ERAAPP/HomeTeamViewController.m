//
//  HomeTeamViewController.m
//  ERAAPP
//
//  Created by Krishna Kothapalli on 08/09/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HomeTeamViewController.h"
#import "SelectTeamViewController.h"
#import "CustomCellPlayer.h"
#import "TestViewController.h"
#import "detailsDB.h"
#import "ERAAPPAppDelegate.h"
#import "ERAApplicationViewController.h"
#import "ADDRoasterViewControler.h"

@implementation HomeTeamViewController

@synthesize PlayerTableView,playerNameTextField,JerseyNumberTextField,handlingTextField,addplayerView,teamID,teamName,viewAddPlayerBox,labelPlayerName,labelJerseyNumeber,labelHanded,buttonSave,buttonAddPlayer,homeTeamNameTextField,homeTeamLocationTextField,homeTeamNameLabel,homeTeamLocationlabel,buttonCatcher,buttonPitcher,type;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    arrayPlayerdataArray =[[NSMutableArray alloc]init];
    addplayerView.hidden=YES;
    DBObj =[[detailsDB alloc]init];
    
    teamNames_Array=[[NSMutableArray alloc]init];
    arrayPitcher =[[NSMutableArray alloc]init];
    arraycatcher =[[NSMutableArray alloc]init];
    //-----Ambadas----  
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.backBarButtonItem=nil;
  
    //type =@"Pitcher";
    //PlayerTableView.frame =CGRectMake(0, 0, 500, 600);
    
    UIBarButtonItem *navi_back = [[UIBarButtonItem alloc] 
                                  initWithTitle:@"Back"                                            
                                  style:UIBarButtonItemStyleBordered 
                                  target:self 
                                  action:@selector(BackTopreviousMethod:)];
    self.navigationItem.leftBarButtonItem = navi_back;
    if(type==@"Pitcher")
    {
        self.navigationItem.title=@"Pitcher";
    }
    else{
         self.navigationItem.title=@"Catcher";
    }
   
    
    [buttonPitcher setBackgroundColor:[UIColor lightGrayColor]];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    // Return the number of rows in the section.
    
    if (type==@"Catcher") 
    {
        return [arraycatcher count];   
    }
    else
    {
        return [arrayPitcher count];
    }

	
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
     CustomCellPlayer *cell;
    
    
    if (type==@"Pitcher") {
       
        cell=[[[NSBundle mainBundle]loadNibNamed:@"CustomCellPlayer" owner:self options:nil]objectAtIndex:0];
        
        
        cell.Handled.text =[[arrayPitcher objectAtIndex:indexPath.row]objectForKey:@"Handling"];
        cell.playerName.text =[[arrayPitcher objectAtIndex:indexPath.row]objectForKey:@"PlayerName"];
        cell.JerseyNumber.text =[[arrayPitcher objectAtIndex:indexPath.row]objectForKey:@"JerseyNumber"];
    }
    if (type==@"Catcher") {
       
        cell=[[[NSBundle mainBundle]loadNibNamed:@"CustomCellPlayer" owner:self options:nil]objectAtIndex:0];
        
        
        cell.Handled.text =[[arraycatcher objectAtIndex:indexPath.row]objectForKey:@"Handling"];
        cell.playerName.text =[[arraycatcher objectAtIndex:indexPath.row]objectForKey:@"PlayerName"];
        cell.JerseyNumber.text =[[arraycatcher objectAtIndex:indexPath.row]objectForKey:@"JerseyNumber"];
    }
       
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell ;
    
    
    
    
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    [tableView beginUpdates];
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        if(type==@"Pitcher")
        {
             [arrayPitcher removeObjectAtIndex:indexPath.row]; 
        }
        if(type==@"Catcher")
        {
             [arraycatcher removeObjectAtIndex:indexPath.row]; 
        }
       
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [PlayerTableView reloadData];
        //[DBObj deletePlayerFromDatabase:[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"TeamName"]];
    }       
    [tableView endUpdates];

    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    addplayerView.hidden=NO;
    
    if(type==@"Pitcher")
    {
    
        playerNameTextField.text =[[arrayPitcher objectAtIndex:indexPath.row]objectForKey:@"PlayerName"];
        JerseyNumberTextField.text =[[arrayPitcher objectAtIndex:indexPath.row]objectForKey:@"JerseyNumber"];
         //   btn_batterside.titleLabel.text =[[arrayPitcher objectAtIndex:indexPath.row]objectForKey:@"Handling"];
        [btn_batterside setTitle:[[arrayPitcher objectAtIndex:indexPath.row]objectForKey:@"Handling"] forState:UIControlStateNormal];
        NSLog(@"%@",btn_batterside.titleLabel.text);
    }
    if(type==@"Catcher")
    {
        //btn_batterside.titleLabel.text =[[arraycatcher objectAtIndex:indexPath.row]objectForKey:@"Handling"];
        
        
          [btn_batterside setTitle:[[arraycatcher objectAtIndex:indexPath.row]objectForKey:@"Handling"] forState:UIControlStateNormal];
        playerNameTextField.text =[[arraycatcher objectAtIndex:indexPath.row]objectForKey:@"PlayerName"];
        JerseyNumberTextField.text =[[arraycatcher objectAtIndex:indexPath.row]objectForKey:@"JerseyNumber"];
        
        
        NSLog(@"%@",btn_batterside.titleLabel.text);
    }
    buttonAddPlayer.enabled=NO;
    buttonSave.enabled=NO;
  // JerseyNumberTextField.userInteractionEnabled=NO;
    MODE =@"edit";
    replaceObject =indexPath.row;
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


-(void)insertTeamNameDB
{
    if([homeTeamNameTextField.text length]==0&&[homeTeamLocationTextField.text length]==0)
    {
        alert=[[UIAlertView alloc] initWithTitle:@"Fill all the Fields" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
        [alert show];
        
    }
    
    else
    {
        
        if ([self PlayerVadiation:homeTeamLocationTextField.text]==NO)
        {
            alert=[[UIAlertView alloc] initWithTitle:@"Location is not numeric" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
            
            [alert show];
            
            
            return; 
        }
        [homeTeamNameTextField resignFirstResponder];
        [homeTeamLocationTextField resignFirstResponder];
        
       // [self getTeamDb];
        teamNames_Array =[DBObj selectPlayerstFromDb];
        
//        for(int i=0;i<[teamNames_Array count];i++) {
//            
//            if ([homeTeamNameTextField.text isEqualToString:[[teamNames_Array objectAtIndex:i]objectForKey:@"TeamName"]])
//            {
//                alert=[[UIAlertView alloc] initWithTitle:@"Team Name Already Exist" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
//                
//                [alert show];
//                return;
//                
//            }
//        }
        
        
        mdict=[[NSMutableDictionary alloc]init];
        // NSString *Name,*Id,*address,*Mail;
        
        [mdict setObject:homeTeamNameTextField.text forKey:@"TeamName"];  
        [mdict setObject:homeTeamLocationTextField.text forKey:@"Location"];
        
        
        
        
        [DBObj insertIntoDb:mdict];
        
        
        
        
        
       // editRoasterViewController = [[EditRoasterViewController alloc]initWithNibName:@"EditRoasterViewController" bundle:nil];
        
        //editRoasterViewController.teamName=teamNameTextField.text;
        //[self.navigationController pushViewController:editRoasterViewController animated:YES];    [editRoasterViewController release];
    }
    
}

-(IBAction)Pitcher:(id)sender
{
        type =@"Pitcher";
     [PlayerTableView reloadData];
}
-(IBAction)Catcher:(id)sender;
{
        type =@"Catcher";
     [PlayerTableView reloadData];
}

-(IBAction)AddPlayerOnRoaster{
    
    MODE =@"nonedit";
    buttonAddPlayer.enabled=NO;
    buttonSave.enabled=NO;
   // JerseyNumberTextField.userInteractionEnabled=YES;
    addplayerView.hidden=NO;
    playerNameTextField.text=@"";
    JerseyNumberTextField.text=@"";
    btn_batterside.titleLabel.text=nil;
    
    
}

-(IBAction)sumbmitPlayerToDabase{
    
    
    
    
    if([playerNameTextField.text length]==0||[JerseyNumberTextField.text length]==0||[btn_batterside.titleLabel.text length]==0)
    {
        str_message =@"Please Select the all Value!";
        
        [self alertMessageMethod:str_message];
        
    }
    
    
    else
    {   addplayerView.hidden=YES;
        [playerNameTextField resignFirstResponder];
        [JerseyNumberTextField resignFirstResponder];
        [handlingTextField resignFirstResponder];
        if([arrayPlayerdataArray count]>=50)
        {
            str_message =@"User Cant store more then 50 Players!";
            [self alertMessageMethod:str_message];
            return;
        }
        
        
        else
        {
            
            mdict=[[NSMutableDictionary alloc]init];
            //   [mdict setObject:textInputView.text forKey:@"Text"]; 
            if ([self validateTextField:JerseyNumberTextField.text]==NO)
            {
                str_message=@"Please Enter Jersy number Numeric Value";
                [self alertMessageMethod:str_message];
                return; 
            }
            
            else if ([self PlayerVadiation:playerNameTextField.text]==NO)
            {
                str_message=@"Please Enter valid name";
                [self alertMessageMethod:str_message];
                return; 
            }
            
            else
            {
                if([MODE isEqualToString:@"edit"])//editMode
                {
                    
              

                    
                
                    if(type==@"Pitcher")
                    {
                        for (int i=0; i<[arrayPitcher count]; i++) {
                            
                            
                            NSString *jerseyrnumber =[[arrayPitcher objectAtIndex:i]objectForKey:@"JerseyNumber"];
                            
                            if ([JerseyNumberTextField.text isEqualToString:jerseyrnumber]&&replaceObject!=i) {
                                
                                UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number already exist" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                                [alrt_message show];
                                addplayerView.hidden=NO;
                                
                                return;
                            }
                            
                        }
                   
                        
                        [mdict setObject:@"Team" forKey:@"TeamName"];
                        [mdict setObject:playerNameTextField.text forKey:@"PlayerName"];
                        [mdict setObject:JerseyNumberTextField.text forKey:@"JerseyNumber"];
                        [mdict setObject:btn_batterside.titleLabel.text forKey:@"Handling"];
                         [mdict setObject:@"P" forKey:@"Status"];
                        [arrayPitcher replaceObjectAtIndex:replaceObject withObject:mdict];
                            
                            buttonAddPlayer.enabled=YES;
                            buttonSave.enabled=YES;
                       
                    }
                    if(type==@"Catcher")
                    {
                        for (int i=0; i<[arraycatcher count]; i++) {
                            
                            
                            NSString *jerseyrnumber =[[arraycatcher objectAtIndex:i]objectForKey:@"JerseyNumber"];
                            
                            if ([JerseyNumberTextField.text isEqualToString:jerseyrnumber]&&replaceObject!=i) {
                                
                                UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number already exist" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                                [alrt_message show];
                                addplayerView.hidden=NO;
                                
                                return;
                            }
                            
                        }
                            
                            [mdict setObject:@"Team"forKey:@"TeamName"];
                            [mdict setObject:playerNameTextField.text forKey:@"PlayerName"];
                            [mdict setObject:JerseyNumberTextField.text forKey:@"JerseyNumber"];
                            [mdict setObject:btn_batterside.titleLabel.text forKey:@"Handling"];
                         [mdict setObject:@"C" forKey:@"Status"];
                         //   JerseyNumberTextField.userInteractionEnabled=YES;
                            [arraycatcher replaceObjectAtIndex:replaceObject withObject:mdict];
                            
                            buttonAddPlayer.enabled=YES;
                            buttonSave.enabled=YES;
                                          }
                    
                    
                    [PlayerTableView reloadData];
                    return;
                        
                        
                  
                    }
                
                if(type==@"Pitcher")
                {
                    
                    for (int i=0; i<[arrayPitcher count]; i++) {
                        
                        NSString *jerseyrnumber =[[arrayPitcher objectAtIndex:i]objectForKey:@"JerseyNumber"];
                        
                        if ([JerseyNumberTextField.text isEqualToString:jerseyrnumber]) {
                            
                            UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number already exist" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                            [alrt_message show];
                              addplayerView.hidden=NO;
                            return;
                            
                        }
                    }
                    
                }
                if(type==@"Catcher")
                {
                    for (int i=0; i<[arraycatcher count]; i++) {
                        
                        NSString *jerseyrnumber =[[arraycatcher objectAtIndex:i]objectForKey:@"JerseyNumber"];
                        
                        if ([JerseyNumberTextField.text isEqualToString:jerseyrnumber]) {
                            
                            UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number already exist" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                            [alrt_message show];
                              addplayerView.hidden=NO;
                            return;
                            
                        }
                    }

                }
                
                                
               
                
                [mdict setObject:@"Team" forKey:@"TeamName"];
                [mdict setObject:playerNameTextField.text forKey:@"PlayerName"];
                [mdict setObject:JerseyNumberTextField.text forKey:@"JerseyNumber"];
                [mdict setObject:btn_batterside.titleLabel.text forKey:@"Handling"];
                
              
                if(type==@"Pitcher")
                {
                    
                     [mdict setObject:@"P"forKey:@"Status"];
                    
                    [arrayPitcher addObject:mdict];
                    
                }
                if(type==@"Catcher")
                {
                    
                     [mdict setObject:@"C"forKey:@"Status"];
                      [arraycatcher addObject:mdict];
                }
                buttonAddPlayer.enabled=YES;
                buttonSave.enabled=YES;
                [PlayerTableView reloadData];
                
                
            }//last else end
        }
        
    }
}

-(IBAction)CancelPlayerView{
    
    buttonAddPlayer.enabled=YES;
    buttonSave.enabled=YES;
    addplayerView.hidden=YES;
    [playerNameTextField resignFirstResponder];
    [JerseyNumberTextField resignFirstResponder];
  //  JerseyNumberTextField.userInteractionEnabled=YES;
}
-(IBAction)FinalSubmitPlayer
{
        

    
    [arrayPlayerdataArray removeAllObjects];
    
    if(type==@"Pitcher")
    {
        
        [DBObj deletehomePlayerFromDatabase:@"P"];
        
          [arrayPlayerdataArray addObjectsFromArray:arrayPitcher];
        
    }
    if(type==@"Catcher")
    {
        [DBObj deletehomePlayerFromDatabase:@"C"];
        
          [arrayPlayerdataArray addObjectsFromArray:arraycatcher];
    }
    
 
  
    
    NSLog(@"%@",arrayPlayerdataArray);
    
    if ([arrayPlayerdataArray count]==0) {
        
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please ADD Players" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alrt_message show];
        
    }
    else
    {
           NSUserDefaults *teamNamehome =[NSUserDefaults standardUserDefaults];
        
        [DBObj deletehomePlayerFromDatabase: [teamNamehome objectForKey:@"TeamName"]];
        //   [mdict setObject:textInputView.text forKey:@"Text"]; 
        
     
        [teamNamehome setObject:homeTeamNameTextField.text forKey:@"TeamName"]; 
        //[teamNamehome setObject:@"Loc"forKey:@"Location"];
        
        
        
        for (int i=0 ;i <[arrayPlayerdataArray count];i++){
            
            mdict=[arrayPlayerdataArray objectAtIndex:i];
            [DBObj insertHomePlayerIntoDb:mdict];
        }
        
      
        ADDRoasterViewControler *AddRoasterViewControler = 
        [[ADDRoasterViewControler alloc] initWithNibName: 
         @"ADDRoasterViewControler" bundle:nil];
        
        // self.navigationController.navigationBar.topItem.title=nil;
        //  [oAuthLoginView dismissModalViewControllerAnimated:YES];
        [self.navigationController popViewControllerAnimated:NO];
        [self.navigationController pushViewController:AddRoasterViewControler animated:NO];            
    }
  
    
}




//--------Ambadas -----
//--------Ambadas -----

//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    addplayerView.hidden=YES;
//}
-(void)BackTopreviousMethod:(id)sender
{ 
    

        
        
        ERAApplicationViewController *eRAApplicationViewController = 
        [[ERAApplicationViewController alloc] initWithNibName: 
         @"ERAApplicationViewController" bundle:nil];
        
        // self.navigationController.navigationBar.topItem.title=nil;
        //  [oAuthLoginView dismissModalViewControllerAnimated:YES];
        [self.navigationController popViewControllerAnimated:NO];
        [self.navigationController pushViewController:eRAApplicationViewController animated:NO];   
        

}
-(IBAction)BatterSideMethod:(id)sender
{
    str_batterside =@"batterside";
    str_message =@"Please Select the batter side";
    [self alertMessageMethod:str_message];
    
}

-(BOOL)validateTextField:(NSString *)candidate 
{
    //#define REG_EX_USERNAME_VALIDATION @"[a-zA-Z\\d]"
    //   NSPredicate *userNameValidation = [ NSPredicate predicateWithFormat:@"SELF MATCHES %@", REG_EX_USERNAME_VALIDATION];
    //    int retVal  = [ userNameValidation evaluateWithObject:candidate];
    NSLog(@"validateTextField: Called");
    
    
    NSString *emailRegex=@"^(?:|0|[0-9]\\d*)(?:\\.\\d*)?$";
    
    //NSString *emailRegex=@"[a-zA-Z\\d];
    NSLog(@"-all string value %@-",emailRegex);
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex]; 
    return [emailTest evaluateWithObject:candidate];
    
}
-(BOOL)PlayerVadiation :(NSString *)PlayerName
{
    unichar c;
    
    if ([PlayerName length]>0)
    {
        c = [PlayerName characterAtIndex:0];
    }
    else
    {
        return YES;
    }
    
    if ([[NSCharacterSet letterCharacterSet] characterIsMember:c])
    {
        return YES;
    }
    return NO;
    
}

-(void)alertMessageMethod:(NSString *)message
{
    
    if ([str_message isEqualToString:@"Please Enter Jersy number Numeric Value"]||[str_message isEqualToString:@"Please Select the all Value!"]||[str_message isEqualToString:@"User Cant store more then 50 Players!"]||[str_message isEqualToString:@"Please Enter valid name"]) 
    {   
        addplayerView.hidden=NO;
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:str_message delegate:self cancelButtonTitle:nil otherButtonTitles:@"Okey", nil];
        [alrt_message show];
        return;
        
    }
    if ([str_message isEqualToString:@"Please Select the batter side"])
    {
        
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:str_message delegate:self cancelButtonTitle:@"Left" otherButtonTitles:@"Right", nil];
        [alrt_message show];
        return;
    }
    
    
    if ([str_message isEqualToString:@"You are not added any players! Do you want to go back"]||[str_message isEqualToString:@"Do you want to save this data"]||[str_message isEqualToString:@"You are not Saved Data! Do you want to go back"])
    {
        
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:str_message delegate:self cancelButtonTitle:nil otherButtonTitles:@"Yes", nil];
        [alrt_message show];
        return;
    }
    
    if ([str_message isEqualToString:@"Please Enter Home Team Name And Location"]) 
    {
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:str_message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alrt_message show];
        return;

    }
   
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"----%s",__func__);
    if (buttonIndex==0)
    {
        if ( str_batterside ==@"batterside")
        {
            [btn_batterside setTitle:@"L" forState:UIControlStateNormal];
            [btn_batterside setTintColor:[UIColor blackColor]];
            NSLog(@"---%@---",btn_batterside.titleLabel.text);
        }
        
        if (str_AddPlayercheck ==@"saveAddplayerData")
        {
            str_AddPlayercheck=@"EditPlayerBackbtn";
            str_message=@"You are not Saved Data! Do you want to go back";
            [self alertMessageMethod:str_message];
        }
    }
    else if (buttonIndex==1)
    {
        if ( str_batterside ==@"batterside")
        {
            [btn_batterside setTitle:@"R" forState:UIControlStateNormal];
            NSLog(@"---%@---",btn_batterside.titleLabel.text);
        }
        if (str_AddPlayercheck==@"EditPlayerBackbtn") 
        {
            [[self navigationController]popViewControllerAnimated:YES];
        }
        
        if (str_AddPlayercheck ==@"saveAddplayerData")
        {
            
            [ self FinalSubmitPlayer];
        }
        
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    if(interfaceOrientation != UIInterfaceOrientationPortrait &&
       interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown)
    {
        // landscape
        addplayerView.frame=CGRectMake(110,-110, 900, 796);
        
        
        PlayerTableView.frame =CGRectMake(160, 170, 680, 400);
        
        labelPlayerName.frame=CGRectMake(165, 148, 206, 21);
        labelJerseyNumeber.frame=CGRectMake(379, 148, 230, 21);
        labelHanded.frame=CGRectMake(617,149,216, 21 );
        //buttonAddPlayer.frame =CGRectMake(450, 207, 136, 37);
        
        homeTeamNameLabel.frame=CGRectMake(165, 39,173 , 26);
        homeTeamLocationlabel.frame=CGRectMake(165, 71, 173, 26);
        homeTeamNameTextField.frame=CGRectMake(379, 39, 186, 31);
        homeTeamLocationTextField.frame=CGRectMake(379, 75, 186, 31);
        
        buttonAddPlayer.frame =CGRectMake(700, 100, 121, 37);
        buttonSave.frame =CGRectMake(450, 650, 75, 37);
    }
    else
    {  
        
        addplayerView.frame=CGRectMake(0, 109, 728,796);
        PlayerTableView.frame =CGRectMake(53, 211, 680, 665);
        labelPlayerName.frame=CGRectMake(60, 190, 206, 21);
        labelJerseyNumeber.frame=CGRectMake(274, 190, 230, 21);
        labelHanded.frame=CGRectMake(512,190,216, 21 );
        
        homeTeamNameLabel.frame=CGRectMake(60, 39,173 , 26);
        homeTeamLocationlabel.frame=CGRectMake(60, 76, 173, 26);
        homeTeamNameTextField.frame=CGRectMake(274, 39, 186, 31);
        homeTeamLocationTextField.frame=CGRectMake(274, 74, 186, 31);

        buttonAddPlayer.frame =CGRectMake(611, 100, 121, 37);
        buttonSave.frame =CGRectMake(339, 879, 75, 37);
        // NSLog(@"%@",viewAddPlayerBox.frame);
        // portrait
        // addplayerViewBox.frame=CGRectMake(0, 0,379,269);
        
        
    }
	return YES;
    
}

#pragma TextField Delegate Methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
}

-(void) viewWillAppear:(BOOL)animated
{
    [arrayPitcher removeAllObjects];
    [arraycatcher removeAllObjects];
    [arrayPlayerdataArray removeAllObjects];

    
      arraycatcher=[DBObj selectHomeTeamPlayerstFromDb:@"C"];

    
   

      arrayPitcher=[DBObj selectHomeTeamPlayerstFromDb:@"P"];
        
 
    

    
    [PlayerTableView reloadData];
    [super viewWillAppear:YES];

}



-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    
}


@end
