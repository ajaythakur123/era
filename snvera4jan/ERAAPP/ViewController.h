//
//  ViewController.h
//  ERAPitchChart
//
//  Created by Krishna Kothapalli on 28/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "detailsDB.h"

@class RootViewController;

@interface ViewController : UIViewController<UITextFieldDelegate,UIPopoverControllerDelegate, UISplitViewControllerDelegate,UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,UIScrollViewDelegate>
{
   
    NSString *currentPitchLabel;
    NSString *currentPitchLabelAppend;
    NSString *stringClassMode;
    
    IBOutlet UILabel *pitchLabel0;
    IBOutlet UILabel *pitchLabel1;
    IBOutlet UILabel *pitchLabel2;
    IBOutlet UILabel *pitchLabel3;
    IBOutlet UILabel *pitchLabel4;
    IBOutlet UILabel *pitchLabel5;
    IBOutlet UILabel *pitchLabel6;
    IBOutlet UILabel *pitchLabel7;
    IBOutlet UILabel *pitchLabel8;
    IBOutlet UILabel *pitchLabel9;
    IBOutlet UILabel *pitchLabel10;
    IBOutlet UILabel *pitchLabel11;
    IBOutlet UILabel *pitchLabel12;
    IBOutlet UILabel *pitchLabel13;
    int foulremove;

    
    UIView  *ViewMainContainer;
    
    UIBarButtonItem *settingsButton;
      detailsDB *DBObj;
    IBOutlet UIScrollView *scrollView;
    

    UIScrollView *currentRunningScroll;
  //  UIScrollView *scrollViewStatistics;
    UIScrollView *mask_scrollView;
    UIScrollView *scrollLocation;
    UIScrollView *batter_scrollView;
  IBOutlet  UIScrollView *scrollNumberLine;
     IBOutlet  UIScrollView *scrollMonitor;
    IBOutlet  UIScrollView *scrollPitchLabels;
    
    IBOutlet UIImageView *imageView;
    UILabel *Teamlbl,*Gamelbl,*Datelbl,*Catcherlbl,*Umpirelbl,*lbl,*scoreBoardTeam_lbl,*scoreBoardGame_lbl;
    IBOutlet UIButton *pitch_Chart_Btn,*batter_Btn;
    
    IBOutlet UIButton *buttonHanded;
     IBOutlet UIButton *buttonBaseOnBall;
     IBOutlet UIButton *buttonFoul;
     IBOutlet UIButton *buttonSwing;
    IBOutlet UIButton *buttonPassed;
    IBOutlet UIButton *buttonwild;
    IBOutlet UIButton *buttonCalled;
    NSMutableArray *mArray,*mArray2,*mArray3,*mArray4,*myArray_lbl,*batterNoArray,*batterNameArray;
    NSMutableArray *baterInfo_Array;
    NSMutableArray *batter_detailsArray;
    NSMutableArray *displayView_detailsArray;
    NSMutableArray *arrayTotalInnings;
    NSMutableArray *DIsplaybaterInfo_Array;
    NSMutableArray *arrayInningTag;
    NSArray *Array_defensivePositionsArray;
    
    //settings
    NSMutableArray *teamNames_Array;
      NSMutableArray *arrayOppentplayer;
     NSMutableArray *arrayOppentplayer1;
    NSMutableArray *array_pitcher;
    NSMutableArray * array_catcher;
    UIImageView *numberLine ;
    UIImageView *  numberLineLand;
  
    IBOutlet UIImageView *displayImageView,*graph_ImageView,*numbers_ImageView;
    
    UILabel *showScore_lbl,*batterNo_lbl,*batterName_lbl;
    IBOutlet UILabel *square_lbl,*circle_lbl,*corner_lbl;
    IBOutlet UIButton *btn1,*btn2,*btn3,*btn4,*btn5,*btn6,*btn7,*btn8,*btn9,*btn10,*btn11,*btn12,*btn13,*btn14;
    
    int senderTag;
    int batterInfoTag;
    int batterTag;
    int btnDisplayViewTag;
    int modeValue;
    
    CGRect selfViewFrame;
    CGRect selectedBtnFrame;
    CGRect batterViewFrame;
    
    IBOutlet UIView *displayView;
    IBOutlet UIView *batter_View;
    IBOutlet UIView *batterInfo_View;
    IBOutlet UIView *maskView;
    IBOutlet UIView *view_strikeTypeOfPitch;
    IBOutlet UIView *view_ballTypeOfPitch;
    IBOutlet UIView *view_ballPopUpSubMenu;
    IBOutlet UIView *view_StrikeView;
    IBOutlet UIView *view_hitFairView;
    IBOutlet UIView *view_strikeOutView;
    IBOutlet UIView *view_locationOfHitView;
    IBOutlet UIView *view_defensivePositionsView;
    IBOutlet UIView *view_Players;
    IBOutlet UIView *viewStatistics;
    IBOutlet UIView *viewAddPlayerCP;
    IBOutlet UIView *viewUpperMenu;
    IBOutlet UIView *viewMonitor;
    
    IBOutlet UIView *viewRunnerScored;
    IBOutlet UIView *viewRunnerOut;
    IBOutlet UIView *viewStolenBase;
    //l/p
   // IBOutlet UIToolbar *toolBar;
    
    NSMutableArray *mArrayLandscape;
    NSMutableArray *mArrayLandscape2;
    NSMutableArray *mArrayLandscape3;
    NSMutableArray *mArrayLandscape4;
    NSMutableArray *arrrayMatches;
    NSMutableArray *arrayPitches;
    NSMutableDictionary *dicSetvalues;
    
    
    
    IBOutlet UIView *viewPotrait;
    IBOutlet UIView *viewLandscape;
    IBOutlet UILabel *labelLoading;
     UIScrollView *scrollViewLandscape;
     UIScrollView *batter_scrollViewLandscape;
    
    IBOutlet UIImageView *batter_ImageView;
    IBOutlet UIImageView *batterTitile_imageView;
    IBOutlet UIImageView *imageView_locatiogGraph;
      IBOutlet UIImageView *imageTitleSettings;
             //UIImage *img_fieldLocation; 
    
    IBOutlet UITableView *table_displayOptions;
      UILabel *btnlbl_sno;
    IBOutlet UILabel *labelTitleSettings;
    IBOutlet UILabel *namelbl_batterView;
    IBOutlet UILabel *numberlbl_batterView;
             UILabel *btnlbl_jerseyNumber;
             UILabel *btnlbl_baterName;
             UILabel *btnlbl_Handed;
             UILabel *grid_lbl;
    IBOutlet UILabel *labelbatterNameForMainpop;
    IBOutlet UILabel *labelJerseyNumberForMainpop;
     IBOutlet UILabel *labelAddplayer;
     IBOutlet UILabel *labelEdit;
     IBOutlet UILabel *labelHandedBatter;
    IBOutlet UILabel *labelSettingsOpponent;
     IBOutlet UILabel *labelSettingsPitcher;
     IBOutlet UILabel *labelSettingsCatcher;
    
    IBOutlet UILabel *labelPlayerTitle;
    
    IBOutlet UITextField *playerNameCp;
    IBOutlet UITextField *jerseyNumberCP;
    
    IBOutlet UITextField *nametxt_BatterView;
    IBOutlet UITextField *jerseyNumbertxt_BatterView;
    IBOutlet UITextField *handedtxt_BatterView;
     IBOutlet UIView  *viewSetteings;
    IBOutlet UIButton *btn_checkboxmainmenu;
    IBOutlet UIButton *editBtn;
    IBOutlet UIButton *buttonSelectPlayer;
    IBOutlet UIButton *buttonAddPlayerRoster;
    IBOutlet UIButton *buttonMonitor;
     IBOutlet  UIButton  *buttonHandedCP;
        IBOutlet  UIButton  *buttonHandedBatter;
      IBOutlet  UIButton  *buttonStrikeOut;
     IBOutlet  UIButton  *buttonOnBase;
     IBOutlet  UIButton  *buttonFoulStrike;
    
    IBOutlet UIBarButtonItem *buttonCatcher;
     IBOutlet UIBarButtonItem *buttonPitcher;
     IBOutlet UIButton *buttonSelect;
    
     IBOutlet UIButton *buttonAddCP;
    
    UIButton *btnBack;
    UIBarButtonItem *navi_back;
    UIView *viewNav;
    UIButton *btnSelect;
   
    
    IBOutlet UIButton *saveBtn;
             UIButton *btn_loadSenderidbtn;
             UIButton *btn_loadstrikesenderid;
             UIButton *btn_submenucheckbox; 
             UIButton *btn_strikecheckboxOutPitch;
             UIButton *btn_strikeTypeofPitcbox;
             UIButton *btn_hitfaircheckbox;
             UIButton *btn_strikeoutCheckbox;
    
   
    
    BOOL mode;
    BOOL boolRemoveForwardPitchLabel;
    BOOL error;
    BOOL LastFoul;
    BOOL LastFoulconstraint;
    BOOL scrollViewIdentification;
    NSString *buttonTitle;
    NSString *alertMessage;
    NSString *alertMessageType;
    NSString *batterInfoStr;
    
    UIAlertView *alert;
    NSString *teamName;
    NSString *stringPitcher;
    NSString *stringCatcher;
    NSString *stringDate;
    int LeadOut;
    int FirstInningOut;
    int tag_outComeOfAtBat;
    int potrait;
    int nextBatter;
    NSMutableDictionary  *dict_oneinnngsData;
    
    NSMutableDictionary *dicInnings;
    NSMutableDictionary *dicMatchData;
    //ajay
    
    NSString *fieldImage;
    NSMutableString     *stringKeyMainPitch;
    NSMutableString     *stringKeyPitch;
    NSMutableString     *stringMainPich;
    
    
    NSMutableDictionary *dicAllInnings;
    NSMutableDictionary *dicMainPitch;
    NSMutableDictionary *dicStrikePitch;
    NSMutableDictionary *dicStrikeDetails;
    NSMutableDictionary *dicOutRecord;
    NSMutableDictionary *dicAtBatRecord;
    NSMutableDictionary *dicDisplayMatches;
    
    NSString *matchId;
    NSString *stringAdvHitfair;
     NSString *type;
    int tagPitchLabel;
    int strikePitchrtag;
    int ballerPitchTag;
    int mainGridTag;
    int once;
    int intnextbatter;
    int tempStrike;
    int tempFoul;
    int intOutPlyers;
    int intFoulRecord;
    int intTotalBalls;
    int foul;
    int LastTotalFoulRecord;
    int totalFoulRecord;
    bool boolFoul;
    bool boolalertEof;
    bool boolTwoATBatRecord;
     IBOutlet UILabel *lblTotalBalls;
     IBOutlet UILabel *lblplayersBalls;
     IBOutlet UILabel *lblOut;
     IBOutlet UILabel *lblbatterPitch;
      IBOutlet  UIImageView *imageEndOfInning;
     UIImageView * imgv_eoi;
       UIImageView * imgEndOfInning;
    //PlayerBall count
    int currentPitchTag;
    int intNumberOfBalls1;
    int intPlayerForRows;
    int intprvMainTag;
    int intAdvanceTag;
    int jerseyNOMatch;
    int intLayerBatterTag;
    int intLayerPitchTag;
    int intEndOfInning;
    int intRecordBalls;
    int intRecordStrike;
    int intRecordOut;
    int intLastOutRecord;
    int intLastBallRecord;
    int intLaststrikeRecord;
    int intStrikeOut;
    int intLastAtBatRecord;
    int intOutLastRecord;
    
    int intalertInning;
    //////monitor
    int intMonitorStrike;
    //balls
    int intMonitorFB_Ball;
    int intMonitorCB_Ball;
    int intMonitorCU_Ball;
    int intMonitorSL_Ball;
    int intMonitorCF_Ball;
    int intMonitorKK_Ball;
    int intMonitorFS_Ball;
    int intMonitorSN_Ball;
    //strike
    int intMonitorFB_Strike;
    int intMonitorCB_Strike;
    int intMonitorCU_Strike;
    int intMonitorSL_Strike;
    int intMonitorCF_Strike;
    int intMonitorKK_Strike;
    int intMonitorFS_Strike;
    int intMonitorSN_Strike;
    
    int intTotalInningsCountMonitor;
    int intAtBatMonitor;
    int intAtBatMorethan3Monitor;
    int intFirstPitchstrike;
    ///total
    int intMonitorFB_Total;
    int intMonitorCB_Total;
    int intMonitorCU_Total;
    ///lead of 
    int intMonitorLeadOfOut;
    int intMonitorTwoOutRecord;
    int intMonitorTwoOutRecordCount;
    
    UIPopoverController   *popoverController;
    //toolbar
    IBOutlet UILabel *labelDate;
    IBOutlet UILabel *labelPitcher;
    IBOutlet UILabel *labelCatcher;
    IBOutlet UILabel *labelOpponent;
    IBOutlet UIToolbar *toolbar;
    
    
    
    
    /////////monitor
    
    IBOutlet UILabel *mlabelTotalPitches;
    IBOutlet UILabel *mlabelStrikePercentage;
    IBOutlet UILabel *mlabelStrikeScore;
    ///balls
    IBOutlet UILabel *mlabelCurveball_B;
    IBOutlet UILabel *mlabelFastball_B;
    IBOutlet UILabel *mChangesUp_B;
    IBOutlet UILabel *mSlider_B;
    IBOutlet UILabel *mCutFb_B;
    IBOutlet UILabel *mknuckle_B;
    IBOutlet UILabel *mforkball_B;
    IBOutlet UILabel *msniker_B;
    
    ///balls
    IBOutlet UILabel *mlabelCurveball_S;
    IBOutlet UILabel *mlabelFastball_S;
    IBOutlet UILabel *mChangesUp_S;
    IBOutlet UILabel *mSlider_S;
    IBOutlet UILabel *mCutFb_S;
    IBOutlet UILabel *mknuckle_S;
    IBOutlet UILabel *mforkball_S;
    IBOutlet UILabel *msniker_S;
    
    //total
    IBOutlet UILabel *mlabelCurveball_T;
    IBOutlet UILabel *mlabelFastball_T;
    IBOutlet UILabel *mlabelChangesUp_T;
    IBOutlet UILabel *mlabelPitches_MP;
    IBOutlet UILabel *mlabelFastball_MP;
    IBOutlet UILabel *mChangesUp_MP;
    
    
    IBOutlet UILabel *mlabelPitchesMixFastballs;
    IBOutlet UILabel *mlabelPitchesMixChangesUps;
    IBOutlet UILabel *mlabelPitchesMixCurveBalls;
   
    IBOutlet UILabel *mlabelPitchesMixFastballs_P;
    IBOutlet UILabel *mlabelPitchesMixChangesUps_P;
    IBOutlet UILabel *mlabelPitchesMixCurveBalls_P;
    
    IBOutlet UILabel *mlabelTotalAtBats;
    IBOutlet UILabel *mlabelAtBatsMoreThan3Pitches;
    IBOutlet UILabel *mlabelInningsPitches;
    IBOutlet UILabel *mlabelLeadsOffOuts;
    IBOutlet UILabel *mlabel2OutOffbats;
    IBOutlet UILabel *mlabelAtBats;
    IBOutlet UILabel *mlabelOfFirstPitchesStrikes;
    IBOutlet UILabel *mlabelPitchesStrikepercentage;
    
    ///red pitch scroll view
    IBOutlet UIScrollView *scrollRedPitch;
    
    NSString *stringRunnerScored;
    NSString *stringRunnnerOut;
    NSString *stringStolenBase;
    
  //////////Current base track
  
    NSString *stringCrntHomeBase;
    NSString *stringCrntB1;
    NSString *stringCrntB2;
    NSString *stringCrntB3;
    
    
    NSMutableDictionary *dicRunOut;
    
    NSString *strOutLastRecord;
    int      intOutLastRunner;
    
    }
@property (nonatomic, strong) NSMutableArray *baterInfo_Array;
@property (nonatomic, strong)  NSString *stringClassMode;
@property (nonatomic, strong)  UIBarButtonItem *buttonCatcher;
@property (nonatomic, strong)  UIBarButtonItem *buttonPitcher;
@property (nonatomic, strong)  UIButton *buttonSelect;


@property (nonatomic, strong)  UILabel *labelDate;
@property (nonatomic, strong)  UILabel *labelPitcher;
@property (nonatomic, strong) UILabel *labelCatcher;
@property (nonatomic, strong) UILabel *labelOpponent;



@property (nonatomic, strong) UIScrollView *currentRunningScroll;
@property (nonatomic, strong) UIButton  *buttonAddCP;
@property (nonatomic, strong) UIButton  *buttonHandedBatter;
@property (nonatomic, strong) UIButton  *buttonHanded;
@property (nonatomic, strong) UIButton  *buttonHandedCP;
@property (nonatomic, strong) UILabel *lblTotalBalls;
@property (nonatomic, strong) UILabel *lblplayersBalls;
@property (nonatomic, strong) UILabel *lblOut;

@property (nonatomic, strong) UILabel *lblbatterPitch;

@property (nonatomic, strong)  NSString *stringPitcher;
@property (nonatomic, strong)  NSString *stringDate;
@property (nonatomic, strong)  NSString *stringCatcher;
@property (nonatomic, strong) UILabel *labelSettingsOpponent;
@property (nonatomic, strong) UILabel *labelSettingsPitcher;
@property (nonatomic, strong) UILabel *labelSettingsCatcher;
@property (nonatomic, strong)  UILabel *labelbatterNameForMainpop;
@property (nonatomic, strong) UIImageView *imageTitleSettings;
@property (nonatomic, strong) UILabel *labelTitleSettings;
@property (nonatomic, strong) UIView  *viewSetteings;
@property (nonatomic,strong) NSString *teamName;
@property (nonatomic,strong) NSString *matchId;
@property (nonatomic, strong)  UIToolbar *toolbar;
@property (nonatomic, strong) id detailItem;
@property (nonatomic, strong) IBOutlet UILabel *detailDescriptionLabel;

@property (nonatomic,strong) NSMutableArray *mArray,*mArray2,*mArray3,*mArray4,*myArray_lbl,*gridButtons_Array;

@property (nonatomic,strong) IBOutlet UIView *view_strikeTypeOfPitch;
@property (nonatomic,strong) IBOutlet UIView *view_ballTypeOfPitch;
@property (nonatomic,strong) IBOutlet UIView *viewAddPlayerCP;

-(void)StrikepopupMethod:(UIButton *)sender;

-(void)BallpopupMethod:(UIButton *)sender;
-(void)btnLabel:(UIButton *)sender;
-(void)loadButtonOnPitchlabel;
-(void)pitchLabel :(NSString *)String;
-(void)endofInning;
-(void)closeDisplayViewLandscape;
-(void)NextBatterView :(int) mainTag;
-(void)configureView;
-(void)Settings:(id)sender;
-(int)PitchToBatter ;
-(void)addLayerBatter :(int)batterLayerTag;
-(void)addLayerPitch :(int)PitchLayerTag;
-(void)addOutInDic;
-(void)LastImageBatter;
-(void)OutDisplay;
-(void)previousDisplayPitch;
-(void)TotalBallsofMatch;
-(int)CheckForFirstRow;
-(void)foulCount;
-(int)serialValidation:(int)tag;
-(void)popAddPlayer;
-(void)twoLeadOut;
-(void)displayAllBatter;
-(void)displayPitchAll;
-(void)LeadOfOut;
-(IBAction)AddPLayerOnRoster;
-(IBAction)AddPLayer;
-(IBAction)CancelSettingTable:(id)sender;
-(IBAction)NextBatter:(id)sender;
-(IBAction)btn:(id)sender;
-(IBAction)done:(id)sender;
-(IBAction)selectOppentTeam:(id)sender;
-(IBAction)InMonitor:(id)sender;
-(IBAction)selectCatcher:(id)sender;
-(IBAction)selectPitcher:(id)sender;

-(IBAction)addCatcherPitcher:(id)sender;
-(IBAction)CancelADDCP;
-(IBAction)handedADDCP;
-(IBAction)AddPLayerADDCP;

-(void)DisplayOpponentPlayer;
-(IBAction)SelectOppentPlayer;
-(IBAction)cancelPlayer;
-(IBAction)Back;
-(IBAction)handedForBatter;
-(void)displayLastset;

-(IBAction)runnerScored:(id)sender;
-(IBAction)runnerOut:(id)sender;
-(IBAction)stolenBase:(id)sender;

-(IBAction)runnerScoredButtons:(id)sender;
-(IBAction)runnerOutButtons:(id)sender;
-(IBAction)stolenBaseButtons:(id)sender;

-(int)intialValueColoumnTag;
-(void)runnerOutRec;
-(void)montiorDisplayRecord;
-(void)removeForwardPitchLabel;
-(void)getEndOfInning;
-(void)recordBallStrike;
-(void) bringScrollSubViewToFrontOnMainViewLocation:(UIView *) view;
-(void)CLoseBatterMainWindow;
-(void)BallsCalculation;
-(void)ColoumnNextBatterRecord;
-(void) strikeBallReplacement; 
-(void) bringScrollSubViewToFrontOnMainView:(UIView *) view;
-(void) alertMessageMethod:(NSString *)Message;
-(void) drawButtonsInPotraitMode;
-(void) drawButtonsInLandScapeMode;
-(void) drawButtonsForBatterInPotraitMode;
-(void) drawButtonsForBatterInLandScapeMode;
-(void) checkDisplayMatches;
-(void) resetMainViewSettings:(UIView *) view;
-(BOOL) validateTextField:(NSString *)candidate;
-(int)identifyStrikeEmptyPitch :(UIButton *)sender;
-(int)identifyBallsEmptyPitch :(UIButton *)sender;

/////new

-(void)LeadOfOutInnings;
@end
