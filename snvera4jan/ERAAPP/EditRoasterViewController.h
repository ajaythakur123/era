//
//  EditRoasterViewController.h
//  ERAApplication
//
//  Created by ajay thakur on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "detailsDB.h"

@interface EditRoasterViewController : UIViewController
{
     UITableView *PlayerTableView;
    
    detailsDB *DBObj;
    UITextField *textField;
    
    NSMutableArray *textArray;
    NSMutableDictionary *mdict;
     NSMutableArray *arrayPlayerdataArray;
    IBOutlet   UIView *addplayerView;
    IBOutlet   UIView *viewAddPlayerBox;
    IBOutlet   UITextField *playerNameTextField;
    IBOutlet   UITextField *JerseyNumberTextField;
    IBOutlet   UITextField *handlingTextField;
    IBOutlet   UILabel *labelPlayerName;
    IBOutlet   UILabel *labelJerseyNumeber;
    IBOutlet   UILabel *labelHanded;
    IBOutlet   UIButton *buttonAddPlayer;
    IBOutlet   UIButton *buttonSave;
    NSMutableArray *finalTextArray;
    
    NSArray *TextArrayCopy ;
    NSMutableArray *array1;
    NSMutableArray *  ArraySortName ;
    NSArray *sortedArray ;
    int teamID;
    NSString *teamName;
    //-------Ambadas-----
    
    NSString   *str_message;
    NSString   *str_batterside;
    NSString   *str_AddPlayercheck;
    
    NSString * MODE;
    int replaceObject;
    
    IBOutlet UIButton *btn_batterside;

}
@property (nonatomic, strong) NSString *teamName;
@property(nonatomic,assign) int teamID;
@property (nonatomic, strong) IBOutlet   UIButton *buttonAddPlayer;
@property (nonatomic, strong) IBOutlet   UIButton *buttonSave;
@property (nonatomic, strong) IBOutlet UITableView *PlayerTableView;
@property (nonatomic, strong)  UITextField *playerNameTextField;
@property (nonatomic, strong)  UITextField *JerseyNumberTextField;
@property (nonatomic, strong)  UITextField *handlingTextField;
@property (nonatomic, strong)  IBOutlet   UIView *addplayerView;
@property (nonatomic, strong)  UIView *viewAddPlayerBox;
@property (nonatomic, strong)  UILabel *labelPlayerName;
@property (nonatomic, strong)   UILabel *labelJerseyNumeber;
@property (nonatomic, strong)   UILabel *labelHanded;

-(IBAction)AddPlayerOnRoaster;
-(IBAction)sumbmitPlayerToDabase;
-(void)getPlayerDb;
-(IBAction)CancelPlayerView;
-(IBAction)FinalSubmitPlayer;

//----Ambadas-----
-(BOOL)validateTextField:(NSString *)candidate ;
-(void)alertMessageMethod:(NSString *)message;
-(IBAction)BatterSideMethod:(id)sender;
-(BOOL)PlayerVadiation :(NSString *)PlayerName;



@end
