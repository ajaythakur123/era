//
//  ADDRoasterViewControler.m
//  ERAApplication
//
//  Created by ajay thakur on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ADDRoasterViewControler.h"
#import "SelectTeamViewController.h"
#import "ExistRoasterViewController.h"
#import "HomeTeamViewController.h"

@implementation ADDRoasterViewControler
@synthesize buttonAddRoster,buttonExistingRoster,buttonHomeTeam,buttonPitcher,buttonCatcher;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
-(IBAction)AddRoaster{
    SelectTeamViewController *selectTeamViewController = [[SelectTeamViewController alloc]initWithNibName:@"SelectTeamViewController" bundle:nil];
    [self.navigationController pushViewController:selectTeamViewController animated:YES];
}
-(IBAction)AddExistingRoster
{
    ExistRoasterViewController *existRoasterViewController = [[ExistRoasterViewController alloc]initWithNibName:@"ExistRoasterViewController" bundle:nil];
    [self.navigationController pushViewController:existRoasterViewController animated:YES];

}






- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations

    
    
    if(interfaceOrientation != UIInterfaceOrientationPortrait &&
       interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown)
    {
        // landscape
//        buttonHomeTeam.frame=CGRectMake(420, 245, 180, 37);
        buttonAddRoster.frame=CGRectMake(400, 311,261,37);
        buttonExistingRoster.frame=CGRectMake(400, 379,261,37);
        
        
        
    }
    else
    {  
        // portrait
         //buttonHomeTeam.frame=CGRectMake(2, 360, 180, 37);//(294, 360,180,37)
         buttonAddRoster.frame=CGRectMake(254, 389,261,37);//(294, 502, 180, 37)
         buttonExistingRoster.frame=CGRectMake(254,460,261,37);//294, 431,180,37
    }
	return YES;
}

@end
