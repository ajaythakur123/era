//
//  RecallMatchesViewController.h
//  ERAAPP
//
//  Created by Ajay Thakur on 12/13/12.
//
//

#import <UIKit/UIKit.h>
#import "detailsDB.h"

@interface RecallMatchesViewController : UIViewController
{
     detailsDB *DBObj;
  IBOutlet  UITableView *matchsTableView;
    NSMutableArray *arrayMatches;
}

@end
