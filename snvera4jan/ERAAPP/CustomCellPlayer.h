//
//  CustomCellPlayer.h
//  ERAApplication
//
//  Created by ajay thakur on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCellPlayer : UITableViewCell
{
       IBOutlet UILabel *playerName;
    IBOutlet UILabel *JerseyNumber;
    IBOutlet  UILabel *Handled;
}
@property (nonatomic, strong) UILabel *playerName;
@property (nonatomic, strong) UILabel *JerseyNumber;

@property (nonatomic, strong) UILabel *Handled;

@end
