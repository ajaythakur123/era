//
//  SelectTeamViewController.h
//  ERAApplication
//
//  Created by ajay thakur on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "detailsDB.h"
#import "EditRoasterViewController.h"
@interface SelectTeamViewController : UIViewController

{
    detailsDB *DBObj;
    NSMutableDictionary *mdict;
     NSMutableArray  *teamNames_Array;
    NSMutableArray  *location_Array;
    IBOutlet UILabel    *title_Label;
    IBOutlet UIView   *tableviewList;
    IBOutlet UILabel  *labelSelectTeamLabel;
    IBOutlet UILabel  *labelSelectlocation;
    IBOutlet UITextField *teamNameTextField;
    IBOutlet UITextField *locationTextField;
    IBOutlet UIButton *buttonSave;
    EditRoasterViewController *editRoasterViewController;
    UIAlertView *alert;
    UITextField       *textAlert;
    NSString *type;
    UIBarButtonItem *editExistTeamDataButton;
    IBOutlet UITableView       *tblw_alldatatable;
}

@property (nonatomic, strong)  UITextField *teamNameTextField;
@property (nonatomic, strong)  UITextField *locationTextField;
@property (nonatomic, strong)  UILabel  *labelSelectTeamLabel;
@property (nonatomic, strong)  UILabel  *labelSelectlocation;
@property (nonatomic,strong)   UIButton *buttonSave;
-(IBAction)roasterLoader;
-(IBAction)selectTeam;
-(IBAction)selectLocation;
-(IBAction)HideTableview;
-(IBAction)submit;
-(void)getTeamDb;
-(void)editExistTeamDataButtonClicked:(id) sender;
-(void)insertTeamNameDB;
-(BOOL)PlayerVadiation :(NSString *)PlayerName;
@end
