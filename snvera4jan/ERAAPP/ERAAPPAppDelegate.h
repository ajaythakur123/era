//
//  ERAAPPAppDelegate.h
//  ERAAPP
//
//  Created by Ambadas B on 4/24/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "sqlite3.h"

@class RootViewController;
@class DetailViewController;
@class ViewController;
@interface ERAAPPAppDelegate : NSObject <UIApplicationDelegate>
{
    sqlite3 *dataBaseConnection;
}
@property(nonatomic,strong)UINavigationController *tempnav;
@property(nonatomic,strong)UIViewController *tempviewcontroller;

@property (nonatomic, assign) sqlite3 *dataBaseConnection;
@property (nonatomic, strong) IBOutlet UIWindow *window;

@property (nonatomic, strong) IBOutlet UISplitViewController *splitViewController;

@property (nonatomic, strong) IBOutlet RootViewController *rootViewController;

@property (nonatomic, strong) IBOutlet DetailViewController *detailViewController;
@property (nonatomic, strong) IBOutlet ViewController *viewController;
@property (nonatomic, strong) NSMutableString *myHomeTeamName;
- (void) createEditableCopyOfDatabaseIfNeeded ;
- (void) openDatabaseConnection ;
@end
