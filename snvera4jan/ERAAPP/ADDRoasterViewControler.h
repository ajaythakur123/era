//
//  ADDRoasterViewControler.h
//  ERAApplication
//
//  Created by ajay thakur on 8/15/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ADDRoasterViewControler : UIViewController

{
    UIButton *buttonAddRoster;
    UIButton *buttonExistingRoster;
    
    UIButton *buttonHomeTeam;
    UIButton *buttonCatcher;
    UIButton *buttonPitcher;
    
    
}
@property(nonatomic,strong)IBOutlet UIButton *buttonAddRoster;
@property(nonatomic,strong)IBOutlet UIButton *buttonExistingRoster;
@property(nonatomic,strong)IBOutlet UIButton *buttonHomeTeam;
@property(nonatomic,strong)IBOutlet UIButton *buttonPitcher;
@property(nonatomic,strong)IBOutlet UIButton *buttonCatcher;


-(IBAction)Pitcher;
-(IBAction)Catcher;
-(IBAction)AddRoaster;
-(IBAction)AddExistingRoster;
@end
