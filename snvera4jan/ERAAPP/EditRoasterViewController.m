//
//  EditRoasterViewController.m
//  ERAApplication
//
//  Created by ajay thakur on 8/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EditRoasterViewController.h"
#import "detailsDB.h"
#import "CustomCellPlayer.h"
#import "SelectTeamViewController.h"
#import "ADDRoasterViewControler.h"
@implementation EditRoasterViewController

@synthesize PlayerTableView,playerNameTextField,JerseyNumberTextField,handlingTextField,addplayerView,teamID,teamName,viewAddPlayerBox,labelPlayerName,labelJerseyNumeber,labelHanded,buttonSave,buttonAddPlayer;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    arrayPlayerdataArray =[[NSMutableArray alloc]init];
    addplayerView.hidden=YES;
     DBObj =[[detailsDB alloc]init];
    
    //-----Ambadas----  
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.backBarButtonItem=nil;
    
       //PlayerTableView.frame =CGRectMake(0, 0, 500, 600);
    
    UIBarButtonItem *navi_back = [[UIBarButtonItem alloc] 
                                  initWithTitle:@"Back"                                            
                                  style:UIBarButtonItemStyleBordered 
                                  target:self 
                                  action:@selector(BackTopreviousMethod:)];
    self.navigationItem.leftBarButtonItem = navi_back;
   // addplayerView.frame=CGRectMake(20, 109, 728,796);
    //viewAddPlayerBox.frame=CGRectMake(0,0,379,269);
  //  addplayerViewBox.backgroundColor = [UIColor greenColor];
}
#pragma mark -
#pragma mark Table view data source


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[self getPlayerDb];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{
    // Return the number of rows in the section.
	return [arrayPlayerdataArray count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{
 
    

    CustomCellPlayer *cell;
    cell=[[[NSBundle mainBundle]loadNibNamed:@"CustomCellPlayer" owner:self options:nil]objectAtIndex:0];
   
    
    cell.Handled.text =[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"Handling"];
    cell.playerName.text =[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"PlayerName"];
    cell.JerseyNumber.text =[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"JerseyNumber"];
    
      [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell ;

    
    
    
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
    [tableView beginUpdates];
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [arrayPlayerdataArray removeObjectAtIndex:indexPath.row]; 
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [PlayerTableView reloadData];
        //[DBObj deletePlayerFromDatabase:[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"TeamName"]];
    }       
    [tableView endUpdates];
    
    
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
       addplayerView.hidden=NO;
    btn_batterside.titleLabel.text =[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"Handling"];
     playerNameTextField.text =[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"PlayerName"];
      JerseyNumberTextField.text =[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"JerseyNumber"];
     MODE =@"edit";
   // JerseyNumberTextField.userInteractionEnabled=NO;
    replaceObject =indexPath.row;
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}


-(IBAction)AddPlayerOnRoaster{
     
     MODE =@"nonedit";
   //  JerseyNumberTextField.userInteractionEnabled=YES;
    addplayerView.hidden=NO;
    playerNameTextField.text=@"";
    JerseyNumberTextField.text=@"";
    btn_batterside.titleLabel.text=nil;
    
}
-(IBAction)sumbmitPlayerToDabase{
    
 
  
 
    if([JerseyNumberTextField.text length]==0||[btn_batterside.titleLabel.text length]==0)
    {
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please add Jersey Number and Handed" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alrt_message show];
        
        return;
    }
    
    
    else
    {    addplayerView.hidden=YES;
        [playerNameTextField resignFirstResponder];
        [JerseyNumberTextField resignFirstResponder];
        [handlingTextField resignFirstResponder];
        if([arrayPlayerdataArray count]>=50)
        {
            str_message =@"User Cant store more then 50 Players!";
            [self alertMessageMethod:str_message];
            return;
        }
        
        
        else
        {
            
           mdict=[[NSMutableDictionary alloc]init];
            //   [mdict setObject:textInputView.text forKey:@"Text"]; 
            if ([self validateTextField:JerseyNumberTextField.text]==NO)
            {
                str_message=@"Please Enter Jersy number Numeric Value";
                [self alertMessageMethod:str_message];
                return; 
            }
            
           else if ([self PlayerVadiation:playerNameTextField.text]==NO)
            {
                str_message=@"Please Enter valid name";
                [self alertMessageMethod:str_message];
                return; 
            }
            
            
            
            else
            {
                if([MODE isEqualToString:@"edit"])//editMode
                {
                    
                    for (int i=0; i<[arrayPlayerdataArray count]; i++) {
                        
                        
                        NSString *jerseyrnumber =[[arrayPlayerdataArray objectAtIndex:i]objectForKey:@"JerseyNumber"];
                        
                        if ([JerseyNumberTextField.text isEqualToString:jerseyrnumber]&&replaceObject!=i) {
                            
                            UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number already exist" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                            [alrt_message show];
                            addplayerView.hidden=NO;
                            
                            return;
                            
                            
                        }
                        
                    }

                    
                    
//                    if ([[[arrayPlayerdataArray objectAtIndex:replaceObject]objectForKey:@"JerseyNumber"] isEqualToString:JerseyNumberTextField.text]) {

                    
                    [mdict setObject:teamName forKey:@"TeamName"];
                    [mdict setObject:playerNameTextField.text forKey:@"PlayerName"];
                    [mdict setObject:JerseyNumberTextField.text forKey:@"JerseyNumber"];
                    [mdict setObject:btn_batterside.titleLabel.text forKey:@"Handling"];
                    
                    [arrayPlayerdataArray replaceObjectAtIndex:replaceObject withObject:mdict];
                   //r  JerseyNumberTextField.userInteractionEnabled=YES;
                    [PlayerTableView reloadData];
                    return;
                        
                        
                   // }
//                    else
//                        
//                    {
//                        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number already exist" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
//                        [alrt_message show];
//                        [alrt_message release];
//                         addplayerView.hidden=NO;
//                        return;
//                        
//                    }
                }

            for (int i=0; i<[arrayPlayerdataArray count]; i++) {
                
                NSString *jerseyrnumber =[[arrayPlayerdataArray objectAtIndex:i]objectForKey:@"JerseyNumber"];
                
                if ([JerseyNumberTextField.text isEqualToString:jerseyrnumber]) {
                    
                    UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number already exist" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                    [alrt_message show];
                    addplayerView.hidden=NO;
                    
                    return;
                    
                    
                }
                
            }
            
            
            [mdict setObject:teamName forKey:@"TeamName"];
            [mdict setObject:playerNameTextField.text forKey:@"PlayerName"];
            [mdict setObject:JerseyNumberTextField.text forKey:@"JerseyNumber"];
            [mdict setObject:btn_batterside.titleLabel.text forKey:@"Handling"];
            
            [arrayPlayerdataArray addObject:mdict];
            
            [PlayerTableView reloadData];
                
                
            }//last else end
        }
        
    }
    
    
}
-(IBAction)CancelPlayerView{
   //  JerseyNumberTextField.userInteractionEnabled=YES;
    addplayerView.hidden=YES;
    [playerNameTextField resignFirstResponder];
    [JerseyNumberTextField resignFirstResponder];
}
-(IBAction)FinalSubmitPlayer
{
    if ([arrayPlayerdataArray count]==0) {
        
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please ADD Players" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alrt_message show];
        
    }
    else
    {
  
     
    
    
 
    
    
    for (int i=0 ;i <[arrayPlayerdataArray count];i++){
        
       mdict=[arrayPlayerdataArray objectAtIndex:i];
       [DBObj insertPlayerIntoDb:mdict];
    }
    
        
        NSArray* viewControllersInStack = self.navigationController.viewControllers;
        UIViewController* targetViewController = [viewControllersInStack objectAtIndex:1];
        [self.navigationController popToViewController:targetViewController animated:YES];
        
        
    
    }
    
}
-(void)getPlayerDb{
    
  
    
    
    arrayPlayerdataArray =[DBObj selectPlayerstFromDb];
    
 //   ArraySortName =  [[NSMutableArray alloc]init];
//    finalSortArray =  [[NSMutableArray alloc]init];
//    for (int i=0;i<[arrayName count];i++) {
//        
//        [ArraySortName addObject:[[arrayName objectAtIndex:i]objectForKey:@"Name"]];
//    }
//    NSLog(@"%d",[arrayName count]);
    
   


    
}



//--------Ambadas -----
//--------Ambadas -----

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //addplayerView.hidden=YES;
}
-(void)BackTopreviousMethod:(id)sender
{ 
    
    if ([arrayPlayerdataArray count]!=0)
    {
        str_AddPlayercheck =@"saveAddplayerData";
        str_message=@"Do you want to save this data";
        
        [self alertMessageMethod:str_message];
        return;
    }
    str_AddPlayercheck=@"EditPlayerBackbtn";
    str_message=@"You are not added any players! Do you want to go back";
    [self alertMessageMethod:str_message];
    
}
-(IBAction)BatterSideMethod:(id)sender
{
    str_batterside =@"batterside";
    str_message =@"Please Select the batter side";
    [self alertMessageMethod:str_message];
    
}

-(BOOL)validateTextField:(NSString *)candidate 
{
//#define REG_EX_USERNAME_VALIDATION @"[a-zA-Z\\d]"
//   NSPredicate *userNameValidation = [ NSPredicate predicateWithFormat:@"SELF MATCHES %@", REG_EX_USERNAME_VALIDATION];
//    int retVal  = [ userNameValidation evaluateWithObject:candidate];
    NSLog(@"validateTextField: Called");
    
    
    NSString *emailRegex=@"^(?:|0|[0-9]\\d*)(?:\\.\\d*)?$";
    
    //NSString *emailRegex=@"[a-zA-Z\\d];
NSLog(@"-all string value %@-",emailRegex);
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex]; 
    return [emailTest evaluateWithObject:candidate];
    
  
    
    
}
-(BOOL)PlayerVadiation :(NSString *)PlayerName
{
    unichar c;
    
    if ([PlayerName length]>0)
    {
        c = [PlayerName characterAtIndex:0];
    }
    else
    {
        return YES;
    }
    
    if ([[NSCharacterSet letterCharacterSet] characterIsMember:c])
    {
        return YES;
    }
    return NO;

}

-(void)alertMessageMethod:(NSString *)message
{
    
    if ([str_message isEqualToString:@"Please Enter Jersy number Numeric Value"]||[str_message isEqualToString:@"Please Select the all Value!"]||[str_message isEqualToString:@"User Cant store more then 50 Players!"]||[str_message isEqualToString:@"Please Enter valid name"]) 
    {   
        addplayerView.hidden=NO;
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:str_message delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alrt_message show];
        return;
        
    }
    if ([str_message isEqualToString:@"Please Select the batter side"])
    {
        
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:str_message delegate:self cancelButtonTitle:@"Left" otherButtonTitles:@"Right", nil];
        [alrt_message show];
        return;
    }
    
    
    if ([str_message isEqualToString:@"You are not added any players! Do you want to go back"]||[str_message isEqualToString:@"Do you want to save this data"]||[str_message isEqualToString:@"You are not Saved Data! Do you want to go back"])
    {
        
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:str_message delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [alrt_message show];
        return;
    }
    
    
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSLog(@"----%s",__func__);
    if (buttonIndex==0)
    {
        if ( str_batterside ==@"batterside")
        {
            [btn_batterside setTitle:@"L" forState:UIControlStateNormal];
            [btn_batterside setTintColor:[UIColor blackColor]];
            NSLog(@"---%@---",btn_batterside.titleLabel.text);
        }
        
        if (str_AddPlayercheck ==@"saveAddplayerData")
        {
            str_AddPlayercheck=@"EditPlayerBackbtn";
            str_message=@"You are not Saved Data! Do you want to go back";
            [self alertMessageMethod:str_message];
        }
    }
    else if (buttonIndex==1)
    {
        if ( str_batterside ==@"batterside")
        {
            [btn_batterside setTitle:@"R" forState:UIControlStateNormal];
            NSLog(@"---%@---",btn_batterside.titleLabel.text);
        }
        if (str_AddPlayercheck==@"EditPlayerBackbtn") 
        {
            [[self navigationController]popViewControllerAnimated:YES];
        }
        
        if (str_AddPlayercheck ==@"saveAddplayerData")
        {
            
            [ self FinalSubmitPlayer];
        }
        
    }
}
//-------------------


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    if(interfaceOrientation != UIInterfaceOrientationPortrait &&
       interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown)
    {
        // landscape
        addplayerView.frame=CGRectMake(110,-110, 900, 796);
 
            
         PlayerTableView.frame =CGRectMake(160, 170, 680, 400);
      
        labelPlayerName.frame=CGRectMake(165, 148, 206, 21);
        labelJerseyNumeber.frame=CGRectMake(379, 148, 230, 21);
        labelHanded.frame=CGRectMake(617,149,216, 21 );
         //buttonAddPlayer.frame =CGRectMake(450, 207, 136, 37);
        
         buttonAddPlayer.frame =CGRectMake(700, 100, 121, 37);
          buttonSave.frame =CGRectMake(450, 650, 75, 37);
    }
    else
    {  
        
         
         addplayerView.frame=CGRectMake(0, 109, 728,796);
        PlayerTableView.frame =CGRectMake(53, 169, 680, 665);
        labelPlayerName.frame=CGRectMake(60, 148, 206, 21);
        labelJerseyNumeber.frame=CGRectMake(274, 148, 230, 21);
        labelHanded.frame=CGRectMake(512,149,216, 21 );
        
        buttonAddPlayer.frame =CGRectMake(611, 100, 121, 37);
        buttonSave.frame =CGRectMake(339, 868, 75, 37);
          // NSLog(@"%@",viewAddPlayerBox.frame);
        // portrait
       // addplayerViewBox.frame=CGRectMake(0, 0,379,269);
       
        
    }
	return YES;

}
@end
