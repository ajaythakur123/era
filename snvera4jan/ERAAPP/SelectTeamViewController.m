//
//  SelectTeamViewController.m
//  ERAApplication
//
//  Created by ajay thakur on 8/9/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SelectTeamViewController.h"
//#import "RoasterLoaderViewcontroler.h"
#import "detailsDB.h"
#import "EditRoasterViewController.h"

@implementation SelectTeamViewController

@synthesize teamNameTextField,locationTextField,labelSelectlocation,labelSelectTeamLabel,buttonSave;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
      DBObj=[[detailsDB alloc]init];
    
    teamNames_Array = [[NSMutableArray alloc]initWithObjects:@"Team1",@"Team2",@"Team3",@"Team4", nil];
    
    location_Array = [[NSMutableArray alloc]initWithObjects:@"Loc1",@"Loc2",@"Loc3",@"Loc4", nil];
   // editExistTeamDataButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit existing Roster" style:UIBarButtonSystemItemDone target:self action:@selector(editExistTeamDataButtonClicked:)];	
  
    //----------------
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    teamNameTextField.text=@"";
    locationTextField.text=@"";
}


-(IBAction)HideTableview{
    
    [tableviewList removeFromSuperview];  
    
}
-(IBAction)selectTeam{
    NSLog(@"-->teamName_ButtonAction: Method Called<--");
    
    type=@"teamName";
    [tblw_alldatatable reloadData];
    [tableviewList setFrame:CGRectMake(301,298, 170, 166)];
    

        
        [self.view addSubview:tableviewList];
}
-(IBAction)addNew:(id)sender
{
   
    
}     

-(void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex 
{
    if ([type isEqualToString: @"teamName"])
    {
        if(![teamNames_Array containsObject:textAlert.text])
        {
            [teamNames_Array addObject:textAlert.text]; 
            
            [tblw_alldatatable reloadData];
        }
           
        
    } 
    
    if ([type isEqualToString: @"Location"])
    {
        if(![location_Array containsObject:textAlert.text])
        {
             [location_Array addObject:textAlert.text];  
            
             [tblw_alldatatable reloadData];
        }
         
        
    } 

    
}

-(IBAction)submit

{
    
}
-(IBAction)selectLocation
{
    type=@"Location";
    [tblw_alldatatable reloadData];
    [tableviewList setFrame:CGRectMake(301,356, 170, 166)];
    
    
    
    [self.view addSubview:tableviewList];

}
-(IBAction)roasterLoader{
    
   
    [self insertTeamNameDB];
       

    
}
#pragma mark get team Name 
-(void)getTeamDb{
    
    
 
    teamNames_Array =[DBObj selectPlayerstFromDb];
  
    
    
}
-(void)insertTeamNameDB
{
    if([teamNameTextField.text length]==0)
    {
        alert=[[UIAlertView alloc] initWithTitle:@"Fill all the Fields" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
        
        [alert show];
        
        
    }
 
    else
    {
        
//        if ([self PlayerVadiation:locationTextField.text]==NO)
//        {
//            alert=[[UIAlertView alloc] initWithTitle:@"Location is not numeric" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
//            
//            [alert show];
//            
//
//            return; 
//        }
        [teamNameTextField resignFirstResponder];
        //[locationTextField resignFirstResponder];
        
        [self getTeamDb];
        for(int i=0;i<[teamNames_Array count];i++) {
            
            if ([teamNameTextField.text isEqualToString:[[teamNames_Array objectAtIndex:i]objectForKey:@"TeamName"]])
            {
                alert=[[UIAlertView alloc] initWithTitle:@"Team Name Already Exist" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
                
                [alert show];
                return;
                
            }
        }
        
        
       mdict=[[NSMutableDictionary alloc]init];
        // NSString *Name,*Id,*address,*Mail;
        
        [mdict setObject:teamNameTextField.text forKey:@"TeamName"];  
        [mdict setObject:@"Loc" forKey:@"Location"];
        
        
        
        
        [DBObj insertIntoDb:mdict];
        
        
        
        
        
       editRoasterViewController = [[EditRoasterViewController alloc]initWithNibName:@"EditRoasterViewController" bundle:nil];
        
        editRoasterViewController.teamName=teamNameTextField.text;
        [self.navigationController pushViewController:editRoasterViewController animated:YES];    
    }
    
}
-(BOOL)PlayerVadiation :(NSString *)PlayerName
{
    unichar c;
    
    if ([PlayerName length]>0)
    {
        c = [PlayerName characterAtIndex:0];
    }
    else
    {
        return YES;
    }
    
    if ([[NSCharacterSet letterCharacterSet] characterIsMember:c])
    {
        return YES;
    }
    return NO;
    
}
-(void)editExistTeamDataButtonClicked:(id) sender
{
    
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if(interfaceOrientation != UIInterfaceOrientationPortrait &&
       interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown)
    {
        // landscape
        labelSelectTeamLabel.frame=CGRectMake(420, 161,137,21);
        teamNameTextField.frame=CGRectMake(420,190,203,31);
        labelSelectlocation.frame=CGRectMake(420,237,137,21);
        locationTextField.frame=CGRectMake(420,270, 203,31);
        buttonSave.frame =CGRectMake(420, 320, 78, 31);
        
        
        
    }
    else
    {  
        // portrait
        
        labelSelectTeamLabel.frame=CGRectMake(259, 241,137,21);
        teamNameTextField.frame=CGRectMake(259,270,203,31);
        labelSelectlocation.frame=CGRectMake(259,317,137,21);
        locationTextField.frame=CGRectMake(259,350, 203,31);
        buttonSave.frame =CGRectMake(259, 336, 78, 31);
    }
    return YES;

}


@end
