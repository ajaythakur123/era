//
//  ExistRoasterViewController.h
//  ERAApplication
//
//  Created by ajay thakur on 8/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "detailsDB.h"

@interface ExistRoasterViewController : UIViewController
{
    UITableView *PlayerTableView;
    UITableView *TeamTableView;
    
    detailsDB *DBObj;
    UITextField *textField;
    
    NSMutableArray *textArray;
    NSMutableArray *arrayPlayerdataArray;
    NSMutableArray *teamNames_Array;
    IBOutlet   UIView *addplayerView;
     IBOutlet   UIView *hidenViewTable;
    IBOutlet   UITextField *playerNameTextField;
    IBOutlet   UITextField *JerseyNumberTextField;
    IBOutlet   UITextField *handlingTextField;
    IBOutlet   UILabel *labelPlayerName;
    IBOutlet   UILabel *labelJerseyNumeber;
    IBOutlet   UILabel *labelHanded;
    IBOutlet   UIButton *buttonAddPlayer;
    IBOutlet   UIButton *buttonSave;
    IBOutlet   UIButton *buttonTeamSelect;
    IBOutlet   UIButton *buttonEdit;
    
    
    NSMutableArray *finalTextArray;
    NSMutableDictionary *mdict;
    NSArray *TextArrayCopy ;
    NSMutableArray *array1;
    NSMutableArray *  ArraySortName ;
    NSArray *sortedArray ;
    int teamID;
    NSString *teamName;
    IBOutlet UILabel  *selectTeam;
    IBOutlet UIView   *tableviewList;
    NSString *type;
    
    
    NSString * MODE;
    int replaceObject;
    //-------Ambadas-----
    
    NSString   *str_message;
    NSString   *str_batterside;
    
      NSString   *str_AddPlayercheck;
    BOOL  bl_TeamName;

    
    IBOutlet UIButton *btn_batterside;
    
    NSUInteger sourceIndex;
    NSUInteger targetIndex;
    
}
@property (nonatomic, strong) IBOutlet   UIButton *buttonAddPlayer;
@property (nonatomic, strong) IBOutlet   UIButton *buttonSave;
@property (nonatomic, strong)  UILabel *labelPlayerName;
@property (nonatomic, strong)   UILabel *labelJerseyNumeber;
@property (nonatomic, strong)   UILabel *labelHanded;

@property (nonatomic, strong) NSString *teamName;
@property(nonatomic,assign) int teamID;
@property (nonatomic, strong) IBOutlet UITableView *PlayerTableView;
@property (nonatomic, strong) IBOutlet UITableView *TeamTableView;

@property (nonatomic, strong)  UITextField *playerNameTextField;
@property (nonatomic, strong)  UITextField *JerseyNumberTextField;
@property (nonatomic, strong)  UITextField *handlingTextField;
@property (nonatomic, strong)  IBOutlet   UIView *addplayerView;

-(IBAction)Edit:(id)sender;
-(void)BackTopreviousMethod:(id)sender;
-(IBAction)AddPlayerOnRoaster;
-(IBAction)sumbmitPlayerToDabase;
-(void)getPlayerDb;
-(IBAction)CancelPlayerView;
-(IBAction)FinalSubmitPlayer;
//-(void)insertTeamNameDB;
//----Ambadas-----
-(BOOL)validateTextField:(NSString *)candidate ;
-(void)alertMessageMethod:(NSString *)message;
-(IBAction)BatterSideMethod:(id)sender;
-(BOOL)PlayerVadiation :(NSString *)PlayerName;
-(IBAction)selectTeam;
@end
