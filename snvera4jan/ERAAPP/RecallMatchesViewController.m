//
//  RecallMatchesViewController.m
//  ERAAPP
//
//  Created by Ajay Thakur on 12/13/12.
//
//

#import "RecallMatchesViewController.h"
#import "detailsDB.h"
#import "CustomCellPlayer.h"
#import "ViewController.h"

@implementation RecallMatchesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
     DBObj =[[detailsDB alloc]init];
    
   arrayMatches =[DBObj selectAllTotalInningsFromDb];
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
	return [arrayMatches count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
//    
    CustomCellPlayer *cell;
    cell=[[[NSBundle mainBundle]loadNibNamed:@"CustomCellPlayer" owner:self options:nil]objectAtIndex:0];
    
    
    cell.playerName.text =[[arrayMatches objectAtIndex:indexPath.row]objectForKey:@"Id"];
    cell.JerseyNumber.text =[[arrayMatches objectAtIndex:indexPath.row]objectForKey:@"Date"];
    cell.Handled.text=[[arrayMatches objectAtIndex:indexPath.row]objectForKey:@"TeamName"];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell ;
    
    
    
    
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    
//    [tableView beginUpdates];
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        
//        [arrayPlayerdataArray removeObjectAtIndex:indexPath.row];
//        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//        [PlayerTableView reloadData];
//        //[DBObj deletePlayerFromDatabase:[[arrayPlayerdataArray objectAtIndex:indexPath.row]objectForKey:@"TeamName"]];
//    }
//    [tableView endUpdates];
    
    
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ViewController *viewController = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    // [self presentedViewController:viewController animated:YES];
    viewController.teamName =[[arrayMatches objectAtIndex:indexPath.row]objectForKey:@"TeamName"];
    viewController.stringDate =[[arrayMatches objectAtIndex:indexPath.row]objectForKey:@"Date"];
        viewController.matchId =[[arrayMatches objectAtIndex:indexPath.row]objectForKey:@"Id"];
    
   viewController.stringClassMode=@"recallGame";
    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self.navigationController presentModalViewController:viewController animated:YES];
    //  [self.navigationController popViewControllerAnimated:<#(BOOL)#>];
    //  [self.navigationController pushViewController:viewController animated:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

@end
