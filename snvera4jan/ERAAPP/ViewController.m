//
//  ViewController.m
//  ERAPitchChart
//
//  Created by Krishna Kothapalli on 28/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "RootViewController.h"
#import "ERAAPPAppDelegate.h"
#import "detailsDB.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()
@property (nonatomic, strong) UIPopoverController *popoverController;
- (void)configureView;
@end

@implementation ViewController
@synthesize currentRunningScroll,stringClassMode;
@synthesize toolbar=_toolbar;
@synthesize detailItem=_detailItem;
@synthesize popoverController=_myPopoverController;
@synthesize detailDescriptionLabel=_detailDescriptionLabel,stringCatcher,stringPitcher,lblbatterPitch,lblOut,lblplayersBalls,lblTotalBalls,buttonCatcher,buttonPitcher,buttonSelect;

@synthesize mArray,mArray2,mArray3,mArray4,myArray_lbl,gridButtons_Array;
@synthesize view_strikeTypeOfPitch,buttonHanded,baterInfo_Array;
@synthesize view_ballTypeOfPitch,viewSetteings,imageTitleSettings,labelTitleSettings,labelbatterNameForMainpop,labelSettingsCatcher,labelSettingsPitcher,labelSettingsOpponent,stringDate,labelDate,labelCatcher,labelPitcher,labelOpponent,viewAddPlayerCP,buttonHandedBatter,matchId;

@synthesize teamName,buttonAddCP,buttonHandedCP;
int x;
int y;
BOOL con;


#pragma mark - Managing the detail item

/*
 When setting the detail item, update the view and dismiss the popover controller if it's showing.
 
 */

-(void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) 
    {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
    
    if (self.popoverController != nil) {
        [self.popoverController dismissPopoverAnimated:YES];
    }        
}

-(void)configureView
{
    // Update the user interface for the detail item.
    
    self.detailDescriptionLabel.text = [self.detailItem description];
    
}


#pragma mark - Split view support
//- (void)splitViewController:(UISplitViewController *)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController: (UIPopoverController *)pc
//{
//    barButtonItem.title = @"In Game Monitor";
//    NSMutableArray *items = [[self.toolbar items] mutableCopy];
//    [items insertObject:barButtonItem atIndex:0];
//    [self.toolbar setItems:items animated:YES];
//    [items release];
//    self.popoverController = pc;
//}
//
//// Called when the view is shown again in the split view, invalidating the button and popover controller.
//- (void)splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
//{
//    NSMutableArray *items = [[self.toolbar items] mutableCopy];
//    [items removeObjectAtIndex:0];
//    [self.toolbar setItems:items animated:YES];
//    [items release];
//    self.popoverController = nil;
//}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}


-(void) hideDisplayView:(NSNotification *)notifi
{
    
//    if(displayView.hidden==NO)
//    {
//    [UIView beginAnimations:nil context:nil];
//    [UIView setAnimationDuration:0.5];
//    [displayView setFrame:CGRectMake(107, 157, 96, 89)];
//    [UIView commitAnimations];
//    [displayView removeFromSuperview];
//    [batterInfo_View addSubview:maskView];
//       
//    }
//    
//    if(batterInfo_View.hidden==NO)
//    {
//        [UIView beginAnimations:nil context:nil];
//        [UIView setAnimationDuration:0.5];
//        [batterInfo_View setFrame:CGRectMake(107, 157, 96, 89)];
//        [UIView commitAnimations];
//        [batterInfo_View removeFromSuperview];
//    }
//    
//    [scrollView setAlpha:1];
//    [scrollView setScrollEnabled:YES];
//    [scrollView setUserInteractionEnabled:YES];
//    [batter_scrollView setAlpha:1];
//    [batter_scrollView setScrollEnabled:YES];
//    [batter_scrollView setUserInteractionEnabled:YES];
//    [mask_scrollView setHidden:YES];
//    
//    [btn_loadSenderidbtn setBackgroundColor:[UIColor clearColor]];
//    [btn_loadstrikesenderid setBackgroundColor:[UIColor clearColor]];
//    [view_ballTypeOfPitch setHidden:YES];
//    [view_ballPopUpSubMenu setHidden:YES];
//    [view_defensivePositionsView setHidden:YES];
//    [view_hitFairView setHidden:YES];
//    [view_locationOfHitView setHidden:YES];
//    [view_strikeOutView setHidden:YES];
//    [view_strikeTypeOfPitch setHidden:YES];
//    [view_StrikeView setHidden:YES];
//    
//    NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
//    [dict setObject:btn1.currentTitle forKey:@"0"];
//    [dict setObject:btn2.currentTitle forKey:@"1"];
//    [dict setObject:btn3.currentTitle forKey:@"2"];
//    [dict setObject:btn4.currentTitle forKey:@"3"];
//    [dict setObject:btn5.currentTitle forKey:@"4"];
//    [dict setObject:btn6.currentTitle forKey:@"5"];
//    [dict setObject:btn7.currentTitle forKey:@"6"];
//    [dict setObject:btn8.currentTitle forKey:@"7"];
//    [dict setObject:btn9.currentTitle forKey:@"8"];
//    [dict setObject:btn10.currentTitle forKey:@"9"];
//    [dict setObject:btn12.currentTitle forKey:@"10"];
//    [dict setObject:btn12.currentTitle forKey:@"11"];
//    [dict setObject:btn13.currentTitle forKey:@"12"];
//    [dict setObject:btn14.currentTitle forKey:@"13"];
//    NSLog(@"Current Dictionary : %@",dict);
//    NSLog(@"Button SubClassesArray : %@",pitch_Chart_Btn.subviews);
//    UIButton *button = [mArray objectAtIndex:btnDisplayViewTag];
//    for (UIView *view in button.subviews) {
//        if ([view isMemberOfClass:[UILabel class]]) {
//            UILabel *templbl=(UILabel *)view;
//            switch (templbl.tag) {
//
//                case 0:
//                    [templbl setText:[dict objectForKey:@"0"]];
//                    break;
//                case 1:
//                    [templbl setText:[dict objectForKey:@"1"]];
//                    break;
//                case 2:
//                    [templbl setText:[dict objectForKey:@"2"]];
//                    break;
//                case 3:
//                    [templbl setText:[dict objectForKey:@"3"]];
//                    break;
//                case 4:
//                    [templbl setText:[dict objectForKey:@"4"]];
//                    break;
//                case 5:
//                    [templbl setText:[dict objectForKey:@"5"]];
//                    break;
//                case 6:
//                    [templbl setText:[dict objectForKey:@"6"]];
//                    break;
//                case 7:
//                    [templbl setText:[dict objectForKey:@"7"]];
//                    break;
//                case 8:
//                    [templbl setText:[dict objectForKey:@"8"]];
//                    break;
//                case 9:
//                    [templbl setText:[dict objectForKey:@"9"]];
//                    break;
//                case 10:
//                    [templbl setText:[dict objectForKey:@"10"]];
//                    break;
//                case 11:
//                    [templbl setText:[dict objectForKey:@"11"]];
//                    break;
//                case 12:
//                    [templbl setText:[dict objectForKey:@"12"]];
//                    break;
//                case 13:
//                    [templbl setText:[dict objectForKey:@"13"]];
//                    break;
//
//                default:
//                    break;
//
//            }
//        }
//    }
//    
//    [displayView_detailsArray replaceObjectAtIndex:btnDisplayViewTag withObject:dict];
//    [dict release];
//    
//    NSLog(@"displayViewArray : %@",displayView_detailsArray);
}

#pragma mark - View lifecycl

- (void)viewDidLoad
{
    viewMonitor.hidden=YES;
      strOutLastRecord =@"0";
    intOutLastRunner=0;
    buttonCatcher.enabled=YES;
    buttonMonitor.enabled=YES;
    buttonPitcher.enabled=YES;
    scrollViewIdentification=YES;
    self.navigationItem.hidesBackButton=YES;
    self.navigationItem.backBarButtonItem=nil;
    
    //PlayerTableView.frame =CGRectMake(0, 0, 500, 600);
    
    navi_back = [[UIBarButtonItem alloc]
                 initWithTitle:@"In Game Monitor"
                 style:UIBarButtonItemStyleBordered
                 target:self
                 action:@selector(Statistic:)];
    self.navigationItem.leftBarButtonItem = navi_back;
    
    btnBack = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	btnBack.frame = CGRectMake(495,0,50, 34);
	btnBack.backgroundColor = [UIColor redColor];
    btnBack.titleLabel.text=@"Back";
    
	//[btn1 setBackgroundImage:[UIImage imageNamed:@"Newcal.png"] forState:UIControlStateNormal];
	[btnBack addTarget:self action:@selector(Settings:) forControlEvents:UIControlEventTouchUpInside];
	btnBack.tag=1;
    
    btnSelect = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	btnSelect.frame = CGRectMake(550,0, 50, 34);
	btnSelect.backgroundColor = [UIColor whiteColor];
    btnSelect.titleLabel.text=@"Back";
    
	//[btn1 setBackgroundImage:[UIImage imageNamed:@"Newcal.png"] forState:UIControlStateNormal];
	[btnSelect addTarget:self action:@selector(month:) forControlEvents:UIControlEventTouchUpInside];
	btnSelect.tag=1;
    
    viewNav  =[[UIView alloc]initWithFrame:CGRectMake(0,4, 600,34)];
    viewNav.backgroundColor=[UIColor blueColor];
    
    [viewNav addSubview:btnBack];
    [viewNav addSubview:btnSelect];
    
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:viewNav];
    [barButtonItem setStyle:UIBarButtonItemStyleBordered];
    
    self.navigationItem.rightBarButtonItem = barButtonItem;
    

    
    
    lblOut.text=@"0";
    lblTotalBalls.text=@"0";
    lblplayersBalls.text=@"0";
    
    [table_displayOptions setTag:10];
    teamNames_Array=[[NSMutableArray alloc]init];
   
    dicAllInnings =[[NSMutableDictionary alloc]init];
    array_pitcher=[[NSMutableArray alloc]init];
    array_catcher=[[NSMutableArray alloc]init];
    arrayOppentplayer=[[NSMutableArray alloc]init];
    arrayOppentplayer1=[[NSMutableArray alloc]init];
    dicInnings =[[NSMutableDictionary alloc]init];
     batter_detailsArray=[[NSMutableArray alloc]init];
    dicOutRecord =[[NSMutableDictionary alloc]init];
    dicStrikePitch =[[NSMutableDictionary alloc]init];
     dicAtBatRecord =[[NSMutableDictionary alloc]init];
     displayView_detailsArray=[[NSMutableArray alloc]init];
    dicDisplayMatches =[[NSMutableDictionary alloc]init];
    dicMatchData =[[NSMutableDictionary alloc]init];
      dicLastSetLabelRecords =[[NSMutableDictionary alloc]init];
        dicSetvaluesDisplay =[[NSMutableDictionary alloc]init];
    
 dicRunOut= [[NSMutableDictionary alloc]init];
    arrrayMatches=[[NSMutableArray alloc]init];
      arrayInningTag=[[NSMutableArray alloc]init];
      arrayTotalInnings=[[NSMutableArray alloc]init];
    mArray=[[NSMutableArray alloc]init];
    mArray2=[[NSMutableArray alloc]init];
    mArray3=[[NSMutableArray alloc]init];
    mArray4=[[NSMutableArray alloc]init];
    myArray_lbl=[[NSMutableArray alloc]init];
    arrayPitches=[[NSMutableArray alloc]init];
    //l/p
    mArrayLandscape=[[NSMutableArray alloc]init];
    mArrayLandscape2=[[NSMutableArray alloc]init];
    mArrayLandscape3=[[NSMutableArray alloc]init];
    mArrayLandscape4=[[NSMutableArray alloc]init];
    
    
    
    nextBatter =0;
      Array_defensivePositionsArray=[[NSArray alloc]initWithObjects:@"1 - Pitcher",@"2 - Catcher",@"3 - 1st Baseman",@"4 - 2nd Baseman",@"5 - 3rd Baseman",@"6 - Shortstop",@"7 - Left Field ",@"8 - Center Field",@"9 - Right Field", nil];

 
    
    labelSettingsPitcher.text= stringPitcher;
    labelSettingsCatcher.text =stringCatcher;
    labelSettingsOpponent.text=teamName;
     
    labelOpponent.text=teamName;
    labelPitcher.text=stringPitcher;
    labelCatcher.text=stringCatcher;
    labelDate.text=stringDate;
   

    DBObj =[[detailsDB alloc]init];
    
    
              selfViewFrame=self.view.frame;
          
    scrollView = [[TPKeyboardAvoidingScrollView alloc]initWithFrame:CGRectMake(0, 20, 768, 1004)];
           [scrollView setBackgroundColor:[UIColor whiteColor]];
           [scrollView setTag:100];
           imageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 25, 1100, 1004)];
           [imageView setImage:[UIImage imageNamed:@"pitch_Chart_1body.png"]];
           [scrollView setBounces:NO];
     scrollView.delegate=self;
    
    scrollViewLandscape = [[UIScrollView alloc]initWithFrame:CGRectMake(0,0, 700, 730)];
    [scrollViewLandscape setBackgroundColor:[UIColor whiteColor]];
    [scrollViewLandscape setTag:1];
    [scrollViewLandscape setBounces:NO];
    scrollViewLandscape.delegate=self;
    
    //scrollNumberLine =[[UIScrollView alloc]initWithFrame:CGRectMake(2, 56, 130, 1004)];
     [scrollNumberLine setBounces:NO];

    scrollPitchLabels.contentSize=CGSizeMake(2220, 104);
   // scrollPitchLabels.pagingEnabled=YES;
    
    scrollMonitor.contentSize=CGSizeMake(1024, 1200);
   numberLine = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"scaleImage.png"]];
 numberLineLand = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"scaleLandscape.jpg"]];
//

    [scrollNumberLine addSubview:numberLine];
    [scrollNumberLine addSubview:numberLineLand];
    //[numberLine release];
    
    batter_scrollViewLandscape=[[UIScrollView alloc]initWithFrame:CGRectMake(2, 56, 130, 1004)];
   // batter_scrollViewLandscape=[[UIScrollView alloc]initWithFrame:CGRectMake(2, 56, 130, 1004)];
    [batter_scrollViewLandscape setBackgroundColor:[UIColor whiteColor]];
        [batter_scrollViewLandscape setBounces:NO];
      batter_scrollViewLandscape.delegate=self;
          // [self.view addSubview:scrollView];
    
           mask_scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 20, 310, 1004)];
           [mask_scrollView setBackgroundColor:[UIColor clearColor]];
           [scrollView setBounces:NO];
           [mask_scrollView setAlpha:0];
           [self.view addSubview:mask_scrollView];
   
    scrollLocation=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 580, 180, 265)];
    [scrollLocation setBackgroundColor:[UIColor clearColor]];
    [scrollLocation setBounces:NO];
   // [scrollLocation setAlpha:0];
     [scrollLocation setContentSize:CGSizeMake(213,365)];
   
    
//    scrollViewStatistics=[[UIScrollView alloc]initWithFrame:CGRectMake(0, 0,320, 730)];
//    [scrollViewStatistics setContentSize:CGSizeMake(320, 1500)];
//    [scrollViewLandscape setBackgroundColor:[UIColor blueColor]];
   // [scrollViewStatistics addSubview:viewStatistics];
   // [scrollViewStatistics setBackgroundColor:[UIColor blackColor]];
    
    
    
    
    
    
   //// [self.view addSubview:viewStatistics];
    
           batter_scrollView=[[UIScrollView alloc]initWithFrame:CGRectMake(2, 56, 130, 1004)];
           [batter_scrollView setBackgroundColor:[UIColor whiteColor]];
           batter_ImageView=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 130, 1004)];
           [batter_ImageView setImage:[UIImage imageNamed:@"pitch_Chart_1strip.png"]];
           [batter_scrollView setBounces:NO];
    [batter_scrollView setTag:200];
    batter_scrollView.delegate=self;
           //[self.view addSubview:batter_scrollView];

   

           //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideDisplayView:) name:@"removeDisplayView" object:nil];
    
                   batterNoArray=[[NSMutableArray alloc]init];
           batterNameArray=[[NSMutableArray alloc]init];
    
    

   
    
    
    UILabel *labelOppent=[[UILabel alloc]initWithFrame:CGRectMake(300,8,140,28)];
    [labelOppent setFont:[UIFont boldSystemFontOfSize:16]];
    [labelOppent setBackgroundColor:[UIColor clearColor]];
       labelOppent.textColor =[UIColor whiteColor];
    [labelOppent setTextAlignment:UITextAlignmentCenter];
    labelOppent.text=teamName;
   // [_toolbar addSubview:labelOppent];
    
    UILabel *labeldate=[[UILabel alloc]initWithFrame:CGRectMake(140,8,120,28)];
    labeldate.text=stringDate;
    labeldate.textColor =[UIColor whiteColor];
    [labeldate setBackgroundColor:[UIColor clearColor]];

 
    baterInfo_Array=[[NSMutableArray alloc]init];
    for (int i=0; i<9; i++) {
        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
        [baterInfo_Array addObject:dict];
    }
  
    
    
    [super viewDidLoad];

}

- (void)viewWillAppear:(BOOL)animated
{
    
    boolTwoATBatRecord=YES;
    
    NSLog(@"TeamName : %@",teamName);
    //batter_detailsArray=[DBObj selectTeamPlayerstFromDb:teamName];
    
    NSLog(@"batter Details : %@",batter_detailsArray);
    
    
    
    int k;
    if([batter_detailsArray count]>9)
    {
        k=9;
    }
    else
    {
        k=[batter_detailsArray count];
    }
    for (int  j=0; j<k; j++) {
        
        
        [baterInfo_Array replaceObjectAtIndex:j withObject:[batter_detailsArray objectAtIndex:j]];
    }
    
    //    if (k==0) {
    //        [batter_detailsArray removeAllObjects];
    //
    //        for (int i=0; i<9; i++) {
    //            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
    //            [baterInfo_Array addObject:dict];
    //            [dict release];
    //        }
    //    }
    
    [super viewWillAppear:animated];
}


- (void)viewDidAppear:(BOOL)animated
{
    [array_catcher removeAllObjects];
    array_catcher =[DBObj selectHomeTeamPlayerstFromDb:@"C"];
    if ([array_catcher count]>0) {
         labelCatcher.text=[[array_catcher objectAtIndex:0]objectForKey:@"PlayerName"];
    }
   
    
    [array_pitcher removeAllObjects];
    array_pitcher =[DBObj selectHomeTeamPlayerstFromDb:@"P"];
    if ([array_pitcher count]>0)
    {
           labelPitcher.text=[[array_pitcher objectAtIndex:0]objectForKey:@"PlayerName"];;
    }
 
    
    buttonCatcher.enabled=YES;
    buttonMonitor.enabled=YES;
    buttonPitcher.enabled=YES;
    
    error=NO;
    
    intEndOfInning=0;
    intRecordBalls=0;
    intRecordStrike=0;
    
    intOutPlyers =0;
    intTotalBalls=0;
    
    intNumberOfBalls1=0;
    intPlayerForRows =0;
    
    type =@"others";
    
    once=1;
    intnextbatter=1;
    
    NSLog(@"%s",__func__);
    
    scrollView.contentSize=CGSizeMake(2228, 1471);
    scrollView.frame = CGRectMake(155, 0, 810, 1004);
    //scrollView.pagingEnabled=YES;
    
    //    scrollNumberLine.frame = CGRectMake(116, 0, 810, 30);
    //    [self.view addSubview:scrollNumberLine];
    scrollView.delegate =self;
    
    [batter_ImageView setFrame:CGRectMake(0, 0, 157, 1280)];
    
    
    
    [batter_scrollView setContentSize:CGSizeMake(155, 1471)];
    [batter_scrollView setFrame:CGRectMake(0, 0, 155, 1004)];
    //l
    
    
    
    
    
    
    [self drawButtonsInPotraitMode];
    
    
    
    [self drawButtonsForBatterInPotraitMode];
    
    [batter_ImageView setFrame:CGRectMake(0, 0, 157, 1230)];
    
    [batter_scrollViewLandscape setContentSize:CGSizeMake(158,1271)];
    
    [batter_scrollViewLandscape setFrame:CGRectMake(0,2, 158, 730)];
    
    
    imageView.frame=CGRectMake(0, 25, 1350, 1285);
    
    scrollViewLandscape.contentSize=CGSizeMake(2194, 1271);
    
    scrollViewLandscape.frame = CGRectMake(158,2, 900, 730);
    
    
    
    [scrollViewLandscape setBackgroundColor:[UIColor whiteColor]];
    
    
    
    
    
    [self drawButtonsInLandScapeMode];
    
    [self drawButtonsForBatterInLandScapeMode];
    
  //  [viewLandscape addSubview:scrollViewStatistics];
    [self loadButtonOnPitchlabel];
    labelLoading.hidden=YES;
     [self checkDisplayMatches];
    
     viewMonitor.hidden=NO;
    [super viewDidAppear:animated];
    
    
}
#pragma mark///////////////////////// pitches ...98.................................labels

-(void)loadButtonOnPitchlabel
{
    x=0;
    y=22;
    
    imgv_eoi = [[UIImageView alloc]
                initWithImage:[UIImage imageNamed:@"Pitchlabelnumber.jpg"]];
    [imgv_eoi setFrame:CGRectMake(0,0, 2111, 22)];

    [scrollPitchLabels addSubview:imgv_eoi];
    for (int i=0;i<99; i++) {
        
        pitch_Chart_Btn=[UIButton buttonWithType:UIButtonTypeCustom];
        
        if (i>1)
            if (i%49==0) {
                x=0;
                y+=40;
                
            }
        
        [pitch_Chart_Btn setFrame:CGRectMake(x,y ,45, 41)];
       // [pitch_Chart_Btn setBackgroundColor:[UIColor blueColor]];
        [pitch_Chart_Btn setTag:i];
        //[pitch_Chart_Btn setFont:[UIFont fontWithName:@"System-Bold" size:23]];
       pitch_Chart_Btn.titleLabel.font = [UIFont systemFontOfSize:20];
        [pitch_Chart_Btn setBackgroundImage:[UIImage imageNamed:@"Pitchlabelbox.jpg"] forState:UIControlStateNormal];
        pitch_Chart_Btn.showsTouchWhenHighlighted=YES;
        [pitch_Chart_Btn addTarget:self action:@selector(btnLabel:) forControlEvents:UIControlEventTouchUpInside];
        [arrayPitches addObject:pitch_Chart_Btn];
//        [mArrayLandscape2 addObject:[NSValue valueWithCGRect:pitch_Chart_Btn.frame]];
        
 
        
        
        [scrollPitchLabels addSubview:pitch_Chart_Btn];
        x+=45;
    }
      //[scrollPitchLabels addSubview:imgv_eoi];
}

#pragma mark ////////////////////////////Action
-(void)btnLabel:(UIButton *)sender
{
    
     UIButton *buttonClear=[arrayPitches objectAtIndex:currentPitchTag];
        
        buttonClear.layer.cornerRadius=0.0f;
        buttonClear.layer.masksToBounds=YES;
        buttonClear.layer.borderColor=[[UIColor clearColor]CGColor];
        buttonClear.layer.borderWidth= 0.0f;
//
    
    currentPitchTag=sender.tag;
    
    NSLog(@"%d",sender.tag);
    UIButton *button=[arrayPitches objectAtIndex:sender.tag];
    
    button.layer.cornerRadius=0.0f;
    button.layer.masksToBounds=YES;
    button.layer.borderColor=[[UIColor blueColor]CGColor];
    button.layer.borderWidth= 4.0f;
 
    
    if(sender.tag<49)
    {
        [self BallpopupMethod:sender];
    }
    if(sender.tag>48)
    {
        [self StrikepopupMethod:sender];
    }
}

-(void) checkDisplayMatches

{
     // NSLog(@"%@",arrrayMatches);
    
    if(stringClassMode==@"recallGame")
    {
      dicDisplayMatches =  [DBObj selectTotalInningsFromDb:labelDate.text :labelOpponent.text];

        
      NSData *data =  [dicDisplayMatches objectForKey:@"TotalInnings"];
     arrrayMatches = (NSMutableArray *)[[NSKeyedUnarchiver unarchiveObjectWithData:data] mutableCopy];
       // [arrrayMatches retain];
     
         dicAllInnings =[arrrayMatches objectAtIndex:0];
         dicInnings = [arrrayMatches objectAtIndex:1];
         dicOutRecord=[arrrayMatches objectAtIndex:2];
         dicAtBatRecord =[arrrayMatches objectAtIndex:3];
         baterInfo_Array =[arrrayMatches objectAtIndex:4];
        dicLastSetLabelRecords =[arrrayMatches objectAtIndex:5];
        
        NSMutableDictionary *dicBatter =[[NSMutableDictionary alloc]init];
        for (int i=0; i<[baterInfo_Array count]; i++) {
            
            dicBatter =[[baterInfo_Array objectAtIndex:i]mutableCopy];
            if([dicBatter objectForKey:@"JerseyNumber"])
            {
                [batter_detailsArray addObject:[baterInfo_Array objectAtIndex:i]];
            }
            
        }
        
     

        [ self displayAllBatter];
        [self displayPitchAll];
        [self montiorDisplayRecord];
            
          NSLog(@"=======================batter_detailsArray%@",batter_detailsArray);

        NSLog(@"%@",arrrayMatches);
        
        NSLog(@"111111%@",dicAllInnings);
        NSLog(@"222222%@",dicInnings);
        NSLog(@"333333%@",dicOutRecord);
        NSLog(@"44444444.......................%@",baterInfo_Array);
        NSLog(@"555555555%@",dicAtBatRecord);
        
    }
    
    
    
}


#pragma mark save alll  data dictionary////////
-(IBAction)Back
{
    
    
 
    
   // [self displayPitchAll];
    
    NSLog(@"%@",dicAllInnings);
     NSLog(@"%@",dicInnings);
     NSLog(@"%@",dicOutRecord);
    NSLog(@"%@",baterInfo_Array);
    NSLog(@"%@",dicAtBatRecord);
    
    if([arrayTotalInnings count]>0)
    {
        [arrayTotalInnings removeAllObjects];
    }
    [arrayTotalInnings addObject:dicAllInnings];
    [arrayTotalInnings addObject:dicInnings];
    [arrayTotalInnings addObject:dicOutRecord];
    [arrayTotalInnings addObject:dicAtBatRecord];
    [arrayTotalInnings addObject:baterInfo_Array];
    [arrayTotalInnings addObject:dicLastSetLabelRecords];
    
    NSLog(@"%@",arrayTotalInnings);
    NSData *TotalInnings =[NSKeyedArchiver archivedDataWithRootObject:arrayTotalInnings];
    
    NSMutableArray *a = (NSMutableArray *)[NSKeyedUnarchiver unarchiveObjectWithData:TotalInnings];
       NSLog(@"%@",a);
    [array_catcher addObject:dicAllInnings];
    
    [dicMatchData setObject:labelDate.text forKey:@"Date"];
    [dicMatchData setObject:labelOpponent.text forKey:@"TeamName"];
    [dicMatchData setObject:TotalInnings forKey:@"TotalInnings"];

    
    if(stringClassMode==@"recallGame")
    {
        [DBObj deleteMatchDB:matchId];
        
    }
   
  [DBObj insertMatchSummary:dicMatchData];

  

//    
    [popoverController dismissPopoverAnimated:YES];
    
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark ===================== Display record

-(void)displayAllBatter
{
    for (int j=0; j<9;j++) {
        
        
        
        
        UIButton *button = [mArray3 objectAtIndex:j];
        for (UIView *view in button.subviews) {
            if ([view isMemberOfClass:[UILabel class]]) {
                UILabel *templbl=(UILabel *)view;
                switch (templbl.tag) {
                        
                    case 0:
                        [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"PlayerName"]];
                        break;
                    case 1:
                        [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"JerseyNumber"]];
                        break;
                    case 2:
                        [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"Handling"]];
                        break;
                        
                    default:
                        break;
                        
                }
                NSLog(@"Temp Label Tag : %d",templbl.tag);
            }
        }
        
        //Landscape
        
        UIButton *buttonLandscape = [mArrayLandscape3 objectAtIndex:j];
        for (UIView *view in buttonLandscape.subviews) {
            if ([view isMemberOfClass:[UILabel class]]) {
                UILabel *templbl=(UILabel *)view;
                switch (templbl.tag) {
                        
                    case 0:
                        [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"PlayerName"]];
                        break;
                    case 1:
                        [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"JerseyNumber"]];
                        break;
                    case 2:
                        [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"Handling"]];
                        break;
                        
                    default:
                        break;
                        
                }
                NSLog(@"Temp Label Tag : %d",templbl.tag);
            }
            
            
            
        }//end
    }
}
#pragma mark ===================== Display record
-(void)displayPitchAll
{
    NSLog(@"111111%@",dicAllInnings);
    NSLog(@"222222%@",dicInnings);
    NSLog(@"333333%@",dicOutRecord);
    NSLog(@"44444444.......................%@",baterInfo_Array);
    NSLog(@"555555555%@",dicAtBatRecord);
   // [self displayLastset];
     
    for (int i =0; i<90;i++) {
        
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",i];
        
        if([dicAllInnings objectForKey:string])
        {
            dicMainPitch =[dicAllInnings objectForKey:string];
            
            NSLog(@"Button SubClassesArray : %@",pitch_Chart_Btn.subviews);
            
            UIButton *button = [mArray objectAtIndex:i];
            
            for (UIView *view in button.subviews) {
                
                if ([view isMemberOfClass:[UILabel class]]) {
                    
                    UILabel *templbl=(UILabel *)view;
                    
                    NSLog(@"taggggggggggggggggggggggggggggggggggg%d",templbl.tag);
                    //for()
                    
                    stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d%d",i,templbl.tag];
                    //stringKeyPitch =[NSMutableString stringWithFormat:@"%d",templbl.tag];
                   // [stringKeyMainPitch appendString:stringKeyPitch];
                    
                    NSLog(@"%@",stringKeyMainPitch);
                   // [templbl setText:[[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"pitchLabel"]];
                    
                    
                    
                    NSLog(@"%@",templbl.text);
                    
                    
                    NSMutableString * string1=[NSMutableString stringWithFormat:@"PlayerBox%d",i];
                    
                  dicSetvaluesDisplay = [dicLastSetLabelRecords objectForKey:string1];
                    
                    NSString *key =[NSString stringWithFormat:@"k%d",templbl.tag
                                    ];
                    
                    [templbl setText:[dicSetvaluesDisplay objectForKey:key] ];

                    
                    if(templbl.tag==14)
                    {
                        for (int k=0; k<100; k++) {
                            
                            
                              stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d%d",i,k];
                            
                            NSLog(@"%@",stringKeyMainPitch);
                            
                            

                        if([[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"lblbatterPitch"])
                            
                        {
                             [templbl setText:[[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"lblbatterPitch"]];
                            NSLog(@"labelbatererrrrrrrrrrrrrrrrrrrrrrrrr%@",templbl.text);
                        }
                        }
                    }
                    if(templbl.tag==15)
                    {
                        
                        if(lblOut)
                            
                        {
                          //  NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
                            [dicOutRecord objectForKey:string];
                          //  [self displayOut :i];
                            templbl.text= [dicOutRecord objectForKey:string];
                            NSLog(@"%@",templbl.text);
                        }
                    }
                    if(templbl.tag==16)
                    {
                        if(lblplayersBalls)
                            
                        {
                            NSMutableString * stringPb=[NSMutableString stringWithFormat:@"PlayerBoxPb%d",i];
                            [dicOutRecord objectForKey:stringPb];
                            //  [self displayOut :i];
                            templbl.text= [dicOutRecord objectForKey:stringPb];
                            NSLog(@"%@",templbl.text);
                        }
                        
                    }
                    if(templbl.tag==17)
                    {
                        if(lblTotalBalls)
                            
                        {
                            
                            NSMutableString * stringTb=[NSMutableString stringWithFormat:@"PlayerBoxTb%d",i];
                            [dicOutRecord objectForKey:stringTb];
                            //  [self displayOut :i];
                            templbl.text= [dicOutRecord objectForKey:stringTb];
                            
                            intTotalBalls = [templbl.text intValue];
                            NSLog(@"%@",templbl.text);
                        }
                        
                    }

                    
                    
                }
                
                
                if([view isMemberOfClass:[UIImageView class]])
                {
                    
                    UIImageView *image =(UIImageView *)view;
                    
                    if(image.tag==i+100)
                    {
                        
                        [image setImage:[UIImage imageNamed:@""]];
                        for (int l=0; l<100;l++) {
                            
                            
//                            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
//                            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
//                            [stringKeyMainPitch appendString:stringKeyPitch];
                            
                            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d%d",i,l];
                            
                            NSLog(@"image battterereeerrrrrrrrrrrrrrrtrrrrrrrr%@",stringKeyMainPitch);
                            
                            
                            
                            ///image batter
                            if([[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"])
                            {
                                imageView_locatiogGraph.hidden=NO;
                                NSString *imageLocation  =[[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"];
                                NSLog(@"................image.....%@",imageLocation);
                                
                                [image setImage:[UIImage imageNamed:imageLocation]];
                                
                                
                            }
                        }
                        
                        
                    }///image for short
                    
                    if(image.tag==i+200)
                    {
                        
                        NSMutableString *stringInn =[NSMutableString stringWithFormat:@"PlayerBox%d",i];
                        
                        
                        NSLog(@"..............dicInnings........%@",dicInnings);
                        if([dicInnings objectForKey:stringInn])
                        {
                            
                            
                            [image setImage:[UIImage imageNamed:@"Eoi_shape.png"]];
                            
                            
                            
                        }
                        
                    }////image for innning
                    
                    
                }

            }
            
            UIButton *buttonLandscape = [mArrayLandscape objectAtIndex:i];
            
            for (UIView *view in buttonLandscape.subviews) {
                
                if ([view isMemberOfClass:[UILabel class]]) {
                    
                    UILabel *templbl=(UILabel *)view;
                    
                    
                    
                    stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",i];
                    stringKeyPitch =[NSMutableString stringWithFormat:@"%d",templbl.tag];
                    [stringKeyMainPitch appendString:stringKeyPitch];
                    
                    NSLog(@"%@",stringKeyMainPitch);
                    [templbl setText:[[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"pitchLabel"]];
                    
                    
                    
                    NSLog(@"%@",templbl.text);
                    
                }
            }
            
            
        }
        
    }
    //[self montiorDisplayRecord];
}


#pragma mark ------------------------------------------delegate popover---------------------

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController
{
    return YES;
}

/* Called on the delegate when the user has taken action to dismiss the popover. This is not called when -dismissPopoverAnimated: is called directly.
 */
- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    
    scrollViewIdentification = YES;
}



#pragma mark -----------action InMonitor
-(IBAction)InMonitor:(id)sender
{
    

    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.4];
    
    
    if(mode==YES)//p
    {
        [scrollNumberLine setFrame:CGRectMake(153,44,635, 21)];
        //[numberLine setImage:[UIImage imageNamed:@"scaleImage.png"]];
        scrollNumberLine.contentSize=CGSizeMake(2228,25);
        numberLineLand.hidden=YES;
        numberLine.hidden=NO;
    if(viewPotrait.frame.origin.x==687)
    {
        viewPotrait.frame = CGRectMake(0,65, 1024, 941);
        viewUpperMenu.frame =CGRectMake(0,0, 768, 66);
        
    }
    else
    {
         viewUpperMenu.frame =CGRectMake(687,0, 768, 66);
        viewPotrait.frame = CGRectMake(687,65, 1024, 941);
    }
    
    }
    else//l
    {
        
        
        
        if(viewLandscape.frame.origin.x==887)
        {
            viewLandscape.frame = CGRectMake(0,65, 1024, 917);
            viewUpperMenu.frame =CGRectMake(0,0, 968, 66);
            
        }
        else
        {
            viewUpperMenu.frame =CGRectMake(887,0, 968, 66);
            viewLandscape.frame = CGRectMake(887,65, 1024, 917);
        }

        
    }
    
    
    
    
    [UIView commitAnimations];
    
    if(viewPotrait.frame.origin.x==687||viewLandscape.frame.origin.x==887)
    {
     
           [self montiorDisplayRecord];
            
        }
    
 
}

-(void)Settings:(id)sender
{
    
    UIViewController* popoverViewController = [[UIViewController alloc]init];
    [popoverViewController.view addSubview:viewSetteings];
    popoverViewController.view.backgroundColor = [UIColor blackColor];
	popoverViewController.contentSizeForViewInPopover =	CGSizeMake(267, 407);
    popoverController = [[UIPopoverController alloc]initWithContentViewController:popoverViewController];
    
    popoverController.delegate = self;
    [popoverController setPopoverContentSize:CGSizeMake(267, 407)];
    
    ///   [popoverController presentPopoverFromBarButtonItem:navi_back permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    [popoverController presentPopoverFromRect:btnBack.frame inView:btnBack.superview permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    //[popoverController presentPopoverFromRect:settingsButton inView:view_ballTypeOfPitch permittedArrowDirections:UIPopoverArrowDirectionUp | UIPopoverArrowDirectionDown animated:YES];
}

-(void)BAckBUttonMethod:(id)sender
{
    
    potrait=4;
    //    ERAAPPAppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    //    UIWindow *window =  [[UIApplication sharedApplication] keyWindow];
    //    window.rootViewController = appDelegate.tempnav;
    
    
    NSArray* viewControllersInStack = self.navigationController.viewControllers;
    UIViewController* targetViewController = [viewControllersInStack objectAtIndex:1];
    [self.navigationController popToViewController:targetViewController animated:YES];
    
}

 
#pragma mark drawButtonsInPotraitMode - pitch
-(void) drawButtonsInPotraitMode
{
    x=0;
    y=0;
    
    for (int i=0;i<90; i++)
    {
        if (i>1)
            if (i%10==0) {
                x=0;
                y+=156;
                
            }
        
        pitch_Chart_Btn=[UIButton buttonWithType:UIButtonTypeCustom];
        [pitch_Chart_Btn setBackgroundColor:[UIColor clearColor]];
        [pitch_Chart_Btn setFrame:CGRectMake(x,y ,204, 158)];
        [pitch_Chart_Btn setBackgroundImage:[UIImage imageNamed:@"graph212.png"] forState:UIControlStateNormal];
        pitch_Chart_Btn.showsTouchWhenHighlighted=YES;
        [pitch_Chart_Btn setTag:i];
        [pitch_Chart_Btn addTarget:self action:@selector(btn:) forControlEvents:UIControlEventTouchUpInside];
        [mArray addObject:pitch_Chart_Btn];
        [mArray2 addObject:[NSValue valueWithCGRect:pitch_Chart_Btn.frame]];
        
        //bi
        imgv_eoi = [[UIImageView alloc]
                    initWithImage:[UIImage imageNamed:@""]];
        [imgv_eoi setFrame:CGRectMake(3,88, 70,70)];
        [imgv_eoi setTag:i+100];
        [pitch_Chart_Btn addSubview:imgv_eoi];
        
        imgEndOfInning = [[UIImageView alloc]
                    initWithImage:[UIImage imageNamed:@""]];
        [imgEndOfInning setFrame:CGRectMake(108, 116, 50,39)];
        [imgEndOfInning setTag:i+200];
        [pitch_Chart_Btn addSubview:imgEndOfInning];
        
        
        
        int a=4;
        int b=4;
        
        for (int j=0; j<18; j++)
        {
            
            if(j<7)
            {
                grid_lbl=[[UILabel alloc]initWithFrame:CGRectMake(a,b,25,24)];
                 [grid_lbl setBackgroundColor:[UIColor clearColor]];
           // [grid_lbl setFont:[UIFont fontWithName:@"System-Bold" size:23]];
                [grid_lbl setTextAlignment:UITextAlignmentCenter];
            }
            
            if (j>6&&j<14) {
              
                if(j==7)
                {
                a=4;
                b=34;
                }
                grid_lbl=[[UILabel alloc]initWithFrame:CGRectMake(a,b,25,24)];
                 [grid_lbl setBackgroundColor:[UIColor clearColor]];
            }
            if(j==14)//Batter
            {
                a=90;
                b=68;
            grid_lbl=[[UILabel alloc]initWithFrame:CGRectMake(a,b,35,35)];
                 [grid_lbl setBackgroundColor:[UIColor clearColor]];
                // [grid_lbl setFont:[UIFont fontWithName:@"System-Bold" size:20]];
                 [grid_lbl setTextAlignment:UITextAlignmentCenter];
            }
            
            if(j==15)///Outs
            {
                a=145;
                b=68;
                grid_lbl=[[UILabel alloc]initWithFrame:CGRectMake(a,b,22,22)];
                 [grid_lbl setBackgroundColor:[UIColor clearColor]];
            }
            if(j==16)//Ball/plyr
            {
                a=160;
                b=94;
                grid_lbl=[[UILabel alloc]initWithFrame:CGRectMake(a,b,34,18)];
                 [grid_lbl setBackgroundColor:[UIColor clearColor]];
            }
            if(j==17)//totalballs
            {
                a=162;
                b=115;
                grid_lbl=[[UILabel alloc]initWithFrame:CGRectMake(a,b,30,30)];
                 [grid_lbl setBackgroundColor:[UIColor clearColor]];
            }
            
            
            
          //  grid_lbl=[[UILabel alloc]initWithFrame:CGRectMake(a,b,20,26)];
           
            [grid_lbl setTag:j];
            [grid_lbl setTextAlignment:UITextAlignmentCenter];
            [grid_lbl setFont:[UIFont boldSystemFontOfSize:15]];

            [pitch_Chart_Btn addSubview:grid_lbl];
            a+=28;
            
        }
        
        [scrollView addSubview:pitch_Chart_Btn];
        x+=203;
    }
    [viewPotrait addSubview:scrollView];
}

#pragma mark batter portrait
-(void) drawButtonsForBatterInPotraitMode
{
    x=0;
    y=0;
    
    for (int i=0;i<9; i++) {
        batter_Btn=[UIButton buttonWithType:UIButtonTypeCustom];
        [batter_Btn setFrame:CGRectMake(x,y ,156, 158)];
        [batter_Btn setBackgroundColor:[UIColor clearColor]];
        [batter_Btn setTag:i];
        batter_Btn.showsTouchWhenHighlighted=YES;
        [batter_Btn addTarget:self action:@selector(batterInfo:) forControlEvents:UIControlEventTouchUpInside];
        [mArray3 addObject:batter_Btn];
        [mArray4 addObject:[NSValue valueWithCGRect:batter_Btn.frame]];
        [batter_Btn setBackgroundImage:[UIImage imageNamed:@"box2.png"] forState:UIControlStateNormal];
        
        
        
        
        
        btnlbl_sno=[[UILabel alloc]initWithFrame:CGRectMake(3,3,20,20)];
        [btnlbl_sno setBackgroundColor:[UIColor clearColor]];
        [btnlbl_sno setTextAlignment:UITextAlignmentCenter];
        [btnlbl_sno setTag:3];
        [btnlbl_sno setFont:[UIFont fontWithName:@"Noteworthy-Bold" size:18]];
        [btnlbl_sno setAdjustsFontSizeToFitWidth:YES];
        [btnlbl_sno setText:[NSString stringWithFormat:@"#%d",i+1]];
        [batter_Btn addSubview:btnlbl_sno];
        
        
        
        
        
        btnlbl_baterName=[[UILabel alloc]initWithFrame:CGRectMake(5, 100, 145, 50)];
        [btnlbl_baterName setBackgroundColor:[UIColor clearColor]];
        [btnlbl_baterName setTextAlignment:UITextAlignmentCenter];
        [btnlbl_baterName setTag:0];
        [btnlbl_baterName setFont:[UIFont fontWithName:@"Noteworthy-Bold" size:23]];
        [btnlbl_baterName setAdjustsFontSizeToFitWidth:YES];
        [btnlbl_baterName setText:[[baterInfo_Array objectAtIndex:i] objectForKey:@"PlayerName"]];
        [batter_Btn addSubview:btnlbl_baterName];
        
        btnlbl_jerseyNumber=[[UILabel alloc]initWithFrame:CGRectMake(2, 2, 73, 75)];
        [btnlbl_jerseyNumber setBackgroundColor:[UIColor clearColor]];
        [btnlbl_jerseyNumber setTextAlignment:UITextAlignmentCenter];
        [btnlbl_jerseyNumber setTag:1];
        [btnlbl_jerseyNumber setFont:[UIFont fontWithName:@"Noteworthy-Bold" size:30]];
        [btnlbl_jerseyNumber setTextAlignment:UITextAlignmentCenter];
        [btnlbl_jerseyNumber setText:[[baterInfo_Array objectAtIndex:i] objectForKey:@"JerseyNumber"]];
        [batter_Btn addSubview:btnlbl_jerseyNumber];
        
        btnlbl_Handed=[[UILabel alloc]initWithFrame:CGRectMake(85, 2, 45, 75)];
        [btnlbl_Handed setBackgroundColor:[UIColor clearColor]];
        [btnlbl_Handed setTextAlignment:UITextAlignmentCenter];
        [btnlbl_Handed setTag:2];
        [btnlbl_Handed setFont:[UIFont fontWithName:@"Noteworthy-Bold" size:30]];
        [btnlbl_Handed setTextAlignment:UITextAlignmentCenter];
        [btnlbl_Handed setText:[[baterInfo_Array objectAtIndex:i] objectForKey:@"Handling"]];
        [batter_Btn addSubview:btnlbl_Handed];
        
        [batter_scrollView addSubview:batter_Btn];
        
        y+=156;
        
    }
    [viewPotrait addSubview:batter_scrollView];
}

#pragma mark pitchs-landscapes
-(void) drawButtonsInLandScapeMode

{
    x=0;
    y=0;
    
    for (int i=0;i<90; i++) {
        
        pitch_Chart_Btn=[UIButton buttonWithType:UIButtonTypeCustom];
        
        if (i>1)
            if (i%10==0) {
                x=0;
                y+=136;
                
            }
        
        [pitch_Chart_Btn setFrame:CGRectMake(x,y ,216, 137)];
        [pitch_Chart_Btn setBackgroundColor:[UIColor clearColor]];
        [pitch_Chart_Btn setTag:i];
        [pitch_Chart_Btn setBackgroundImage:[UIImage imageNamed:@"graph_180new2.png"] forState:UIControlStateNormal];
        pitch_Chart_Btn.showsTouchWhenHighlighted=YES;
        [pitch_Chart_Btn addTarget:self action:@selector(btn:) forControlEvents:UIControlEventTouchUpInside];
        [mArrayLandscape addObject:pitch_Chart_Btn];
        [mArrayLandscape2 addObject:[NSValue valueWithCGRect:pitch_Chart_Btn.frame]];
        
        imgv_eoi = [[UIImageView alloc]
                    initWithImage:[UIImage imageNamed:@""]];
        [imgv_eoi setFrame:CGRectMake(2,75, 63, 60)];
             [pitch_Chart_Btn setBackgroundColor:[UIColor clearColor]];
        [imgv_eoi setTag:i+100];
        [pitch_Chart_Btn addSubview:imgv_eoi];
        
        imgEndOfInning = [[UIImageView alloc]
                          initWithImage:[UIImage imageNamed:@""]];
        [imgEndOfInning setFrame:CGRectMake(112,97, 50, 39)];
             [pitch_Chart_Btn setBackgroundColor:[UIColor clearColor]];
        [imgEndOfInning setTag:i+200];
        [pitch_Chart_Btn addSubview:imgEndOfInning];

        
        
        
        int a=4;
        int b=4;
        
        for (int j=0; j<18; j++)
        {
            
            if(j<7)
            {
                grid_lbl=[[UILabel alloc]initWithFrame:CGRectMake(a,b,25,20)];
                [grid_lbl setBackgroundColor:[UIColor clearColor]];
                [grid_lbl setTextAlignment:UITextAlignmentCenter];
            }
            
            if (j>6&&j<14) {
                
                if(j==7)
                {
                    a=4;
                    b=26;
                }
                grid_lbl=[[UILabel alloc]initWithFrame:CGRectMake(a,b,24,20)];
                [grid_lbl setBackgroundColor:[UIColor clearColor]];
                 [grid_lbl setTextAlignment:UITextAlignmentCenter];
            }
            if(j==14)//Batter
            {
                a=101;
                b=56;
                grid_lbl=[[UILabel alloc]initWithFrame:CGRectMake(a,b,37,36)];
                [grid_lbl setBackgroundColor:[UIColor clearColor]];
            }
            
            if(j==15)///Outs
            {
                a=162;
                b=52;
                grid_lbl=[[UILabel alloc]initWithFrame:CGRectMake(a,b,20,20)];
                [grid_lbl setBackgroundColor:[UIColor clearColor]];
            }
            if(j==16)//Ball/plyr
            {
                a=180;
                b=75;
                grid_lbl=[[UILabel alloc]initWithFrame:CGRectMake(a,b,20,18)];
                [grid_lbl setBackgroundColor:[UIColor clearColor]];
            }
            if(j==17)//totalballs
            {
                a=174;
                b=95;
                grid_lbl=[[UILabel alloc]initWithFrame:CGRectMake(a,b,30,30)];
                [grid_lbl setBackgroundColor:[UIColor clearColor]];
            }
            
            
            
            //  grid_lbl=[[UILabel alloc]initWithFrame:CGRectMake(a,b,20,26)];
            
            [grid_lbl setTag:j];
            [grid_lbl setTextAlignment:UITextAlignmentCenter];
            [grid_lbl setFont:[UIFont boldSystemFontOfSize:13]];
            
            [pitch_Chart_Btn addSubview:grid_lbl];
            a+=30;
            
        }
        
        
        
        
        [scrollViewLandscape addSubview:pitch_Chart_Btn];
        x+=216;
    }
    [viewLandscape addSubview:scrollViewLandscape];
}


#pragma mark landscape -batter
-(void) drawButtonsForBatterInLandScapeMode
{
    x=0;
    y=0;
    
    for (int i=0;i<9; i++) {
        batter_Btn=[UIButton buttonWithType:UIButtonTypeCustom];
        [batter_Btn setFrame:CGRectMake(x,y ,158, 137)];
        [batter_Btn setTag:i];
        batter_Btn.showsTouchWhenHighlighted=YES;
        [batter_Btn setBackgroundImage:[UIImage imageNamed:@"box_158.png"] forState:UIControlStateNormal];
        [batter_Btn addTarget:self action:@selector(batterInfo:) forControlEvents:UIControlEventTouchUpInside];
        [mArrayLandscape3 addObject:batter_Btn];
        [mArrayLandscape4 addObject:[NSValue valueWithCGRect:batter_Btn.frame]];
        
        
        btnlbl_sno=[[UILabel alloc]initWithFrame:CGRectMake(3,3,20,20)];
        [btnlbl_sno setBackgroundColor:[UIColor clearColor]];
        [btnlbl_sno setTextAlignment:UITextAlignmentCenter];
        [btnlbl_sno setTag:3];
        [btnlbl_sno setFont:[UIFont fontWithName:@"Noteworthy-Bold" size:18]];
        [btnlbl_sno setAdjustsFontSizeToFitWidth:YES];
        [btnlbl_sno setText:[NSString stringWithFormat:@"#%d",i+1]];
        [batter_Btn addSubview:btnlbl_sno];
        
        
        btnlbl_baterName=[[UILabel alloc]initWithFrame:CGRectMake(5, 80, 145, 50)];
        [btnlbl_baterName setBackgroundColor:[UIColor clearColor]];
        [btnlbl_baterName setTag:0];
        [btnlbl_baterName setTextAlignment:UITextAlignmentCenter];
        [btnlbl_baterName setFont:[UIFont fontWithName:@"Noteworthy-Bold" size:23]];
        [btnlbl_baterName setText:[[baterInfo_Array objectAtIndex:i] objectForKey:@"PlayerName"]];
        [batter_Btn addSubview:btnlbl_baterName];
        
        btnlbl_jerseyNumber=[[UILabel alloc]initWithFrame:CGRectMake(0, 3, 73, 63)];
        [btnlbl_jerseyNumber setBackgroundColor:[UIColor clearColor]];
        [btnlbl_jerseyNumber setTag:1];
        [btnlbl_jerseyNumber setTextAlignment:UITextAlignmentCenter];
        [btnlbl_jerseyNumber setFont:[UIFont fontWithName:@"Noteworthy-Bold" size:30]];
        [btnlbl_jerseyNumber setTextAlignment:UITextAlignmentCenter];
        [btnlbl_jerseyNumber setText:[[baterInfo_Array objectAtIndex:i] objectForKey:@"JerseyNumber"]];
        [batter_Btn addSubview:btnlbl_jerseyNumber];
        
        btnlbl_Handed=[[UILabel alloc]initWithFrame:CGRectMake(85, 3, 50, 63)];
        [btnlbl_Handed setBackgroundColor:[UIColor clearColor]];
        [btnlbl_Handed setTag:2];
        [btnlbl_Handed setTextAlignment:UITextAlignmentCenter];
        [btnlbl_Handed setFont:[UIFont fontWithName:@"Noteworthy-Bold" size:30]];
        [btnlbl_Handed setTextAlignment:UITextAlignmentCenter];
        [btnlbl_Handed setText:[[baterInfo_Array objectAtIndex:i] objectForKey:@"Handling"]];
        [batter_Btn addSubview:btnlbl_Handed];
        

        [batter_scrollViewLandscape addSubview:batter_Btn];
        y+=136;
    }
    [viewLandscape addSubview:batter_scrollViewLandscape];
    
}


#pragma mark identifyEmptyPitch 
-(int)identifyBallsEmptyPitch :(UIButton *)sender
{
     UIButton *pitchLabelBall=[arrayPitches objectAtIndex:sender.tag];
    
   
      if(pitchLabelBall.titleLabel.text.length==0)
      {
          return 1;
      }
    
     

    return 0;
}

-(int)identifyStrikeEmptyPitch :(UIButton *)sender
{
    

    
    UIButton *pitchLabelBall=[arrayPitches objectAtIndex:sender.tag];
    
    
    if(pitchLabelBall.titleLabel.text.length==0)
    {
        return 1;
    }
    
    
    return 0;

    
    
}


-(int)serialValidation:(int)tag
{
    int tagValue =0;
    int  previousLabelOpp=0;
    
   
 
    if(tag>0&&tag<49)
    {
        UIButton *pitchLabelBall=[arrayPitches objectAtIndex:tag-1];
        previousLabelOpp=tag-1;
        UIButton *pitchLabelStrike=[arrayPitches objectAtIndex:previousLabelOpp+49];
        
        if(pitchLabelBall.titleLabel.text.length==0&&pitchLabelStrike.titleLabel.text.length==0)
                    {
                        tagValue=1;
                    }
        
    }
    //strike
    if(tag>49&&tag<99)
    {
        UIButton *pitchLabelStrike=[arrayPitches objectAtIndex:tag-1];
        previousLabelOpp=tag-1;
        UIButton *pitchLabelBall=[arrayPitches objectAtIndex:previousLabelOpp-49];
        
        if(pitchLabelBall.titleLabel.text.length==0&&pitchLabelStrike.titleLabel.text.length==0)
        {
            tagValue=1;
        }
        

    }
//    if(tag==1||tag==8)
//    {
//        
//        
//        if(pitchLabel0.text.length==0&&pitchLabel7.text.length==0)
//        {
//            tagValue=1;
//        }
//        
//        
//    }
//    if(tag==2||tag==9)
//    {
//        if(pitchLabel1.text.length==0&&pitchLabel8.text.length==0)
//        {
//            tagValue=1;
//        }
//    }
//    if(tag==3||tag==10)
//    {
//        if(pitchLabel2.text.length==0&&pitchLabel9.text.length==0)
//        {
//            tagValue=1;
//        }
//    }
//    
//    if(tag==4||tag==11)
//    {
//        if(pitchLabel3.text.length==0&&pitchLabel10.text.length==0)
//        {
//            tagValue=1;
//        }
//    }
//    
//    if(tag==5||tag==12)
//    {
//        if(pitchLabel4.text.length==0&&pitchLabel11.text.length==0)
//        {
//            tagValue=1;
//        }
//    }
//    if(tag==6||tag==13)
//    {
//        if(pitchLabel5.text.length==0&&pitchLabel12.text.length==0)
//        {
//            tagValue=1;
//        }
//    }
    
    return tagValue;
    
}


#pragma mark -------===========-------=--------------baller popupmethod first
-(void)BallpopupMethod:(UIButton *)sender
{
    
    [viewRunnerOut removeFromSuperview];
    [viewRunnerScored removeFromSuperview];
    [viewStolenBase removeFromSuperview];
    
   LastFoul =NO;
    LastFoulconstraint=NO;
    NSLog(@"%@",btn2);
    tagPitchLabel=sender.tag;
    //serial order
   if([self serialValidation :sender.tag]==1)
   {
       
       UIAlertView * alert1=[[UIAlertView alloc] initWithTitle:@"Add Balls On serial order" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
       
       [alert1 show];
       return;

   }
    
    
    
    buttonStrikeOut.enabled=NO;
    buttonBaseOnBall.enabled=NO;
   [self recordBallStrike];
    
    
    
    
    if(intRecordBalls>=3)
    {
           buttonBaseOnBall.enabled=YES;
    }
    
    if(intRecordBalls==4)
    {
        
       if(intLastBallRecord<sender.tag&&sender.tag<49)
        {
            
            UIAlertView * alert1=[[UIAlertView alloc] initWithTitle:@"Not more than 4 balls" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
            
            [alert1 show];
            return;
            
        }
        
        int empty =[self identifyBallsEmptyPitch:sender];
            
        if(empty==1)
        {
            UIAlertView * alert1=[[UIAlertView alloc] initWithTitle:@"Not more than 4 balls" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
            
            [alert1 show];
            return;

            
        }
      
        
        
    }
    
    
    if(intRecordStrike==2)
    {
        buttonStrikeOut.enabled=YES;
        
              
    }
    if(intRecordStrike==3)
    {
       
        
        
        if(intLaststrikeRecord-49<sender.tag&&sender.tag<49)
        {
            if(LastFoulconstraint==NO)
            {
                
            UIAlertView * alert1=[[UIAlertView alloc] initWithTitle:@"Not more than 3 strike" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
            
            [alert1 show];
            return;
            }
        }
        
    }
    
    
    
    if(intRecordStrike>3)
    {
        
        if(intFoulRecord!=intRecordStrike)
        {
            
            if(intLaststrikeRecord-49<sender.tag&&sender.tag<49)
            {
                if(LastFoulconstraint==NO)
                {
                    
            UIAlertView * alert1=[[UIAlertView alloc] initWithTitle:@"Alreday Out" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
            
            [alert1 show];
            return;
                }
            }
        }
        
    }
    
    
    //////out
    if(intLastOutRecord<99&&intLastOutRecord>48)
    {
        if(intLaststrikeRecord-49<sender.tag&&sender.tag<49)
        {
            if(LastFoulconstraint==NO)
            {
                
            
            UIAlertView * alert1=[[UIAlertView alloc] initWithTitle:@"Already out" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
            
            [alert1 show];
            return;
            }
        }
    }

    //*************************END OF CONSTRIAN PITCH*************************
    
    
    lblbatterPitch.text=@"";
    imageView_locatiogGraph.hidden=YES;
    strikePitchrtag= sender.tag;
    ballerPitchTag=sender.tag;
    
    [self.view bringSubviewToFront:mask_scrollView];
//[self.view bringSubviewToFront:scrollLocation];
   // [self.view bringSubviewToFront:scrollLocation];
    
    if(mode==YES){
        [displayView setFrame:CGRectMake(scrollView.center.x-405, scrollView.center.y-380, 449, 282)];
        [mask_scrollView setFrame:CGRectMake(scrollView.frame.origin.x, 404, 310, 500)];
        [mask_scrollView setContentSize:CGSizeMake(310, 870)];
    }
    else{
        [displayView setFrame:CGRectMake(scrollView.center.x-280, scrollView.center.y-420, 449, 282)];
        
        [mask_scrollView setFrame:CGRectMake(scrollView.center.x-280, 364, 310, 500)];
        [mask_scrollView setContentSize:CGSizeMake(310, 1200)];
        
    }
    
    [mask_scrollView setContentSize:CGSizeMake(310, 620)];
    [mask_scrollView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
 
    
       [self bringScrollSubViewToFrontOnMainView:view_ballTypeOfPitch];
        btn_loadSenderidbtn=sender;
        [view_strikeTypeOfPitch setHidden:YES];
        [view_StrikeView setHidden:YES];
        [view_strikeOutView setHidden:YES];
        [view_hitFairView setHidden:YES];
        [view_locationOfHitView setHidden:YES];
        //[view_defensivePositionsView setHidden:YES];
//        [btn_loadstrikesenderid setBackgroundColor:[UIColor clearColor]];  
//    
//    if (btn_loadSenderidbtn==btn1)
//    {
//        [btn2 setBackgroundColor:[UIColor clearColor]];
//        [btn3 setBackgroundColor:[UIColor clearColor]];
//        [btn4 setBackgroundColor:[UIColor clearColor]];
//        [btn5 setBackgroundColor:[UIColor clearColor]];
//        [btn6 setBackgroundColor:[UIColor clearColor]];
//        [btn7 setBackgroundColor:[UIColor clearColor]];
//    }
//    if (btn_loadSenderidbtn==btn2) 
//    {
//        [btn1 setBackgroundColor:[UIColor clearColor]];
//        [btn3 setBackgroundColor:[UIColor clearColor]];
//        [btn4 setBackgroundColor:[UIColor clearColor]];
//        [btn5 setBackgroundColor:[UIColor clearColor]];
//        [btn6 setBackgroundColor:[UIColor clearColor]];
//        [btn7 setBackgroundColor:[UIColor clearColor]];
//    }
//    if (btn_loadSenderidbtn==btn3) {
//        [btn2 setBackgroundColor:[UIColor clearColor]];
//        [btn1 setBackgroundColor:[UIColor clearColor]];
//        [btn4 setBackgroundColor:[UIColor clearColor]];
//        [btn5 setBackgroundColor:[UIColor clearColor]];
//        [btn6 setBackgroundColor:[UIColor clearColor]];
//        [btn7 setBackgroundColor:[UIColor clearColor]];
//    }
//    if (btn_loadSenderidbtn==btn4) {
//        [btn2 setBackgroundColor:[UIColor clearColor]];
//        [btn3 setBackgroundColor:[UIColor clearColor]];
//        [btn1 setBackgroundColor:[UIColor clearColor]];
//        [btn5 setBackgroundColor:[UIColor clearColor]];
//        [btn6 setBackgroundColor:[UIColor clearColor]];
//        [btn7 setBackgroundColor:[UIColor clearColor]];
//    }
//    if (btn_loadSenderidbtn==btn5) {
//        [btn2 setBackgroundColor:[UIColor clearColor]];
//        [btn3 setBackgroundColor:[UIColor clearColor]];
//        [btn4 setBackgroundColor:[UIColor clearColor]];
//        [btn1 setBackgroundColor:[UIColor clearColor]];
//        [btn6 setBackgroundColor:[UIColor clearColor]];
//        [btn7 setBackgroundColor:[UIColor clearColor]];
//    }
//    if (btn_loadSenderidbtn==btn6) {
//        [btn2 setBackgroundColor:[UIColor clearColor]];
//        [btn3 setBackgroundColor:[UIColor clearColor]];
//        [btn4 setBackgroundColor:[UIColor clearColor]];
//        [btn5 setBackgroundColor:[UIColor clearColor]];
//        [btn1 setBackgroundColor:[UIColor clearColor]];
//        [btn7 setBackgroundColor:[UIColor clearColor]];
//    }
//    if (btn_loadSenderidbtn==btn7) {
//        [btn2 setBackgroundColor:[UIColor clearColor]];
//        [btn3 setBackgroundColor:[UIColor clearColor]];
//        [btn4 setBackgroundColor:[UIColor clearColor]];
//        [btn5 setBackgroundColor:[UIColor clearColor]];
//        [btn6 setBackgroundColor:[UIColor clearColor]];
//        [btn1 setBackgroundColor:[UIColor clearColor]];
//    }
    [mask_scrollView setHidden:NO];
    [self.view bringSubviewToFront:mask_scrollView];
//[self.view bringSubviewToFront:scrollLocation];
    NSLog(@"%s",__FUNCTION__);

}

#pragma mark Baller TABLE NUMBER MENU METHOd 

-(IBAction)BallpopMainMenuMethod:(UIButton *)sender
{
      buttonAddCP.hidden=YES;
    type=@"Others";
   
    dicStrikeDetails =[[NSMutableDictionary alloc]init];
    
    [self.view bringSubviewToFront:mask_scrollView];
  
    if(mode==YES){
        [displayView setFrame:CGRectMake(scrollView.center.x-405, scrollView.center.y-380, 449, 282)];
        [mask_scrollView setFrame:CGRectMake(scrollView.frame.origin.x, 404, 310, 500)];
        // [mask_scrollView setContentSize:CGSizeMake(scrollView.frame.size.width-500, 870)];
    }
    else{
        [displayView setFrame:CGRectMake(scrollView.center.x-280, scrollView.center.y-420, 449, 282)];
        
        [mask_scrollView setFrame:CGRectMake(scrollView.center.x-280, 364, 310, 500)];
        //  [mask_scrollView setContentSize:CGSizeMake(310, 1200)];
        
    }

    [mask_scrollView setContentSize:CGSizeMake(310, 620)];
    
       

    
//    btn_checkboxmainmenu = sender;
    
    if([sender tag]==1)
    {
      
           
         currentPitchLabel =@"1";
            [self pitchLabel:@"1"];
           // [btn_loadSenderidbtn setTitle:@"" forState:UIControlStateNormal];
      
            
            view_ballTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_ballPopUpSubMenu];    
            
            [dicStrikeDetails setObject: @"1"  forKey:@"pitchLabel"];
            
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
            [dicStrikeDetails setObject: @"BFB" forKey:@"monitor"];
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",ballerPitchTag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
              NSLog(@"%@",dicMainPitch);
            [self BallsCalculation];
            
        
    }
    
    if([sender tag] == 2)
    {
   
            
            currentPitchLabel =@"2";
            [self pitchLabel:@"2"];
            view_ballTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_ballPopUpSubMenu];
            
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
            [dicStrikeDetails setObject:@"2" forKey:@"pitchLabel"];
             [dicStrikeDetails setObject: @"BCB" forKey:@"monitor"];
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",ballerPitchTag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
              [self BallsCalculation];
            
        
    }
    
    if ([sender tag]==3)
    {
                 
            currentPitchLabel =@"3";
            [self pitchLabel:@"3"];
            view_ballTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_ballPopUpSubMenu];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
             [dicStrikeDetails setObject: @"BCU" forKey:@"monitor"];
            [dicStrikeDetails setObject:@"3" forKey:@"pitchLabel"];
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",ballerPitchTag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            [self BallsCalculation];

        
    }
    
    if ([sender tag]==4)
    {
       
            currentPitchLabel =@"4";
            [self pitchLabel:@"4"];
            view_ballTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_ballPopUpSubMenu];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
             [dicStrikeDetails setObject: @"BSL" forKey:@"monitor"];
            [dicStrikeDetails setObject:@"4" forKey:@"pitchLabel"];
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",ballerPitchTag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
              [self BallsCalculation];

        
        
    }
    
    if ([sender tag] == 5)
    {                   
        
                 currentPitchLabel =@"5";
            [self pitchLabel:@"5"];
            view_ballTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_ballPopUpSubMenu];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
             [dicStrikeDetails setObject: @"BCF" forKey:@"monitor"];
            [dicStrikeDetails setObject:@"5" forKey:@"pitchLabel"];
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",ballerPitchTag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);

            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
               [self BallsCalculation];
        
        
    }
    
    if ([sender tag] == 6)
    {
        
            
            currentPitchLabel =@"6";
            [self pitchLabel:@"6"];
            view_ballTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_ballPopUpSubMenu];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
             [dicStrikeDetails setObject: @"BKK" forKey:@"monitor"];
            [dicStrikeDetails setObject :@"6" forKey:@"pitchLabel"];
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",ballerPitchTag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            [self BallsCalculation];
    
        
    }
    
    if ([sender tag] ==7)
    {
        
                   currentPitchLabel =@"7";
            [self pitchLabel:@"7"];
            
            view_ballTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_ballPopUpSubMenu];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
             [dicStrikeDetails setObject: @"BFS" forKey:@"monitor"];
            [dicStrikeDetails setObject:@"7" forKey:@"pitchLabel"];
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",ballerPitchTag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
              [self BallsCalculation];
        
        
    }
    
    if ([sender tag]==8)
    {
        
        
                   currentPitchLabel =@"8";
            [self pitchLabel:@"8"];
            view_ballTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_ballPopUpSubMenu];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
             [dicStrikeDetails setObject: @"BSN" forKey:@"monitor"];
            [dicStrikeDetails setObject:@"8" forKey:@"pitchLabel"];
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",ballerPitchTag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
             [self BallsCalculation];
        
        
    }
    
  
    
}

#pragma mark baller 2 subview.
-(IBAction)BallpopSubMenuMethod:(UIButton*)sender
{
    
   
    NSLog(@"--%s--",__func__);
    [view_ballTypeOfPitch setHidden:YES];
    [self.view bringSubviewToFront:mask_scrollView];
  
    if(mode==YES){
        [displayView setFrame:CGRectMake(scrollView.center.x-405, scrollView.center.y-380, 449, 282)];
        [mask_scrollView setFrame:CGRectMake(scrollView.frame.origin.x, 404, 310, 500)];
        //[mask_scrollView setContentSize:CGSizeMake(scrollView.frame.size.width-500, 870)];
    }
    else{
        [displayView setFrame:CGRectMake(scrollView.center.x-280, scrollView.center.y-420, 449, 282)];
        
        [mask_scrollView setFrame:CGRectMake(scrollView.center.x-280, 364, 310, 500)];
        //[mask_scrollView setContentSize:CGSizeMake(scrollView.frame.size.width-500, 1200)];
        
    }
    [mask_scrollView setContentSize:CGSizeMake(310, 620)];

  
    
    
    if ( [sender tag]==1)
    {        
                      lblbatterPitch.text =@"BB";
           
            
            [dicStrikeDetails setObject:currentPitchLabel forKey:@"pitchLabel"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
           // NSLog(@"%@",btn_loadSenderidbtn.titleLabel.text );
            [dicStrikeDetails setObject:lblbatterPitch.text forKey:@"lblbatterPitch"];
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",ballerPitchTag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            [self BallsCalculation];
        
        
    }
    
    if([sender tag] ==2)
    {
        
                    lblbatterPitch.text =@"HBP"; 
            NSLog(@"%@",currentPitchLabel);
            [dicStrikeDetails setObject:currentPitchLabel forKey:@"pitchLabel"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
           // NSLog(@"%@",btn_loadSenderidbtn.titleLabel.text );
            [dicStrikeDetails setObject:lblbatterPitch.text forKey:@"lblbatterPitch"];
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",ballerPitchTag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            [self BallsCalculation];
            

          
    }
    
    
    if ([sender tag]==3)
    {
        
                  
            //[btn_loadSenderidbtn setTitle: forState:UIControlStateNormal];
            [self pitchLabel:@"PB"];
            [dicStrikeDetails setObject:@"PB" forKey:@"pitchLabel"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
           // NSLog(@"%@",btn_loadSenderidbtn.titleLabel.text );
            [dicStrikeDetails setObject:lblbatterPitch.text forKey:@"lblbatterPitch"];
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",ballerPitchTag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            [self BallsCalculation];
            

        
        
        
    }
    
    if([sender tag]==4)
    {
        
       
            [self pitchLabel:@"WP"];
            [dicStrikeDetails setObject:@"WP" forKey:@"pitchLabel"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
           // NSLog(@"%@",btn_loadSenderidbtn.titleLabel.text );
            [dicStrikeDetails setObject:lblbatterPitch.text forKey:@"lblbatterPitch"];
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",ballerPitchTag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            [self BallsCalculation];
            


        
    }
    
    if ([sender tag]==5)
    {
      
    }
    
    
  
    view_ballPopUpSubMenu.hidden=YES;

    //[dict_oneinnngsData setObject:@"batterOuterLabel" forKey:@"batterOuterLabel"];
      
}



#pragma mark======================================== First StrikePOPUP METHOD

-(void)StrikepopupMethod:(UIButton *)sender
{
    [viewRunnerOut removeFromSuperview];
    [viewRunnerScored removeFromSuperview];
    [viewStolenBase removeFromSuperview];
    
    
     buttonAddCP.hidden=YES;
      LastFoul =NO;
    LastFoulconstraint=NO;
    
     tagPitchLabel= sender.tag;
    
    //serial order
    if([self serialValidation :sender.tag]==1)
    {
        
        UIAlertView * alert1=[[UIAlertView alloc] initWithTitle:@"Add Balls On serial order" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
        
        [alert1 show];
        return;
        
    }

    
    intalertInning=10;
    
    
       buttonFoulStrike.enabled=YES;
       buttonSwing.enabled=YES;
    buttonPassed.enabled=YES;
    buttonwild.enabled=YES;
    buttonCalled.enabled=YES;
    
       buttonStrikeOut.enabled=NO;
   
    
    [self recordBallStrike];


    
    if(intRecordBalls==3)
    {
        
    }
    
    if(intRecordBalls==4)
    {
        if(intLastBallRecord+49<sender.tag&&sender.tag>48)
        {
            
            ////////checking foul
           
                
            
            
                UIAlertView * alert1=[[UIAlertView alloc] initWithTitle:@"Not more than 4 balls" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
                
                [alert1 show];
                return;
            
                     
        }
        
        
    }
    
    
    if(intRecordStrike>=2)
    {
        buttonSwing.enabled=NO;
        buttonPassed.enabled=NO;
        buttonwild.enabled=NO;
        buttonCalled.enabled=NO;
        
        buttonStrikeOut.enabled=YES;
        
        if(intFoulRecord==2)
        {
            buttonFoulStrike.enabled=YES;
        }

        
        
    }
    if(intRecordStrike==3)
    {
         buttonStrikeOut.enabled=YES;
        
         if(intLaststrikeRecord<sender.tag&&sender.tag>48)
         {
             
             if(intFoulRecord<3)
             {
                 if(LastFoulconstraint==NO)
                 {
                     
             UIAlertView * alert1=[[UIAlertView alloc] initWithTitle:@"Not more than 3 strike" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
             
             [alert1 show];
             return;
                 }
             }
             
             
                    
         }
        
        int empty =[self identifyStrikeEmptyPitch:sender];
        
        if(empty==1&&intFoulRecord!=3)
        {
            if(LastFoulconstraint==NO)
            {
                
            
            UIAlertView * alert1=[[UIAlertView alloc] initWithTitle:@"Not more than 3 strike" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
            
            [alert1 show];
            return;
            }
        }
        
    }////end if intRecordStrike

    
    if(intRecordStrike>3)
    {
        
        if(intFoulRecord!=intRecordStrike)
        {
            
            
            if(intLaststrikeRecord<sender.tag&&sender.tag>48)
            {
                if(LastFoulconstraint==NO)
                {
                    
            UIAlertView * alert1=[[UIAlertView alloc] initWithTitle:@"Not more than 3 strike" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
            
            [alert1 show];
            return;
                }
            }
        }
        

        
        
    }
    

    //////out
    if(intLastOutRecord<99&&intLastOutRecord>48)
    {
      if(intLaststrikeRecord<sender.tag&&sender.tag>48)
        {
            if(LastFoulconstraint==NO)
            {
                
            UIAlertView * alert1=[[UIAlertView alloc] initWithTitle:@"Already Out" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
            
            [alert1 show];
            return;
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    //*************************END OF CONSTRIAN PITCH*************************
    

    
    mask_scrollView.hidden=NO;
    imageView_locatiogGraph.hidden=YES;
    lblbatterPitch.text=@"";
    strikePitchrtag=sender.tag;
    stringKeyPitch =[NSMutableString stringWithFormat:@"S%d",sender.tag];
    
    NSLog(@"%@",stringKeyPitch);
    

    
    NSLog(@"%s",__FUNCTION__);
    [self.view bringSubviewToFront:mask_scrollView];
   
    if(mode==YES){
        [displayView setFrame:CGRectMake(scrollView.center.x-405, scrollView.center.y-380, 449, 282)];
        [mask_scrollView setFrame:CGRectMake(scrollView.frame.origin.x, 404, 310, 500)];
        //  [mask_scrollView setContentSize:CGSizeMake(scrollView.frame.size.width-500, 870)];
    }
    else{
        [displayView setFrame:CGRectMake(scrollView.center.x-280, scrollView.center.y-420, 449, 282)];
        
        [mask_scrollView setFrame:CGRectMake(scrollView.center.x-280, 364, 310, 500)];
        //[mask_scrollView setContentSize:CGSizeMake(scrollView.frame.size.width-500, 1200)];
        
    }


    [mask_scrollView setContentSize:CGSizeMake(310, 620)];
    [mask_scrollView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    
     [self bringScrollSubViewToFrontOnMainView:view_strikeTypeOfPitch];
        [view_ballTypeOfPitch setHidden:YES];
        [view_ballPopUpSubMenu setHidden:YES];


}

#pragma mark first subview of call /alloc
-(IBAction) strikeTypeOfPitchMethod:(UIButton*)sender
{
    type=@"Others";
    
      dicStrikeDetails = [[NSMutableDictionary alloc]init];
   
    
    if ( [sender tag]==1)
    {
       
        
        [self pitchLabel:@"1"];
            //[btn_loadstrikesenderid setTitle:@"1" forState:UIControlStateNormal];
            
            currentPitchLabel=@"1";
            [dicStrikeDetails setObject:@"1" forKey:@"pitchLabel"];
             [dicStrikeDetails setObject: @"SFB" forKey:@"monitor"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
            NSLog(@"%@",dicStrikeDetails);
            
            view_strikeTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_StrikeView];
            
        
    }
    if([sender tag] == 2)
    {
        
           // [btn_loadstrikesenderid setTitle:@"2" forState:UIControlStateNormal];
              [self pitchLabel:@"2"];
             currentPitchLabel=@"2";
            [dicStrikeDetails setObject:@"2"forKey:@"pitchLabel"];
             [dicStrikeDetails setObject: @"SCB" forKey:@"monitor"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
            NSLog(@"%@",dicStrikeDetails);
            
            view_strikeTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_StrikeView];

          
        
    }
    if([sender tag]==3)
    {            
          //  [btn_loadstrikesenderid setTitle:@"3" forState:UIControlStateNormal];
          
            [self pitchLabel:@"3"];
             currentPitchLabel=@"3";
            [dicStrikeDetails setObject:@"3"forKey:@"pitchLabel"];
             [dicStrikeDetails setObject: @"SCU" forKey:@"monitor"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
            
            NSLog(@"%@",dicStrikeDetails);
            view_strikeTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_StrikeView];


            
        
        
    }
    
    if([sender tag]==4)
        
    {
        
                    [self pitchLabel:@"4"];
             currentPitchLabel=@"4";
            [dicStrikeDetails setObject:@"4"forKey:@"pitchLabel"];
             [dicStrikeDetails setObject: @"SSL" forKey:@"monitor"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
            
            NSLog(@"%@",dicStrikeDetails);

            view_strikeTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_StrikeView];            
        
    }
    if([sender tag] == 5)
    {
        [self pitchLabel:@"5"];
             currentPitchLabel=@"5";
            [dicStrikeDetails setObject:@"5"forKey:@"pitchLabel"];
             [dicStrikeDetails setObject: @"SCF" forKey:@"monitor"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
            NSLog(@"%@",dicStrikeDetails);

            view_strikeTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_StrikeView];
        
    }
    if ([sender tag] == 6)
    {
            [self pitchLabel:@"6"];
             currentPitchLabel=@"6";
             [dicStrikeDetails setObject: @"SKK" forKey:@"monitor"];
            [dicStrikeDetails setObject:@"6"forKey:@"pitchLabel"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
            NSLog(@"%@",dicStrikeDetails);

            view_strikeTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_StrikeView];
        
    }
    if ([sender tag] ==7)
    {
       
            [self pitchLabel:@"7"];
             currentPitchLabel=@"7";
            [dicStrikeDetails setObject:@"7"forKey:@"pitchLabel"];
             [dicStrikeDetails setObject: @"SFS" forKey:@"monitor"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
            NSLog(@"%@",dicStrikeDetails);

            view_strikeTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_StrikeView];
        
        
    }
    
    if ([sender tag]==8)
    {
       
            
            [self pitchLabel:@"8"];
             currentPitchLabel=@"8";
            [dicStrikeDetails setObject:@"8"forKey:@"pitchLabel"];
             [dicStrikeDetails setObject: @"SSN" forKey:@"monitor"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
            NSLog(@"%@",dicStrikeDetails);

            view_strikeTypeOfPitch.hidden=YES;
            [self bringScrollSubViewToFrontOnMainView:view_StrikeView];            
        
    }
   
}

#pragma mark 2 subview appendind last -Strike
-(IBAction)SrikeOptionMethod:(UIButton*)sender
{  
    NSLog(@"%s",__func__);
    [self.view bringSubviewToFront:mask_scrollView];
   
    
    if(mode==YES){
        [displayView setFrame:CGRectMake(scrollView.center.x-405, scrollView.center.y-380, 449, 282)];
        [mask_scrollView setFrame:CGRectMake(scrollView.frame.origin.x, 404, 310, 500)];
        //[mask_scrollView setContentSize:CGSizeMake(scrollView.frame.size.width-500, 870)];
    }
    else{
        [displayView setFrame:CGRectMake(scrollView.center.x-280, scrollView.center.y-420, 449, 282)];
        
        [mask_scrollView setFrame:CGRectMake(scrollView.center.x-280, 364, 310, 500)];
        // [mask_scrollView setContentSize:CGSizeMake(scrollView.frame.size.width-500, 1200)];
        
    }
    
    [mask_scrollView setContentSize:CGSizeMake(310, 1050)];
    [view_strikeTypeOfPitch setHidden:YES];

    if([sender tag]==1)
    {
              
        
        
        currentPitchLabelAppend =[NSString stringWithFormat:@"%@C",currentPitchLabel];
            
            [self pitchLabel:currentPitchLabelAppend];
            
            [dicStrikeDetails setObject:currentPitchLabelAppend forKey:@"pitchLabel"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
             [dicStrikeDetails setObject:@"S" forKey:@"strike"];
           
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
           // [dicMainPitch removeAllObj];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
              view_StrikeView.hidden=YES;
                [self BallsCalculation];
        
      }
    
    if([sender tag] == 2)
    {
        
            
            currentPitchLabelAppend =[NSString stringWithFormat:@"%@S",currentPitchLabel];
            
            [self pitchLabel:currentPitchLabelAppend];
            
            [dicStrikeDetails setObject:currentPitchLabelAppend forKey:@"pitchLabel"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
           // [dicStrikeDetails setObject:btn_loadstrikesenderid.titleLabel.text forKey:@"pitchLabel"];
            
             [dicStrikeDetails setObject:@"S" forKey:@"strike"];
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
             [self BallsCalculation];
            view_StrikeView.hidden=YES;
        
    }
    
    if ([sender tag]==3)
    {
                  
            currentPitchLabelAppend =[NSString stringWithFormat:@"%@F",currentPitchLabel];
            
            [self pitchLabel:currentPitchLabelAppend];
            
            [dicStrikeDetails setObject:currentPitchLabelAppend forKey:@"pitchLabel"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
         //   [dicStrikeDetails setObject:btn_loadstrikesenderid.titleLabel.text forKey:@"pitchLabel"];
            
            [dicStrikeDetails setObject:@"F" forKey:@"foul"];
            [dicStrikeDetails setObject:@"S" forKey:@"strike"];
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);

            view_StrikeView.hidden=YES;
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
                [self BallsCalculation];
        
    }
    
    if ([sender tag]==4)///Strike out
    {
        view_StrikeView.hidden =YES;
        [self bringScrollSubViewToFrontOnMainView:view_strikeOutView];
    }
    
    if ([sender tag] == 5)
    {
        view_StrikeView.hidden=YES;
        [self bringScrollSubViewToFrontOnMainView:view_hitFairView];
    }
    
    if ([sender tag] == 6)
    {
       
            [self pitchLabel:@"PB"];
            
            [dicStrikeDetails setObject:@"PB" forKey:@"pitchLabel"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
             [dicStrikeDetails setObject:@"S" forKey:@"strike"];
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            [btn_loadstrikesenderid setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
               [self BallsCalculation];
            view_StrikeView.hidden=YES;
        
        
    }
    
    if([sender tag] ==7)
        
    {
        
                    [self pitchLabel:@"WP"];
            [dicStrikeDetails setObject:@"WP" forKey:@"pitchLabel"];
             [dicStrikeDetails setObject:@"S" forKey:@"strike"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);

            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            view_StrikeView.hidden=YES;
               [self BallsCalculation];
        
        
    }
    
    if (sender.tag>=1 && sender.tag<=50 )
    {
        //[btn_loadstrikesenderid setBackgroundColor:[UIColor clearColor]];
        //[imageView_locatiogGraph setImage:nil];
        imageView_locatiogGraph.hidden =YES;
    }
 
}


#pragma mark strike hit fair -OUT RECORD
-(IBAction)hitFairOptionMethod:(UIButton *)sender
{
    intAdvanceTag=1;
    [scrollLocation setHidden:NO];
    
      
    [view_locationOfHitView setFrame:CGRectMake(0, 0, 213, 365)];
    
      [view_defensivePositionsView setFrame:CGRectMake(0,0, 190, 316)];
    [table_displayOptions scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
       // dict_oneinnngsData =[[NSMutableDictionary alloc]init];
    
    tag_outComeOfAtBat=sender.tag;
    
 
    
    btn_hitfaircheckbox = sender;
    
    if ( [sender tag]==1)//////on base
    {
        
             lblbatterPitch.text=@"1b";
            // [scrollLocation setFrame:CGRectMake(600,400, 310, 500)];
            [self bringScrollSubViewToFrontOnMainViewLocation:view_locationOfHitView];
            //  [table_displayOptions reloadData];
        
        
    }
    
    if([sender tag] == 2)
    {
        
     
             lblbatterPitch.text=@"2b";
        [self bringScrollSubViewToFrontOnMainViewLocation:view_locationOfHitView];
             // [table_displayOptions reloadData];
        
        
    }
    
    if ([sender tag]==3)
    {
      
             lblbatterPitch.text=@"3b";
          [self bringScrollSubViewToFrontOnMainViewLocation:view_locationOfHitView];
             // [table_displayOptions reloadData];
        
        
    }
    
    
    if ([sender tag]==4)
    {
        
        
             lblbatterPitch.text=@"Bunt";
          [self bringScrollSubViewToFrontOnMainViewLocation:view_locationOfHitView];
            //  [table_displayOptions reloadData];
        
        
    }
    
    if ([sender tag] == 5)
    {
        
             lblbatterPitch.text=@"HR";
          [self bringScrollSubViewToFrontOnMainViewLocation:view_locationOfHitView];
             // [table_displayOptions reloadData];
        
    }
    
    if ([sender tag] == 6)
    {      
        [view_defensivePositionsView setFrame:CGRectMake(0,0, 190, 316)];
        
                    lblbatterPitch.text=@"E";
             type=@"Others";
            error=YES;
            [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];
            //[self.view addSubview:view_defensivePositionsView];
              [table_displayOptions reloadData];
        
        
    }
    
    if ([sender tag] ==7)
    {
        
        [view_defensivePositionsView setFrame:CGRectMake(0,0, 190, 316)];
        
                    type=@"Others";
             lblbatterPitch.text=@"FC";
          error=YES;
            
           [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];
              [table_displayOptions reloadData];
        
        
    }
    
    if ([sender tag]==8)///out
    {
                   intOutPlyers++;
           // lblOut.text =[NSString stringWithFormat:@"%d",intOutPlyers];
            
             lblbatterPitch.text=@"GB";
             type=@"Others";
         
       [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];
               [table_displayOptions reloadData];
        
    }
    
    
    if([sender tag]==9)
    {
                   intOutPlyers++;
            //lblOut.text =[NSString stringWithFormat:@"%d",intOutPlyers];
             lblbatterPitch.text=@"DP";
             type=@"Others";
         
         [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];
               [table_displayOptions reloadData];
        
    }
    
    if ([sender tag]==10)
    {
        [view_defensivePositionsView setFrame:CGRectMake(0, 0, 190, 316)];
        
                  intOutPlyers++;
           // lblOut.text =[NSString stringWithFormat:@"%d",intOutPlyers];
             lblbatterPitch.text=@"TP";
             type=@"Others";
            [table_displayOptions reloadData];
          [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];
        
        
    }
    
    
    if([sender tag]==11)
    {
        [view_defensivePositionsView setFrame:CGRectMake(0,0, 190, 316)];
        
                    intOutPlyers++;
          //  lblOut.text =[NSString stringWithFormat:@"%d",intOutPlyers];
             lblbatterPitch.text=@"SacB";
             type=@"Others";
            [table_displayOptions reloadData];
            [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];
        
        
        
    }
    
    if ([sender tag]==12)
    {
        [view_defensivePositionsView setFrame:CGRectMake(0, 0, 190, 316)];

       
            intOutPlyers++;
           //type=@"Others";
            //lblOut.text =[NSString stringWithFormat:@"%d",intOutPlyers];
             lblbatterPitch.text=@"FB/LD";
            [table_displayOptions reloadData];
            [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];
        
    }
    if([sender tag]==13)
    {
        [view_defensivePositionsView setFrame:CGRectMake(0,0, 190, 316)];
                   intOutPlyers++;
           // lblOut.text =[NSString stringWithFormat:@"%d",intOutPlyers];
             lblbatterPitch.text=@"SacF";
             type=@"Others";
            [table_displayOptions reloadData];
           [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];
        
    }
    if ([sender tag]==14)
    {
        [view_defensivePositionsView setFrame:CGRectMake(0,0, 190, 316)];

                   intOutPlyers++;
          //  lblOut.text =[NSString stringWithFormat:@"%d",intOutPlyers];
             type=@"Others";
            intAdvanceTag=0;
            //[table_displayOptions reloadData];
            
            [self bringScrollSubViewToFrontOnMainViewLocation:view_locationOfHitView];
        
    }
    
    if ([sender tag]==15)
    {
                  lblbatterPitch.text=@"";
            
            imgv_eoi = [[UIImageView alloc] 
                                     initWithImage:[UIImage imageNamed:@"Eoi_shape.png"]];
            [lblbatterPitch addSubview:imgv_eoi];
            [view_hitFairView removeFromSuperview];
            [view_locationOfHitView removeFromSuperview];

        
    }
     if (sender.tag==1 || sender.tag==2 || sender.tag==3 || sender.tag==4 || sender.tag==5||sender.tag==14)
        [view_defensivePositionsView setHidden:YES];
    else
        [view_locationOfHitView setHidden:YES];
    NSLog(@"-->Tag Value : %d -->",sender.tag);
}

#pragma mark backToStrikeOPtionsFromHitFairView

-(IBAction)backToStrikeOPtionsFromHitFairView:(id)sender


{
     [view_locationOfHitView setHidden:YES];
    [view_defensivePositionsView setHidden:YES];
    
    [self.view bringSubviewToFront:mask_scrollView];
    if(mode==YES){
        [displayView setFrame:CGRectMake(scrollView.center.x-405, scrollView.center.y-380, 449, 282)];
        [mask_scrollView setFrame:CGRectMake(scrollView.frame.origin.x, 404, 310, 500)];
        //[mask_scrollView setContentSize:CGSizeMake(scrollView.frame.size.width-500, 870)];
    }
    else{
        [displayView setFrame:CGRectMake(scrollView.center.x-280, scrollView.center.y-420, 449, 282)];
        
        [mask_scrollView setFrame:CGRectMake(scrollView.center.x-280, 364, 310, 500)];
        //[mask_scrollView setContentSize:CGSizeMake(scrollView.frame.size.width-500, 1200)];
        
    }
    
    [mask_scrollView setContentSize:CGSizeMake(310, 620)];
    [mask_scrollView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];

    [view_hitFairView setHidden:YES];
    [self bringScrollSubViewToFrontOnMainView:view_StrikeView];
}

#pragma mark locationOfHitViewAction Subview strike last -add images-Base
-(IBAction) locationOfHitViewAction:(id)sender
{
    
    [scrollLocation setHidden:YES];
   // NSString *string;
    imageView_locatiogGraph.hidden=NO;
    NSLog(@"%s",__func__);
    UIImage *img_batLocation;
        NSLog(@"%d",[sender tag]);
    
    if (tag_outComeOfAtBat==5) {
        switch ([sender tag])
        {
            case 1:
                img_batLocation = [UIImage imageNamed:@"HcenterField.png"];
                fieldImage=@"HcenterField.png";
                break;
           
            case 4:
                img_batLocation = [UIImage imageNamed:@"HleftField.png"];
                fieldImage=@"HleftField.png";
                break;
            case 5:
                img_batLocation = [UIImage imageNamed:@"HrightField.png"];
                fieldImage=@"HrightField.png";
                break;
                
            case 6:
                img_batLocation = [UIImage imageNamed:@"HleftField.png"];
                fieldImage=@"HleftField.png";
                break;
            case 7:
                img_batLocation = [UIImage imageNamed:@"HcenterField.png"];
                  fieldImage=@"HcenterField.png";
                break;
            case 8:
                img_batLocation = [UIImage imageNamed:@"HrightField.png"];
                 fieldImage=@"HrightField.png";
                break;
            default:
            {
                img_batLocation = [UIImage imageNamed:@"HcenterField.png"];
                fieldImage=@"HcenterField.png";
            }
                break;
        }
        
    }
    else
    switch ([sender tag])
    {
        case 1:
           
            if(intAdvanceTag==0)
            {
                lblbatterPitch.text =@"1to";
                
              
                 [view_defensivePositionsView setHidden:NO];
              [view_defensivePositionsView setFrame:CGRectMake(0,0, 190, 316)];
                  type=@"Others"; 
               
                [table_displayOptions reloadData];
                [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];

                [view_locationOfHitView setHidden:YES];
                    [view_defensivePositionsView setHidden:NO];
                return;
                
            }
            img_batLocation = [UIImage imageNamed:@"middleField.png"];
            
         // string =  [lblbatterPitch.text stringByAppendingFormat:@""];
            fieldImage =@"middleField.png";
                     
            break;
        case 2:
            
            if(intAdvanceTag==0)
            {
                lblbatterPitch.text =@"2to";
                [view_defensivePositionsView setFrame:CGRectMake(0,0, 190, 316)];
                  type=@"Others";
                [table_displayOptions reloadData];
                [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];

                [view_locationOfHitView setHidden:YES];
                [view_defensivePositionsView setHidden:NO];
                [scrollLocation setHidden:NO];

                return;
                
            }
            img_batLocation = [UIImage imageNamed:@"1base.png"];
            fieldImage =@"1base.png";
          //  string =  [lblbatterPitch.text stringByAppendingFormat:@""];
            break;
       
        case 3:
            if(intAdvanceTag==0)
            {
                lblbatterPitch.text =@"3to";
               
              [view_defensivePositionsView setFrame:CGRectMake(0,0, 190, 316)];
                  type=@"Others";
              
                    [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];
                  [table_displayOptions reloadData];
                [view_locationOfHitView setHidden:YES];
                return;
                
            }
            img_batLocation = [UIImage imageNamed:@"2base.png"];
            fieldImage =@"2base.png";
             // string = [lblbatterPitch.text stringByAppendingFormat:@""];
            break;
       
        case 4:
            if(intAdvanceTag==0)
            {
                lblbatterPitch.text =@"4to";
                [view_locationOfHitView setHidden:YES];
               [view_defensivePositionsView setFrame:CGRectMake(0,0, 190, 316)];
                
                       [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];
                 [table_displayOptions reloadData];
                [view_locationOfHitView setHidden:YES];
                return;
                
            }
            img_batLocation = [UIImage imageNamed:@"3base.png"];
            fieldImage =@"3base.png";
           //   string = [lblbatterPitch.text stringByAppendingFormat:@""];
            break;
       
        case 5:
            if(intAdvanceTag==0)
            {
                lblbatterPitch.text =@"5to";
                [view_locationOfHitView setHidden:YES];
            [view_defensivePositionsView setFrame:CGRectMake(0,0, 190, 316)];
                 [table_displayOptions reloadData];
              [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];
                [view_locationOfHitView setHidden:YES];
                return;
                
            }
            img_batLocation = [UIImage imageNamed:@"shortstop.png"];
            fieldImage =@"shortstop.png";
           //   string = [lblbatterPitch.text stringByAppendingFormat:@""];
            break;
       
        case 6:
            if(intAdvanceTag==0)
            {
                lblbatterPitch.text =@"6to";
                [view_locationOfHitView setHidden:YES];
             [view_defensivePositionsView setFrame:CGRectMake(0,0, 190, 316)];
                 [table_displayOptions reloadData];
                      [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];
                 [view_locationOfHitView setHidden:YES];
                return;
                
            }
            img_batLocation = [UIImage imageNamed:@"left.png"];
            fieldImage =@"left.png";
       //       string = [lblbatterPitch.text stringByAppendingFormat:@""];
            break;
        case 7:
            if(intAdvanceTag==0)
            {
                lblbatterPitch.text =@"7to";
                //[view_locationOfHitView setHidden:YES];
          [view_defensivePositionsView setFrame:CGRectMake(0,0, 190, 316)];
                 [table_displayOptions reloadData];
                     [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];
                 [view_locationOfHitView setHidden:YES];
                return;
                
            }
            img_batLocation = [UIImage imageNamed:@"center.png"];
            fieldImage =@"center.png";
       //       string =  [lblbatterPitch.text stringByAppendingFormat:@""];
            break;
        case 8:
            if(intAdvanceTag==0)
            {
                lblbatterPitch.text =@"8to";
                [view_locationOfHitView setHidden:YES];
              [view_defensivePositionsView setFrame:CGRectMake(0,0, 190, 316)];
                 [table_displayOptions reloadData];
               [self bringScrollSubViewToFrontOnMainViewLocation:view_defensivePositionsView];
                 [view_locationOfHitView setHidden:YES];
                return;
                
            }
            img_batLocation = [UIImage imageNamed:@"right.png"];
            fieldImage =@"right.png";
          //    string =  [lblbatterPitch.text stringByAppendingFormat:@""];
            break;
        default:
            break;
    }
    [imageView_locatiogGraph setImage:img_batLocation];
    [btn_loadstrikesenderid setBackgroundColor:[UIColor clearColor]];
    
    [view_hitFairView removeFromSuperview];
    [view_locationOfHitView removeFromSuperview];
    
    [btn_hitfaircheckbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"] forState:UIControlStateNormal];
    
    [dicStrikeDetails setObject:fieldImage forKey:@"StrikerFieldImage"];
    if(lblbatterPitch.text.length!=0)
    {
       // lblbatterPitch.text =string;
        
        [dicStrikeDetails setObject:lblbatterPitch.text forKey:@"lblbatterPitch"];
    }
   
     //[dicStrikeDetails setObject: @"H" forKey:@"lblOut"];
    [dicStrikeDetails setObject:@"H" forKey:@"strike"];
    stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
    stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
    [stringKeyMainPitch appendString:stringKeyPitch];
    
    [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
    NSLog(@"%@",dicMainPitch);
    
    [self strikeBallReplacement];
    
    NSLog(@"%@",dicMainPitch);
  [self BallsCalculation];
    
      NSLog(@"%@",lblbatterPitch.text);
}

#pragma mark strike out-outcome of bat ks/kc -out
-(IBAction)strikeOutoptionMethod:(UIButton*)sender
{
    
    buttonTitle = btn_loadstrikesenderid.titleLabel.text;
    NSLog(@"The Title Of the Button : %@",buttonTitle);
    
   
    NSLog(@"%s",__func__);
    [view_StrikeView removeFromSuperview];
    if (btn_strikeoutCheckbox) 
    {
        if (btn_strikeoutCheckbox.tag == sender.tag)
        {
            if (btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
            {
                [btn_strikeoutCheckbox setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"btn_selected" ofType:@"png"]]forState:UIControlStateNormal];
            }
            else 
            {
                [btn_strikeoutCheckbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
            }
        }
        else
        {            
            [sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
            [btn_strikeoutCheckbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
        }
    }
    else
    {
        [sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
    }
    btn_strikeoutCheckbox = sender;
    
    //copy
    
    if( [sender tag]==1)//on base
    {
        
      //  [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
       // [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        if (btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lblbatterPitch.text = [NSString stringWithFormat:@""];
        }
        
        else
        {
            
           // [btn_loadstrikesenderid setTitle:[NSString stringWithFormat:@"%@S",buttonTitle] forState:UIControlStateNormal];
            
            currentPitchLabelAppend =[NSString stringWithFormat:@"%@S",currentPitchLabel];
            
            [self pitchLabel:currentPitchLabelAppend];
            
            [dicStrikeDetails setObject:currentPitchLabelAppend forKey:@"pitchLabel"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
            lblbatterPitch.text = [NSString stringWithFormat:@"Ksd3s"];
            
           // [dicStrikeDetails setObject:btn_loadstrikesenderid.titleLabel.text forKey:@"pitchLabel"];
            
          //  NSLog(@"%@",btn_loadstrikesenderid.titleLabel.text );
            [dicStrikeDetails setObject:lblbatterPitch.text forKey:@"lblbatterPitch"];
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            [self BallsCalculation];

            
        }
        lblbatterPitch.textColor = [UIColor whiteColor];
        
       // vw_OnBaseview.hidden=YES;
        
    }
    
    
    if([sender tag] == 2)
    {
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        lblbatterPitch.textColor = [UIColor whiteColor];
        
        if (btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lblbatterPitch.text = [NSString stringWithFormat:@""];
        }
        
        else
        {
            
           // intOutPlyers++;
           // lblOut.text =[NSString stringWithFormat:@"%d",intOutPlyers];
           // [btn_loadstrikesenderid setTitle:[NSString stringWithFormat:@"%@C",buttonTitle] forState:UIControlStateNormal];
            lblbatterPitch.text = [NSString stringWithFormat:@"Kcd3s"];
            
            currentPitchLabelAppend =[NSString stringWithFormat:@"%@C",currentPitchLabel];
            
            [self pitchLabel:currentPitchLabelAppend];
            
            [dicStrikeDetails setObject:currentPitchLabelAppend forKey:@"pitchLabel"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
           // [dicStrikeDetails setObject:btn_loadstrikesenderid.titleLabel.text forKey:@"pitchLabel"];
            
           // NSLog(@"%@",btn_loadstrikesenderid.titleLabel.text );
            [dicStrikeDetails setObject:lblbatterPitch.text forKey:@"lblbatterPitch"];
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            [self BallsCalculation];

            
        }
        
        //vw_OnBaseview.hidden=YES;
        
    }
    
    if ([sender tag]==3)///Strike out swinging, foul tip to catcher (Ks2)
    {
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        if (btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lblbatterPitch.text = [NSString stringWithFormat:@""];
        }
        
        else
        {
              //[btn_loadstrikesenderid setTitle:[NSString stringWithFormat:@"%@S",buttonTitle] forState:UIControlStateNormal];
            lblbatterPitch.text = [NSString stringWithFormat:@"Ks2"];
            
            [dicStrikeDetails setObject: @"o" forKey:@"lblOut"];
            currentPitchLabelAppend =[NSString stringWithFormat:@"%@S",currentPitchLabel];
            
            [self pitchLabel:currentPitchLabelAppend];
            
            [dicStrikeDetails setObject:currentPitchLabelAppend forKey:@"pitchLabel"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
            //[dicStrikeDetails setObject:btn_loadstrikesenderid.titleLabel.text forKey:@"pitchLabel"];
            
         //   NSLog(@"%@",btn_loadstrikesenderid.titleLabel.text );
            
            [dicStrikeDetails setObject:lblbatterPitch.text forKey:@"lblbatterPitch"];
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            [self BallsCalculation];

            
        }
        lblbatterPitch.textColor = [UIColor whiteColor];
        //vw_OnBaseview.hidden=YES;
        
    }
    
    if ([sender tag]==4)
    {
        
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        if (btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lblbatterPitch.text = [NSString stringWithFormat:@""];
        }
        
        else
        {
            lblbatterPitch.text = [NSString stringWithFormat:@"W"];
            
            [dicStrikeDetails setObject:currentPitchLabel forKey:@"pitchLabel"];
           // currentPitchLabelAppend =[NSString stringWithFormat:@"%@C",currentPitchLabel];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
            [self pitchLabel:currentPitchLabel];
            
            //NSLog(@"%@",btn_loadstrikesenderid.titleLabel.text );
            [dicStrikeDetails setObject:lblbatterPitch.text forKey:@"lblbatterPitch"];
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            [self BallsCalculation];

            
        }
        
        lblbatterPitch.textColor = [UIColor yellowColor];
       // vw_OnBaseview.hidden=YES;
        
    }
    
    
    if ([sender tag] == 5)
    {
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        
        
        if (btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lblbatterPitch.text = [NSString stringWithFormat:@""];
        }
        
        else
        {
            intOutPlyers++;
          //  lblOut.text =[NSString stringWithFormat:@"%d",intOutPlyers];
            lblbatterPitch.text = [NSString stringWithFormat:@"Ks"];
            
            [dicStrikeDetails setObject:currentPitchLabel forKey:@"pitchLabel"];
            
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
            
            [self pitchLabel:currentPitchLabel];
            
           // NSLog(@"%@",btn_loadstrikesenderid.titleLabel.text );
            [dicStrikeDetails setObject:lblbatterPitch.text forKey:@"lblbatterPitch"];
            
            [dicStrikeDetails setObject: @"o" forKey:@"lblOut"];
           
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            [self BallsCalculation];

            
        }
        
        lblbatterPitch.textColor = [UIColor whiteColor];
       // vw_OnBaseview.hidden=YES;
        
    }
    
    
    if ([sender tag] == 6)
    {
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
       
        
        if (btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lblbatterPitch.text = [NSString stringWithFormat:@""];
        }
        
        else
        {
            intOutPlyers++;
            //lblOut.text =[NSString stringWithFormat:@"%d",intOutPlyers];
            lblbatterPitch.text = [NSString stringWithFormat:@"Kc"];
            
             [self pitchLabel:currentPitchLabel];
            
            [dicStrikeDetails setObject:currentPitchLabel forKey:@"pitchLabel"];
           // [dicStrikeDetails setObject:btn_loadstrikesenderid.titleLabel.text forKey:@"pitchLabel"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
          //  NSLog(@"%@",btn_loadstrikesenderid.titleLabel.text );
            [dicStrikeDetails setObject:lblbatterPitch.text forKey:@"lblbatterPitch"];
              [dicStrikeDetails setObject: @"o" forKey:@"lblOut"];
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            [self BallsCalculation];

        }
        
        lblbatterPitch.textColor = [UIColor whiteColor];
       // vw_OnBaseview.hidden=YES;
        
    }
    
    
    if ([sender tag] ==7)/////strike out swing put down 
    {
        
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        
        
        if(btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lblbatterPitch.text = [NSString stringWithFormat:@""];
        }
        
        else
        {
            intOutPlyers++;
            
           //   [btn_loadstrikesenderid setTitle:[NSString stringWithFormat:@"%@S",buttonTitle] forState:UIControlStateNormal];
        //    lblOut.text =[NSString stringWithFormat:@"%d",intOutPlyers];
            lblbatterPitch.text = [NSString stringWithFormat:@"Ks2"];
            
            currentPitchLabelAppend =[NSString stringWithFormat:@"%@S",currentPitchLabel];
            
            [self pitchLabel:currentPitchLabelAppend];
            
            [dicStrikeDetails setObject:currentPitchLabelAppend forKey:@"pitchLabel"];
           // [dicStrikeDetails setObject:btn_loadstrikesenderid.titleLabel.text forKey:@"pitchLabel"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
           // NSLog(@"%@",btn_loadstrikesenderid.titleLabel.text );
            [dicStrikeDetails setObject:lblbatterPitch.text forKey:@"lblbatterPitch"];
             [dicStrikeDetails setObject: @"o" forKey:@"lblOut"];
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            [self BallsCalculation];

        }
        
        lblbatterPitch.textColor = [UIColor whiteColor];
        //vw_OnBaseview.hidden=YES;
    }
    
    if ([sender tag]==8)
    {
        
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
//        [lbl_jerseyNumber setText:nil];
//        [lbl_batterName setText:nil];
//        [lbl_RorL setText:nil];
        
        if(btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lblbatterPitch.text = [NSString stringWithFormat:@""];
        }
        
        else
        {
            intOutPlyers++;
            
              [btn_loadstrikesenderid setTitle:[NSString stringWithFormat:@"%@C",buttonTitle] forState:UIControlStateNormal];
            
          //  lblOut.text =[NSString stringWithFormat:@"%d",intOutPlyers];
            lblbatterPitch.text = [NSString stringWithFormat:@"Kc2"];
            
            currentPitchLabelAppend =[NSString stringWithFormat:@"%@C",currentPitchLabel];
            
            [self pitchLabel:currentPitchLabelAppend];
            
            [dicStrikeDetails setObject:currentPitchLabelAppend forKey:@"pitchLabel"];
            //[dicStrikeDetails setObject:btn_loadstrikesenderid.titleLabel.text forKey:@"pitchLabel"];
            [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
            [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
           // NSLog(@"%@",btn_loadstrikesenderid.titleLabel.text );
            [dicStrikeDetails setObject:lblbatterPitch.text forKey:@"lblbatterPitch"];
            [dicStrikeDetails setObject:@"o" forKey:@"lblOut"];
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
            NSLog(@"%@",dicMainPitch);
            [self strikeBallReplacement];
            NSLog(@"%@",dicMainPitch);
            [self BallsCalculation];

        }
        
        lblbatterPitch.textColor = [UIColor whiteColor];
       // vw_OnBaseview.hidden=YES;
        
    }
    
    if ([sender tag]==9)
    {
        
       // type = @"EoiAlert";
//        bl_alertPopup=TRUE;
//        
//        message=@"Did This Inning Represent a Shutdown Opportunity For The Pitcher" ;
//        [self alertMessageMethod:message];
        
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        lblbatterPitch.text=@"";
        
       imgv_eoi = [[UIImageView alloc] 
                    initWithImage:[UIImage imageNamed:@"Eoi_shape.png"]];
        [lblbatterPitch addSubview:imgv_eoi];
        lblbatterPitch.backgroundColor = [UIColor clearColor];
        
        
        
        [dicStrikeDetails setObject: labelCatcher.text  forKey:@"Catcher"];
        [dicStrikeDetails setObject: labelPitcher.text  forKey:@"Pitcher"];
        [dicStrikeDetails setObject:btn_loadstrikesenderid.titleLabel.text forKey:@"pitchLabel"];
        
        NSLog(@"%@",btn_loadstrikesenderid.titleLabel.text );
        [dicStrikeDetails setObject:@"Eoi_shape.png" forKey:@"lblbatterPitch"];
        
        stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
        stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
        [stringKeyMainPitch appendString:stringKeyPitch];
        
        [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
        NSLog(@"%@",dicMainPitch);
        [self strikeBallReplacement];
        NSLog(@"%@",dicMainPitch);
        [self BallsCalculation];

      
        
       // vw_OnBaseview.hidden=YES;
        
    }
    
    
//    if ([sender tag]==10)
//    {
//        
//        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
//        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
//        
//        //vw_OnBaseview.hidden=YES;
//        
//    }

    
    
    
    
    
//    if (sender.tag==9) {
//        alertMessage=@"Did this inning represent a shutdown opportunity for the Pitcher";
//        [self alertMessageMethod:alertMessage];
//    }
    [view_strikeOutView removeFromSuperview];
    [btn_loadstrikesenderid setBackgroundColor:[UIColor clearColor]];
}

-(IBAction)backToStrikeTypeOfPitch:(id)sender
{    
    [view_StrikeView setHidden:YES];
    [self bringScrollSubViewToFrontOnMainView:view_strikeTypeOfPitch];
}
-(IBAction)backToStrikeOptionsView:(id)sender
{
    [view_strikeOutView setHidden:YES];
    [self bringScrollSubViewToFrontOnMainView:view_StrikeView];
}

-(IBAction)gridButtonAction:(id)sender
{
    NSLog(@"Clicked button tag :%d ",[sender tag]);
    
    if ([sender backgroundColor]==[UIColor blueColor]) {
        
        [sender setBackgroundColor:[UIColor clearColor]];
    }
    
    else
        [sender setBackgroundColor:[UIColor blueColor]];

}

-(void) ERAExitMethod:(id) sender
{
    alertMessage=@"Are you sure you want to exit from ERA App";
    [self alertMessageMethod:alertMessage];
    alertMessageType= @"ExitFromApp";
}


-(void)alertMessageMethod:(NSString *)Message
{
    if ([Message isEqualToString:@"Did this inning represent a shutdown opportunity for the Pitcher" ]||[Message isEqualToString:@"Do you want to save this data"]||[Message isEqualToString:@"Are you sure you want to exit from ERA App" ]||[Message isEqualToString:@"“Was a shutdown accomplished?” "                                                                                                                                                                                                                                  ] )
    {
        UIAlertView *alrt_msg = [[UIAlertView alloc]initWithTitle:@"Message!" message:Message delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        [alrt_msg show];
    }
    else
    if ([Message isEqualToString:@"Please enter numbers only in the Jersey field."]||[Message isEqualToString:@"Please fill jersery number and handed"] || [Message isEqualToString:@"JerseyNumber already exists."]) {
         
        alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:Message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];

    }
    else if([Message isEqualToString:@"End of Inning?"])
    {
        UIAlertView *alrt_msg = [[UIAlertView alloc]initWithTitle:@"Message!" message:Message delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:nil, nil];
        [alrt_msg show];
        
    }
    
}

#pragma mark alert delegate 
-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0) {
        
        
        
        
       if( alertMessageType==@"AddBatter")
       {
           [self popAddPlayer];
           alertMessageType=@"";
           return;
           
       }
        
        
        
        if (alertMessageType==@"End of Inning?") {
           
            [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
            [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
           // lblbatterPitch.text=@"";
              imageEndOfInning.hidden=NO;
//            imgv_eoi = [[UIImageView alloc]
//                        initWithImage:[UIImage imageNamed:@"Eoi_shape.png"]];
//            [lblbatterPitch addSubview:imgv_eoi];
//            [imgv_eoi release];
            lblbatterPitch.backgroundColor = [UIColor clearColor];
            
            
            [self endofInning];
            alertMessageType=@"";

        }
        else
           
           [buttonHandedCP setTitle:@"L" forState:UIControlStateNormal];
           [buttonHandedBatter setTitle:@"L" forState:UIControlStateNormal];
    }
else
{
    if (alertMessageType==@"End of Inning?")
    
    {
        
        
    }
    
    else
    
          [buttonHandedCP setTitle:@"R" forState:UIControlStateNormal];
       [buttonHandedBatter setTitle:@"R" forState:UIControlStateNormal];
}
    
    
    
    
}


#pragma mark -----------------popAddPlayer
-(void)popAddPlayer
{
    
    namelbl_batterView.text=@"";
    numberlbl_batterView.text=@"";
    //[self resetMainViewSettings:displayView];
    
    
    if([batter_detailsArray count]>0)
   {
          batterInfoTag=[batter_detailsArray count] +1; 
   }

   
    
    buttonSelectPlayer.hidden=NO;
    maskView.hidden=NO;
    view_Players.hidden =YES;
    editBtn.hidden =NO;
    NSLog(@"%s",__func__);
 
    NSLog(@"batterdetails :  %@",batter_detailsArray);

    
    [displayView removeFromSuperview];
    [batterInfo_View removeFromSuperview];
    //selectedBtnFrame=sender.frame;
    //[batterInfo_View setFrame:sender.frame];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    [batterInfo_View addSubview:maskView];
    
    if (mode==YES)
        [batterInfo_View setFrame:CGRectMake(scrollView.center.x-350, scrollView.center.y-350, 410, 429)];
    else
        [batterInfo_View setFrame:CGRectMake(scrollView.center.x-300, scrollView.center.y-395, 410, 429)];
    
    
    [UIView commitAnimations];
    [self.view addSubview:batterInfo_View];
    scrollView.alpha=0.3;
    batter_scrollView.alpha=0.3;
    scrollViewLandscape.alpha=0.3;
    batter_scrollViewLandscape.alpha=0.3;
    [mask_scrollView setHidden:YES];
    [scrollView setUserInteractionEnabled:NO];
    [batter_scrollView setUserInteractionEnabled:NO];
    
    
        editBtn.hidden=YES;
        labelEdit.hidden=YES;
        buttonHanded.hidden=YES;
        buttonAddPlayerRoster.hidden=NO;
        labelAddplayer.hidden=NO;
    
    
    
}

#pragma mark end of innning recording

-(void)endofInning
{
    intEndOfInning++;
    
    NSString *strInnging =[NSString stringWithFormat:@"%d",intEndOfInning];

   
    NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
   // NSMutableString * stringtag=[NSMutableString stringWithFormat:@"PlayerBoxTag%d",mainGridTag];
    
  
        
        [dicInnings setObject:strInnging forKey:string];
        boolTwoATBatRecord=YES;

   
    //[dicInnings setObject:(NSString *)mainGridTag forKey:stringtag];

    
    
}

#pragma mark -------------------------------------------ibaction for settings

-(IBAction)CancelSettingTable:(id)sender
{
    
    //[table_displayOptions removeFromSuperview];
    [view_defensivePositionsView removeFromSuperview];
}

-(IBAction)selectOppentTeam:(id)sender{
 
   // type=@"Settings";
     [view_defensivePositionsView setHidden:NO];
   
    type=@"OppnentTeam";
    
    [teamNames_Array removeAllObjects];
    teamNames_Array =[DBObj selectPlayerstFromDb];
   // [imageTitleSettings setFrame:CGRectMake(0, 0, 169, 34)];
     //[labelTitleSettings setFont:[UIFont systemFontOfSize:20]];
    labelTitleSettings.text=@"Oppnent Team";
    [view_defensivePositionsView setFrame:CGRectMake(31,116,190,250)];
       
    [viewSetteings addSubview:view_defensivePositionsView];
   [table_displayOptions reloadData];
}

-(IBAction)selectCatcher:(id)sender;{
     
      [view_defensivePositionsView setHidden:NO];
      buttonAddCP.hidden=NO;
     //type=@"Settings";
    //[imageTitleSettings setFrame:CGRectMake(0, 0, 3, 34)];
    
      type=@"Catcher";
    labelTitleSettings.text=@"Catcher";
   [array_catcher removeAllObjects];
    array_catcher =[DBObj selectHomeTeamPlayerstFromDb:@"C"];
    [view_defensivePositionsView setFrame:CGRectMake(0,0,190,316)];
    
    
    
      [table_displayOptions reloadData];
    
    
    
    if(popoverController)
    {
        [popoverController dismissPopoverAnimated:YES];
        
        
    }
    
    
    UIViewController* popoverViewController = [[UIViewController alloc]init];
    [popoverViewController.view addSubview:view_defensivePositionsView];
    popoverViewController.view.backgroundColor = [UIColor blackColor];
	popoverViewController.contentSizeForViewInPopover =	CGSizeMake(190, 316);
    popoverController = [[UIPopoverController alloc]initWithContentViewController:popoverViewController];
    
    popoverController.delegate = self;
    [popoverController setPopoverContentSize:CGSizeMake(190, 316)];
    
       [popoverController presentPopoverFromBarButtonItem:buttonCatcher permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    //[popoverController presentPopoverFromRect:buttonCatcher.frame inView:btnBack.superview permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];

   // [viewSetteings addSubview:view_defensivePositionsView];
     
}
-(IBAction)selectPitcher:(id)sender{
     type=@"Pitcher";
      [view_defensivePositionsView setHidden:NO];
    buttonAddCP.hidden=NO;
    labelTitleSettings.text=@"Pitcher"; 
    type=@"Pitcher";
      [array_pitcher removeAllObjects];
    array_pitcher =[DBObj selectHomeTeamPlayerstFromDb:@"P"];
    
    [view_defensivePositionsView setFrame:CGRectMake(0,0,190,316)];
    //[viewSetteings addSubview:view_defensivePositionsView];
       [table_displayOptions reloadData];
    
    
    [table_displayOptions reloadData];
    
    
    
    if(popoverController)
    {
        [popoverController dismissPopoverAnimated:YES];
        
        
    }
    
    
    UIViewController* popoverViewController = [[UIViewController alloc]init];
    [popoverViewController.view addSubview:view_defensivePositionsView];
    popoverViewController.view.backgroundColor = [UIColor blackColor];
	popoverViewController.contentSizeForViewInPopover =	CGSizeMake(190, 316);
    popoverController = [[UIPopoverController alloc]initWithContentViewController:popoverViewController];
    
    popoverController.delegate = self;
    [popoverController setPopoverContentSize:CGSizeMake(190, 316)];
    
    [popoverController presentPopoverFromBarButtonItem:buttonPitcher permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
    
}

#pragma mark SelectOppentPlayer

-(IBAction)SelectOppentPlayer
{
    jerseyNOMatch=1;
    
    view_defensivePositionsView.hidden=NO;
    buttonHandedBatter.hidden=YES;
    buttonAddCP.hidden=YES;

    maskView.hidden=YES;
    view_Players.hidden =NO;
    
    
    [self DisplayOpponentPlayer];
 
     NSLog(@"%@",batter_detailsArray);
     NSLog(@"%@",arrayOppentplayer);
    
    
    [view_defensivePositionsView setFrame:CGRectMake(40,25,190,220)];
    [view_Players addSubview:view_defensivePositionsView];
    [table_displayOptions reloadData];
    
}

-(void)DisplayOpponentPlayer
{
    [arrayOppentplayer removeAllObjects];
    [arrayOppentplayer1 removeAllObjects];
    type=@"OppentPlayers";
    labelTitleSettings.text=@"PLayers";
    arrayOppentplayer1=[DBObj selectTeamPlayerstFromDb:labelSettingsOpponent.text];
    
    
    
    
    
    NSLog(@"%@",arrayOppentplayer);
    NSLog(@"%@",batter_detailsArray);
    
    
    for(int i=0 ;i<[arrayOppentplayer1 count];i++)
    {
        NSLog(@"%d",i);
        BOOL foundDuplicate=NO;
        
        for (int j=0;j<[batter_detailsArray count];j++ )
        {
            
            
            
            if([[[batter_detailsArray objectAtIndex:j]objectForKey:@"JerseyNumber"] isEqualToString:[[arrayOppentplayer1 objectAtIndex:i]objectForKey:@"JerseyNumber"]])
            {
                
                
                foundDuplicate=YES;
                break;
                
            }
            
            
            
            NSLog(@"%@",arrayOppentplayer);
            
            
        }
        
        if (foundDuplicate==NO) {
            [arrayOppentplayer addObject:[arrayOppentplayer1 objectAtIndex:i]];
        }
    }
    
}


#pragma mark cancelPlayer
-(IBAction)cancelPlayer
{
    buttonHandedBatter.hidden=YES;
        view_Players.hidden =YES;
    maskView.hidden=NO;
    batterInfo_View.hidden=NO;
      buttonAddCP.hidden=NO;
    //[view_Players removeFromSuperview];
}


#pragma mark //////------------------------AddPLayerOnRoster
-(IBAction)AddPLayerOnRoster
{
    //maskView.hidden=YES;
    type=@"OppentAddPlayer";
  
    
    viewAddPlayerCP.hidden=NO;
    if(mode==YES)
    {
           [viewAddPlayerCP setFrame:CGRectMake(scrollView.center.x-340, scrollView.center.y-260, 379, 269)];
    }
    else
    {
           [viewAddPlayerCP setFrame:CGRectMake(470, scrollView.center.y-340, 379, 269)];
    }
    
 
    [self.view addSubview:viewAddPlayerCP];
    jerseyNumberCP.text=@"";
    playerNameCp.text=@"";
    [buttonHandedCP setTitle:@"" forState:UIControlStateNormal];
}



# pragma mark tableView Delegate Methods...

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     scrollViewIdentification=NO;
    
    if(type==@"OppnentTeam")
    {
          return [teamNames_Array count];
    }
     if (type==@"Catcher") {
          return [array_catcher count];
    }
    if (type==@"Pitcher") {
          return [array_pitcher count];
    }
    if (type==@"OppentPlayers") {
        return [arrayOppentplayer count];
    }
    if (type==@"Others")
 
    {
          return [Array_defensivePositionsArray count];
    }
    

}

-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier] ;
    //if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];

    if(type==@"OppnentTeam")
    {
       
        
        cell.textLabel.text=[[teamNames_Array objectAtIndex:indexPath.row]objectForKey:@"TeamName"]; 
              
       
        

    }
    else if (type==@"Catcher") {
       
        
        
        cell.textLabel.text=[[array_catcher objectAtIndex:indexPath.row]objectForKey:@"PlayerName"]; 
      
        
 
    }
    else if (type==@"Pitcher") {
        
        cell.textLabel.text=[[array_pitcher objectAtIndex:indexPath.row]objectForKey:@"PlayerName"];
     
        
  
    }
    else if (type==@"OppentPlayers") {
        
        cell.textLabel.text=[[arrayOppentplayer objectAtIndex:indexPath.row]objectForKey:@"PlayerName"];
        
        
        
    }
    else if (type==@"Others") 
    {
        
       cell.textLabel.text=[Array_defensivePositionsArray objectAtIndex:indexPath.row];
        
       
    }

    tableView.separatorColor=[UIColor clearColor];
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];

    return cell;
    

}
#pragma mark Table last subview
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    
    NSString *string;
    
    imageView_locatiogGraph.hidden=NO;
    
     if (type==@"OppentPlayers") {
         
         
         buttonMonitor.enabled=YES;
         buttonPitcher.enabled=YES;
         buttonCatcher.enabled=YES;
         
         if([batter_detailsArray count]-1<batterInfoTag||[batter_detailsArray count]==0)
         {
             
             [batter_detailsArray addObject:[arrayOppentplayer objectAtIndex:indexPath.row]]; 
             
         }
         else {
        
             
             [batter_detailsArray replaceObjectAtIndex:batterInfoTag withObject:[arrayOppentplayer objectAtIndex:indexPath.row]];
             
         }
         
         view_Players.hidden =YES;
         maskView.hidden=NO;
         batterInfo_View.hidden=NO;
         
         NSLog(@"%@",arrayOppentplayer);

         
       numberlbl_batterView.text =  [[arrayOppentplayer objectAtIndex:indexPath.row]objectForKey:@"PlayerName"];
       namelbl_batterView.text =  [[arrayOppentplayer objectAtIndex:indexPath.row]objectForKey:@"JerseyNumber"];

        

          
      

           [self CLoseBatterMainWindow];
        // [self resetMainViewSettings:batterInfo_View];
         
       //  [player release];
         return;
     }
    
    if(type==@"OppnentTeam")
    {
        buttonMonitor.enabled=YES;
        buttonPitcher.enabled=YES;
        buttonCatcher.enabled=YES;
    
        labelSettingsOpponent.text=[[teamNames_Array objectAtIndex:indexPath.row]objectForKey:@"TeamName"]; 
        
        
        baterInfo_Array=[[NSMutableArray alloc]init];
        for (int i=0; i<9; i++) {
            NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
            [baterInfo_Array addObject:dict];
        }
        
        [batter_detailsArray removeAllObjects];
        batter_detailsArray=[DBObj selectTeamPlayerstFromDb:labelSettingsOpponent.text];
        int k;
        if([batter_detailsArray count]>9)
        {
            k=9;
        }
        else
        {
            k=[batter_detailsArray count];
        }
        for (int  j=0; j<k; j++) {
            
            [baterInfo_Array replaceObjectAtIndex:j withObject:[batter_detailsArray objectAtIndex:j]];
        }
        
        if ([baterInfo_Array count]==0) {
            
        }
        NSLog(@"Batter_details Array : %@",batter_detailsArray);
        NSLog(@"Batter_details Array : %@",baterInfo_Array);
        
        //[detailsDBObj release];
        
        for (int j=0; j<9;j++) {
            
            
            
            
            UIButton *button = [mArray3 objectAtIndex:j];
            for (UIView *view in button.subviews) {
                if ([view isMemberOfClass:[UILabel class]]) {
                    UILabel *templbl=(UILabel *)view;
                    switch (templbl.tag) {
                            
                        case 0:
                            [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"PlayerName"]];
                            break;
                        case 1:
                            [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"JerseyNumber"]];
                            break;
                        case 2:
                            [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"Handling"]];
                            break;
                            
                        default:
                            break;
                            
                    }
                    NSLog(@"Temp Label Tag : %d",templbl.tag);
                }
            }
            
            
            
            
            //Landscape
            
            UIButton *buttonLandscape = [mArrayLandscape3 objectAtIndex:j];
            for (UIView *view in buttonLandscape.subviews) {
                if ([view isMemberOfClass:[UILabel class]]) {
                    UILabel *templbl=(UILabel *)view;
                    switch (templbl.tag) {
                            
                        case 0:
                            [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"PlayerName"]];
                            break;
                        case 1:
                            [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"JerseyNumber"]];
                            break;
                        case 2:
                            [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"Handling"]];
                            break;
                            
                        default:
                            break;
                            
                    }
                    NSLog(@"Temp Label Tag : %d",templbl.tag);
                }
                
                
                
            }//end

            
            
        }
        

        
        
        
        
        [view_defensivePositionsView removeFromSuperview];
        
        type=@"Others";
       return;
    
    }
    
    if(type==@"Pitcher")
    {
        buttonMonitor.enabled=YES;
        buttonPitcher.enabled=YES;
        buttonCatcher.enabled=YES;
        labelPitcher.text =[[array_pitcher objectAtIndex:indexPath.row]objectForKey:@"PlayerName"];
            [view_defensivePositionsView removeFromSuperview];
         [popoverController dismissPopoverAnimated:YES];
        type=@"Others";
           return;
        
    }if(type==@"Catcher")
    {
        buttonMonitor.enabled=YES;
        buttonPitcher.enabled=YES;
        buttonCatcher.enabled=YES;
        labelCatcher.text =[[array_catcher objectAtIndex:indexPath.row]objectForKey:@"PlayerName"];
            [view_defensivePositionsView removeFromSuperview];
         [popoverController dismissPopoverAnimated:YES];
        type=@"Others";
        return;
        
    }
    
    
    
    
    UIImage *img_fieldLocation=[[UIImage alloc]init];
     
    // Out Come Of At Bat ball hit position.....
    NSLog(@"outComeOfBat Tag:  %d",tag_outComeOfAtBat); 
    
    
    if(tag_outComeOfAtBat==6)
    {
        switch (indexPath.row)
        {
                
            case 0:
                img_fieldLocation = [UIImage imageNamed:@"Bpitcher.png"];
                
                fieldImage=@"Bpitcher.png";
                
                break;
                
            case 1:
                img_fieldLocation = [UIImage imageNamed:@"Bcatcher.png"];
                fieldImage=@"Bcatcher.png";
                break;
                
            case 2:
                img_fieldLocation = [UIImage imageNamed:@"B1baseman.png"];
                
                fieldImage=@"B1baseman.png";
                break;
                
            case 3:
                img_fieldLocation = [UIImage imageNamed:@"B2baseman.png"];
                fieldImage=@"B2baseman.png";
                break;
                
            case 4:
                img_fieldLocation = [UIImage imageNamed:@"B3baseman.png"];
                fieldImage=@"B3baseman.png";
                break;
                
            case 5:
                img_fieldLocation= [UIImage imageNamed:@"Bshprtstop.png"];
                fieldImage=@"Bshprtstop.png";
                break;
                
            case 6:
                img_fieldLocation = [UIImage imageNamed:@"Bleftfield.png"];
                fieldImage=@"Bleftfield.png";
                break;
                
            case 7:
                img_fieldLocation = [UIImage imageNamed:@"BcenterField.png"];
                fieldImage=@"BcenterField.png";
                break;
                
            case 8:
                img_fieldLocation = [UIImage imageNamed:@"Bleftfield.png"];
                fieldImage=@"Bleftfield.png";
                break;
                
            default:
                break;
                
        }
        
        //[dict_oneinnngsData setObject:img_fieldLocation forKey:@"hitFairgroundball"];
    }    
    
    else
        if (tag_outComeOfAtBat==7)
        {
            switch (indexPath.row)
            {
                case 0:
                {
                    img_fieldLocation = [UIImage imageNamed:@"Bfpitcher.png"];
                    
                    fieldImage=@"Bfpitcher.png";
                    break;
                }
                case 1:
                {
                    img_fieldLocation = [UIImage imageNamed:@"Bfcatcher.png"];
                    fieldImage=@"Bfcatcher.png";
                    break;
                }
                case 2:
                {
                    img_fieldLocation = [UIImage imageNamed:@"Bf1baseman.png"];
                    fieldImage=@"Bf1baseman.png";
                    break;
                }
                case 3:
                {
                    img_fieldLocation = [UIImage imageNamed:@"Bf2baseman.png"];
                    fieldImage=@"Bf2baseman.png";
                    break;
                }
                case 4:
                {
                    img_fieldLocation = [UIImage imageNamed:@"Bf3baseman.png"];
                    fieldImage=@"Bf3baseman.png";
                    break;
                }
                case 5:
                {
                    img_fieldLocation = [UIImage imageNamed:@"Bfshortstop.png"];
                    fieldImage=@"Bfshortstop.png";
                    break;
                }
                case 6:
                {
                    img_fieldLocation = [UIImage imageNamed:@"Bfleftfield.png"];
                    fieldImage=@"Bfleftfield.png";
                    break;
                }
                case 7:
                {
                    img_fieldLocation = [UIImage imageNamed:@"Bfcenterfield.png"];
                    fieldImage=@"Bfcenterfield.png";
                    break;
                }
                case 8:
                {
                    img_fieldLocation = [UIImage imageNamed:@"Bfrightfield.png"];
                    fieldImage=@"Bfrightfield.png";
                    break;
                }
                    
                default:
                    break;
            }
            //[dict_oneinnngsData setObject:img_fieldLocation forKey:@"hitFairgroundDoubleplay"]; 
            
        }
    
        else
            if(tag_outComeOfAtBat==8) 
            {
                switch (indexPath.row)
                {
                    case 0:
                    {
                        img_fieldLocation = [UIImage imageNamed:@"Gpitcher.png"];
                        fieldImage=@"Gpitcher.png";
                        break;
                    }
                    case 1:
                    {
                        img_fieldLocation = [UIImage imageNamed:@"Gcatcher.png"];
                        fieldImage=@"Gcatcher.png";
                        break;
                    }
                    case 2:
                    {
                        img_fieldLocation = [UIImage imageNamed:@"G1baseman.png"];
                        fieldImage=@"G1baseman.png";
                        break;
                    }
                    case 3:
                    {
                        img_fieldLocation = [UIImage imageNamed:@"G2baseman.png"];
                        fieldImage=@"G2baseman.png";
                        break;
                    }
                    case 4:
                    {
                        img_fieldLocation = [UIImage imageNamed:@"G3baseman.png"];
                        fieldImage=@"G3baseman.png";
                        break;
                    }
                    case 5:
                    {
                        img_fieldLocation = [UIImage imageNamed:@"Gshortstop.png"];
                        fieldImage=@"Gshortstop.png";
                        break;
                    }
                    case 6:
                    {
                        img_fieldLocation = [UIImage imageNamed:@"Gleftfield.png"];
                        fieldImage=@"Gleftfield.png";
                        break;
                    }
                    case 7:
                    {
                        img_fieldLocation = [UIImage imageNamed:@"GcenterField.png"];
                        fieldImage=@"GcenterField.png";
                        break;
                    }
                    case 8:
                    {
                        img_fieldLocation = [UIImage imageNamed:@"GrightField.png"];
                        fieldImage=@"GrightField.png";
                        break;
                    }
                    default:
                        break;
                }
                
               // [dict_oneinnngsData setObject:img_fieldLocation forKey:@"hitFairgroundTripleplay"];
            }
    
            else
                if (tag_outComeOfAtBat==9)
                {
                    switch(indexPath.row)
                    {
                        case 0:
                        {
                            img_fieldLocation = [UIImage imageNamed:@"Gpitcher.png"];
                            fieldImage=@"Gpitcher.png";
                            break;
                        }
                        case 1:
                        {
                            img_fieldLocation = [UIImage imageNamed:@"Gcatcher.png"];
                            fieldImage=@"Gcatcher.png";
                            break;
                        }
                        case 2:
                        {
                            img_fieldLocation = [UIImage imageNamed:@"G1baseman.png"];
                            fieldImage=@"G1baseman.png";
                            break;
                        }
                        case 3:
                        {
                            img_fieldLocation = [UIImage imageNamed:@"G2baseman.png"];
                            fieldImage=@"G2baseman.png";
                            break;
                        }
                        case 4:
                        {
                            img_fieldLocation = [UIImage imageNamed:@"G3baseman.png"];
                            fieldImage=@"G3baseman.png";
                            break;
                        }
                        case 5:
                        {
                            img_fieldLocation = [UIImage imageNamed:@"Gshortstop.png"];
                            fieldImage=@"Gshortstop.png";
                            break;
                        }
                        case 6:
                        {
                            img_fieldLocation = [UIImage imageNamed:@"Gleftfield.png"];
                            fieldImage=@"GrightField.png";
                            break;
                        }
                        case 7:
                        {
                            img_fieldLocation = [UIImage imageNamed:@"GcenterField.png"];
                            fieldImage=@"GrightField.png";
                            break;
                        }
                        case 8:
                        {
                            img_fieldLocation = [UIImage imageNamed:@"GrightField.png"];
                            fieldImage=@"GrightField.png";
                            break;
                        }
                        default:
                            break;
                    }
                    // [dict_oneinnngsData setObject:img_fieldLocation forKey:@"hitFairgroundSacrifiesbunt"];
                    
                }
    
                else
                    if (tag_outComeOfAtBat==10) 
                    {
                        switch (indexPath.row)
                        {
                            case 0:
                            {
                                img_fieldLocation = [UIImage imageNamed:@"Gpitcher.png"];
                                fieldImage=@"Gpitcher.png";
                                break;
                            }
                            case 1:
                            {
                                img_fieldLocation = [UIImage imageNamed:@"Gcatcher.png"];
                                fieldImage=@"Gcatcher.png";
                                break;
                            }
                            case 2:
                            {
                                img_fieldLocation = [UIImage imageNamed:@"G2baseman.png"];
                                fieldImage=@"G2baseman.png";
                                break;
                            }
                            case 3:
                            {
                                img_fieldLocation = [UIImage imageNamed:@"G2baseman.png"];
                                fieldImage=@"G2baseman.png";
                                break;
                            }
                            case 4:
                            {
                                img_fieldLocation = [UIImage imageNamed:@"G3baseman.png"];
                                fieldImage=@"G3baseman.png";
                                break;
                            }
                            case 5:
                            {
                                img_fieldLocation= [UIImage imageNamed:@"Gshortstop.png"];
                                fieldImage=@"Gshortstop.png";
                                break;
                            }
                            case 6:
                            {
                                img_fieldLocation = [UIImage imageNamed:@"Gleftfield.png"];
                                fieldImage=@"Gleftfield.png";
                                break;
                            }
                            case 7:
                            {
                                img_fieldLocation = [UIImage imageNamed:@"GcenterField.png"];
                                fieldImage=@"Bpitcher.png";
                                break;
                            }
                            case 8:
                            {
                                img_fieldLocation = [UIImage imageNamed:@"GrightField.png"];
                                fieldImage=@"GrightField.png";
                                break;
                            }
                                
                            default:
                                break;
                        }
                        // [dict_oneinnngsData setObject:img_fieldLocation forKey:@"hitFairgroundFlyieBall"];
                        
                    }
    
                    else
                        if (tag_outComeOfAtBat==11) ////sac bunt hint 
                        {
                            switch (indexPath.row)
                            {
                                case 0:
                                {    img_fieldLocation = [UIImage imageNamed:@"Spitcher.png"];
                                    fieldImage=@"Spitcher.png";
                                    break;
                                }
                                case 1:
                                {
                                    img_fieldLocation = [UIImage imageNamed:@"Scatcher.png"]; 
                                    fieldImage=@"Scatcher.png";
                                    break;
                                }
                                case 2:
                                {
                                    img_fieldLocation = [UIImage imageNamed:@"S1baseman.png"]; 
                                    fieldImage=@"S1baseman.png";
                                    break;
                                }
                                case 3:
                                { img_fieldLocation = [UIImage imageNamed:@"s2baseman.png"];
                                    fieldImage=@"s2baseman.png";
                                    break;
                                }
                                case 4:
                                { img_fieldLocation = [UIImage imageNamed:@"S3baseman.png"];
                                    fieldImage=@"S3baseman.png";
                                    break;
                                }
                                case 5:
                                { img_fieldLocation = [UIImage imageNamed:@"Sshortstop.png"];
                                    fieldImage=@"Sshortstop.png";
                                    break;
                                }
                                case 6:
                                { img_fieldLocation = [UIImage imageNamed:@"SleftField.png"]; 
                                  
                                    fieldImage=@"SleftField.png";               
                                    break;
                                }
                                case 7:
                                { img_fieldLocation = [UIImage imageNamed:@"ScenterField.png"];
                                    fieldImage=@"ScenterField.png";
                                    
                                    break;
                                }
                                case 8:
                                { img_fieldLocation = [UIImage imageNamed:@"Srightfield.png"];
                                    fieldImage=@"Srightfield.png";
                                    break;
                                    
                                }
                                default:
                                    break;
                            }
                            //[dict_oneinnngsData setObject:img_fieldLocation forKey:@"hitFairgroundsacriefiesflybal"];
                            
                        } 
    
                        else
                            if(tag_outComeOfAtBat==12) 
                            {
                                switch (indexPath.row)
                                {
                                    case 0:
                                    {
                                        img_fieldLocation = [UIImage imageNamed:@"Fpitcher.png"];
                                        fieldImage=@"Fpitcher.png";
                                        break;
                                    }
                                    case 1:
                                    {
                                        img_fieldLocation = [UIImage imageNamed:@"Fcather.png"];
                                        fieldImage=@"Fcather.png";
                                        break;
                                    }
                                    case 2:
                                    {
                                        img_fieldLocation = [UIImage imageNamed:@"F1baseman.png"];
                                        fieldImage=@"F1baseman.png";
                                        break;
                                    }
                                    case 3:
                                    {
                                        img_fieldLocation = [UIImage imageNamed:@"F2baseman.png"];
                                        fieldImage=@"F2baseman.png";
                                        break;
                                    }
                                    case 4:
                                    {
                                        img_fieldLocation = [UIImage imageNamed:@"F3baseman.png"];
                                        fieldImage=@"F3baseman.png";
                                        break;
                                    }
                                    case 5:
                                    {
                                        img_fieldLocation = [UIImage imageNamed:@"Fshortstop.png"];
                                        fieldImage=@"Fshortstop.png";
                                        break;
                                    }
                                    case 6:
                                    {
                                        img_fieldLocation = [UIImage imageNamed:@"Fleftfield.png"];
                                        fieldImage=@"Fleftfield.png";
                                        break;
                                    }
                                    case 7:
                                    {
                                        img_fieldLocation = [UIImage imageNamed:@"Fcenterfield.png"];
                                        fieldImage=@"Fcenterfield.png";
                                        break;
                                    }
                                    case 8:
                                    {
                                        img_fieldLocation = [UIImage imageNamed:@"Frightfield.png"];
                                        fieldImage=@"Frightfield.png";
                                        break;
                                    }
                                    default:
                                        break;
                                }
                            } 
    
                            else
                                if (tag_outComeOfAtBat==13) 
                                {
                                    switch (indexPath.row)
                                    {
                                        case 0:
                                        {
                                            img_fieldLocation = [UIImage imageNamed:@"Spitcher.png"];
                                            fieldImage=@"Spitcher.png";
                                            break;
                                        }
                                        case 1:
                                        {
                                            img_fieldLocation = [UIImage imageNamed:@"Scatcher.png"];
                                            fieldImage=@"Scatcher.png";
                                            break;
                                        }
                                        case 2:
                                        {
                                            img_fieldLocation = [UIImage imageNamed:@"S1baseman.png"];
                                            fieldImage=@"S1baseman.png";
                                            break;
                                        }
                                        case 3:
                                        {
                                            img_fieldLocation = [UIImage imageNamed:@"S2baseman.png"];
                                            fieldImage=@"S2baseman.png";
                                            break;
                                        }
                                        case 4:
                                        {
                                            img_fieldLocation = [UIImage imageNamed:@"S3baseman.png"];
                                            fieldImage=@"S3baseman.png";
                                            break;
                                        }
                                        case 5:
                                        {
                                            img_fieldLocation = [UIImage imageNamed:@"Sshortstop.png"];
                                            fieldImage=@"Sshortstop.png";
                                            break;
                                        }
                                        case 6:
                                        {
                                            img_fieldLocation = [UIImage imageNamed:@"SleftField.png"];
                                            
                                            fieldImage=@"SleftField.png";
                                            break;
                                        }
                                        case 7:
                                        {
                                            img_fieldLocation = [UIImage imageNamed:@"ScenterField.png"];
                                            break;
                                            fieldImage=@"ScenterField.png";
                                        }
                                        case 8:
                                        {
                                            img_fieldLocation = [UIImage imageNamed:@"Srightfield.png"];
                                            fieldImage=@"Srightfield.png";
                                            break;
                                        }
                                        default:
                                            break;
                                    }
                                }
                                else
        
    NSLog(@"%s",__func__);
    [imageView_locatiogGraph setImage:img_fieldLocation];
    [btn_loadstrikesenderid setBackgroundColor:[UIColor clearColor]];
    [view_hitFairView removeFromSuperview];
    [view_defensivePositionsView removeFromSuperview];
    [btn_hitfaircheckbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"] forState:UIControlStateNormal];
    
    if(intAdvanceTag==0)
    {
        intAdvanceTag =1;
        
        
    }
    else{
        
       [dicStrikeDetails setObject:fieldImage forKey:@"StrikerFieldImage"]; 
    }
    
    
    if(indexPath.row==0)
    {
      string =  [lblbatterPitch.text  stringByAppendingFormat:@"1"];
    }
    if(indexPath.row==1)
    {
         string = [lblbatterPitch.text  stringByAppendingFormat:@"2"];
        
    }
    if(indexPath.row==2)
    {
         string = [lblbatterPitch.text  stringByAppendingFormat:@"3"];
    }
    if(indexPath.row==3)
    {
         string = [lblbatterPitch.text  stringByAppendingFormat:@"4"];
    }
    if(indexPath.row==4)
    {
         string = [lblbatterPitch.text  stringByAppendingFormat:@"5"];
    }
    if(indexPath.row==5)
    {
         string = [lblbatterPitch.text  stringByAppendingFormat:@"6"];
    }
    
    
    if(indexPath.row==6)
    {
        string =  [lblbatterPitch.text  stringByAppendingFormat:@"7"];
    }
    if(indexPath.row==7)
    {
         string = [lblbatterPitch.text  stringByAppendingFormat:@"8"];
    }
    if(indexPath.row==8)
    {
         string = [lblbatterPitch.text  stringByAppendingFormat:@"9"];
    }
    
    lblbatterPitch.text =string;
     [dicStrikeDetails setObject:lblbatterPitch.text forKey:@"lblbatterPitch"];
    if(error==NO)
    {
        [dicStrikeDetails setObject: @"o" forKey:@"lblOut"];
        
    
    }
  
    
    stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
    stringKeyPitch =[NSMutableString stringWithFormat:@"%d",strikePitchrtag];
    [stringKeyMainPitch appendString:stringKeyPitch];
    
    [dicMainPitch setObject:dicStrikeDetails forKey:stringKeyMainPitch];
    
    // NSLog(@"........%@",dicStrikePitch);
    NSLog(@"/////////%@",dicMainPitch);
    [self strikeBallReplacement];
    NSLog(@"%@",dicMainPitch);
      [self BallsCalculation];
    [scrollLocation setHidden:YES];
    error=NO;

}


- (void)viewDidUnload
{
        self.mArray=nil;
        self.mArray2=nil;
        self.mArray3=nil;
        self.mArray4=nil;
    //labelAddplayer=nil;
        [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    
}



#pragma mark//////////////// =======================================scroll delegate method


- (void)scrollViewDidScroll:(UIScrollView *)scrollView1
{
    if (self.currentRunningScroll == nil) {
        self.currentRunningScroll = scrollView1;
    }
    
       
    
    
    if ([self.currentRunningScroll isEqual:batter_scrollView])
    {
        scrollView.contentOffset =CGPointMake(2, scrollView1.contentOffset.y );
        
//                batter_scrollView.delegate=self;
//               scrollView.delegate=nil;
    }
     if ([self.currentRunningScroll isEqual:scrollView]) {
             batter_scrollView.contentOffset =CGPointMake(2, scrollView1.contentOffset.y );
//
             scrollNumberLine.contentOffset =CGPointMake( scrollView1.contentOffset.x, 1);
         
         scrollView.delegate=self;
//                batter_scrollView.delegate=nil;
         }
    
    if ([self.currentRunningScroll isEqual:batter_scrollViewLandscape])
    {
        scrollViewLandscape.contentOffset =CGPointMake(2, scrollView1.contentOffset.y );
        
//        batter_scrollViewLandscape.delegate=self;
//        scrollViewLandscape.delegate=nil;
    }
     if ([self.currentRunningScroll isEqual:scrollViewLandscape]) {
        batter_scrollViewLandscape.contentOffset =CGPointMake(2, scrollView1.contentOffset.y );
          scrollNumberLine.contentOffset =CGPointMake( scrollView1.contentOffset.x, 1);
//        scrollViewLandscape.delegate=self;
//        batter_scrollViewLandscape.delegate=nil;
    }
         
    NSLog(@",,,,,,scrollViewDidScroll1111.....%@",scrollView1);
}





- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView5

{

      NSLog(@",,,,,,scrollViewDidScroll55555.....%@",scrollView5);
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView3 willDecelerate:(BOOL)decelerate
{
    
    NSLog(@"%@",type);
   
    if(scrollViewIdentification==NO)
    {
        
        return;
    }
    
    
    
    self.currentRunningScroll = nil;
     // batter_scrollView.contentOffset =CGPointMake(2, scrollView6.contentOffset.y );
              batter_scrollView.delegate=self;
              scrollView.delegate=self;
      NSLog(@",,,,,,scrollViewDidScrol6666.....%@",scrollView3);
    
       if (mode==YES)
       {
           
       
    
    //3 coloumn
    if(scrollView3.contentOffset.x>0&&scrollView3.contentOffset.x<100)
    {
        scrollView3.contentOffset=CGPointMake(100, scrollView3.contentOffset.y);
    }
    
    if(scrollView3.contentOffset.x>=100&&scrollView3.contentOffset.x<203)
    {
        scrollView3.contentOffset=CGPointMake(203, scrollView3.contentOffset.y);
    }
    //4 coloumn
    if(scrollView3.contentOffset.x>=203&&scrollView3.contentOffset.x<303)
    {
        scrollView3.contentOffset=CGPointMake(203, scrollView3.contentOffset.y);
    }
    if(scrollView3.contentOffset.x>=303&&scrollView3.contentOffset.x<405)
    {
        scrollView3.contentOffset=CGPointMake(405, scrollView3.contentOffset.y);
    }
    //5 coloumn
    if(scrollView3.contentOffset.x>=405&&scrollView3.contentOffset.x<505)
    {
        scrollView3.contentOffset=CGPointMake(405, scrollView3.contentOffset.y);
    }
    if(scrollView3.contentOffset.x>=505&&scrollView3.contentOffset.x<608)
    {
        scrollView3.contentOffset=CGPointMake(608, scrollView3.contentOffset.y);
    }
    
    //6 coloumn
    if(scrollView3.contentOffset.x>=608&&scrollView3.contentOffset.x<708)
    {
        scrollView3.contentOffset=CGPointMake(608, scrollView3.contentOffset.y);
    }
    if(scrollView3.contentOffset.x>=708&&scrollView3.contentOffset.x<812)
    {
        scrollView3.contentOffset=CGPointMake(812, scrollView3.contentOffset.y);
    }
    
    //7 coloumn
    if(scrollView3.contentOffset.x>=812&&scrollView3.contentOffset.x<912)
    {
        scrollView3.contentOffset=CGPointMake(812, scrollView3.contentOffset.y);
    }
    if(scrollView3.contentOffset.x>=912&&scrollView3.contentOffset.x<1014)
    {
        scrollView3.contentOffset=CGPointMake(1014, scrollView3.contentOffset.y);
    }
    
    //8 coloumn
    if(scrollView3.contentOffset.x>=1014&&scrollView3.contentOffset.x<1114)
    {
        scrollView3.contentOffset=CGPointMake(1014, scrollView3.contentOffset.y);
    }
    if(scrollView3.contentOffset.x>=1114&&scrollView3.contentOffset.x<1217)
    {
        scrollView3.contentOffset=CGPointMake(1217, scrollView3.contentOffset.y);
    }
    
    //9 coloumn
    if(scrollView3.contentOffset.x>=1217&&scrollView3.contentOffset.x<1317)
    {
        scrollView3.contentOffset=CGPointMake(1217, scrollView3.contentOffset.y);
    }
    if(scrollView3.contentOffset.x>=1317&&scrollView3.contentOffset.x<=1418)
    {
        scrollView3.contentOffset=CGPointMake(1418, scrollView3.contentOffset.y);
    }
    
    //////////////////////////rows
           if(scrollView3.contentOffset.y>=0&&scrollView3.contentOffset.y<86)
           {
               scrollView3.contentOffset=CGPointMake(scrollView3.contentOffset.x,0);
           }
           if(scrollView3.contentOffset.y>=86&&scrollView3.contentOffset.y<156)
           {
               scrollView3.contentOffset=CGPointMake(scrollView3.contentOffset.x,156);
           }
    //row6
    if(scrollView3.contentOffset.y>=156&&scrollView3.contentOffset.y<226)
    {
        scrollView3.contentOffset=CGPointMake(scrollView3.contentOffset.x,156);
    }
    if(scrollView3.contentOffset.y>=226&&scrollView3.contentOffset.y<312)
    {
        scrollView3.contentOffset=CGPointMake(scrollView3.contentOffset.x,312);
    }
    //row7
    
    if(scrollView3.contentOffset.y>=312&&scrollView3.contentOffset.y<382)    {
        scrollView3.contentOffset=CGPointMake(scrollView3.contentOffset.x,312);
    }
    
    if(scrollView3.contentOffset.y>=382&&scrollView3.contentOffset.y<=468)
    {
        scrollView3.contentOffset=CGPointMake(scrollView3.contentOffset.x,468);
    }
    

       }

    
       else////////landscape
       {
           
           if(scrollView3.contentOffset.x>0&&scrollView3.contentOffset.x<108)
           {
               scrollView3.contentOffset=CGPointMake(0, scrollView3.contentOffset.y);
           }
           
           if(scrollView3.contentOffset.x>=108&&scrollView3.contentOffset.x<218)
           {
               scrollView3.contentOffset=CGPointMake(218, scrollView3.contentOffset.y);
           }
           //5 coloumn
           if(scrollView3.contentOffset.x>=218&&scrollView3.contentOffset.x<326)
           {
               scrollView3.contentOffset=CGPointMake(218, scrollView3.contentOffset.y);
           }
           if(scrollView3.contentOffset.x>=326&&scrollView3.contentOffset.x<435)
           {
               scrollView3.contentOffset=CGPointMake(435, scrollView3.contentOffset.y);
           }
           //6 coloumn
           if(scrollView3.contentOffset.x>=435&&scrollView3.contentOffset.x<553)
           {
               scrollView3.contentOffset=CGPointMake(435, scrollView3.contentOffset.y);
           }
           if(scrollView3.contentOffset.x>=553&&scrollView3.contentOffset.x<650)
           {
               scrollView3.contentOffset=CGPointMake(650, scrollView3.contentOffset.y);
           }
           
           //7 coloumn
           if(scrollView3.contentOffset.x>=650&&scrollView3.contentOffset.x<758)
           {
               scrollView3.contentOffset=CGPointMake(650, scrollView3.contentOffset.y);
           }
           if(scrollView3.contentOffset.x>=758&&scrollView3.contentOffset.x<865)
           {
               scrollView3.contentOffset=CGPointMake(865, scrollView3.contentOffset.y);
           }
           
           //8 coloumn
           if(scrollView3.contentOffset.x>=865&&scrollView3.contentOffset.x<973)
           {
               scrollView3.contentOffset=CGPointMake(865, scrollView3.contentOffset.y);
           }
           if(scrollView3.contentOffset.x>=973&&scrollView3.contentOffset.x<1080)
           {
               scrollView3.contentOffset=CGPointMake(1080, scrollView3.contentOffset.y);
           }
           
           //9 coloumn
           if(scrollView3.contentOffset.x>=1080&&scrollView3.contentOffset.x<1188)
           {
               scrollView3.contentOffset=CGPointMake(1080, scrollView3.contentOffset.y);
           }
           if(scrollView3.contentOffset.x>=1188&&scrollView3.contentOffset.x<1280)
           {
               scrollView3.contentOffset=CGPointMake(1282, scrollView3.contentOffset.y);
           }
           
           //10 coloumn
           //      if(scrollView6.contentOffset.x>=1080&&scrollView6.contentOffset.x<1170)
           //      {
           //          scrollView6.contentOffset=CGPointMake(1080, scrollView6.contentOffset.y);
           //      }
           //      if(scrollView6.contentOffset.x>=1170&&scrollView6.contentOffset.x<=1257)
           //      {
           //          scrollView6.contentOffset=CGPointMake(1257, scrollView6.contentOffset.y);
           //      }
           //
           //      //////////////////////////rows
           //
           //      //row6
           if(scrollView3.contentOffset.y>=0&&scrollView3.contentOffset.y<68)
           {
               scrollView3.contentOffset=CGPointMake(scrollView3.contentOffset.x,0);
           }
           if(scrollView3.contentOffset.y>=68&&scrollView3.contentOffset.y<139)
           {
               scrollView3.contentOffset=CGPointMake(scrollView3.contentOffset.x,139);
           }
           //row7
           
           if(scrollView3.contentOffset.y>=139&&scrollView3.contentOffset.y<207)    {
               scrollView3.contentOffset=CGPointMake(scrollView3.contentOffset.x,139);
           }
           
           if(scrollView3.contentOffset.y>=207&&scrollView3.contentOffset.y<=275)
           {
               scrollView3.contentOffset=CGPointMake(scrollView3.contentOffset.x,275);
           }
           
           //row8
           
           if(scrollView3.contentOffset.y>=275&&scrollView3.contentOffset.y<343)    {
               scrollView3.contentOffset=CGPointMake(scrollView3.contentOffset.x,275);
           }
           
           if(scrollView3.contentOffset.y>=343&&scrollView3.contentOffset.y<=411)
           {
               scrollView3.contentOffset=CGPointMake(scrollView3.contentOffset.x,411);
           }
           
           //row8
           
           if(scrollView3.contentOffset.y>=411&&scrollView3.contentOffset.y<479)    {
               scrollView3.contentOffset=CGPointMake(scrollView3.contentOffset.x,411);
           }
           
           if(scrollView3.contentOffset.y>=479&&scrollView3.contentOffset.y<=547)
           {
               scrollView3.contentOffset=CGPointMake(scrollView3.contentOffset.x,547);
           }
           
           
       }
    
    
}



- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView2
{
    
    
    
     NSLog(@",,,,,,scrollViewDidScroll2.22222....%@",scrollView2);
}// called on finger up as we are moving

#pragma mark last
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView6
{    self.currentRunningScroll = nil;
     NSLog(@"%@",type);
    
    NSLog(@",,,,,,scrollViewDidScroll3..33333...%f",scrollView6.contentOffset.x);
    NSLog(@",,,,,,scrollViewDidScroll3..33333...%f",scrollView6.contentOffset.y);
    
    if(scrollViewIdentification==NO)
    {
        
        return;
    }
    
    
    if (mode==YES)
    {

    //3 coloumn
    if(scrollView6.contentOffset.x>0&&scrollView6.contentOffset.x<100)
    {
          scrollView6.contentOffset=CGPointMake(100, scrollView6.contentOffset.y);
    }
    
    if(scrollView6.contentOffset.x>=100&&scrollView6.contentOffset.x<203)
    {
        scrollView6.contentOffset=CGPointMake(203, scrollView6.contentOffset.y);
    }
     //4 coloumn
    if(scrollView6.contentOffset.x>=203&&scrollView6.contentOffset.x<303)
    {
        scrollView6.contentOffset=CGPointMake(203, scrollView6.contentOffset.y);
    }
    if(scrollView6.contentOffset.x>=303&&scrollView6.contentOffset.x<405)
    {
        scrollView6.contentOffset=CGPointMake(405, scrollView6.contentOffset.y);
    }
     //5 coloumn
    if(scrollView6.contentOffset.x>=405&&scrollView6.contentOffset.x<505)
    {
        scrollView6.contentOffset=CGPointMake(405, scrollView6.contentOffset.y);
    }
    if(scrollView6.contentOffset.x>=505&&scrollView6.contentOffset.x<608)
    {
        scrollView6.contentOffset=CGPointMake(608, scrollView6.contentOffset.y);
    }
    
    //6 coloumn
    if(scrollView6.contentOffset.x>=608&&scrollView6.contentOffset.x<708)
    {
        scrollView6.contentOffset=CGPointMake(608, scrollView6.contentOffset.y);
    }
    if(scrollView6.contentOffset.x>=708&&scrollView6.contentOffset.x<812)
    {
        scrollView6.contentOffset=CGPointMake(812, scrollView6.contentOffset.y);
    }
    
    //7 coloumn
    if(scrollView6.contentOffset.x>=812&&scrollView6.contentOffset.x<912)
    {
        scrollView6.contentOffset=CGPointMake(812, scrollView6.contentOffset.y);
    }
    if(scrollView6.contentOffset.x>=912&&scrollView6.contentOffset.x<1014)
    {
        scrollView6.contentOffset=CGPointMake(1014, scrollView6.contentOffset.y);
    }
    
    //8 coloumn
    if(scrollView6.contentOffset.x>=1014&&scrollView6.contentOffset.x<1114)
    {
        scrollView6.contentOffset=CGPointMake(1014, scrollView6.contentOffset.y);
    }
    if(scrollView6.contentOffset.x>=1114&&scrollView6.contentOffset.x<1217)
    {
        scrollView6.contentOffset=CGPointMake(1217, scrollView6.contentOffset.y);
    }
    
    //9 coloumn
    if(scrollView6.contentOffset.x>=1217&&scrollView6.contentOffset.x<1317)
    {
        scrollView6.contentOffset=CGPointMake(1217, scrollView6.contentOffset.y);
    }
    if(scrollView6.contentOffset.x>=1317&&scrollView6.contentOffset.x<=1418)
    {
        scrollView6.contentOffset=CGPointMake(1418, scrollView6.contentOffset.y);
    }
    
    //////////////////////////rows
    
       //row6
        
        if(scrollView6.contentOffset.y>=0&&scrollView6.contentOffset.y<86)
        {
            scrollView6.contentOffset=CGPointMake(scrollView6.contentOffset.x,0);
        }
        if(scrollView6.contentOffset.y>=86&&scrollView6.contentOffset.y<156)
        {
            scrollView6.contentOffset=CGPointMake(scrollView6.contentOffset.x,156);
        }
        
    if(scrollView6.contentOffset.y>=156&&scrollView6.contentOffset.y<226)
    {
         scrollView6.contentOffset=CGPointMake(scrollView6.contentOffset.x,156);
    }
     if(scrollView6.contentOffset.y>=226&&scrollView6.contentOffset.y<312)
    {
         scrollView6.contentOffset=CGPointMake(scrollView6.contentOffset.x,312);
    }
      //row7
    
    if(scrollView6.contentOffset.y>=312&&scrollView6.contentOffset.y<382)    {
        scrollView6.contentOffset=CGPointMake(scrollView6.contentOffset.x,312);
    }
    
    if(scrollView6.contentOffset.y>=382&&scrollView6.contentOffset.y<=468)
    {
         scrollView6.contentOffset=CGPointMake(scrollView6.contentOffset.x,468);
    }
          
    }

    
  else////////landscape
  {
   
      //4 coloumn
      if(scrollView6.contentOffset.x>0&&scrollView6.contentOffset.x<108)
      {
          scrollView6.contentOffset=CGPointMake(0, scrollView6.contentOffset.y);
      }
      
      if(scrollView6.contentOffset.x>=108&&scrollView6.contentOffset.x<218)
      {
          scrollView6.contentOffset=CGPointMake(218, scrollView6.contentOffset.y);
      }
      //5 coloumn
      if(scrollView6.contentOffset.x>=218&&scrollView6.contentOffset.x<326)
      {
          scrollView6.contentOffset=CGPointMake(218, scrollView6.contentOffset.y);
      }
      if(scrollView6.contentOffset.x>=326&&scrollView6.contentOffset.x<435)
      {
          scrollView6.contentOffset=CGPointMake(435, scrollView6.contentOffset.y);
      }
      //6 coloumn
      if(scrollView6.contentOffset.x>=435&&scrollView6.contentOffset.x<553)
      {
          scrollView6.contentOffset=CGPointMake(435, scrollView6.contentOffset.y);
      }
      if(scrollView6.contentOffset.x>=553&&scrollView6.contentOffset.x<650)
      {
          scrollView6.contentOffset=CGPointMake(650, scrollView6.contentOffset.y);
      }
      
      //7 coloumn
      if(scrollView6.contentOffset.x>=650&&scrollView6.contentOffset.x<758)
      {
          scrollView6.contentOffset=CGPointMake(650, scrollView6.contentOffset.y);
      }
      if(scrollView6.contentOffset.x>=758&&scrollView6.contentOffset.x<865)
      {
          scrollView6.contentOffset=CGPointMake(865, scrollView6.contentOffset.y);
      }
      
      //8 coloumn
      if(scrollView6.contentOffset.x>=865&&scrollView6.contentOffset.x<973)
      {
          scrollView6.contentOffset=CGPointMake(865, scrollView6.contentOffset.y);
      }
      if(scrollView6.contentOffset.x>=973&&scrollView6.contentOffset.x<1080)
      {
          scrollView6.contentOffset=CGPointMake(1080, scrollView6.contentOffset.y);
      }
      
      //9 coloumn
      if(scrollView6.contentOffset.x>=1080&&scrollView6.contentOffset.x<1188)
      {
          scrollView6.contentOffset=CGPointMake(1080, scrollView6.contentOffset.y);
      }
      if(scrollView6.contentOffset.x>=1188&&scrollView6.contentOffset.x<1280)
      {
          scrollView6.contentOffset=CGPointMake(1282, scrollView6.contentOffset.y);
      }
      
   
      //      //////////////////////////rows
      //
      //      //row6
      if(scrollView6.contentOffset.y>=0&&scrollView6.contentOffset.y<68)
      {
          scrollView6.contentOffset=CGPointMake(scrollView6.contentOffset.x,0);
      }
      if(scrollView6.contentOffset.y>=68&&scrollView6.contentOffset.y<139)
      {
          scrollView6.contentOffset=CGPointMake(scrollView6.contentOffset.x,139);
      }
      //row7
      
      if(scrollView6.contentOffset.y>=139&&scrollView6.contentOffset.y<207)    {
          scrollView6.contentOffset=CGPointMake(scrollView6.contentOffset.x,139);
      }
      
      if(scrollView6.contentOffset.y>=207&&scrollView6.contentOffset.y<=275)
      {
          scrollView6.contentOffset=CGPointMake(scrollView6.contentOffset.x,275);
      }
      
      //row8
      
      if(scrollView6.contentOffset.y>=275&&scrollView6.contentOffset.y<343)    {
          scrollView6.contentOffset=CGPointMake(scrollView6.contentOffset.x,275);
      }
      
      if(scrollView6.contentOffset.y>=343&&scrollView6.contentOffset.y<=411)
      {
          scrollView6.contentOffset=CGPointMake(scrollView6.contentOffset.x,411);
      }
      
      //row8
      
      if(scrollView6.contentOffset.y>=411&&scrollView6.contentOffset.y<479)    {
          scrollView6.contentOffset=CGPointMake(scrollView6.contentOffset.x,411);
      }
      
      if(scrollView6.contentOffset.y>=479&&scrollView6.contentOffset.y<=547)
      {
          scrollView6.contentOffset=CGPointMake(scrollView6.contentOffset.x,547);
      }
      
  }
    
       // NSLog(@",,,,,,scrollViewDidScroll3..33333...%@",scrollView3);
}




 

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

-(void)didRotateFromInterfaceOrientation: (UIInterfaceOrientation) fromInterfaceOrientation
{
    

    
}


-(IBAction)done:(UIButton *)sender
{
        [displayView removeFromSuperview];
        [batterInfo_View removeFromSuperview];
        [scrollView setAlpha:1];
        [batter_scrollView setAlpha:1];
    [scrollViewLandscape setAlpha:1];
    [batter_scrollViewLandscape setAlpha:1];
 
}

#pragma mark NEXT BATTER FOR BATTER

-(IBAction)nextBatter:(UIButton *)sender
{
    [nametxt_BatterView becomeFirstResponder];
    //[batter_detailsArray removeAllObjects];
    //batter_detailsArray=[DBObj selectTeamPlayerstFromDb:teamName];
    
    if ([batter_detailsArray count]>9) {
    alert=[[UIAlertView alloc] initWithTitle:@"Not more than 10" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
        
        [alert show];
        return;

    
    }
    
    if (batterInfoTag>[batter_detailsArray count]-1)
        
    {
        intnextbatter=2;
    }
    
      if(intnextbatter==1)
      {
          nametxt_BatterView.text=@"";
          jerseyNumbertxt_BatterView.text=@"";
          //handedtxt_BatterView.text =@"";
          intnextbatter++;
          
          return;
      }
    

 
    if ([jerseyNumbertxt_BatterView.text length]<1 || [buttonHandedBatter.titleLabel.text length]<1) {
        alertMessage=@"Please fill jersery number and handed";
        [self alertMessageMethod:alertMessage];
        return;
    }
    else
    {   
        if (![self validateTextField:jerseyNumbertxt_BatterView.text]) {
            alertMessage=@"Please enter numbers only in the Jersey field.";
            [self alertMessageMethod:alertMessage];
            return;
        }
        
        if ([nametxt_BatterView.text length]<1) {
            namelbl_batterView.text=@"";
        }
        
        
        
        
        
        
        for (int i=0; i<[batter_detailsArray count]; i++) {
            if([jerseyNumbertxt_BatterView.text isEqualToString:[[batter_detailsArray objectAtIndex:i] objectForKey:@"JerseyNumber"]]){
                alertMessage=@"JerseyNumber already exists.";
                [self alertMessageMethod:alertMessage];
                return;
            }
            
        }
        
        
        NSMutableDictionary *mdict1=[[NSMutableDictionary alloc]init];
        [mdict1 setObject:nametxt_BatterView.text forKey:@"PlayerName"];
        [mdict1 setObject:jerseyNumbertxt_BatterView.text forKey:@"JerseyNumber"];
        [mdict1 setObject:buttonHandedBatter.titleLabel.text forKey:@"Handling"];
        [mdict1 setObject:teamName forKey:@"TeamName"];
      //  [DBObj insertPlayerIntoDb:mdict1];
        [batter_detailsArray addObject:mdict1];
        intnextbatter=10;
        nametxt_BatterView.text=@"";
        jerseyNumbertxt_BatterView.text=@"";
        buttonHandedBatter.titleLabel.text =@"";

    }

}

#pragma mark  ------------AT  BAT FINISH-------------------IBAction NextBatter FOR PITCH
-(IBAction)NextBatter:(id)sender
{
    
   
    
   NSMutableString * stringInn=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];

    
    if(![lblOut.text isEqualToString:@"3"])
    {
        if([dicInnings objectForKey:stringInn])
        {
            [dicInnings removeObjectForKey:stringInn];
        }
    }
    
//    
    [viewRunnerOut removeFromSuperview];
    [viewRunnerScored removeFromSuperview];
    [viewStolenBase removeFromSuperview];
    
    [self displayLastset];
    
    if(currentPitchTag)
    {
        UIButton *button=[arrayPitches objectAtIndex:currentPitchTag];
        
        button.layer.cornerRadius=0.0f;
        button.layer.masksToBounds=YES;
        button.layer.borderColor=[[UIColor blueColor]CGColor];
        button.layer.borderWidth= 0.0f;
    }
    // type=@"scroll";
    scrollViewIdentification=YES;
    buttonMonitor.enabled=YES;
    buttonPitcher.enabled=YES;
    buttonCatcher.enabled=YES;
    buttonAddCP.hidden=YES;
    NSLog(@"%d",mainGridTag);
    
    [mask_scrollView setHidden:YES];
    
    NSLog(@" dicAllInnings : %@",dicAllInnings);
    
    
    
    NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    
    if([dicAllInnings objectForKey:string]||lblOut.text)
    {
        dicMainPitch =[dicAllInnings objectForKey:string];
        
        NSLog(@"Button SubClassesArray : %@",pitch_Chart_Btn.subviews);
        
        UIButton *button = [mArray objectAtIndex:mainGridTag];
        
        for (UIView *view in button.subviews) {
            
            if ([view isMemberOfClass:[UILabel class]]) {
                
                UILabel *templbl=(UILabel *)view;
                
                
                
                stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
                stringKeyPitch =[NSMutableString stringWithFormat:@"%d",templbl.tag];
                [stringKeyMainPitch appendString:stringKeyPitch];
                
                NSLog(@"%@",stringKeyMainPitch);
                NSString *key =[NSString stringWithFormat:@"k%d",templbl.tag
                                ];
                
                [templbl setText:[dicSetvalues objectForKey:key] ];
                
                
                
                NSLog(@"%@",templbl.text);
                
                
                if(templbl.tag==14)
                {
                    
                    if(lblbatterPitch)
                        
                    {
                        templbl.text=lblbatterPitch.text;
                        NSLog(@"%@",templbl.text);
                    }
                    
                }
                if(templbl.tag==15)
                {
                    
                    if(lblOut)
                        
                    {
                        templbl.text=lblOut.text;
                        NSLog(@"%@",templbl.text);
                    }
                }
                if(templbl.tag==16)
                {
                    if(lblplayersBalls)
                        
                    {
                        templbl.text=lblplayersBalls.text;
                        NSLog(@"%@",templbl.text);
                    }
                    
                }
                if(templbl.tag==17)
                {
                    if(lblTotalBalls)
                        
                    {
                        templbl.text=lblTotalBalls.text;
                        NSLog(@"%@",templbl.text);
                    }
                    
                }
                
                
                
                
            }
            
            
            if([view isMemberOfClass:[UIImageView class]])
            {
                
                UIImageView *image =(UIImageView *)view;
                
                if(image.tag==mainGridTag+100)
                {
                    
                    [image setImage:[UIImage imageNamed:@""]];
                    for (int i=0; i<100; i++) {
                        
                        
                        stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
                        stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
                        [stringKeyMainPitch appendString:stringKeyPitch];
                        
                        NSLog(@"%@",stringKeyMainPitch);
                        
                        
                        ///image batter
                        if([[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"])
                        {
                            imageView_locatiogGraph.hidden=NO;
                            NSString *imageLocation  =[[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"];
                            NSLog(@"................image.....%@",imageLocation);
                            
                            [image setImage:[UIImage imageNamed:imageLocation]];
                            
                            
                        }
                    }
                    
                    
                }///image for short
                
                if(image.tag==mainGridTag+200)
                {
                    
                    NSMutableString *stringInn =[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
                    
                    
                    NSLog(@"..............dicInnings........%@",dicInnings);
                    if([dicInnings objectForKey:stringInn])
                    {
                        
                        
                        [image setImage:[UIImage imageNamed:@"Eoi_shape.png"]];
                        
                        
                        
                    }
                    
                    else{
                        [image setImage:[UIImage imageNamed:@""]];
                    }
                    
                }////image for innning
                
                
            }
            
        }///end of for loop
        
        
        [self closeDisplayViewLandscape];
        
        
        
    }
    
    
    
    [self resetMainViewSettings:displayView];
    //out
    [self addOutInDic];
    
    
    
    viewPotrait.userInteractionEnabled=YES;

    
    
    
    
    
    
    //////////****************/Next batter
    

//    UIButton *pitchLabelBall=[arrayPitches objectAtIndex:0];
//    
//        UIButton *pitchLabelStrike=[arrayPitches objectAtIndex:49];
   
    
    intAtBatMorethan3Monitor=0;
    
    ///check for empty field
    
//if(pitchLabelBall.titleLabel.text.length==0&&pitchLabelStrike.titleLabel.text.length==0 &&lblOut.text.length==0)
//{
//    
//    UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please finish at bat for this batter" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
//    [alert1 show];
//    return;
//
//    
//}
//    
   
    
       NSMutableString * stringAtBat=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    
     NSMutableString * string3AtBat=[NSMutableString stringWithFormat:@"Player3Box%d",mainGridTag];
       [dicAtBatRecord setObject:@"A" forKey:stringAtBat];
    
    
    NSLog(@"%@",dicAllInnings);
    if([dicAllInnings objectForKey:stringAtBat])
    {
        intAtBatMorethan3Monitor = [[[dicAllInnings objectForKey:stringAtBat] allKeys]count];
        
        if(intAtBatMorethan3Monitor>3)
        {
             [dicAtBatRecord setObject:@"3A" forKey:string3AtBat];
        }
    }
    
    if([lblOut.text isEqualToString:@"2"])
    {
        
        boolTwoATBatRecord=NO;
        
        
    }
    
    if(boolTwoATBatRecord==YES)
    {
        NSMutableString * stringTwo=[NSMutableString stringWithFormat:@"PlayerBoxTwo%d",mainGridTag];
        [dicAtBatRecord setObject:lblOut.text forKey:stringTwo];
        
        
    }
    
    ////pitcher rec
    
      NSMutableString * stringAtBatPitcher=[NSMutableString stringWithFormat:@"AtBatPitcher%d",mainGridTag];
    
      [dicAtBatRecord setObject:labelPitcher.text forKey:stringAtBatPitcher];
    
    
    ///////////////diplay pitchers
    
  //  [self previousDisplayPitch];

    
    nextBatter++;
 
    
    if(nextBatter>8)
    {
       [self ColoumnNextBatterRecord];
        nextBatter=0;
    }
    else{
         mainGridTag =mainGridTag+10;
    }
   
    
//    NSString *str= [[baterInfo_Array objectAtIndex:nextBatter] objectForKey:@"JerseyNumber"];
    
//    if([str length]==0)
//    {
//        nextBatter=0;
//        mainGridTag =0;
//        
//        UIAlertView *alert1=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please Add Next Batter" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
//        alertMessageType=@"AddBatter";
//        viewPotrait.userInteractionEnabled=YES;
//        [alert1 show];
//        return;
//        
//    }

       
    imageEndOfInning.hidden=YES;

        [labelbatterNameForMainpop setText:[[baterInfo_Array objectAtIndex:nextBatter] objectForKey:@"PlayerName"]];
        
        [labelJerseyNumberForMainpop setText:[[baterInfo_Array objectAtIndex:nextBatter] objectForKey:@"JerseyNumber"]];
        
   
     
     
   
    NSLog(@",,,,,%@",dicMainPitch);
    
   

    dicMainPitch =[[NSMutableDictionary alloc]init];
    
    NSMutableString * string1=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    if([dicAllInnings objectForKey:string1])
    {
        dicMainPitch =[dicAllInnings objectForKey:string1];
    }
    
    //[self NextBatterView:nextBatter];
   
    
    lblOut.text=@"";
    lblbatterPitch.text=@"";
    imageView_locatiogGraph.hidden=YES;
  //  [self BallsCalculation];
    
    stringRunnerScored=@"";
    stringRunnnerOut=@"";
    stringStolenBase=@"";
//
  
    
}


-(void)ColoumnNextBatterRecord
{
    if(mainGridTag==80)
    {
        mainGridTag=1;
    }
    if(mainGridTag==81)
    {
         mainGridTag=2;
    }
    
    if(mainGridTag==82)
    {
         mainGridTag=3;
    }
    
    if(mainGridTag==83)
    {
         mainGridTag=4;
    }
    
    if(mainGridTag==84)
    {
         mainGridTag=5;
    }
    
    if(mainGridTag==85)
    {
         mainGridTag=6;
    }
    
    if(mainGridTag==86)
    {
         mainGridTag=7;
    }
    if(mainGridTag==87)
    {
         mainGridTag=8;
    }
    
    if(mainGridTag==88)
    {
         mainGridTag=9;
    }
    
    if(mainGridTag==89)
    {
         mainGridTag=0;
    }
    
    
    
}



#pragma mark .........Next Batter View pitch /
-(void)NextBatterView :(int) mainTag
{
 
    if(currentPitchTag)
    {
        UIButton *button=[arrayPitches objectAtIndex:currentPitchTag];
        
        button.layer.cornerRadius=0.0f;
        button.layer.masksToBounds=YES;
        button.layer.borderColor=[[UIColor blueColor]CGColor];
        button.layer.borderWidth= 0.0f;
    }
    
    NSLog(@"%@",dicAllInnings);
    NSLog(@"%@",dicMainPitch);
    
    for(int i=0;i<99;i++)
  {
      
        UIButton *button=[arrayPitches objectAtIndex:i];
      stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
      
      [stringKeyMainPitch appendString:[NSString stringWithFormat:@"%d",i]];
      
      NSLog(@"%@",stringKeyMainPitch);
      
      NSString *str1=[[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"pitchLabel"];
      [button setTitle:str1 forState:UIControlStateNormal];
  }
    
  
}


-(IBAction)handedForBatter
{
//    
//    UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please Select Hand" delegate:self cancelButtonTitle:@"Right(R)" otherButtonTitles:@"Left(L)",nil];
//    [alert show];
//    [alert release];
    if([labelHandedBatter.text isEqualToString:@"R"])
    {
        labelHandedBatter.text=@"L";
        
    }
    else
    {
       labelHandedBatter.text=@"R";
        
        
    }
      NSMutableDictionary *mdict2=[[NSMutableDictionary alloc]init];
    [mdict2 setObject:nametxt_BatterView.text forKey:@"PlayerName"];
    [mdict2 setObject:[[batter_detailsArray objectAtIndex:batterInfoTag] objectForKey:@"JerseyNumber"] forKey:@"JerseyNumber"];
    [mdict2 setObject:labelHandedBatter.text forKey:@"Handling"];
    [DBObj updatedDB:labelSettingsOpponent.text  dictionary:mdict2];
    [batter_detailsArray replaceObjectAtIndex:batterInfoTag withObject:mdict2];
    
    
    
}


-(IBAction)edit:(UIButton *)sender
{
    
    buttonAddPlayerRoster.hidden=YES;
    buttonSelectPlayer.hidden=YES;
    buttonHanded.hidden=YES;
    buttonHandedBatter.hidden=NO;

    
    [buttonHandedBatter setTitle:labelHandedBatter.text forState:UIControlStateNormal];
    
    
    editBtn.hidden =YES;
        batterInfoStr=@"proceed";
        [maskView removeFromSuperview];
        [nametxt_BatterView becomeFirstResponder];
        NSLog(@"%s",__func__);
}

-(IBAction)AddPLayer
{

    buttonHandedBatter.hidden=NO;
    buttonAddPlayerRoster.hidden=YES;
    buttonSelectPlayer.hidden=YES;
    
    editBtn.hidden =YES;
    batterInfoStr=@"proceed";
    [maskView removeFromSuperview];
    [nametxt_BatterView becomeFirstResponder];
    NSLog(@"%s",__func__);
}

-(IBAction)save:(UIButton *)sender
{
  
    
    buttonMonitor.enabled=YES;
    buttonPitcher.enabled=YES;
    buttonCatcher.enabled=YES;
    
    NSLog(@"%s",__func__);
    if ([jerseyNumbertxt_BatterView.text length]<1 || [buttonHandedBatter.titleLabel.text length]<1) {
        alertMessage=@"Please fill jersery number and handed";
        [self alertMessageMethod:alertMessage];
        return;
    }
    else
    {   
        if (![self validateTextField:jerseyNumbertxt_BatterView.text]) {
            alertMessage=@"Please enter numbers only in the Jersey field.";
            [self alertMessageMethod:alertMessage];
            return;
        }
        
        if ([nametxt_BatterView.text length]<1) {
            namelbl_batterView.text=@"";
        }
        NSLog(@"PlayerName  : %@",nametxt_BatterView.text);
        NSLog(@"JeseyNumber : %@",jerseyNumbertxt_BatterView.text);
        NSLog(@"Handed      : %@",buttonHandedBatter.titleLabel.text);
        /*
         Handling = R;
         JerseyNumber = 2;
         PlayerName = Martin;
         TeamName = "Victorian Bush";
         */
        NSMutableDictionary *mdict1=[[NSMutableDictionary alloc]init];
        NSMutableDictionary *mdict2=[[NSMutableDictionary alloc]init];
     
        NSLog(@"Batter Info Tag : %d",batterInfoTag);
        if (batterInfoTag>[batter_detailsArray count]-1||intnextbatter==10||[batter_detailsArray count]==0) 
        {   
            
            
            
            if ([batter_detailsArray count]>9) {
                UIAlertView * alert1=[[UIAlertView alloc] initWithTitle:@"Not more than 9" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
                
                [alert1 show];
                return;
                
                
            }

            
            for (int i=0; i<[batter_detailsArray count]; i++) {
                if([jerseyNumbertxt_BatterView.text isEqualToString:[[batter_detailsArray objectAtIndex:i] objectForKey:@"JerseyNumber"]]){
                    alertMessage=@"JerseyNumber already exists.";
                    [self alertMessageMethod:alertMessage];
                    return;
                }
                
            }

            [mdict1 setObject:nametxt_BatterView.text forKey:@"PlayerName"];
            [mdict1 setObject:jerseyNumbertxt_BatterView.text forKey:@"JerseyNumber"];
            [mdict1 setObject:buttonHandedBatter.titleLabel.text forKey:@"Handling"];
            [mdict1 setObject:labelSettingsOpponent.text forKey:@"TeamName"];
            [DBObj insertPlayerIntoDb:mdict1];
            [batter_detailsArray addObject:mdict1];
              intnextbatter=1;
        }
        else
        {//update
            [mdict2 setObject:nametxt_BatterView.text forKey:@"PlayerName"];
            [mdict2 setObject:[[batter_detailsArray objectAtIndex:batterInfoTag] objectForKey:@"JerseyNumber"] forKey:@"JerseyNumber"];
            [mdict2 setObject:buttonHandedBatter.titleLabel.text forKey:@"Handling"];
            [DBObj updatedDB:labelSettingsOpponent.text  dictionary:mdict2];
            [batter_detailsArray replaceObjectAtIndex:batterInfoTag withObject:mdict2];
            
        }
        
       // [batter_detailsArray removeAllObjects];
       // batter_detailsArray=[DBObj selectTeamPlayerstFromDb:labelSettingsOpponent.text];
        int k;
        if([batter_detailsArray count]>9)
        {
            k=9;
        }
        else
        {
            k=[batter_detailsArray count];
        }
        for (int  j=0; j<k; j++) {

            [baterInfo_Array replaceObjectAtIndex:j withObject:[batter_detailsArray objectAtIndex:j]];
        }
        NSLog(@"Batter_details Array : %@",batter_detailsArray);
            NSLog(@"Batter_details Array : %@",baterInfo_Array);
        
        //[detailsDBObj release];
    } 
    for (int j=0; j<9;j++) {
        
    
    
    
    UIButton *button = [mArray3 objectAtIndex:j];
    for (UIView *view in button.subviews) {
        if ([view isMemberOfClass:[UILabel class]]) {
            UILabel *templbl=(UILabel *)view;
            switch (templbl.tag) {
                    
                case 0:
                    [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"PlayerName"]];
                    break;
                case 1:
                    [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"JerseyNumber"]];
                    break;
                case 2:
                    [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"Handling"]];
                    break;
                    
                default:
                    break;
                    
            }
            NSLog(@"Temp Label Tag : %d",templbl.tag);
        }
    }


    //Landscape
    
    UIButton *buttonLandscape = [mArrayLandscape3 objectAtIndex:j];
    for (UIView *view in buttonLandscape.subviews) {
        if ([view isMemberOfClass:[UILabel class]]) {
            UILabel *templbl=(UILabel *)view;
            switch (templbl.tag) {
                    
                case 0:
                    [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"PlayerName"]];
                    break;
                case 1:
                    [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"JerseyNumber"]];
                    break;
                case 2:
                    [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"Handling"]];
                    break;
                    
                default:
                    break;
                    
            }
            NSLog(@"Temp Label Tag : %d",templbl.tag);
        }
        
        
        
    }
    
        
    }
    [displayView removeFromSuperview];
    [batterInfo_View removeFromSuperview];
    [scrollView setAlpha:1];
    [batter_scrollView setAlpha:1];
    [scrollViewLandscape setAlpha:1];
    [batter_scrollViewLandscape setAlpha:1];
    [scrollView setUserInteractionEnabled:YES];
    [batter_scrollView setUserInteractionEnabled:YES];
}


#pragma mark --------------batter side action
-(IBAction)batterInfo:(UIButton *)sender
{
    //[self checkDisplayMatches];
    
    buttonSelectPlayer.hidden=NO;
    maskView.hidden=NO;
    view_Players.hidden =YES;
     editBtn.hidden =NO;
    NSLog(@"%s",__func__);
    batterInfoTag=sender.tag;
    //NSLog(@"..........................................%@",baterInfo_Array);
   // NSLog(@"batterdetails :  %@",baterInfo_Array);
    if (sender.tag>0)
    
    {
        NSMutableDictionary *myDict=[baterInfo_Array objectAtIndex:sender.tag-1];
        if ([myDict count]==0) {
            UIAlertView *myAlert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please fill batter details in sequence manner." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [myAlert show];
            return;
        }
        else{
                    numberlbl_batterView.text=[[baterInfo_Array  objectAtIndex:sender.tag] objectForKey:@"JerseyNumber"];
                    namelbl_batterView.text=[[baterInfo_Array objectAtIndex:sender.tag] objectForKey:@"PlayerName"];
                    nametxt_BatterView.text=[[baterInfo_Array objectAtIndex:sender.tag] objectForKey:@"PlayerName"];
                    jerseyNumbertxt_BatterView.text=[[baterInfo_Array objectAtIndex:sender.tag] objectForKey:@"JerseyNumber"];
                    buttonHandedBatter.titleLabel.text=[[baterInfo_Array objectAtIndex:sender.tag] objectForKey:@"Handling"];
            
            labelHandedBatter.text=[[baterInfo_Array objectAtIndex:sender.tag] objectForKey:@"Handling"];
            NSLog(@"%@",labelHandedBatter.text);

        }
    }

    else{
        
        numberlbl_batterView.text=[[baterInfo_Array  objectAtIndex:sender.tag] objectForKey:@"JerseyNumber"];
        namelbl_batterView.text=[[baterInfo_Array objectAtIndex:sender.tag] objectForKey:@"PlayerName"];
        nametxt_BatterView.text=[[baterInfo_Array objectAtIndex:sender.tag] objectForKey:@"PlayerName"];
        jerseyNumbertxt_BatterView.text=[[baterInfo_Array objectAtIndex:sender.tag] objectForKey:@"JerseyNumber"];
        buttonHandedBatter.titleLabel.text=[[baterInfo_Array objectAtIndex:sender.tag] objectForKey:@"Handling"];
        labelHandedBatter.text=[[baterInfo_Array objectAtIndex:sender.tag] objectForKey:@"Handling"];
        NSLog(@"%@",labelHandedBatter.text);
    }

    [displayView removeFromSuperview];
    [batterInfo_View removeFromSuperview];
    selectedBtnFrame=sender.frame;
    [batterInfo_View setFrame:sender.frame];       
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    [batterInfo_View addSubview:maskView];
    
    if (mode==YES)
        [batterInfo_View setFrame:CGRectMake(scrollView.center.x-350, scrollView.center.y-350, 410, 429)];
    else
        [batterInfo_View setFrame:CGRectMake(scrollView.center.x-300, scrollView.center.y-425, 410, 429)];
    
    
    [UIView commitAnimations];
    [self.view addSubview:batterInfo_View];
    scrollView.alpha=0.3;
    batter_scrollView.alpha=0.3;
    scrollViewLandscape.alpha=0.3;
    batter_scrollViewLandscape.alpha=0.3;
    [mask_scrollView setHidden:YES];
    [scrollView setUserInteractionEnabled:NO];
    [batter_scrollView setUserInteractionEnabled:NO];
    
    if([batter_detailsArray count]-1<batterInfoTag||[batter_detailsArray count]==0)
    {
        editBtn.hidden=YES;
        labelEdit.hidden=YES;
        buttonHanded.hidden=YES;
        buttonAddPlayerRoster.hidden=NO;
        labelAddplayer.hidden=NO;
    }
    else {//edit
        
        buttonHanded.hidden=NO;
        editBtn.hidden=NO;
        labelEdit.hidden=NO;
        buttonAddPlayerRoster.hidden=YES;
        labelAddplayer.hidden=YES;
    }
    
    
    buttonMonitor.enabled=NO;
    buttonPitcher.enabled=NO;
    buttonCatcher.enabled=NO;
    
    [self addLayerBatter :sender.tag];
}

#pragma mark Layer for b/p
-(void)addLayerBatter :(int)batterLayerTag
{
    
    
    if(intLayerBatterTag>=0)
    {
        UIButton *button = [mArray3 objectAtIndex:intLayerBatterTag];
        
        
        button.layer.cornerRadius=0.0f;
        button.layer.masksToBounds=YES;
        button.layer.borderColor=[[UIColor clearColor]CGColor];
        button.layer.borderWidth= 0.0f;

        
    }
   
        

    UIButton *button = [mArray3 objectAtIndex:batterLayerTag];
    
        
        button.layer.cornerRadius=0.0f;
        button.layer.masksToBounds=YES;
        button.layer.borderColor=[[UIColor blueColor]CGColor];
        button.layer.borderWidth= 6.0f;
        
 intLayerBatterTag=batterLayerTag;
    
}
-(void)addLayerPitch :(int)PitchLayerTag

{
    
   
    [self addLayerBatter :[self PitchToBatter]];
    
    
    if(intLayerPitchTag>=0)
    {
        UIButton *button = [mArray objectAtIndex:intLayerPitchTag];
        
        
        button.layer.cornerRadius=0.0f;
        button.layer.masksToBounds=YES;
        button.layer.borderColor=[[UIColor clearColor]CGColor];
        button.layer.borderWidth= 0.0f;
        
        
    }
    
    
    
    UIButton *button = [mArray objectAtIndex:PitchLayerTag];
    
    
    button.layer.cornerRadius=0.0f;
    button.layer.masksToBounds=YES;
    button.layer.borderColor=[[UIColor blueColor]CGColor];
    button.layer.borderWidth= 6.0f;
    
    intLayerPitchTag=PitchLayerTag;

    
    
}

#pragma mark 

-(int)CheckForFirstRow
{
    
    int r;
    if(mainGridTag==1||mainGridTag==2||mainGridTag==3||mainGridTag==4||mainGridTag==5||mainGridTag==6||mainGridTag==7||mainGridTag==8||mainGridTag==9)
    {
            r= mainGridTag+79;
        
    }
    
    else
    {
      r= mainGridTag-10;
        
    }
    
    return r;
    
}

#pragma mark----------------------------------------- ibaction main grid view
-(IBAction)btn:(UIButton *)sender
{
    
    NSLog(@"%d",[self PitchToBatter]);
    
    NSLog(@"%d",[self PitchToBatter]);
    
       mainGridTag =sender.tag;
    
    if([batter_detailsArray count]<=[ self PitchToBatter] ||[batter_detailsArray count]==0)
    {
        alert=[[UIAlertView alloc] initWithTitle:@"Please add batter" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
        
        
        [alert show];
        return;
    }
    

    
      
    
    
    

//    if(mainGridTag)
//    {
//        
//        
//        
//        intLastAtBatRecord = [self CheckForFirstRow];
//        
//        NSMutableString * stringAtBat=[NSMutableString stringWithFormat:@"PlayerBox%d",intLastAtBatRecord];
//        
//        
//       if([dicAtBatRecord objectForKey:stringAtBat])
//       {
//        
//       }
//        else
//        {
//            alert=[[UIAlertView alloc] initWithTitle:@"Please finish at bat for this batter" message:@"" delegate:self cancelButtonTitle:nil               otherButtonTitles:@"Ok",nil];
//            
//            [alert show];
//            return;
//
//        }
//       
//        
//        
//        
//    }
//    
    viewPotrait.userInteractionEnabled=NO;
    intalertInning=100;
        buttonAddCP.hidden=YES;
    imageEndOfInning.hidden=YES;
   
    boolRemoveForwardPitchLabel=NO;

    
   
    

    
    
    mask_scrollView.hidden=YES;
    [lblOut setText:@""];
    lblbatterPitch.text=@"";
    imageView_locatiogGraph.hidden=YES;
    
    mainGridTag =0;
     dicMainPitch =[[NSMutableDictionary alloc]init];
    
    NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",sender.tag];
    if([dicAllInnings objectForKey:string])
    {
         dicMainPitch =[dicAllInnings objectForKey:string];
    }
    intprvMainTag =sender.tag;
     mainGridTag =sender.tag;
    [self BallsCalculation];
    [self getOutEntry];
  
    
    btnDisplayViewTag=sender.tag;
    [self alertMessageMethod:batterInfoStr];
        [displayView removeFromSuperview];
        [batterInfo_View removeFromSuperview];
        selectedBtnFrame=sender.frame;
        
    if (sender.tag<=9) 
        batterTag=0;
    
    if (sender.tag>9&&sender.tag<=19) 
        batterTag=1;
    
    if (sender.tag>19&&sender.tag<=29) 
        batterTag=2;
    
    if (sender.tag>29&&sender.tag<=39) 
        batterTag=3;
    
    if (sender.tag>39&&sender.tag<=49) 
        batterTag=4;
    
    if (sender.tag>49&&sender.tag<=59) 
        batterTag=5;
    
    if (sender.tag>59&&sender.tag<=69) 
        batterTag=6;
    
    if (sender.tag>69&&sender.tag<=79) 
        batterTag=7;
    
    if (sender.tag>79&&sender.tag<=89) 
        batterTag=8;

        [displayView setFrame:sender.frame];       
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
       // [mask_scrollView setHidden:NO];
        [self.view bringSubviewToFront:mask_scrollView];
    if(mode==YES){
        [displayView setFrame:CGRectMake(scrollView.center.x-405, scrollView.center.y-380, 449, 282)];
        [mask_scrollView setFrame:CGRectMake(scrollView.frame.origin.x, 404, 310, 500)];
        [mask_scrollView setContentSize:CGSizeMake(310, 870)];
    }
    else{
        [displayView setFrame:CGRectMake(scrollView.center.x-280, scrollView.center.y-420, 449, 282)];
        
        [mask_scrollView setFrame:CGRectMake(scrollView.center.x-280, 364, 310, 500)];
        [mask_scrollView setContentSize:CGSizeMake(310, 1200)];
        
    }
        [UIView commitAnimations];
        [self.view addSubview:displayView];
    
    
    if([baterInfo_Array count]>0)
    {
    [labelbatterNameForMainpop setText:[[baterInfo_Array objectAtIndex:intPlayerForRows] objectForKey:@"PlayerName"]];
  
    [labelJerseyNumberForMainpop setText:[[baterInfo_Array objectAtIndex:intPlayerForRows] objectForKey:@"JerseyNumber"]];

    }
    nextBatter =intPlayerForRows;

    
    
    //display labels
    [self NextBatterView :mainGridTag];
    
    //[imageView_locatiogGraph setImage:nil];
    [mask_scrollView setAlpha:1.0];
    [scrollView setAlpha:0.3];
    [scrollViewLandscape setAlpha:0.3];
    [scrollView setScrollEnabled:NO];
    [scrollView setUserInteractionEnabled:NO];
    [batter_scrollView setAlpha:0.3];
       [batter_scrollViewLandscape setAlpha:0.3];
    [batter_scrollView setScrollEnabled:NO];
    [batter_scrollView setUserInteractionEnabled:NO];
    
        NSLog(@"Batter Tag : %d",batterTag);
    
     [self addLayerPitch:sender.tag];
     [self LastImageBatter];
     [self getEndOfInning];
    
    
    buttonMonitor.enabled=NO;
    buttonPitcher.enabled=NO;
    buttonCatcher.enabled=NO;
    
    stringRunnerScored=@"";
    stringRunnnerOut=@"";
    stringStolenBase=@"";
    
}


#pragma mark Add Catcher/Pitcher
-(IBAction)addCatcherPitcher:(id)sender
{
    
    if(popoverController)
    {
        [popoverController dismissPopoverAnimated:YES];
    }
    viewAddPlayerCP.hidden=NO;
    [popoverController dismissPopoverAnimated:YES];
    jerseyNumberCP.text=@"";
    playerNameCp.text=@"";
    buttonHandedCP.titleLabel.text=@"";
    
    
      [viewAddPlayerCP setFrame:CGRectMake(scrollView.center.x-305, scrollView.center.y-380, 379, 269)];
    [self.view addSubview:viewAddPlayerCP];
    
    
    if ( type==@"Catcher")
    {
        labelPlayerTitle.text=@"Catcher";
    }
    
    if (type==@"Pitcher")
    {
        labelPlayerTitle.text=@"Pitcher";

    }
    
    scrollView.alpha=0.3;
    batter_scrollView.alpha=0.3;
    scrollViewLandscape.alpha=0.3;
    batter_scrollViewLandscape.alpha=0.3;
    [mask_scrollView setHidden:YES];
    [scrollView setUserInteractionEnabled:NO];
    [batter_scrollView setUserInteractionEnabled:NO];

    
}


-(IBAction)CancelADDCP
{
 if(type==@"OppentAddPlayer")
{
    type=@"OppentPlayers";
     viewAddPlayerCP.hidden=YES;
}
    
else{
    viewAddPlayerCP.hidden=YES;
    [self resetMainViewSettings:viewAddPlayerCP];
}
   
}

-(IBAction)handedADDCP
{
    
    [namelbl_batterView resignFirstResponder];
    [jerseyNumbertxt_BatterView resignFirstResponder];
    
    [playerNameCp resignFirstResponder];
    [jerseyNumberCP resignFirstResponder];
    //typeBatter=@"batterside";
    UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please Select the batter side" delegate:self cancelButtonTitle:@"Left" otherButtonTitles:@"Right", nil];
    [alrt_message show];
    
}

#pragma mark AddPLayerADDCP catcher pitcher oppent
-(IBAction)AddPLayerADDCP
{
    
    
    
    if([jerseyNumberCP.text length]==0||[buttonHandedCP.titleLabel.text length]==0||[playerNameCp.text length]==0)
    {
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please fill all the details" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alrt_message show];
        return;
    }
    
    if ([self validateTextField:jerseyNumberCP.text]==NO)
    {
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number Should be numeric" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alrt_message show];
        return;
    }
    
    
    if ( type==@"OppentAddPlayer")
    {
              
        
        
        for (int i=0; i<[arrayOppentplayer1 count]; i++) {
            
            NSString *jerseyrnumber =[[arrayOppentplayer1 objectAtIndex:i]objectForKey:@"JerseyNumber"];
            
            if ([jerseyNumberCP.text isEqualToString:jerseyrnumber]) {
                
                UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number already exist" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                [alrt_message show];
                return;
                
                
            }
            
        }

        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init ];
        
        [dict setObject:labelSettingsOpponent.text forKey:@"TeamName"];
        
        [dict setObject:playerNameCp.text forKey:@"PlayerName"];
        [dict setObject:jerseyNumberCP.text forKey:@"JerseyNumber"];
        [dict setObject:buttonHandedCP.titleLabel.text forKey:@"Handling"];
        [dict setObject:@"C"forKey:@"Status"];
              
        [DBObj insertPlayerIntoDb:dict];
       // [DBObj insertHomePlayerIntoDb:dict];
        viewAddPlayerCP.hidden=YES;
         type=@"OppentPlayers";
        [self DisplayOpponentPlayer];
        [table_displayOptions reloadData];
        // [tblw_alldatatable reloadData];

    }
    
    
    if ( type==@"Catcher")
    {
        
        
        for (int i=0; i<[array_catcher count]; i++) {
            
            NSString *jerseyrnumber =[[array_catcher objectAtIndex:i]objectForKey:@"JerseyNumber"];
            
            if ([jerseyNumberCP.text isEqualToString:jerseyrnumber]) {
                
                UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number already exist" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                [alrt_message show];
                return;
                
                
            }
            
        }
        //
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init ];
        
        [dict setObject:@"Team" forKey:@"TeamName"];
        
        [dict setObject:playerNameCp.text forKey:@"PlayerName"];
        [dict setObject:jerseyNumberCP.text forKey:@"JerseyNumber"];
        [dict setObject:buttonHandedCP.titleLabel.text forKey:@"Handling"];
        [dict setObject:@"C"forKey:@"Status"];
        labelCatcher.text=playerNameCp.text;
        
        [array_pitcher addObject:dict];
        [DBObj insertHomePlayerIntoDb:dict];
         viewAddPlayerCP.hidden=YES;
        
        // [tblw_alldatatable reloadData];
       
    }
    if (type==@"Pitcher")
    {
        
        
        for (int i=0; i<[array_pitcher count]; i++) {
            
            NSString *jerseyrnumber =[[array_pitcher objectAtIndex:i]objectForKey:@"JerseyNumber"];
            
            if ([jerseyNumberCP.text isEqualToString:jerseyrnumber]) {
                
                UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number already exist" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                [alrt_message show];
                return;
                
                
            }
            
        }
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc]init ];
        // [dict setObject:textAlert.text forKey:@"PlayerName"];
        [dict setObject:@"Team" forKey:@"TeamName"];
        [dict setObject:playerNameCp.text forKey:@"PlayerName"];
        [dict setObject:jerseyNumberCP.text forKey:@"JerseyNumber"];
        [dict setObject:buttonHandedCP.titleLabel.text forKey:@"Handling"];
        [dict setObject:@"P"forKey:@"Status"];
        
        labelPitcher.text=playerNameCp.text;
        [DBObj insertHomePlayerIntoDb:dict];
        [array_pitcher addObject:dict];
        viewAddPlayerCP.hidden=YES;
      

        
    }
    [jerseyNumberCP resignFirstResponder];
    [playerNameCp resignFirstResponder];
    [self resetMainViewSettings:viewAddPlayerCP];
    
    
    
}

#pragma mark close batter
-(IBAction)closeWindow:(UIButton *)sender
{
    buttonMonitor.enabled=YES;
    buttonPitcher.enabled=YES;
    buttonCatcher.enabled=YES;
    type=@"scroll";
     scrollViewIdentification=YES;
    [self CLoseBatterMainWindow];
    
    [btn_loadSenderidbtn setBackgroundColor:[UIColor clearColor]];
    [btn_loadstrikesenderid setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
}

-(void)CLoseBatterMainWindow
{
    intnextbatter=1;
    [self resetMainViewSettings:batterInfo_View];
    
   // buttonHandedBatter.hidden=NO;
    //[batter_detailsArray removeAllObjects];
    //batter_detailsArray=[DBObj selectTeamPlayerstFromDb:teamName];
    int k;
    if([batter_detailsArray count]>9)
    {
        k=9;
    }
    else
    {
        k=[batter_detailsArray count];
    }
    for (int  j=0; j<k; j++) {
        
        [baterInfo_Array replaceObjectAtIndex:j withObject:[batter_detailsArray objectAtIndex:j]];
    }
    
    if ([baterInfo_Array count]==0) {
        
    }
    NSLog(@"Batter_details Array : %@",batter_detailsArray);
    NSLog(@"Batter_details Array : %@",baterInfo_Array);
    
    //[detailsDBObj release];
    
    for (int j=0; j<9;j++) {
        
        
        
        
        UIButton *button = [mArray3 objectAtIndex:j];
        for (UIView *view in button.subviews) {
            if ([view isMemberOfClass:[UILabel class]]) {
                UILabel *templbl=(UILabel *)view;
                switch (templbl.tag) {
                        
                    case 0:
                        [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"PlayerName"]];
                        break;
                    case 1:
                        [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"JerseyNumber"]];
                        break;
                    case 2:
                        [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"Handling"]];
                        break;
                        
                    default:
                        break;
                        
                }
                NSLog(@"Temp Label Tag : %d",templbl.tag);
            }
        }
        
        
        //Landscape
        
        UIButton *buttonLandscape = [mArrayLandscape3 objectAtIndex:j];
        for (UIView *view in buttonLandscape.subviews) {
            if ([view isMemberOfClass:[UILabel class]]) {
                UILabel *templbl=(UILabel *)view;
                switch (templbl.tag) {
                        
                    case 0:
                        [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"PlayerName"]];
                        break;
                    case 1:
                        [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"JerseyNumber"]];
                        break;
                    case 2:
                        [templbl setText:[[baterInfo_Array objectAtIndex:j] objectForKey:@"Handling"]];
                        break;
                        
                    default:
                        break;
                        
                }
                NSLog(@"Temp Label Tag : %d",templbl.tag);
            }
            
            
            
        }
        
        

        
        
        
    }
    

}




#pragma mark ---------------------Previous batter Display
-(void)previousDisplayPitch
{
    [self displayLastset];
    
    buttonAddCP.hidden=NO;
    NSLog(@"%d",mainGridTag);
    
    [mask_scrollView setHidden:YES];
    
    NSLog(@" dicAllInnings : %@",dicAllInnings);
    
    
    
    NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    
    if([dicAllInnings objectForKey:string]||lblOut.text)
    {
        dicMainPitch =[dicAllInnings objectForKey:string];
        
        NSLog(@"Button SubClassesArray : %@",pitch_Chart_Btn.subviews);
        
        UIButton *button = [mArray objectAtIndex:mainGridTag];
        
        for (UIView *view in button.subviews) {
            
            if ([view isMemberOfClass:[UILabel class]]) {
                
                UILabel *templbl=(UILabel *)view;
                
                
                
                stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
                stringKeyPitch =[NSMutableString stringWithFormat:@"%d",templbl.tag];
                [stringKeyMainPitch appendString:stringKeyPitch];
                
                NSLog(@"%@",stringKeyMainPitch);
                NSString *key =[NSString stringWithFormat:@"k%d",templbl.tag
                                ];
                
                [templbl setText:[dicSetvalues objectForKey:key] ];

                
                
                NSLog(@"%@",templbl.text);
                
                
                if(templbl.tag==14)
                {
                    
                    if(lblbatterPitch)
                        
                    {
                        templbl.text=lblbatterPitch.text;
                        NSLog(@"%@",templbl.text);
                    }
                    
                }
                if(templbl.tag==15)
                {
                    
                    if(lblOut)
                        
                    {
                        templbl.text=lblOut.text;
                        NSLog(@"%@",templbl.text);
                    }
                }
                if(templbl.tag==16)
                {
                    if(lblplayersBalls)
                        
                    {
                        templbl.text=lblplayersBalls.text;
                        NSLog(@"%@",templbl.text);
                    }
                    
                }
                if(templbl.tag==17)
                {
                    if(lblTotalBalls)
                        
                    {
                        templbl.text=lblTotalBalls.text;
                        NSLog(@"%@",templbl.text);
                    }
                    
                }
                
                
                
                
            }
            
            
            if([view isMemberOfClass:[UIImageView class]])
            {
                
                UIImageView *image =(UIImageView *)view;
                
                if(image.tag==mainGridTag+100)
                {
                    
                    
                    for (int i=0; i<100; i++) {
                        
                        
                        stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
                        stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
                        [stringKeyMainPitch appendString:stringKeyPitch];
                        
                        NSLog(@"%@",stringKeyMainPitch);
                        
                        
                        ///image batter
                        if([[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"])
                        {
                            imageView_locatiogGraph.hidden=NO;
                            NSString *imageLocation  =[[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"];
                            NSLog(@"................image.....%@",imageLocation);
                            
                            [image setImage:[UIImage imageNamed:imageLocation]];
                            
                            
                        }
                    }
                    
                    
                }///image for short
                
                if(image.tag==mainGridTag+200)
                {
                    
                    NSMutableString *stringInn =[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
                    
                    
                    NSLog(@"%@",dicInnings);
                    if([dicInnings objectForKey:stringInn])
                    {
                        
                        
                        [image setImage:[UIImage imageNamed:@"Eoi_shape.png"]];
                        
                        
                        
                    }
                    else{
                     
                          [image setImage:[UIImage imageNamed:@""]];
                    }
                    
                }////image for innning
                
                
            }
            
        }
        
        
        
        
        
        //Landscape
        [self closeDisplayViewLandscape];
        
        
    }
    
    
    
    //[self resetMainViewSettings:displayView];
    
    [self addOutInDic];

    
}

#pragma mark Diplaying last set of pitch

-(void)displayLastset
{
    
    int lasSetBall;
    int lastStrike;
    int firstSetVal;
    int lastSetVal;
    dicSetvalues =[[NSMutableDictionary alloc]init];
    //[arraySetvalues removeAllObjects];
    
    NSLog(@"%d",intLastBallRecord+49);
    
    NSLog(@"%d",intLaststrikeRecord);
    if((intLastBallRecord+49)>intLaststrikeRecord)
    {
        lasSetBall =intLastBallRecord;
    }
    else{
        lastStrike=intLaststrikeRecord;
    }
    
    
    if((lasSetBall>=0&&lasSetBall<=6) ||(lastStrike>=49 &&lastStrike<=55))
    {
        firstSetVal =0;
        lastSetVal=6;
    }
    
    if((lasSetBall>=7&&lasSetBall<=13) ||(lastStrike>=56 &&lastStrike<=62))
    {
        firstSetVal =7;
        lastSetVal=13;
        
    }
    if((lasSetBall>=14&&lasSetBall<=20) ||(lastStrike>=63 &&lastStrike<=69))
    {
        firstSetVal =14;
        lastSetVal=20;
    }
    if((lasSetBall>=21&&lasSetBall<=27) ||(lastStrike>=70 &&lastStrike<=76))
    {
        firstSetVal =21;
        lastSetVal=27;
    }
    if((lasSetBall>=28&&lasSetBall<=34) ||(lastStrike>=77 &&lastStrike<=83))
    {
        firstSetVal =28;
        lastSetVal=34;
    }
    if((lasSetBall>=35&&lasSetBall<=41) ||(lastStrike>=84 &&lastStrike<=90))
    {
        firstSetVal =35;
        lastSetVal=41;
    }
    if((lasSetBall>=42&&lasSetBall<=48) ||(lastStrike>=91 &&lastStrike<=97))
    {
        firstSetVal =42;
        lastSetVal=48;
    }

    
    
    int j;
    int strike;
    strike=7;
    j=0;
    
    for (int i=firstSetVal; i<lastSetVal+1; i++) {
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
        
        if([dicAllInnings objectForKey:string])
        {
            dicMainPitch =[dicAllInnings objectForKey:string];
        }
        
        ////balls
        stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
       
        stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
        [stringKeyMainPitch appendString:stringKeyPitch];
       if([[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"pitchLabel"])
       {
           NSString *str =[[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"pitchLabel"];
           
           NSString *key =[NSString stringWithFormat:@"k%d",j];
           [dicSetvalues setObject:str forKey:key];
           
           
       }
        j++;
      /////////////////////////strikes
      stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
        stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i+49];
        [stringKeyMainPitch appendString:stringKeyPitch];
        if([[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"pitchLabel"])
        {
            NSString *str =[[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"pitchLabel"];
            
            NSString *key =[NSString stringWithFormat:@"k%d",strike];
            [dicSetvalues setObject:str forKey:key];
       
        }
 strike++;
      
         NSLog(@"%d",i);
         NSLog(@"%d",i+49);
        
    }
    
    NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    
    [dicLastSetLabelRecords setObject:dicSetvalues forKey:string];
}




#pragma mark close display view pitcher

-(IBAction)closeDisplayView:(UIButton *)sender
{
    NSMutableString * stringInn=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    
    
    if(![lblOut.text isEqualToString:@"3"])
    {
        if([dicInnings objectForKey:stringInn])
        {
            [dicInnings removeObjectForKey:stringInn];
        }
    }

    
    [viewRunnerOut removeFromSuperview];
    [viewRunnerScored removeFromSuperview];
    [viewStolenBase removeFromSuperview];
    
    [self displayLastset];
    
    if(currentPitchTag)
    {
        UIButton *button=[arrayPitches objectAtIndex:currentPitchTag];
        
        button.layer.cornerRadius=0.0f;
        button.layer.masksToBounds=YES;
        button.layer.borderColor=[[UIColor blueColor]CGColor];
        button.layer.borderWidth= 0.0f;
    }
    // type=@"scroll";
    scrollViewIdentification=YES;
    buttonMonitor.enabled=YES;
    buttonPitcher.enabled=YES;
    buttonCatcher.enabled=YES;
        buttonAddCP.hidden=YES;
    NSLog(@"%d",mainGridTag);
    
     [mask_scrollView setHidden:YES];
    
  NSLog(@" dicAllInnings : %@",dicAllInnings);

   
        
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
        
        if([dicAllInnings objectForKey:string]||lblOut.text)
        {
            dicMainPitch =[dicAllInnings objectForKey:string];
    
    NSLog(@"Button SubClassesArray : %@",pitch_Chart_Btn.subviews);
    
    UIButton *button = [mArray objectAtIndex:mainGridTag];
    
    for (UIView *view in button.subviews) {
        
        if ([view isMemberOfClass:[UILabel class]]) {
            
            UILabel *templbl=(UILabel *)view;
            
            
            
                    stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
                   stringKeyPitch =[NSMutableString stringWithFormat:@"%d",templbl.tag];
                   [stringKeyMainPitch appendString:stringKeyPitch];
                   
                  NSLog(@"%@",stringKeyMainPitch);
                NSString *key =[NSString stringWithFormat:@"k%d",templbl.tag
                                ];
          
                   [templbl setText:[dicSetvalues objectForKey:key] ];
            
            
            
                       NSLog(@"%@",templbl.text);
            
            
            if(templbl.tag==14)
            {
            
                if(lblbatterPitch)
                    
                {
                    templbl.text=lblbatterPitch.text;
                      NSLog(@"%@",templbl.text);
                }
            
            }
            if(templbl.tag==15)
            {
                
                if(lblOut)
                    
                {
                    templbl.text=lblOut.text;
                      NSLog(@"%@",templbl.text);
                }
            }
            if(templbl.tag==16)
            {
                if(lblplayersBalls)
                    
                {
                    templbl.text=lblplayersBalls.text;
                      NSLog(@"%@",templbl.text);
                }
                
            }
            if(templbl.tag==17)
            {
                if(lblTotalBalls)
                    
                {
                    templbl.text=lblTotalBalls.text;
                      NSLog(@"%@",templbl.text);
                }
                
            }
            
            
            
            
        }
        
        
        if([view isMemberOfClass:[UIImageView class]])
        {
          
            UIImageView *image =(UIImageView *)view;
            
            if(image.tag==mainGridTag+100)
            {

                [image setImage:[UIImage imageNamed:@""]];
                for (int i=0; i<100; i++) {
                    
                
                stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
                stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
                [stringKeyMainPitch appendString:stringKeyPitch];
                
                NSLog(@"%@",stringKeyMainPitch);
              
                
                ///image batter
                if([[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"])
                {
                    imageView_locatiogGraph.hidden=NO;
                    NSString *imageLocation  =[[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"];
                    NSLog(@"................image.....%@",imageLocation);
                
            [image setImage:[UIImage imageNamed:imageLocation]];
            
                    
                }
            }
                
                
        }///image for short
            
            if(image.tag==mainGridTag+200)
            {
            
            NSMutableString *stringInn =[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
            
            
            NSLog(@"..............dicInnings........%@",dicInnings);
            if([dicInnings objectForKey:stringInn])
            {
                
                
                [image setImage:[UIImage imageNamed:@"Eoi_shape.png"]];
                

                
            }

            }////image for innning
            
        
    }
    
    }///end of for loop
            
            
            [self closeDisplayViewLandscape];
            
            
               
        }
        
    

    [self resetMainViewSettings:displayView];
    //out
    [self addOutInDic];
    
  
    
    viewPotrait.userInteractionEnabled=YES;
}

-(void)closeDisplayViewLandscape
{
    
    //Landscape
    UIButton *buttonLandscape = [mArrayLandscape objectAtIndex:mainGridTag];
    
    for (UIView *view in buttonLandscape.subviews) {
        
        if ([view isMemberOfClass:[UILabel class]]) {
            
            UILabel *templbl=(UILabel *)view;
            
            
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",templbl.tag];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            NSLog(@"%@",stringKeyMainPitch);
            [templbl setText:[[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"pitchLabel"]];
            
            
            
            NSLog(@"%@",templbl.text);
            
            
            if(templbl.tag==14)
            {
                
                if(lblbatterPitch)
                    
                {
                    templbl.text=lblbatterPitch.text;
                    NSLog(@"%@",templbl.text);
                }
                
            }
            if(templbl.tag==15)
            {
                
                if(lblOut)
                    
                {
                    templbl.text=lblOut.text;
                    NSLog(@"%@",templbl.text);
                }
            }
            if(templbl.tag==16)
            {
                if(lblplayersBalls)
                    
                {
                    templbl.text=lblplayersBalls.text;
                    NSLog(@"%@",templbl.text);
                }
                
            }
            if(templbl.tag==17)
            {
                if(lblTotalBalls)
                    
                {
                    templbl.text=lblTotalBalls.text;
                    NSLog(@"%@",templbl.text);
                }
                
            }
            
            
            
            
        }
        
        
        if([view isMemberOfClass:[UIImageView class]])
        {
            
            UIImageView *image =(UIImageView *)view;
            
            if(image.tag==mainGridTag+100)
            {
                
                [image setImage:[UIImage imageNamed:@""]];
                for (int i=0; i<14; i++) {
                    
                    
                    stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
                    stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
                    [stringKeyMainPitch appendString:stringKeyPitch];
                    
                    NSLog(@"%@",stringKeyMainPitch);
                    
                    
                    ///image batter
                    if([[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"])
                    {
                        imageView_locatiogGraph.hidden=NO;
                        NSString *imageLocation  =[[dicMainPitch objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"];
                        NSLog(@"................image.....%@",imageLocation);
                        
                        [image setImage:[UIImage imageNamed:imageLocation]];
                        
                        
                    }
                }
                
                
            }///image for short
            
            if(image.tag==mainGridTag+200)
            {
                
                NSMutableString *stringInn =[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
                
                
                NSLog(@"..............dicInnings........%@",dicInnings);
                if([dicInnings objectForKey:stringInn])
                {
                    
                    
                    [image setImage:[UIImage imageNamed:@"Eoi_shape.png"]];
                    
                    
                    
                }
                
            }////image for innning
            
            
        }
        
    }//end of for loop
    

    
} 



-(void) resetMainViewSettings:(UIView *)view
{
    [view removeFromSuperview];
    [batter_scrollView setAlpha:1];
    [scrollView setAlpha:1];
    [batter_scrollViewLandscape setAlpha:1];
    [scrollViewLandscape setAlpha:1];
    [batterInfo_View addSubview:maskView];
    [scrollView setScrollEnabled:YES];
    [scrollView setUserInteractionEnabled:YES];
    [batter_scrollView setScrollEnabled:YES];
    [batter_scrollView setUserInteractionEnabled:YES];
    [mask_scrollView setHidden:YES];
[scrollLocation setHidden:YES];
}


#pragma mark --------------------Autorotation
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{ 
    
    
    [scrollView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    [batter_scrollView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated: NO];
    
    [scrollViewLandscape scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    [batter_scrollViewLandscape scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated: NO];
    
    [mask_scrollView scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    
    if(interfaceOrientation == UIInterfaceOrientationPortrait ||
       interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        //
        mode=YES;
      //  viewMonitor.hidden=NO;
        [numbers_ImageView setFrame:CGRectMake(155, 44, 612, 21)];
        
        [batterTitile_imageView setFrame:CGRectMake(3,46, 147, 19)];
        [viewAddPlayerCP setFrame:CGRectMake(scrollView.center.x-340, scrollView.center.y-260, 379, 269)];
        [displayView setFrame:CGRectMake(scrollView.center.x-405, scrollView.center.y-380, 449, 282)];
        [mask_scrollView setFrame:CGRectMake(scrollView.frame.origin.x, 404, 310, 500)];
        [mask_scrollView setContentSize:CGSizeMake(310, 870)];
        [batterInfo_View setFrame:CGRectMake(scrollView.center.x-350, scrollView.center.y-350, 410, 429)];
        [scrollLocation setFrame:CGRectMake(464,404, 310, 500)];
        
       
        //toolbar
        [labelLoading setFrame:CGRectMake(339,537, 98,21)];
        [viewUpperMenu setFrame:CGRectMake(0,0, 768, 96)];
        [labelCatcher setFrame:CGRectMake(398,10, 135, 21)];
        [labelPitcher setFrame:CGRectMake(564,10, 135, 21)];
          [labelDate setFrame:CGRectMake(138,11, 80, 21)];
           [labelOpponent setFrame:CGRectMake(231,11, 150, 21)];
        [_toolbar setFrame:CGRectMake(0,0, 768,44)];
        viewPotrait.hidden=NO;
        viewLandscape.hidden=YES;
     buttonMonitor.hidden=NO;
         [scrollNumberLine setFrame:CGRectMake(153,44,635, 21)];
          //[numberLine setImage:[UIImage imageNamed:@"scaleImage.png"]];
          scrollNumberLine.contentSize=CGSizeMake(2228,25);
          [viewPotrait setFrame:CGRectMake(0, 66, 1024,940)];
        numberLineLand.hidden=YES;
          numberLine.hidden=NO;
        
    }
    
    else
        
    {    // Landscape
        
        mode=NO;
        viewMonitor.hidden=NO;
//        viewLandscape.hidden=YES;
//        viewPotrait.hidden=YES;
   
        
        [viewLandscape setFrame:CGRectMake(0, 66, 1024,916)];
        [numbers_ImageView setFrame:CGRectMake(480, 44, 546, 21)];
        
        [batterTitile_imageView setFrame:CGRectMake(0, 46, 155, 21)];
          [viewAddPlayerCP setFrame:CGRectMake(470, scrollView.center.y-340, 379, 269)];
        [displayView setFrame:CGRectMake(scrollView.center.x-280, scrollView.center.y-420, 449, 282)];
        [batterInfo_View setFrame:CGRectMake(scrollView.center.x-300, scrollView.center.y-395, 410, 429)];
        [mask_scrollView setFrame:CGRectMake(scrollView.center.x-280, 364, 310, 500)];
        [mask_scrollView setContentSize:CGSizeMake(310, 1200)];
         
        [viewUpperMenu setFrame:CGRectMake(0,0, 968, 66)];
        
        [scrollNumberLine setFrame:CGRectMake(156,44, 878, 21)];
         scrollNumberLine.contentSize=CGSizeMake(1780,25);
        numberLine.hidden=YES;
       numberLineLand.hidden=NO;
        
        [scrollLocation setFrame:CGRectMake(585,362, 400, 500)];
      
        //toolbar
        [labelLoading setFrame:CGRectMake(539,437, 98,21)];
        [popoverController dismissPopoverAnimated:NO];
          [labelCatcher setFrame:CGRectMake(650, 10,135, 21)];
          [labelPitcher setFrame:CGRectMake(820, 10,135, 21)];
          [_toolbar setFrame:CGRectMake(0,0, 1024, 44)];
        [labelDate setFrame:CGRectMake(100,11, 80, 21)];
        [labelOpponent setFrame:CGRectMake(430,11, 150, 21)];
        buttonMonitor.hidden=NO;
        
        viewPotrait.hidden=YES;
        viewLandscape.hidden=NO;
        
        //[self.view bringSubviewToFront:_toolbar];
        
        
        
    }
    [graph_ImageView setImage:[UIImage imageNamed:@"Bfcenterfield.png"]];
    return YES; //UIInterfaceOrientationPortrait;

}

#pragma mark runner/stolen

-(IBAction)runnerScored:(id)sender
{
    [view_ballTypeOfPitch setHidden:YES];
    [view_strikeTypeOfPitch setHidden:YES];
    [view_StrikeView setHidden:YES];
    [view_strikeOutView setHidden:YES];
    [view_hitFairView setHidden:YES];
    [view_locationOfHitView setHidden:YES];
    if(viewRunnerOut||viewStolenBase)
    {
        [viewRunnerOut removeFromSuperview];
        [viewStolenBase removeFromSuperview];
    }
    [viewRunnerScored setFrame:CGRectMake(165, 405,264,149)];
    [self.view addSubview:viewRunnerScored];
    
}
-(IBAction)runnerOut:(id)sender
{
    
    [view_ballTypeOfPitch setHidden:YES];
    [view_strikeTypeOfPitch setHidden:YES];
    [view_StrikeView setHidden:YES];
    [view_strikeOutView setHidden:YES];
    [view_hitFairView setHidden:YES];
    [view_locationOfHitView setHidden:YES];
    


    if(viewRunnerScored||viewStolenBase)
    {
        [viewRunnerScored removeFromSuperview];
        [viewStolenBase removeFromSuperview];
    }
        [viewRunnerOut setFrame:CGRectMake(165,405,234, 179)];
    [self.view addSubview:viewRunnerOut];
    
}
-(IBAction)stolenBase:(id)sender
{
    
    [view_ballTypeOfPitch setHidden:YES];
    [view_strikeTypeOfPitch setHidden:YES];
    [view_StrikeView setHidden:YES];
    [view_strikeOutView setHidden:YES];
    [view_hitFairView setHidden:YES];
    [view_locationOfHitView setHidden:YES];
    if(viewRunnerOut||viewRunnerScored)
    {
        [viewRunnerOut removeFromSuperview];
        [viewRunnerScored removeFromSuperview];
    }
        [viewStolenBase setFrame:CGRectMake(165,405,258, 179)];
       [self.view addSubview:viewStolenBase];
}

#pragma mark 

-(IBAction)runnerScoredButtons:(id)sender
{
    
    if([sender tag]==0)
    {
        stringRunnerScored =@"re";
    }
    if([sender tag]==1)
    {
        stringRunnerScored =@"ru";
    }
    [viewRunnerScored removeFromSuperview];
}

-(IBAction)runnerOutButtons:(id)sender
{
    // dicStrikeDetails =[[NSMutableDictionary alloc]init];
    
    //[dicStrikeDetails setObject: @"o" forKey:@"lblOut"];
    
    stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
    stringKeyPitch =[NSMutableString stringWithFormat:@"%d",[sender tag]];
    [stringKeyMainPitch appendString:stringKeyPitch];
    
    [dicRunOut setObject:@"o" forKey:stringKeyMainPitch];
    
     NSLog(@"........%@",dicRunOut);
    NSLog(@"/////////%@",dicMainPitch);
  NSLog(@"///////strOutLastRecord//%d",intOutLastRunner);
    if(intOutLastRunner==0)
    {
        [lblOut setText:@"1"];
    }
    
    if(intOutLastRunner==1)
    {
        [lblOut setText:@"2"];
    }
    if(intOutLastRunner==2)
    {
        [lblOut setText:@"3"];
        //intOutLastRunner=3;
    }
    if(intOutLastRunner==3)
    {
        [lblOut setText:@"1"];
    }
    
    
    if([lblOut.text isEqualToString:@"3"])
    {
        alertMessage=@"End of Inning?";
        [self alertMessageMethod:alertMessage];
        alertMessageType= @"End of Inning?";

    }
    
    //[self strikeBallReplacement];
    //NSLog(@"%@",dicMainPitch);
    //[self OutDisplay];
   // [self BallsCalculation];

    
    
    
    if([sender tag]==0)
    {
        stringRunnnerOut =@"fp";
    }
    if([sender tag]==1)
    {
         stringRunnnerOut =@"bp";
    }
    if([sender tag]==2)
    {
         stringRunnnerOut =@"as";
    }
    [viewRunnerOut removeFromSuperview];
}
-(IBAction)stolenBaseButtons:(id)sender
{
    if([sender tag]==0)
    {
        stringStolenBase =@"sb";
        
    }
    if([sender tag]==1)
    {
        stringStolenBase =@"tb";
    }
    if([sender tag]==2)
    {
        stringStolenBase =@"hm";
    }
    [viewStolenBase removeFromSuperview];
}


-(void) bringScrollSubViewToFrontOnMainView:(UIView *) view
{
    [mask_scrollView setHidden:NO];
    [view setHidden:NO]; 
    [view removeFromSuperview];
    [mask_scrollView addSubview:view];
    [mask_scrollView bringSubviewToFront:view];
}

-(void) bringScrollSubViewToFrontOnMainViewLocation:(UIView *) view
{

    if(mode==YES)
    {
         [scrollLocation setFrame:CGRectMake(464,404, 310, 500)];
    }
    else{
         [scrollLocation setFrame:CGRectMake(585,362, 400, 500)];
    }
   
    [scrollLocation setContentSize:CGSizeMake(310, 500)];
    [scrollLocation scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    
    [scrollLocation setHidden:NO];
    [view_locationOfHitView setHidden:NO];
    [view_defensivePositionsView setHidden:NO];
    [view setHidden:NO];
    //[view removeFromSuperview];
    [scrollLocation addSubview:view];
    [scrollLocation bringSubviewToFront:view];
    [self.view addSubview:scrollLocation];
}

-(BOOL)validateTextField:(NSString *)candidate 
{
    NSLog(@"validateTextField: Called");
    NSString *emailRegex=@"^(?:|0|[0-9]\\d*)(?:\\.\\d*)?$"; 
    NSLog(@"-all string value %@-",emailRegex);
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex]; 
    return [emailTest evaluateWithObject:candidate];
    
}



#pragma mark strikeBallReplacement///////////////////////////////////////////add on main dic

-(void) strikeBallReplacement
{
    if(currentPitchTag<49)
    {
         UIButton *button=[arrayPitches objectAtIndex:currentPitchTag+49];
        button.titleLabel.text=@"";
        
        NSMutableString * string=[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
        [string appendString:[NSMutableString stringWithFormat:@"%d",currentPitchTag+49]];
        [dicMainPitch removeObjectForKey:string];
    }
    if(currentPitchTag>48)
    {
        UIButton *button=[arrayPitches objectAtIndex:currentPitchTag-49];
        button.titleLabel.text=@"";
        
        NSMutableString * string=[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
        [string appendString:[NSMutableString stringWithFormat:@"%d",currentPitchTag-49]];
        [dicMainPitch removeObjectForKey:string];

    }
    
    NSLog(@"%@",dicMainPitch);
    
      NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    
    [dicAllInnings setObject:dicMainPitch forKey:string];
    NSLog(@"%@",dicAllInnings);
    
}

#pragma mark-------------------- BallsCalculation

-(void)BallsCalculation
{
    
    
    
    [self removeForwardPitchLabel];

    
    intTotalBalls=0;
    intNumberOfBalls1=0;
    lblplayersBalls.text=@"0";
    lblOut.text=@"";
    NSLog(@"%@",[dicAllInnings allKeys]);
   NSLog(@"%@",dicAllInnings);
    
    

    //out-not for On main click
    
    if(intalertInning==10)
    {
       [self OutDisplay];  
    }
   
    
    
    
    
  NSLog(@"%@",dicAllInnings);
    
    
    
    //total balls for each box
//    for (int i =0; i<=mainGridTag;i++) {
//    
//    NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",i];
//        
//        if([dicAllInnings objectForKey:string])
//        {
//          intTotalBalls = intTotalBalls +  [[[dicAllInnings objectForKey:string] allKeys]count];
//        }
//
//    }
    
    
    
    [self TotalBallsofMatch];
    
     
    
       
    
    
    intPlayerForRows = [ self PitchToBatter];
    
    //plyers ball count
    if(intPlayerForRows==0)
    {
       
        
        for (int i =0; i<9;i++) {
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",i];
            
            if([dicAllInnings objectForKey:string])
            {
                intNumberOfBalls1 = intNumberOfBalls1 +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
        }
    }
    if(intPlayerForRows==1)
    {
         
        for (int i =10; i<20;i++) {
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",i];
            
            if([dicAllInnings objectForKey:string])
            {
                intNumberOfBalls1 = intNumberOfBalls1 +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
        }
        
    }
   if(intPlayerForRows==2)
    {
       
        
        for (int i =20; i<30;i++) {
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",i];
            
            if([dicAllInnings objectForKey:string])
            {
                intNumberOfBalls1 = intNumberOfBalls1 +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
        }
    }
    if(intPlayerForRows==3)
    {
        
        for (int i =30; i<40;i++) {
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",i];
            
            if([dicAllInnings objectForKey:string])
            {
                intNumberOfBalls1 = intNumberOfBalls1 +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
        }
    }
   if(intPlayerForRows==4)
    {
      
        for (int i =40; i<50;i++) {
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",i];
            
            if([dicAllInnings objectForKey:string])
            {
                intNumberOfBalls1 = intNumberOfBalls1 +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
        }
    }
    if(intPlayerForRows==5)
    {
       
        for (int i =50; i<60;i++) {
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",i];
            
            if([dicAllInnings objectForKey:string])
            {
                intNumberOfBalls1 = intNumberOfBalls1 +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
        }
    }
   if(intPlayerForRows==6)
    {
  
        for (int i =60; i<70;i++) {
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",i];
            
            if([dicAllInnings objectForKey:string])
            {
                intNumberOfBalls1 = intNumberOfBalls1 +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
        }
    }

    if(intPlayerForRows==7)
    {
       
        for (int i =70; i<80;i++) {
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",i];
            
            if([dicAllInnings objectForKey:string])
            {
                intNumberOfBalls1 = intNumberOfBalls1 +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
        }
    }
    if(intPlayerForRows==8)
    {
       
        for (int i =80; i<90;i++) {
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",i];
            
            if([dicAllInnings objectForKey:string])
            {
                intNumberOfBalls1 = intNumberOfBalls1 +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
        }
    }

 lblplayersBalls.text =[NSMutableString stringWithFormat:@"%d",intNumberOfBalls1];
    
    
    
    
}
        
#pragma mark Battter Identification by Pitch
-(int)PitchToBatter
{
    
    
    if(mainGridTag==0||mainGridTag==1||mainGridTag==2||mainGridTag==3||mainGridTag==4||mainGridTag==5||mainGridTag==6||mainGridTag==7||mainGridTag==8||mainGridTag==9)
    {
      
        intPlayerForRows=0;
        
       
    }
    if(mainGridTag==10||mainGridTag==11||mainGridTag==12||mainGridTag==13||mainGridTag==14||mainGridTag==15||mainGridTag==16||mainGridTag==17||mainGridTag==18||mainGridTag==19)
    {
        intPlayerForRows=1;
                
    }
    if(mainGridTag==20||mainGridTag==21||mainGridTag==22||mainGridTag==23||mainGridTag==24||mainGridTag==25||mainGridTag==26||mainGridTag==27||mainGridTag==28||mainGridTag==29)
    {
        intPlayerForRows=2;
        
        
    }
    if(mainGridTag==30||mainGridTag==31||mainGridTag==32||mainGridTag==33||mainGridTag==34||mainGridTag==35||mainGridTag==36||mainGridTag==37||mainGridTag==38||mainGridTag==39)
    {
        intPlayerForRows=3;
        
            
    }
    if(mainGridTag==40||mainGridTag==41||mainGridTag==42||mainGridTag==43||mainGridTag==44||mainGridTag==45||mainGridTag==46||mainGridTag==47||mainGridTag==48||mainGridTag==49)
    {
        intPlayerForRows=4;
        
           
    }
    if(mainGridTag==50||mainGridTag==51||mainGridTag==52||mainGridTag==53||mainGridTag==54||mainGridTag==55||mainGridTag==56||mainGridTag==57||mainGridTag==58||mainGridTag==59)
    {
        intPlayerForRows=5;
        
            
    }
    if(mainGridTag==60||mainGridTag==61||mainGridTag==62||mainGridTag==63||mainGridTag==64||mainGridTag==65||mainGridTag==66||mainGridTag==67||mainGridTag==68||mainGridTag==69)
    {
        intPlayerForRows=6;
    
           
    }
    
    if(mainGridTag==70||mainGridTag==71||mainGridTag==72||mainGridTag==73||mainGridTag==74||mainGridTag==75||mainGridTag==76||mainGridTag==77||mainGridTag==78||mainGridTag==79)
    {
        intPlayerForRows=7;
        
    }
    if(mainGridTag==80||mainGridTag==81||mainGridTag==82||mainGridTag==83||mainGridTag==84||mainGridTag==85||mainGridTag==86||mainGridTag==87||mainGridTag==88||mainGridTag==89)
    {
        intPlayerForRows=8;
        
    }

    return intPlayerForRows;
    
}

#pragma mark LastImageBatter
-(void)LastImageBatter
{
    
    NSMutableString * stringLastImageBatter=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    
    if([dicAllInnings objectForKey:stringLastImageBatter])
    {
    
    int j ;
    j=0;
    
    for (int i=0; i<97; i++) {
        
        
        if(j==0)
        {
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",j];
            [stringKeyMainPitch appendString:stringKeyPitch];
            ///image batter
            if([[[dicAllInnings objectForKey:stringLastImageBatter] objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"])
            {
                imageView_locatiogGraph.hidden=NO;
                NSString *imageLocation  =[[[dicAllInnings objectForKey:stringLastImageBatter] objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"];
                NSLog(@"................image.....%@",imageLocation);
                
                [imageView_locatiogGraph setImage:[UIImage imageNamed:imageLocation]];
            }
            
            ////batter hit Label
            if([[[dicAllInnings objectForKey:stringLastImageBatter] objectForKey:stringKeyMainPitch] objectForKey:@"lblbatterPitch"])
            {
                
                imageEndOfInning.hidden=YES;
                imageView_locatiogGraph.hidden=YES;
                lblbatterPitch.text  =[[[dicAllInnings objectForKey:stringLastImageBatter] objectForKey:stringKeyMainPitch] objectForKey:@"lblbatterPitch"];
                NSLog(@"................batter.....%@", lblbatterPitch.text);
            }

            
            NSLog(@"..............%d",j);
        }
       
        
        if(j<49)
        {
            
                  j=j+49;
         
            
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",j];
            [stringKeyMainPitch appendString:stringKeyPitch];
            ///image batter
            if([[[dicAllInnings objectForKey:stringLastImageBatter] objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"])
            {
                imageView_locatiogGraph.hidden=NO;
                NSString *imageLocation  =[[[dicAllInnings objectForKey:stringLastImageBatter] objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"];
                NSLog(@"................image.....%@",imageLocation);
                 [imageView_locatiogGraph setImage:[UIImage imageNamed:imageLocation]];
            }
            
            ////batter hit Label
            if([[[dicAllInnings objectForKey:stringLastImageBatter] objectForKey:stringKeyMainPitch] objectForKey:@"lblbatterPitch"])
            {
                
                imageEndOfInning.hidden=YES;
                imageView_locatiogGraph.hidden=NO;
                lblbatterPitch.text  =[[[dicAllInnings objectForKey:stringLastImageBatter] objectForKey:stringKeyMainPitch] objectForKey:@"lblbatterPitch"];
                NSLog(@"................batter.....%@", lblbatterPitch.text);
            }
            

            
            NSLog(@"................%d",j);
            
    
        }
        
        if(j>48&&j!=0&&j!=97)
        {
            j =j-48;
            
            
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",j];
            [stringKeyMainPitch appendString:stringKeyPitch];
            ///image batter
            if([[[dicAllInnings objectForKey:stringLastImageBatter] objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"])
            {
                imageView_locatiogGraph.hidden=NO;
                NSString *imageLocation  =[[[dicAllInnings objectForKey:stringLastImageBatter] objectForKey:stringKeyMainPitch] objectForKey:@"StrikerFieldImage"];
                NSLog(@"................image.....%@",imageLocation);
                 [imageView_locatiogGraph setImage:[UIImage imageNamed:imageLocation]];
                
                
                
            }
            
            ////batter hit Label
            if([[[dicAllInnings objectForKey:stringLastImageBatter] objectForKey:stringKeyMainPitch] objectForKey:@"lblbatterPitch"])
            {
                
                imageEndOfInning.hidden=YES;
                imageView_locatiogGraph.hidden=YES;
                lblbatterPitch.text  =[[[dicAllInnings objectForKey:stringLastImageBatter] objectForKey:stringKeyMainPitch] objectForKey:@"lblbatterPitch"];
                NSLog(@"................batter.....%@", lblbatterPitch.text);
            }
            

            
            
            NSLog(@".............................%d",j);
            
        }
        
    
        
        
    }
    
    
    }
}


#pragma mark=========== Ball/strike Constraints
-(void)recordBallStrike
{
    
    
    intLastOutRecord=99;
    intRecordBalls=0;
    intRecordStrike=0;
    intFoulRecord=0;
    
 
    NSMutableString *ballRecord=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    
    if([dicAllInnings objectForKey:ballRecord])
    {
        
        
        
        for (int i=0; i<49; i++)//balls
        {
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            NSLog(@"%@",stringKeyMainPitch);
            if([[[dicAllInnings objectForKey:ballRecord] objectForKey:stringKeyMainPitch]  objectForKey:@"pitchLabel"])
            {
                        
            
                intRecordBalls++;
                intLastBallRecord=i;
            }
        }
        
        
        for (int i=49; i<99; i++)//strikes
        {
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
            [stringKeyMainPitch appendString:stringKeyPitch];
           
            ///outs
            if([[[dicAllInnings objectForKey:ballRecord] objectForKey:stringKeyMainPitch]  objectForKey:@"lblOut"])
            {
                
                intLastOutRecord=i;
                
                // [[dicAllInnings objectForKey:ballRecord] removeObjectForKey:stringKeyMainPitch];
                
                
            }
            
            NSLog(@"%@",stringKeyMainPitch);
            if([[[dicAllInnings objectForKey:ballRecord] objectForKey:stringKeyMainPitch]  objectForKey:@"pitchLabel"])
            {
               
                intRecordStrike++;
                 intLaststrikeRecord=i;
                LastFoulconstraint=NO;
                

                
                if([[[dicAllInnings objectForKey:ballRecord] objectForKey:stringKeyMainPitch]  objectForKey:@"foul"])
                {
                    LastFoulconstraint=YES;
                    intFoulRecord++;
                }
                
            }
        }

        
}

    
      
}


#pragma mark getEndOfInning
-(void)getEndOfInning
{
      NSMutableString *stringInn =[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    
    
    NSLog(@"%@",dicInnings);
    if([dicInnings objectForKey:stringInn])
{
   // lblbatterPitch.text=@"";
    imageEndOfInning.hidden=NO;

    lblbatterPitch.backgroundColor = [UIColor clearColor];
    
}
    else{
        
        imageEndOfInning.hidden=YES;
        
    }

}



#pragma mark========= ========================================OutDisplay for pitchc click
-(void)OutDisplay
{
    intStrikeOut=0;
    foul=0;
    boolalertEof=NO;
    
    imageEndOfInning.hidden=YES;
    [self foulCount];
    
    if(LastFoul ==YES)
    {
        return;
    }
    NSMutableString * stringOut=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    
    if([dicAllInnings objectForKey:stringOut])
    {
        
        
        for (int i=49; i<98; i++)
        {
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            
            
            
            NSLog(@"%@",stringKeyMainPitch);
            if([[[dicAllInnings objectForKey:stringOut] objectForKey:stringKeyMainPitch]  objectForKey:@"lblOut"])
            {
                //[lblOut setText:[[[dicAllInnings objectForKey:stringOut] objectForKey:stringKeyMainPitch] objectForKey:@"lblOut"]];
                
                [self LastOutEntry];
                
                if(intOutLastRecord==3)
                {
                    
                    
                    [lblOut setText:[NSString stringWithFormat:@"%d",1]];
                }
                    else
                    {
                          [lblOut setText:[NSString stringWithFormat:@"%d",intOutLastRecord+1]];
                        
                        if (intOutLastRecord==2) {
                          
                            boolalertEof=YES;

                        }
                     
                        
                    }
                
          
            }
            
            if([[[dicAllInnings objectForKey:stringOut] objectForKey:stringKeyMainPitch]  objectForKey:@"strike"] ==@"S")
            {
                intStrikeOut++;
                
               
               
               
                if(intStrikeOut==3&&foul<=2)
                {
                    
                     //[lblOut setText:[NSString stringWithFormat:@"%d",intOutPlyers]];
                      [self LastOutEntry];
                    if(intOutLastRecord==3)
                    {
                        [lblOut setText:[NSString stringWithFormat:@"%d",1]];
                    }
                    else
                    {
                        [lblOut setText:[NSString stringWithFormat:@"%d",intOutLastRecord+1]];
                        
                        if (intOutLastRecord==2) {
                            
                        boolalertEof=YES;
                            
                        }
                    }
                    
                }
 
          ////////////////////////foul///////
                if(intStrikeOut>3&&foul<intStrikeOut)
                {
                    [self LastOutEntry];
                    if(intOutLastRecord==3)
                    {
                        [lblOut setText:[NSString stringWithFormat:@"%d",1]];
                    }
                    else
                    {
                        [lblOut setText:[NSString stringWithFormat:@"%d",intOutLastRecord+1]];
                        
                        if (intOutLastRecord==2) {
                            
                            boolalertEof=YES;
                            
                        }
                    }
                    
                }
                
                
            }///strikeeeeee
            
            
        }////end of for loop
        
        
        
        
        
        ////////runner outt
        
        
        
        
        
        
        
        
        
        
        
        
        
        if(boolalertEof==YES)
        {
            alertMessage=@"End of Inning?";
            [self alertMessageMethod:alertMessage];
            alertMessageType= @"End of Inning?";
        }
        else
        {
            imageEndOfInning.hidden=YES;
        }
      
        //////////////////////////////////
        
    [self runnerOutRec];
        

}
    
//[self addOutInDic];
    
}


-(void)runnerOutRec
{
    
}

-(void)foulCount
{
    foul=0;
     NSLog(@"%@",dicAllInnings);
    NSMutableString * stringOut=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    
    

    if([dicAllInnings objectForKey:stringOut])
    {
        
        
        for (int i=49; i<99; i++)
        {
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            
            if([[[dicAllInnings objectForKey:stringOut] objectForKey:stringKeyMainPitch]  objectForKey:@"pitchLabel"])
            {
                LastFoul =NO;
            }

             if([[[dicAllInnings objectForKey:stringOut] objectForKey:stringKeyMainPitch]  objectForKey:@"foul"])
             {
                  LastFoul =YES;
                 foul++;
             }
        }
    
    }
}

#pragma mark -----------storing runner stolen base----------addOutInDic
-(void)addOutInDic
{
    NSLog(@"%@",dicOutRecord);
    
    
        NSMutableString * string1=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    
    
    NSLog(@"%@  ",[dicOutRecord objectForKey:string1]);
    
    if(![dicOutRecord objectForKey:string1])
    {
        NSString *str= lblOut.text;
        
        if(str.length>0){
                intOutLastRunner =[str intValue];
        }
        
        else{
           // intOutLastRunner=0;
        }
    
    }
    
    
    
    /////////out
    if(lblOut.text.length)
    {
    
     
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
        [dicOutRecord setObject:lblOut.text forKey:string];
        
 if([lblOut.text isEqualToString:@"2"])
 {
     intMonitorTwoOutRecord=mainGridTag;
 }
     
    }
    else
    {
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
       
        if(  [dicOutRecord objectForKey:string])
        {
         
            [dicOutRecord removeObjectForKey:string];
        }
        
    }
    
    ////Tb    
    if(lblTotalBalls.text.length)
    {
        
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBoxTb%d",mainGridTag];
        [dicOutRecord setObject:lblTotalBalls.text forKey:string];
        
    }
    else
    {
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBoxTb%d",mainGridTag];
        
        if(  [dicOutRecord objectForKey:string])
        {
            
            [dicOutRecord removeObjectForKey:string];
        }
        
    }
    
    ///PB
    if(lblplayersBalls.text.length)
    {
        
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBoxPb%d",mainGridTag];
        [dicOutRecord setObject:lblplayersBalls.text forKey:string];
        
    }
    else
    {
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBoxPb%d",mainGridTag];
        
        if(  [dicOutRecord objectForKey:string])
        {
            
            [dicOutRecord removeObjectForKey:string];
        }
        
    }
    /////////runnerscorred
    
    if(stringRunnerScored.length)
    {
        
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBoxRS%d",mainGridTag];
        [dicOutRecord setObject:stringRunnerScored forKey:string];
        
    }
    else
    {
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBoxRS%d",mainGridTag];
        
        if(  [dicOutRecord objectForKey:string])
        {
            
            [dicOutRecord removeObjectForKey:string];
        }
        
    }
    
    ////////////////runnerOUT
    if(stringRunnnerOut.length)
    {
        
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBoxRO%d",mainGridTag];
        [dicOutRecord setObject:stringRunnnerOut forKey:string];
        
    }
    else
    {
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBoxRO%d",mainGridTag];
        
        if(  [dicOutRecord objectForKey:string])
        {
            
            [dicOutRecord removeObjectForKey:string];
        }
        
    }
    
    
    /////////////stolenBase
    if(stringStolenBase.length)
    {
        
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBoxSB%d",mainGridTag];
        [dicOutRecord setObject:stringStolenBase forKey:string];
        
    }
    else
    {
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBoxSB%d",mainGridTag];
        
        if(  [dicOutRecord objectForKey:string])
        {
            
            [dicOutRecord removeObjectForKey:string];
        }
        
    }
    
    //////rec pitcher for inings
    
    NSMutableString * stringOutPitcher=[NSMutableString stringWithFormat:@"OutPitcher%d",mainGridTag];
    [dicOutRecord setObject:labelPitcher.text forKey:stringOutPitcher];
    
    
}
#pragma mark getOutEntry
-(void)getOutEntry
{
    NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    NSLog(@"......%d",mainGridTag);
    
    if([dicOutRecord objectForKey:string])
    {
        lblOut.text = [dicOutRecord objectForKey:string];
    }
}

#pragma mark LastOutEntry----------------------------algo
-(void)LastOutEntry
{
    NSLog(@"......%@",dicOutRecord);
    
   // intOutLastRecord=intOutLastRunner;
    
    
    
    for (int index=0; index<mainGridTag; (index +=10)) {
        
         NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
        NSLog(@"......%d",index);
        
        if([dicOutRecord objectForKey:string])
        {
            intOutLastRecord = [[dicOutRecord objectForKey:string]intValue];
        }
        
        
        
        
        if(index==80)
        {
            index=1;
            NSLog(@"......%d",index);
            
              NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            
            if([dicOutRecord objectForKey:string])
            {
                intOutLastRecord = [[dicOutRecord objectForKey:string]intValue];
            }
            
            

        }
        if(index==81)
        {
            index=2;
            NSLog(@"......%d",index);
               NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            if([dicOutRecord objectForKey:string])
            {
                intOutLastRecord = [[dicOutRecord objectForKey:string]intValue];
            }
            
            

        }
        
        if(index==82)
        {
            index=3;
            NSLog(@"......%d",index);
               NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            if([dicOutRecord objectForKey:string])
            {
                intOutLastRecord = [[dicOutRecord objectForKey:string]intValue];
            }
            
            

        }
        
        if(index==83)
        {
            index=4;
            NSLog(@"......%d",index);
               NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            if([dicOutRecord objectForKey:string])
            {
                intOutLastRecord = [[dicOutRecord objectForKey:string]intValue];
            }
            
            

        }
        if(index==84)
        {
            index=5;
            NSLog(@"......%d",index);
               NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            if([dicOutRecord objectForKey:string])
            {
                intOutLastRecord = [[dicOutRecord objectForKey:string]intValue];
            }
            
            

        }
        if(index==85)
        {
            index=6;
            NSLog(@"......%d",index);
               NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            if([dicOutRecord objectForKey:string])
            {
                intOutLastRecord = [[dicOutRecord objectForKey:string]intValue];
            }
            
            

        }
        if(index==86)
        {
            index=7;
               NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            if([dicOutRecord objectForKey:string])
            {
                intOutLastRecord = [[dicOutRecord objectForKey:string]intValue];
            }
            
            

            
        }
        if(index==87)
        {
            index=8;
               NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            if([dicOutRecord objectForKey:string])
            {
                intOutLastRecord = [[dicOutRecord objectForKey:string]intValue];
            }
            
            

            
        }
        if(index==88)
        {
            index=9;
               NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            if([dicOutRecord objectForKey:string])
            {
                intOutLastRecord = [[dicOutRecord objectForKey:string]intValue];
            }
            
            

            
        }
        
        
        
    }
  
}

#pragma mark removeForwardPitchLabel

-(void)removeForwardPitchLabel

{
     boolFoul=NO;
//    LastFoul=NO;
//      [self foulCount];
    
    NSLog(@"%@",[dicAllInnings allKeys]);
    NSLog(@"%@",dicAllInnings);
    
    
    
    intRecordBalls=0;
    intRecordStrike=0;
    intFoulRecord=0;
    intLastOutRecord=99;
    
    NSMutableString *ballRecord=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    
    if([dicAllInnings objectForKey:ballRecord])
    {
        
        
        
        for (int i=0; i<49; i++)//balls
        {
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            NSLog(@"%@",stringKeyMainPitch);
            if([[[dicAllInnings objectForKey:ballRecord] objectForKey:stringKeyMainPitch]  objectForKey:@"pitchLabel"])
            {
                
                
                intRecordBalls++;
                intLastBallRecord=i;
            }
        }//balls 
        
        
        for (int i=49; i<99; i++)//strikes
        {
            
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            
            
            NSLog(@"%@",stringKeyMainPitch);
            if([[[dicAllInnings objectForKey:ballRecord] objectForKey:stringKeyMainPitch]  objectForKey:@"pitchLabel"])
            {
                
                intRecordStrike++;
                intLaststrikeRecord=i;
                
                
                
            } 
            
            
            if([[[dicAllInnings objectForKey:ballRecord] objectForKey:stringKeyMainPitch]  objectForKey:@"lblOut"])
            {
                
                intLastOutRecord=i;
                
               // [[dicAllInnings objectForKey:ballRecord] removeObjectForKey:stringKeyMainPitch];
                
                
            }
            
        }///strikes
        
        
    }//if end
    
    
    stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
    stringKeyPitch =[NSMutableString stringWithFormat:@"%d",intLaststrikeRecord];
    [stringKeyMainPitch appendString:stringKeyPitch];
    

    
    foul=10;
    if([[[dicAllInnings objectForKey:ballRecord] objectForKey:stringKeyMainPitch]  objectForKey:@"foul"])
    {
        foulremove=100;
    }

    
    [self removeFromDictionary];
    
    //************remove the Record*************////
    
       
}

-(void)removeFromDictionary
{
    
    NSLog(@"%@",[dicAllInnings allKeys]);
    NSLog(@"%@",dicAllInnings);

    int k;
       NSMutableString *ballRecord=[NSMutableString stringWithFormat:@"PlayerBox%d",mainGridTag];
    
    
    if(intLastOutRecord>48&&intLastOutRecord<99)/////////Out//////////
    {
        k=intLastOutRecord+1;
        
        
        
        for (int i=k; i<99; i++)//strikes
        {
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            if([[dicAllInnings objectForKey:ballRecord] objectForKey:stringKeyMainPitch])
            {
                
                [[dicAllInnings objectForKey:ballRecord] removeObjectForKey:stringKeyMainPitch];
                
            }
            
        }
        
        for (int i=k-49; i<49; i++)//Balls
        {
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            
            if([[dicAllInnings objectForKey:ballRecord] objectForKey:stringKeyMainPitch])
            {
                [[dicAllInnings objectForKey:ballRecord] removeObjectForKey:stringKeyMainPitch];
                
            }
        }

        
        
    }
    
    if(intRecordStrike==3)///////strikes out/////////
    {
        
        if(foulremove==10)
        {
            
           k=intLaststrikeRecord+1;
        for (int i=k; i<99; i++)//strikes
        {
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            if([[dicAllInnings objectForKey:ballRecord] objectForKey:stringKeyMainPitch])
            {
                [[dicAllInnings objectForKey:ballRecord] removeObjectForKey:stringKeyMainPitch];
                
            }
            
        }
        
        for (int i=k-49; i<49; i++)//Balls
        {
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            
            if([[dicAllInnings objectForKey:ballRecord] objectForKey:stringKeyMainPitch])
            {
                [[dicAllInnings objectForKey:ballRecord] removeObjectForKey:stringKeyMainPitch];
                
            }
            
        }
        }
              
    }
    if(intRecordBalls==4)////////4 ballls////////
    {
           k=intLastBallRecord +1;
        
        
        for (int i=k; i<49; i++)//Balls
        {
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            if([[dicAllInnings objectForKey:ballRecord] objectForKey:stringKeyMainPitch])
            {
                [[dicAllInnings objectForKey:ballRecord] removeObjectForKey:stringKeyMainPitch];
                
            }
            
        }
        
        for (int i=k+49; i<99; i++)//strike
        {
            stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d",mainGridTag];
            stringKeyPitch =[NSMutableString stringWithFormat:@"%d",i];
            [stringKeyMainPitch appendString:stringKeyPitch];
            
            
            if([[dicAllInnings objectForKey:ballRecord] objectForKey:stringKeyMainPitch])
            {
                [[dicAllInnings objectForKey:ballRecord] removeObjectForKey:stringKeyMainPitch];
                
            }
            
        }

        
    }
    
    
    
    
    
    
    
    
    
    
    NSLog(@"%@",[dicAllInnings allKeys]);
    NSLog(@"%@",dicAllInnings);

    [self NextBatterView:mainGridTag];
    
}

#pragma mark coulumn initial
-(int)intialValueColoumnTag

{
    
    if(mainGridTag==10||mainGridTag==20||mainGridTag==30||mainGridTag==40||mainGridTag==50||mainGridTag==60||mainGridTag==70||mainGridTag==80)
    {
        
        intLastAtBatRecord=0;
        
        
    }
    if(mainGridTag==11||mainGridTag==21||mainGridTag==31||mainGridTag==41||mainGridTag==51||mainGridTag==61||mainGridTag==71||mainGridTag==81)
    {
       
        
           intLastAtBatRecord=0;
    }
    if(mainGridTag==12||mainGridTag==22||mainGridTag==32||mainGridTag==42||mainGridTag==52||mainGridTag==62||mainGridTag==72||mainGridTag==82)
    {
       
           intLastAtBatRecord=0;
        
    }
    if(mainGridTag==13||mainGridTag==23||mainGridTag==33||mainGridTag==43||mainGridTag==53||mainGridTag==63||mainGridTag==73||mainGridTag==83)
    {
    
           intLastAtBatRecord=0;
        
    }
   if(mainGridTag==14||mainGridTag==24||mainGridTag==34||mainGridTag==44||mainGridTag==54||mainGridTag==64||mainGridTag==74||mainGridTag==84)
    {
        
           intLastAtBatRecord=0;
        
    }
    if(mainGridTag==15||mainGridTag==25||mainGridTag==35||mainGridTag==45||mainGridTag==55||mainGridTag==65||mainGridTag==75||mainGridTag==85)
    {
        intLastAtBatRecord=0;
        
        
    }
  if(mainGridTag==16||mainGridTag==26||mainGridTag==36||mainGridTag==46||mainGridTag==56||mainGridTag==66||mainGridTag==76||mainGridTag==86)
    {
       
           intLastAtBatRecord=6;
        
    }
    
    if(mainGridTag==17||mainGridTag==27||mainGridTag==37||mainGridTag==47||mainGridTag==57||mainGridTag==67||mainGridTag==77||mainGridTag==87)
    {
       
           intLastAtBatRecord=7;
    }
    if(mainGridTag==18||mainGridTag==28||mainGridTag==38||mainGridTag==48||mainGridTag==58||mainGridTag==68||mainGridTag==78||mainGridTag==88)
    {
           intLastAtBatRecord=8;
        
    }
    if(mainGridTag==19||mainGridTag==29||mainGridTag==39||mainGridTag==49||mainGridTag==59||mainGridTag==69||mainGridTag==79||mainGridTag==89)
    {
      
           intLastAtBatRecord=9;
    }

    return intLastAtBatRecord;
}


#pragma mark TotalBallsofMatch
 
-(void)TotalBallsofMatch
{
 
   
    
    for (int index=0; index<=89; (index +=10)) {
        
        
        NSLog(@"......%d",index);
        
        NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
        
        if([dicAllInnings objectForKey:string])
        {
            intTotalBalls = intTotalBalls +  [[[dicAllInnings objectForKey:string] allKeys]count];
        }
        
        
        
        
        
        if(index==80)
        {
            index=1;
            NSLog(@"......%d",index);
            
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            
            if([dicAllInnings objectForKey:string])
            {
                intTotalBalls = intTotalBalls +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
            
            
        }
        if(index==81)
        {
            index=2;
            NSLog(@"......%d",index);
            
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            
            if([dicAllInnings objectForKey:string])
            {
                intTotalBalls = intTotalBalls +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
        }
        
        if(index==82)
        {
            index=3;
            NSLog(@"......%d",index);
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            
            if([dicAllInnings objectForKey:string])
            {
                intTotalBalls = intTotalBalls +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
        }
        
        if(index==83)
        {
            index=4;
            NSLog(@"......%d",index);
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            
            if([dicAllInnings objectForKey:string])
            {
                intTotalBalls = intTotalBalls +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
        }
        if(index==84)
        {
            index=5;
            NSLog(@"......%d",index);
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            
            if([dicAllInnings objectForKey:string])
            {
                intTotalBalls = intTotalBalls +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
        }
        if(index==85)
        {
            index=6;
            NSLog(@"......%d",index);
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            
            if([dicAllInnings objectForKey:string])
            {
                intTotalBalls = intTotalBalls +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
        }
        if(index==86)
        {
            index=7;
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            
            if([dicAllInnings objectForKey:string])
            {
                intTotalBalls = intTotalBalls +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
            
        }
        if(index==87)
        {
            index=8;
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            
            if([dicAllInnings objectForKey:string])
            {
                intTotalBalls = intTotalBalls +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
            
        }
        if(index==88)
        {
            index=9;
            
            NSMutableString * string=[NSMutableString stringWithFormat:@"PlayerBox%d",index];
            
            if([dicAllInnings objectForKey:string])
            {
                intTotalBalls = intTotalBalls +  [[[dicAllInnings objectForKey:string] allKeys]count];
            }
            
            
        }
        
        if(mainGridTag==index)
        {
            break;
            
        }
        
    }
    
    
    lblTotalBalls.text =[NSMutableString stringWithFormat:@"%d",intTotalBalls];
    mlabelTotalPitches.text=[NSMutableString stringWithFormat:@"%d",intTotalBalls];
    

    
}

#pragma mark ..........................Pitch.........Label
-(void)pitchLabel :(NSString *)String
{
    
    
        UIButton *button=[arrayPitches objectAtIndex:currentPitchTag];
    [button setTitle:String forState:UIControlStateNormal];
    
}


#pragma mark===================================================== Monitor
-(void)montiorDisplayRecord
{
    NSLog(@"===================================montiorDisplayRecord%@",dicAtBatRecord);
    NSLog(@"%@",dicInnings);
    NSLog(@"mmmmmmmm111111%@",dicAllInnings);
    NSLog(@"mnmmmmmmmmmm222222%@",dicInnings);
    NSLog(@"mmmmmmmmmmmm333333%@",dicOutRecord);
    NSLog(@"mmmmmmmmmmmmm44444444.......................%@",baterInfo_Array);
    NSLog(@"mmmmmmmmmmmmmm555555555%@",dicAtBatRecord);
        NSLog(@"mmmmmmmmmmmmmintTotalBalls.......................%d",intTotalBalls);
    intMonitorStrike=0;
    intTotalInningsCountMonitor=0;
    intAtBatMonitor=0;
    intAtBatMorethan3Monitor=0;
   
    intMonitorFB_Ball=0;
    intMonitorCB_Ball=0;
    intMonitorCU_Ball=0;
    intMonitorSL_Ball=0;
    intMonitorCF_Ball=0;
    intMonitorKK_Ball=0;
    intMonitorFS_Ball=0;
    intMonitorSN_Ball=0;
   
    intFirstPitchstrike=0;
   
    intMonitorFB_Strike=0;
    intMonitorCB_Strike=0;
    intMonitorCU_Strike=0;
    intMonitorSL_Strike=0;
    intMonitorCF_Strike=0;
    intMonitorKK_Strike=0;
    intMonitorFS_Strike=0;
    intMonitorSN_Strike=0;
    intMonitorTwoOutRecordCount=0;
    intMonitorFB_Total=0;
    intMonitorCB_Total=0;
    intMonitorCU_Total=0;
    
    if([arrayInningTag count]>0)
    {
        [arrayInningTag removeAllObjects];
    }
    
    
    for (int i =0; i<90;i++) {
        
        NSMutableString * stringMonitor=[NSMutableString stringWithFormat:@"PlayerBox%d",i];
        NSMutableString * string3Monitor=[NSMutableString stringWithFormat:@"Player3Box%d",i];
         NSMutableString * stringTwo=[NSMutableString stringWithFormat:@"PlayerBoxTwo%d",i];
        
         NSMutableString * stringAtBatPitcher=[NSMutableString stringWithFormat:@"AtBatPitcher%d",i];
        
        NSMutableString * tag=[NSMutableString stringWithFormat:@"%d",i];
        if([dicInnings objectForKey:stringMonitor])
        {
            
            intTotalInningsCountMonitor++;
            
            [arrayInningTag addObject:tag];
            
            
        }
        
        
        if([[dicAtBatRecord objectForKey:stringAtBatPitcher] isEqualToString:labelPitcher.text])
        {
            
        
    
        if([dicAtBatRecord objectForKey:stringTwo])
        {
            if( i<intMonitorTwoOutRecord)
            {
                intMonitorTwoOutRecordCount++; 
            }
           
            
            
        }
      //  if([dicAtBatRecord objectForKey:stringMonitor]==@"A")
            
            if ([[dicAtBatRecord objectForKey:stringMonitor] isEqualToString:@"A"
                ]) 
        
        {
            intAtBatMonitor++;
            
        }
        if([[dicAtBatRecord objectForKey:string3Monitor]isEqualToString:@"3A"])
            
        {
            intAtBatMorethan3Monitor++;
            
        }
        
        }
        
        ////////////////////////////////////////////////////////ininigs 
        
        if([dicAllInnings objectForKey:stringMonitor])
        {
           // dicMainPitch =[dicAllInnings objectForKey:stringMonitor];
            
            ///number of innings
                     
            for (int k=0; k<99; k++) {
                
                
                stringKeyMainPitch =[NSMutableString stringWithFormat:@"MainPitch%d%d",i,k];
                
                NSLog(@"%@",stringKeyMainPitch);
                ////////////////////////////////////for one pitcher
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"Pitcher"] isEqualToString:labelPitcher.text])
                {
                
              
                
                 if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"strike"] isEqualToString:@"S"])
                 {
                     intMonitorStrike++;
                     if(k==49)
                     {
                         intFirstPitchstrike++;
                     }
                     
                 }
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"lblOut"] isEqualToString: @"o"])
                {
                    intMonitorStrike++;
                        if(k==7)
                        {
                            intFirstPitchstrike++;
                        }
                        
                    }
                
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"BFB"])
                {
                    intMonitorFB_Ball++;
                    
                }
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"BCB"])
                {
                    intMonitorCB_Ball++;
                    
                }
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"BCU"])
                {
                    intMonitorCU_Ball++;
                    
                }
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"BSL"])
                {
                    intMonitorSL_Ball++;
                    
                }
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"BCF"])
                {
                    intMonitorCF_Ball++;
                    
                }
              
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"BKK"])
                {
                    intMonitorKK_Ball++;
                    
                }
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"BFS"])
                {
                    intMonitorFS_Ball++;
                    
                }
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"BSN"])
                {
                    intMonitorSN_Ball++;
                    
                }
                
                ////strike
                
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"SFB"])
                {
                    intMonitorFB_Strike++;
                    
                }
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"SCB"])
                {
                    intMonitorCB_Strike++;
                    
                }
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"SCU"])
                {
                    intMonitorCU_Strike++;
                    
                }
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"SSL"])
                {
                    intMonitorSL_Strike++;
                    
                }
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"SCF"])
                {
                    intMonitorCF_Strike++;
                    
                }
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"SKK"])
                {
                    intMonitorKK_Strike++;
                    
                }
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"SFS"])
                {
                    intMonitorFS_Strike++;
                    
                }
                if([[[[dicAllInnings objectForKey:stringMonitor] objectForKey:stringKeyMainPitch]  objectForKey:@"monitor"] isEqualToString:@"SSN"])
                {
                    intMonitorSN_Strike++;
                    
                }
               


             
        }
            }
    }//end of for loop label pitche buttn

}/////end of for lool main grid box
    
    intMonitorCB_Total=intMonitorCB_Ball+intMonitorCB_Strike;
    intMonitorCU_Total=intMonitorCU_Ball+intMonitorCU_Strike;
    intMonitorFB_Total=intMonitorFB_Ball+intMonitorFB_Strike;
    
    float strike_P = ((float)intMonitorStrike/(float)intTotalBalls)*100;
    float monitor_FBP= ((float)intMonitorFB_Total/(float)intTotalBalls)*100;
    float monitor_CBP= ((float)intMonitorCB_Total/(float)intTotalBalls)*100;
    float monitor_CUP= ((float)intMonitorCU_Total/(float)intTotalBalls)*100;
    float firstPitch =((float)intFirstPitchstrike/(float)intAtBatMonitor)*100;
    
    
    
    NSLog(@"%f",strike_P);
    
    NSMutableString *stringStrikePercentage=[NSMutableString stringWithFormat:@"%f",strike_P];
    if([stringStrikePercentage length]>3)
    {
        mlabelStrikePercentage.text =[NSMutableString stringWithFormat:@"%@%@",[stringStrikePercentage substringToIndex:5],@"%"];
    }
   mlabelTotalPitches.text =[NSMutableString stringWithFormat:@"%d",intTotalBalls];
    mlabelStrikeScore.text=[NSMutableString stringWithFormat:@"(%d/%d)",intMonitorStrike,intTotalBalls];
    //balls
    mlabelFastball_B.text =[NSMutableString stringWithFormat:@"%d",intMonitorFB_Ball];
     mChangesUp_B.text =[NSMutableString stringWithFormat:@"%d",intMonitorCU_Ball];
    mlabelCurveball_B.text =[NSMutableString stringWithFormat:@"%d",intMonitorCB_Ball];
   //strike
    mlabelFastball_S.text =[NSMutableString stringWithFormat:@"%d",intMonitorFB_Strike];
    mChangesUp_S.text =[NSMutableString stringWithFormat:@"%d",intMonitorCU_Strike];
    mlabelCurveball_S.text =[NSMutableString stringWithFormat:@"%d",intMonitorCB_Strike];
    //total
    mlabelFastball_T.text =[NSMutableString stringWithFormat:@"%d",intMonitorFB_Total];
    mlabelChangesUp_T.text =[NSMutableString stringWithFormat:@"%d",intMonitorCU_Total];
    mlabelCurveball_T.text =[NSMutableString stringWithFormat:@"%d",intMonitorCB_Total];
    
    //mix
    mlabelPitchesMixFastballs.text =[NSMutableString stringWithFormat:@"%d/%d",intMonitorFB_Total,intTotalBalls];
     mlabelPitchesMixChangesUps.text =[NSMutableString stringWithFormat:@"%d/%d",intMonitorCU_Total,intTotalBalls];
     mlabelPitchesMixCurveBalls.text =[NSMutableString stringWithFormat:@"%d/%d",intMonitorCB_Total,intTotalBalls];
    
    NSMutableString *PitchesMixFastballs_P=[NSMutableString stringWithFormat:@"%f",monitor_FBP];
    if([PitchesMixFastballs_P length]>3)
    {
        mlabelPitchesMixFastballs_P.text =[NSMutableString stringWithFormat:@"%@%@",[PitchesMixFastballs_P substringToIndex:5],@"%"];
    }
    NSMutableString *PitchesMixChangesUps_P=[NSMutableString stringWithFormat:@"%f",monitor_CUP];
   
    if([PitchesMixChangesUps_P length]>3)
    {
        mlabelPitchesMixChangesUps_P.text =[NSMutableString stringWithFormat:@"%@%@",[PitchesMixChangesUps_P substringToIndex:5],@"%"];
    }
    NSMutableString *PitchesMixCurveBalls_P=[NSMutableString stringWithFormat:@"%f",monitor_CBP];
    
    if([PitchesMixCurveBalls_P length]>3)
    {
        mlabelPitchesMixCurveBalls_P.text =[NSMutableString stringWithFormat:@"%@%@",[PitchesMixCurveBalls_P substringToIndex:5],@"%"];
         }
    
    
   //TOtal at bat 3

mlabelTotalAtBats.text =[NSMutableString stringWithFormat:@"%d",intAtBatMonitor];
 mlabelAtBatsMoreThan3Pitches.text =[NSMutableString stringWithFormat:@"%d",intAtBatMorethan3Monitor];

///lead off
mlabelInningsPitches.text=[NSMutableString stringWithFormat:@"%d",intTotalInningsCountMonitor];

 
    
///first strike pitch
mlabelAtBats.text=[NSMutableString stringWithFormat:@"%d",intAtBatMonitor];

mlabelOfFirstPitchesStrikes.text=[NSMutableString stringWithFormat:@"%d",intFirstPitchstrike];
     NSMutableString *mfirstPitch=[NSMutableString stringWithFormat:@"%f",firstPitch];
    if([mfirstPitch length]>3)
    {
        mlabelPitchesStrikepercentage.text =[NSMutableString stringWithFormat:@"%@%@",[mfirstPitch substringToIndex:5],@"%"];
    }

  mlabel2OutOffbats.text=[NSMutableString stringWithFormat:@"%d",intMonitorTwoOutRecordCount];
    [self LeadOfOut];
    //[self twoLeadOut];
    
}


-(void)LeadOfOut
{
  
    intMonitorLeadOfOut=0;
    LeadOut=0;
    FirstInningOut=0;
    
    NSLog(@"%@",dicOutRecord);
    
    NSMutableString * stringMonitor=[NSMutableString stringWithFormat:@"PlayerBox%d",0];
 
      NSMutableString * stringOutPitcher=[NSMutableString stringWithFormat:@"OutPitcher%d",0];
    
    
    if([[dicOutRecord objectForKey:stringOutPitcher] isEqualToString:labelPitcher.text])
        
    {
    if([dicOutRecord objectForKey:stringMonitor])
    {
        intMonitorLeadOfOut++;
    }
    

    }
    
     for (int i=0; i<[arrayInningTag count]; i++)
     
     {
         
         NSMutableString * stringtag =[arrayInningTag objectAtIndex:i];
    
         LeadOut =[stringtag intValue];
         
         if(LeadOut==90)
         {
             FirstInningOut =1;
         }
        else if(LeadOut==91)
         {
              FirstInningOut =2;
         }
       else  if(LeadOut==92)
         {
              FirstInningOut =3;
         }
         
        else if(LeadOut==93)
         {
              FirstInningOut =4;
         }
         
        else if(LeadOut==94)
         {
              FirstInningOut =5;
         }
        else if(LeadOut==95)
         {
              FirstInningOut =6;
         }
        else if(LeadOut==96)
         {
              FirstInningOut =7;
         }
        else if(LeadOut==97)
         {
              FirstInningOut =8;
         }
        else if(LeadOut==98)
         {
              FirstInningOut =9;
         }
        else
        
        {
            FirstInningOut =LeadOut+10;
        }
        
            NSMutableString * stringTag=[NSMutableString stringWithFormat:@"PlayerBox%d",FirstInningOut];
         
       
           NSMutableString * stringOutPitcherInning=[NSMutableString stringWithFormat:@"OutPitcher%d",FirstInningOut];
         
         if([[dicOutRecord objectForKey:stringOutPitcherInning] isEqualToString:labelPitcher.text])
             
         {
             if([dicOutRecord objectForKey:stringTag])
             {
                 intMonitorLeadOfOut++;
             }
             
         }
         
         
       
         
     }
    
    mlabelLeadsOffOuts.text =[NSMutableString stringWithFormat:@"%d",intMonitorLeadOfOut];
    
    
}


/////newInnings



@end


