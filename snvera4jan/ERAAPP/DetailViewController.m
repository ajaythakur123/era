//
//  DetailViewController.m
//  ERAAPP

//  Copyright 2012 __MyCompanyName__. All rights reserved.
//
#import "DetailViewController.h"
#import "RootViewController.h"
#import "sqlite3.h"
int in_teamNamex;
int in_opponentTeamX;
int in_catcherx;
int in_umpirex;


int int_asgnButtontagvalue;
@interface DetailViewController ()
@property (nonatomic, strong) UIPopoverController *popoverController;
- (void)configureView;
@end
@implementation DetailViewController
@synthesize toolbar=_toolbar,scrv_scoreboard;
@synthesize detailItem=_detailItem;
@synthesize detailDescriptionLabel=_detailDescriptionLabel;
@synthesize popoverController=_myPopoverController;

@synthesize vw_typeOFpitchselect,btn_typeofpitch,vw_StrikeView,vw_OnBaseview,vw_onBaseOptionHitFair,vw_BallPopMainMenu,vw_BallPopUpSubMenu,vw_Mainbaserunnerview,vw_defensivePositions,txtf_gb1,txtf_gb2,txtf_db1,txtf_db2,txtf_agb1,txtf_agb2,vw_LocationOfHitView,nvgn_datepicker,btn_loadstrikesenderid,imgv_Fielderchoice;

#pragma mark - Managing the detail item

/*
 When setting the detail item, update the view and dismiss the popover controller if it's showing.
 
 */


-(void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) 
    {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }

    if (self.popoverController != nil) {
        [self.popoverController dismissPopoverAnimated:YES];
    }        
}

-(void)configureView
{
    // Update the user interface for the detail item.

    self.detailDescriptionLabel.text = [self.detailItem description];
    
}
- (void)viewWillAppear:(BOOL)animated
{
    
    [super viewWillAppear:animated];
}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}
- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
    
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
    return YES;
}

#pragma mark - Split view support
- (void)splitViewController:(UISplitViewController *)svc willHideViewController:(UIViewController *)aViewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController: (UIPopoverController *)pc
{
    barButtonItem.title = @"In Game Monitor";
    NSMutableArray *items = [[self.toolbar items] mutableCopy];
    [items insertObject:barButtonItem atIndex:0];
    [self.toolbar setItems:items animated:YES];
    self.popoverController = pc;
}

// Called when the view is shown again in the split view, invalidating the button and popover controller.
- (void)splitViewController:(UISplitViewController *)svc willShowViewController:(UIViewController *)aViewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    NSMutableArray *items = [[self.toolbar items] mutableCopy];
    [items removeObjectAtIndex:0];
    [self.toolbar setItems:items animated:YES];
    self.popoverController = nil;
}

// Date Picker Button Action.....
-(IBAction)Datepickerbutton:(id)sender
{
    NSLog(@"--> Datepickerbutton: Called! <--");
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *dateString = [dateFormatter stringFromDate:date_picker.date];

    lbl_datepicker.text=dateString;
    lbl_datepicker.backgroundColor = [UIColor clearColor];
    lbl_datepicker.textColor = [UIColor blackColor];
    [lbl_datepicker setFont:[UIFont fontWithName:@"Arial-Bold" size:12]];
    [lbl_datepicker setTextAlignment:UITextAlignmentCenter];

     date_picker.hidden = YES;
     nvgn_datepicker.hidden = YES;
    
    catcher_lbl.hidden=NO;
    //pitcher_lbl.hidden = NO;
    
    
   }


-(IBAction)DatepickerCancel:(id)sender

{
    NSLog(@"--> DatepickerCancel: Called! <--");
    
    nvgn_datepicker.hidden=YES;
    date_picker.hidden=YES;
    
    catcher_lbl.hidden=NO;
   // pitcher_lbl.hidden = NO;
    
}

-(IBAction)DatepickerClicked:(id)sender
{
    NSLog(@"--> DatepickerClicked: Called! <--");
    
    [self.vw_defensivePositions removeFromSuperview];
    bl_DefensivePosiPopups=TRUE;
    vw_BallPopMainMenu.hidden=YES;
    vw_BallPopUpSubMenu.hidden=YES;
    vw_onBaseOptionHitFair.hidden=YES;
    vw_OnBaseview.hidden=YES;
    vw_StrikeView.hidden=YES;
    vw_typeOFpitchselect.hidden=YES;
    vw_LocationOfHitView.hidden=YES;
    catcher_lbl.hidden=YES;
    //pitcher_lbl.hidden = YES;
    date_picker.hidden = NO;
    nvgn_datepicker.hidden =NO;
    [self.scrv_scoreboard bringSubviewToFront:nvgn_datepicker];
}

-(IBAction)EditableBatterinfo:(id)sender
{
    
    NSLog(@"--%s---",__func__);
    [vw_Mainbaserunnerview removeFromSuperview];
    [vw_defensivePositions removeFromSuperview];
    bl_DefensivePosiPopups=TRUE;
    date_picker.hidden = YES;
    nvgn_datepicker.hidden = YES;
    vw_BallPopMainMenu.hidden=YES;
    vw_BallPopUpSubMenu.hidden=YES;
    vw_onBaseOptionHitFair.hidden=YES;
    vw_OnBaseview.hidden=YES;
    vw_StrikeView.hidden=YES;
    vw_typeOFpitchselect.hidden=YES;
    vw_LocationOfHitView.hidden=YES;
    [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
    [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
    
    [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
    [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
    
    if ([teamName_lbl.text length]<1 || [opponent_lbl.text length]<1 || [catcher_lbl.text length]<1 || [lbl_datepicker.text length]<1 ) {
        
        UIAlertView *MsgAlert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Selection Of All  Options Above Are Mandatory." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [MsgAlert show];
        return;
        
    }

   lbl_batterCount.text=[NSString stringWithFormat:@"%d",batterCount]; 
    
if ([sender tag]==1)
    {
        
        //batter_lbl.text=@"0";
        type=@"jerseyNumber";
     
       
        NSString *str_jersyNum = [NSString stringWithFormat:@"%@",lbl_jerseyNumber.text];
        
        if ([str_jersyNum length]==6)
        {
            NSLog(@"---jersy number lenth---");
        } 
        else
        {
            textAlert.text=str_jersyNum;
  
        }
        textAlert.keyboardType = UIKeyboardTypeNumberPad;
        NSLog(@"----Text field lenth--%i",[textAlert.text length]);
        [alert show];
        
}
    if ([sender tag]==2)
    {
        
    type=@"BatterName";
     
        
        NSString *str_batterName = [NSString stringWithFormat:@"%@",   lbl_batterName.text];
        
        if ([str_batterName length]==6)
        {
            
        }
        else
        {
        textAlert.text=str_batterName; 
        }
[alert show];
    }
    
if([sender tag]==3)
{
 type=@"R/LOption";
 UIAlertView  *alertlr = [[UIAlertView alloc] 
                  initWithTitle:@"Alert!" 
                  message:@"“Please select the type of batsman” " 
                  delegate:self  
                  cancelButtonTitle:@"Left(L)" 
                  otherButtonTitles:@"Right(R)",nil];
        [alertlr show];
       
    }
}

-(void)hideMenu:(NSNotification *)note 
{  
    
    NSLog(@"Received Notification:"); 
    [vw_Mainbaserunnerview removeFromSuperview];
    [vw_defensivePositions removeFromSuperview];
    bl_DefensivePosiPopups=TRUE;
    date_picker.hidden = YES;
    nvgn_datepicker.hidden = YES;
    vw_BallPopMainMenu.hidden=YES;
    vw_BallPopUpSubMenu.hidden=YES;
  
    vw_onBaseOptionHitFair.hidden=YES;
    vw_OnBaseview.hidden=YES;
    
    vw_StrikeView.hidden=YES;
    vw_typeOFpitchselect.hidden=YES;
    vw_LocationOfHitView.hidden=YES;
    
    catcher_lbl.hidden=NO;
    //pitcher_lbl.hidden = NO;

    [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
    [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
    
    [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
    [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
}

// Implement viewDidLoad to do additional setup after loading  typically from a nib.
- (void)viewDidLoad
{
//    strm_issaved =[[NSString alloc]init ];
//    strm_sqlprevData =[[NSString alloc]init];
        
    ArrayOfButtons=[[NSMutableArray alloc]init];
    ArrayOfLabels=[[NSMutableArray alloc]init];
    //----label jerseyNumber----
    [lbl_jerseyNumber setBackgroundColor:[UIColor clearColor]];
    [lbl_jerseyNumber setTextColor:[UIColor blackColor]];
    [lbl_jerseyNumber setFont:[UIFont fontWithName:@"Arial-Bold" size:15]];
    [lbl_jerseyNumber setTextAlignment:UITextAlignmentLeft];
       
        //---batter name---
    [lbl_batterName setBackgroundColor:[UIColor clearColor]];
    [lbl_batterName setTextColor:[UIColor blackColor]];
    [lbl_batterName setFont:[UIFont fontWithName:@"Arial-Bold" size:15]];
    [lbl_batterName setTextAlignment:UITextAlignmentLeft];
        
//----label right/left option-----
//    lbl_RorL=[[UILabel alloc]initWithFrame:CGRectMake(310, 285, 70, 16)];
//    [lbl_RorL setBackgroundColor:[UIColor clearColor]];
//    [lbl_RorL setTextColor:[UIColor blackColor]];
//    [lbl_RorL setFont:[UIFont fontWithName:@"Arial-Bold" size:15]];
//    [lbl_RorL setTextAlignment:UITextAlignmentLeft];
//    [self.scrv_scoreboard addSubview:lbl_RorL];
    
[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideMenu:) name:@"hideComplete" object:nil]; 
    
// Adding catcher_lbl to scrollView....
    catcher_lbl=[[UILabel alloc]initWithFrame:CGRectMake(609, 100, 70, 16)];
    [catcher_lbl adjustsFontSizeToFitWidth];
    [catcher_lbl setBackgroundColor:[UIColor clearColor]];
    [catcher_lbl setTextColor:[UIColor blackColor]];
    [catcher_lbl setTextAlignment:UITextAlignmentCenter];
    [catcher_lbl setFont:[UIFont fontWithName:@"Arial" size:12]];
    [self.scrv_scoreboard addSubview:catcher_lbl];
        
// Adding date_lbl to scrollView....
    pitcher_lbl=[[UILabel alloc]initWithFrame:CGRectMake(606, 127.5, 70, 16)];
    [pitcher_lbl setBackgroundColor:[UIColor clearColor]];
    [pitcher_lbl setFont:[UIFont fontWithName:@"Arial" size:12]];
    [pitcher_lbl setTextColor:[UIColor blackColor]];
    [pitcher_lbl setTextAlignment:UITextAlignmentCenter];
    [self.scrv_scoreboard addSubview:pitcher_lbl];
   
    
    // Adding team_Name_lbl to scrollView....
    teamName_lbl=[[UILabel alloc]initWithFrame:CGRectMake(188, 86, 70, 16)];
    in_teamNamex=188;
    [teamName_lbl setBackgroundColor:[UIColor clearColor]];
    [teamName_lbl setTextColor:[UIColor blackColor]];
    [teamName_lbl setFont:[UIFont fontWithName:@"Arial" size:12]];
    [teamName_lbl setTextAlignment:UITextAlignmentCenter];
    [self.scrv_scoreboard addSubview:teamName_lbl];
    
    // Adding Game_Opponent_lbl to scrollView....
     opponent_lbl=[[UILabel alloc]initWithFrame:CGRectMake(188, 113, 70, 16)];
    [opponent_lbl setBackgroundColor:[UIColor clearColor]];
    [opponent_lbl setTextColor:[UIColor blackColor]];
    [opponent_lbl setFont:[UIFont fontWithName:@"Arial" size:12]];
    [opponent_lbl setTextAlignment:UITextAlignmentCenter];
    [self.scrv_scoreboard addSubview:opponent_lbl];
    
    lbl_circleout=[[UILabel alloc]initWithFrame:CGRectMake(547, 350, 38, 21)];
    [lbl_circleout setBackgroundColor:[UIColor clearColor]];
    [lbl_circleout setTextColor:[UIColor whiteColor]];
    [lbl_circleout setFont:[UIFont fontWithName:@"Arial" size:12]];
    [lbl_circleout setTextAlignment:UITextAlignmentCenter];
    [self.scrv_scoreboard addSubview:lbl_circleout];
    
    lbl_pitchCount=[[UILabel alloc]initWithFrame:CGRectMake(633, 387, 42, 27)];
    [lbl_pitchCount setBackgroundColor:[UIColor clearColor]];
    [lbl_pitchCount setTextColor:[UIColor blackColor]];
    [lbl_pitchCount setFont:[UIFont fontWithName:@"Arial" size:20]];
    [lbl_pitchCount setTextAlignment:UITextAlignmentCenter];
    [self.scrv_scoreboard addSubview:lbl_pitchCount];
    
        //-----base runner button-----
//    btn_baseRunnerDispalyValue = [UIButton buttonWithType:UIButtonTypeCustom];
//    [btn_baseRunnerDispalyValue setTitle:@"B" forState:UIControlStateNormal];
//    [btn_baseRunnerDispalyValue setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    [btn_baseRunnerDispalyValue addTarget:self 
//               action:@selector(mainBaseRunnerview:)
//     forControlEvents:UIControlEventTouchUpInside];
//    btn_baseRunnerDispalyValue.frame = CGRectMake(635.0, 365.0, 40.0, 25.0);
    [self.scrv_scoreboard addSubview:btn_baseRunnerDispalyValue];
   
    bl_alertPopup=TRUE;
    ballpopup=TRUE;
    bl_hideimage=TRUE;
    mainbaserunnerpopup=TRUE;
    bl_DefensivePosiPopups=TRUE;
    vw_LocationOfHitView.hidden=YES;
    
array_defnsiveposidata = [[NSMutableArray alloc]initWithObjects:@"1 - Pitcher",@"2 - Catcher",@"3 - 1st Baseman",@"4 - 2nd Baseman",@"5 - 3rd Baseman",@"6 - Shortstop",@"7 - Left Field ",@"8 - Center Field",@"9 - Right Field", nil];
    
 teamNames_Array = [[NSMutableArray alloc]initWithObjects:@"Team1",@"Team2",@"Team3",@"Team4", nil];
   
  opponents_Array = [[NSMutableArray alloc]initWithObjects:@"Opteam1",@"Opteam2",@"Opteam3",@"Opteam4", nil];
    
    pitchers_Array = [[NSMutableArray alloc]initWithObjects:@"Pitcher1",@"Pitcher2",@"Pitcher3",@"Pitcher4", nil];
    
    catchers_Array = [[NSMutableArray alloc]initWithObjects:@"Catcher1",@"Catcher2",@"Catcher3",@"Catcher4", nil];
    
     scrv_scoreboard.contentSize = CGSizeMake(768, 2000);
    
    [scrv_scoreboard scrollRectToVisible:CGRectMake(0, 0, 768, 1004) animated:YES];
    
    array_redrowbtns =[[NSMutableArray alloc]init];
    [ArrayOfButtons addObject:[UIButton buttonWithType:UIButtonTypeCustom]];
  
    //-----Right side button in detail view controller------
    
    UIBarButtonItem *flexibleSpace =[[UIBarButtonItem alloc] initWithBarButtonSystemItem: UIBarButtonSystemItemFlexibleSpace target: nil action: nil];
    UIBarButtonItem *ExitButton = [[UIBarButtonItem alloc] initWithTitle:@"EXIT" style:UIBarButtonItemStyleBordered target:nil action: @selector(ERAExitMethod:)];
   
    
    UIBarButtonItem *NewBatter = [[UIBarButtonItem alloc] initWithTitle:@"New Game" style:UIBarButtonItemStyleBordered target:nil action: @selector(NewGameMethod:)]; 
    
    NSArray *buttonArray = [NSArray arrayWithObjects: flexibleSpace,NewBatter,ExitButton, nil];
   
    [_toolbar setItems: buttonArray];
           

    UILabel *titleLabel = [[UILabel alloc] initWithFrame: CGRectMake( 284, 3, 200, 35 )];
    titleLabel.text=@"PITCHING CHART";
    [titleLabel setBackgroundColor:[UIColor clearColor]];
    [titleLabel setTextColor:[UIColor whiteColor]];
    [titleLabel setFont:[UIFont fontWithName:@"Bold" size:20]];
    titleLabel.textAlignment = UITextAlignmentCenter;
    [_toolbar addSubview: titleLabel];
    [self.view addSubview: _toolbar];
    
//---save button----
    btn_save = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_save addTarget:self 
               action:@selector(SaveButtonMethod:)
     forControlEvents:UIControlEventTouchDown];
    [btn_save setTitle:@"" forState:UIControlStateNormal];
    btn_save.frame = CGRectMake(591.0, 227.0, 80.0, 20.0);
    [self.scrv_scoreboard addSubview:btn_save];

//----NewBatter----
    
    UIButton *btn_newbatter = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_newbatter addTarget:self 
                 action:@selector(NewBatterMethod:)
       forControlEvents:UIControlEventTouchDown];
    [btn_newbatter setTitle:@"" forState:UIControlStateNormal];
    
    btn_newbatter.frame = CGRectMake(383.0, 227.0, 80.0, 20.0);
    [self.scrv_scoreboard addSubview:btn_newbatter];
//------date picker------
    date_picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(360,133,325, 180)];
    [date_picker setDate:[NSDate date] animated:YES];
    date_picker.datePickerMode = UIDatePickerModeDate;
    [date_picker setHidden:YES];
    nvgn_datepicker.hidden =YES;
    [self.scrv_scoreboard addSubview:date_picker];
    
    batterCount=1;
    
    int a=358;
    int b=106;
for (int i=0; i<9; i++) 
{
        batter_lbl=[[UILabel alloc]initWithFrame:CGRectMake(a, b, 18, 18)];
        [batter_lbl setTextAlignment:UITextAlignmentCenter];
      [batter_lbl setFont:[UIFont fontWithName:@"ArialMT" size:12]];
        [batter_lbl setBackgroundColor:[UIColor clearColor]];
        [batter_lbl setTag:i];
        [ArrayOfLabels addObject:batter_lbl];
        [self.scrv_scoreboard addSubview:batter_lbl];
        a+=21;
        
}
    
    
//    matchid.text =@"123";
//    lbl_jerseyNumber.text =@"123";
//    lbl_batterName.text =@"ambadas";
//    lbl_RorL.text =@"L";
//    opponent_lbl.text=@"opponent1";
//    teamName_lbl.text =@"team1";
//    pitcher_lbl.text =@"pitcher1";
//    catcher_lbl.text=@"catcher1";
//    lbl_datepicker.text=@"03/12/2012";
    [super viewDidLoad];
    
}
-(void)SaveButtonMethod:(id)sender
{
NSLog(@"---%s---",__func__);
if(teamName_lbl.text.length<=0||opponent_lbl.text.length<=0||lbl_datepicker.text.length<=0||catcher_lbl.text.length<=0||pitcher_lbl.text.length
       <=0)
    {
        UIAlertView *alrt_checkdata = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"'Please select the values!'" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Okey", nil];
        [alrt_checkdata show];
        return;
    }
       
    [self insertIntoMatchMainInfo];
    [self insertIntoMatchSummary];
    
}
-(void)insertIntoMatchMainInfo
{
    ERAAPPAppDelegate *appDelegate = (ERAAPPAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *query;
	query = [NSString stringWithFormat:@"insert into matchmaininfo values (NULL,\"%@\", \"%@\",\"%@\",\"%@\")",matchid.text, teamName_lbl.text, opponent_lbl.text,lbl_datepicker.text];  
	
	const char *sql = [query cStringUsingEncoding:NSUTF8StringEncoding];
	sqlite3_stmt *statement = nil;
    
	if(sqlite3_prepare_v2(appDelegate.dataBaseConnection,sql, -1, &statement, NULL)!= SQLITE_OK)
	{
        
		NSAssert1(0,@"error preparing statement",sqlite3_errmsg(appDelegate.dataBaseConnection));
        
	}
	else
	{
		sqlite3_step(statement);
        
	
    }
	sqlite3_finalize(statement);
    

}
-(void)insertIntoMatchSummary
{
    
    
    if (btn_ballbox1.titleLabel.text.length<=0)
    {
        btn_ballbox1.titleLabel.text=@"0"; 
    }
    if (btn_ballbox2.titleLabel.text.length<=0)
    {
        btn_ballbox2.titleLabel.text=@"0";
    }
    if (btn_ballbox3.titleLabel.text.length<=0)
    {
        btn_ballbox3.titleLabel.text=@"0";
    }
    if (btn_ballbox4.titleLabel.text.length<=0)
    {
        btn_ballbox4.titleLabel.text=@"0"; 
    }
    if (btn_ballbox5.titleLabel.text.length<=0)
    {
        btn_ballbox5.titleLabel.text=@"0"; 
    }
    if (btn_ballbox6.titleLabel.text.length<=0)
    {
        btn_ballbox6.titleLabel.text=@"0"; 
    }
    if (btn_ballbox7.titleLabel.text.length<=0)
    {
        btn_ballbox7.titleLabel.text=@"0"; 
    }
    if(btn_strikebox8.titleLabel.text.length<=0)
    {
        btn_strikebox8.titleLabel.text=@"0"; 
    }
    if (btn_strikebox9.titleLabel.text.length<=0)
    {
        btn_strikebox9.titleLabel.text=@"0";
    }
    if (btn_strikebox10.titleLabel.text.length<=0)
    {
        btn_strikebox10.titleLabel.text=@"0";
    }
    if (btn_strikebox11.titleLabel.text.length<=0)
    {
        btn_strikebox11.titleLabel.text=@"0"; 
    }
    if (btn_strikebox12.titleLabel.text.length<=0)
    {
        btn_strikebox12.titleLabel.text=@"0"; 
    }
    if (btn_strikebox13.titleLabel.text.length<=0)
    {
        btn_strikebox13.titleLabel.text=@"0"; 
    }
    if (btn_strikebox14.titleLabel.text.length<=0)
    {
        btn_strikebox14.titleLabel.text=@"0"; 
    }
    NSArray *arr_ballboxinfo=[[NSArray alloc]initWithObjects:
                              btn_ballbox1.titleLabel.text,btn_ballbox2.titleLabel.text,btn_ballbox3.titleLabel.text,btn_ballbox4.titleLabel.text,btn_ballbox5.titleLabel.text,btn_ballbox6.titleLabel.text,btn_ballbox7.titleLabel.text, nil];
    NSLog(@"---%@--",arr_ballboxinfo);
   str_ballboxinfo =[[NSMutableString alloc] init];
    for (NSObject * obj in arr_ballboxinfo)
    {
        [str_ballboxinfo appendString:[obj description]];
        
    }
    NSLog(@"The concatenated string is %@", str_ballboxinfo);
    //-----strike box value-----
    NSArray *arr_strikebox =[[NSArray alloc]initWithObjects:
                             btn_strikebox8.titleLabel.text,btn_strikebox9.titleLabel.text,btn_strikebox10.titleLabel.text,btn_strikebox11.titleLabel.text,btn_strikebox12.titleLabel.text,btn_strikebox13.titleLabel.text,btn_strikebox14.titleLabel.text, nil];
   str_strikeboxinfo = [[NSString alloc] init];
    for (NSString * obj in arr_strikebox)
    {
        str_strikeboxinfo  = [str_strikeboxinfo stringByAppendingString:obj];
    }
    NSLog(@"The concatenated string is %@", str_strikeboxinfo);
 //------------------   
    ERAAPPAppDelegate *appDelegate = (ERAAPPAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSString *query;
    query =[NSString stringWithFormat:@"insert into matchsummary values (NULL,\"%@\", \"%@\",\"%@\",\"%@\",\"%@\", \"%@\",\"%@\",\"%@\",\"%@\", \"%@\",\"%@\",\"%@\",\"%@\", \"%@\",\"%@\",\"%@\")",catcher_lbl.text, pitcher_lbl.text ,lbl_batterCount.text,lbl_jerseyNumber.text,lbl_batterName.text,lbl_RorL.text,str_ballboxinfo,str_strikeboxinfo,lbl_currpitchcount.text,lbl_pitchCount.text,lbl_imagedata.text,lbl_batRuns.text,lbl_circleout.text,lbl_batteronbase.text,lbl_scoreboard1.text,lbl_scoreboard2.text];  
	
	
	const char *sql = [query cStringUsingEncoding:NSUTF8StringEncoding];
	sqlite3_stmt *statement = nil;
    
	if(sqlite3_prepare_v2(appDelegate.dataBaseConnection,sql, -1, &statement, NULL)!= SQLITE_OK)
	{
		NSAssert1(0,@"error preparing statement",sqlite3_errmsg(appDelegate.dataBaseConnection));
	}
	else
	{
		sqlite3_step(statement);
        UIAlertView *alrt_checkdata = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"'Your Database saved successfully!'" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Okey", nil];
        [alrt_checkdata show];
        

        
	}
	sqlite3_finalize(statement);
    
    
}
-(void)NewGameMethod:(id)sender
{
   // NSLog(@"---%s---",__func__);
    UIAlertView *alrt_warn = [[UIAlertView alloc]initWithTitle:@"Warning!!" message:@"Work is under progress!!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Okey   ", nil];
    [alrt_warn show];
    
}


-(void)NewBatterMethod:(id)sender
{
    NSLog(@"---%s---",__func__);
    if(teamName_lbl.text.length<=0||opponent_lbl.text.length<=0||lbl_datepicker.text.length<=0||catcher_lbl.text.length<=0||pitcher_lbl.text.length
       <=0)
    {
        UIAlertView *alrt_checkdata = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"'Please select the above values!'" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Okey", nil];
        [alrt_checkdata show];
        return;
    }
    if (batterCount>9)
    { 
        batterCount=1;
    }
  lbl_batterCount.text=[NSString stringWithFormat:@"%d",batterCount];
    batterCount++;
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:2];
    [UIView setAnimationTransition:UIViewAnimationTransitionCurlDown
                           forView:vw_atBatAnimation cache:YES];
    [self.view addSubview:vw_atBatAnimation];
    [UIView commitAnimations];
    vw_atBatAnimation.hidden=YES;
    

}
-(IBAction)TypeOfPitchMethod:(UIButton*)sender
{
    
  
    btn_loadSenderidbtn=nil;
    
    if(btn_loadstrikesenderid == btn_strikebox8)
    {
        //btn_ballbox1.titleLabel.text = nil;
        [btn_ballbox1 setTitle:nil forState:UIControlStateNormal];
        if (bl_ballbox1==FALSE)
        {
            
            in_cummulativepitchcountValue++;
            bl_ballbox1=TRUE;
            
        }
        
    }
    
    if(btn_loadstrikesenderid ==  btn_strikebox9)
    {
       [btn_ballbox2 setTitle:nil forState:UIControlStateNormal];
        if (bl_ballbox2==FALSE)
        {
            
            in_cummulativepitchcountValue++;
            bl_ballbox2=TRUE;
             
        }
          
    }
    
    if(btn_loadstrikesenderid ==  btn_strikebox10)
    {
        [btn_ballbox3 setTitle:nil forState:UIControlStateNormal];
        
        if (bl_ballbox3==FALSE)
        {
            in_cummulativepitchcountValue++;
            bl_ballbox3=TRUE;
            
        }
    }

    
    if(btn_loadstrikesenderid ==  btn_strikebox11)
    {
    [btn_ballbox4 setTitle:nil forState:UIControlStateNormal];
        if (bl_ballbox4==FALSE)
        {
            in_cummulativepitchcountValue++;
            bl_ballbox4=TRUE;
        }
        
    }
    if(btn_loadstrikesenderid ==  btn_strikebox12)
    {
        [btn_ballbox5 setTitle:nil forState:UIControlStateNormal];
        
        if (bl_ballbox5==FALSE)
        {
            in_cummulativepitchcountValue++;
            bl_ballbox5=TRUE;
        }
    }
    if(btn_loadstrikesenderid ==  btn_strikebox13)
    {
        [btn_ballbox6 setTitle:nil forState:UIControlStateNormal];
        
        if (bl_ballbox6==FALSE)
        {
            in_cummulativepitchcountValue++;
            bl_ballbox6=TRUE;
        }
    }
    
    if(btn_loadstrikesenderid ==  btn_strikebox14)
    {
       [btn_ballbox7 setTitle:nil forState:UIControlStateNormal];
        
        if (bl_ballbox7==FALSE)
        {
            
            in_cummulativepitchcountValue++;
            bl_ballbox7=TRUE;
            
            
        }
    }
NSLog(@"TypeOfPitchMethod called");
if(btn_strikeTypeofPitcbox) 
    
    {
        if (btn_strikeTypeofPitcbox.tag == sender.tag )
        {
            if (btn_strikeTypeofPitcbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]  ) 
            {
                [btn_strikeTypeofPitcbox setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"btn_selected" ofType:@"png"]]forState:UIControlStateNormal];
                }
            
            else
               
            {
                    [btn_strikeTypeofPitcbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
                }
        }
        
        else
        
        {
                        
            [sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
            [btn_strikeTypeofPitcbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
        }
    }
    
    else
    
    {
        
        [sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
    }
    
     btn_strikeTypeofPitcbox = sender;
     
    if ( [sender tag]==1)
    
    {
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        if (btn_strikeTypeofPitcbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]  ) 
        {
             [btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];
            vw_StrikeView.hidden=YES;
            
            strBtn=[btn_loadSenderidbtn currentTitle];
            
            //---cummulative pitchcount-----           
            in_cummulativepitchcountValue--;
            bl_unselected=TRUE;
            
        }
        
        else
        {
            if (bl_unselected==TRUE) 
            {
                in_cummulativepitchcountValue++;
                bl_unselected=FALSE;
                
            }
          
            [btn_loadstrikesenderid setTitle:@"1" forState:normal];
                       
            vw_typeOFpitchselect.hidden=YES;
            vw_StrikeView.hidden=NO;

        }
                
        if (vw_StrikeView.hidden==NO)
       
        {
            [btn_loadstrikesenderid setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
            }
}
   
        
 if([sender tag] == 2)
    
 {
        
    [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
     if (btn_strikeTypeofPitcbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]  ) 
         
        {
            [btn_loadstrikesenderid setTitle:nil forState:normal];
            
            vw_StrikeView.hidden=YES;
            //---cummulative pitchcount-----           
            in_cummulativepitchcountValue--;
            bl_unselected=TRUE;
            
        }
     
        else
        
        {
           
          
            if (bl_unselected==TRUE) 
            {
                in_cummulativepitchcountValue++;
                bl_unselected=FALSE;
                
            }

            
          [btn_loadstrikesenderid setTitle:@"2" forState:normal];  
            
            vw_typeOFpitchselect.hidden=YES;
            vw_StrikeView.hidden=NO;

        }
               
        if (vw_StrikeView.hidden==NO)
        
        {
            [btn_loadstrikesenderid setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
      }
     
}
     
if([sender tag]==3)

  {
       [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        if (btn_strikeTypeofPitcbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]  ) 
        {
            [btn_loadstrikesenderid setTitle:nil forState:normal];
            vw_StrikeView.hidden=YES;
            //---cummulative pitchcount-----           
            in_cummulativepitchcountValue--;
            bl_unselected=TRUE;
            
        }
      
        else
        {
            if (bl_unselected==TRUE) 
            {
                in_cummulativepitchcountValue++;
                bl_unselected=FALSE;
                
            }

            
            [btn_loadstrikesenderid setTitle:@"3" forState:normal];  
            vw_typeOFpitchselect.hidden=YES;
            vw_StrikeView.hidden=NO;
        }
        
        if (vw_StrikeView.hidden==NO)
        {
            [btn_loadstrikesenderid setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
            
        }
        
}
     
if([sender tag]==4)

{
        
[btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
[btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
    
    if (btn_strikeTypeofPitcbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]  ) 
    {
        [btn_loadstrikesenderid setTitle:nil forState:normal];
        vw_StrikeView.hidden=YES;
        //---cummulative pitchcount-----           
        in_cummulativepitchcountValue--;
        bl_unselected=TRUE;
    }
    
    else
    {
        
        
        if (bl_unselected==TRUE) 
        {
            in_cummulativepitchcountValue++;
            bl_unselected=FALSE;
            
        }

    
        [btn_loadstrikesenderid setTitle:@"4" forState:normal];
        vw_typeOFpitchselect.hidden=YES;
        vw_StrikeView.hidden=NO;
        
    }
             
        if (vw_StrikeView.hidden==NO)
        {
            [btn_loadstrikesenderid setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
    }
}
if([sender tag] == 5)
{
    [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        if (btn_strikeTypeofPitcbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]  ) 
        {
            [btn_loadstrikesenderid setTitle:nil forState:normal];
            
            vw_StrikeView.hidden=YES;
            //---cummulative pitchcount-----           
            in_cummulativepitchcountValue--;
            bl_unselected=TRUE;
            
        }
      
        else
        {
            
            if (bl_unselected==TRUE) 
            {
                in_cummulativepitchcountValue++;
                bl_unselected=FALSE;
                
            }

            
        [btn_loadstrikesenderid setTitle:@"5" forState:normal];
            
            vw_typeOFpitchselect.hidden=YES;
            vw_StrikeView.hidden=NO;
            
        }
        
      if (vw_StrikeView.hidden==NO)
      {
            [btn_loadstrikesenderid setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
            
        }
  }
    
     if ([sender tag] == 6)
     {
         
     [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        if (btn_strikeTypeofPitcbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]  ) 
        {
            [btn_loadstrikesenderid setTitle:nil forState:normal];
            vw_StrikeView.hidden=YES;
            
            //---cummulative pitchcount-----           
            in_cummulativepitchcountValue--;
            bl_unselected=TRUE;
        }
         
        else
        {
            if (bl_unselected==TRUE) 
            {
                in_cummulativepitchcountValue++;
                bl_unselected=FALSE;
                
            }

            if (bl_unselected==TRUE) 
            {
                in_cummulativepitchcountValue++;
                bl_unselected=FALSE;
                
            }

            [btn_loadstrikesenderid setTitle:@"6" forState:normal]; 
            vw_typeOFpitchselect.hidden=YES;
            vw_StrikeView.hidden=NO;

        }
               
        if (vw_StrikeView.hidden==NO)
        {
            [btn_loadstrikesenderid setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
            }
         
   }
     
    if ([sender tag] ==7)
    {
       
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
    
        if (btn_strikeTypeofPitcbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]  ) 
        {
            [btn_loadstrikesenderid setTitle:nil forState:normal];
            
            vw_StrikeView.hidden=YES;
            //---cummulative pitchcount-----           
            in_cummulativepitchcountValue--;
            bl_unselected=TRUE;
        }
        
        else
        {
            if (bl_unselected==TRUE) 
            {
                in_cummulativepitchcountValue++;
                bl_unselected=FALSE;
                
            }

            
            [btn_loadstrikesenderid setTitle:@"7" forState:normal];  
            
            vw_typeOFpitchselect.hidden=YES;
            vw_StrikeView.hidden=NO;
                        
        }
        
        if (vw_StrikeView.hidden==NO)
        {
            [btn_loadstrikesenderid setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
            
        }

    }
    
    if ([sender tag]==8)
    {
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
       
        if (btn_strikeTypeofPitcbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]  ) 
        {
            [btn_loadstrikesenderid setTitle:nil forState:normal];
            vw_StrikeView.hidden=YES;
            //---cummulative pitchcount-----           
            in_cummulativepitchcountValue--;
            bl_unselected=TRUE;
            
        }
        
        else
        {
            if (bl_unselected==TRUE) 
            {
                in_cummulativepitchcountValue++;
                bl_unselected=FALSE;
                
            }

            
            [btn_loadstrikesenderid setTitle:@"8" forState:normal];
            vw_typeOFpitchselect.hidden=YES;
            vw_StrikeView.hidden=NO;
            
            
  
        }
        
        if (vw_StrikeView.hidden==NO)
        {
            [btn_loadstrikesenderid setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
        }

    }

    strBtn = btn_loadstrikesenderid.titleLabel.text;
    NSLog(@"The Title Of the Button : %@",strBtn);
    

        
   // [sender setBackgroundImage:[UIImage imageNamed:@"radio_unselected"] forState:UIControlStateNormal];
}


-(IBAction)SrikeOptionMethod:(UIButton*)sender

{
    NSLog(@"SrikeOptionMethod: Called!");
    
    if (btn_strikecheckboxOutPitch) 
    {
        if (btn_strikecheckboxOutPitch.tag == sender.tag)
        {
            if (btn_strikecheckboxOutPitch.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
            {
                [btn_strikecheckboxOutPitch setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"btn_selected" ofType:@"png"]]forState:UIControlStateNormal];
            }
            
            else 
            {
                [btn_strikecheckboxOutPitch setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
            }
        
        }
        
        else
        {           
            [sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
            [btn_strikecheckboxOutPitch setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
        }
        
    }
    
    else
    {
        
        [sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
    }
    
    btn_strikecheckboxOutPitch = sender;
    
  if([sender tag]==1)
  {
        
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
      if (btn_strikecheckboxOutPitch.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            
    [btn_loadstrikesenderid setTitle:nil forState:normal];
            
            vw_StrikeView.hidden=NO;
            
        }
        
        else
        {
            
            [btn_loadstrikesenderid setTitle:[NSString stringWithFormat:@"%@C",strBtn] forState:UIControlStateNormal];
             vw_StrikeView.hidden=YES;
            
        }
        
    }
    
    if([sender tag] == 2)
    {
                
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        if (btn_strikecheckboxOutPitch.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            [btn_loadstrikesenderid setTitle:nil forState:normal];
             vw_StrikeView.hidden=NO;
            
        }
        
        else
        {
            
            [btn_loadstrikesenderid setTitle:[NSString stringWithFormat:@"%@S",strBtn] forState:UIControlStateNormal];
            
            vw_StrikeView.hidden=YES;
            
        }
        
    }
    
    if ([sender tag]==3)
    
    {
              
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        if (btn_strikecheckboxOutPitch.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            
        [btn_loadstrikesenderid setTitle:nil forState:normal];
            
         vw_StrikeView.hidden=NO;
            
        }
        
        else
        {
            
            [btn_loadstrikesenderid setTitle:[NSString stringWithFormat:@"%@F",strBtn] forState:UIControlStateNormal];
            vw_StrikeView.hidden=YES;
        }

    }
    
if ([sender tag]==4)

{
      
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        vw_StrikeView.hidden =YES;
        vw_OnBaseview.hidden =NO;
        
        if (vw_OnBaseview.hidden==NO)
        {
            [btn_loadstrikesenderid setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
        }

    }
    
    if ([sender tag] == 5)
    {
        
    [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        vw_StrikeView.hidden=YES;
        vw_onBaseOptionHitFair.hidden =NO;
        
        if (vw_onBaseOptionHitFair.hidden==NO)
        {
            [btn_loadstrikesenderid setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
            
        }
        
    }
    
    if ([sender tag] == 6)
    {
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        if (btn_strikecheckboxOutPitch.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            [btn_loadstrikesenderid setTitle:nil forState:normal];
            
             vw_StrikeView.hidden=NO;
            
        }
        
        else
        {
            
            [btn_loadstrikesenderid setTitle:[NSString stringWithFormat:@"%@P",strBtn] forState:UIControlStateNormal];
            [btn_loadstrikesenderid setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
           
            vw_StrikeView.hidden=YES;
        }
        
    }
    
if([sender tag] ==7)
    
    {
       
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];

        if (btn_strikecheckboxOutPitch.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            [btn_loadstrikesenderid setTitle:nil forState:normal];
            vw_StrikeView.hidden=NO;
            
        }
        
        else
        {
            [btn_loadstrikesenderid setTitleColor:[UIColor yellowColor] forState:UIControlStateNormal];
            [btn_loadstrikesenderid setTitle:[NSString stringWithFormat:@"%@W",strBtn] forState:UIControlStateNormal];
            
            vw_StrikeView.hidden=YES;
            
        }

    }
    lbl_pitchCount.text=[NSString stringWithFormat:@"%d",in_cummulativepitchcountValue]; 
    NSLog(@"Tag Value Is : %d",sender.tag);
    [sender setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
}

-(IBAction)Onbaseoptionmethod:(UIButton*)sender
{
NSLog(@"---%s---",__func__);
    
imgv_eoi.image = nil;
lbl_batRuns.text=nil;
    
    if (batterCount==9) 
    {
        
        batterCount=0;
    }

    
if (btn_strikeoutCheckbox) 
   
{
    if (btn_strikeoutCheckbox.tag == sender.tag)
    {
            if (btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
            {
                [btn_strikeoutCheckbox setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"btn_selected" ofType:@"png"]]forState:UIControlStateNormal];
            }
        
            else 
                {
                    [btn_strikeoutCheckbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
              }
        
        }
    
        else
        {            
            [sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
            [btn_strikeoutCheckbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
        }
    }
    
    else
    {
        
        [sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
    }
    
    btn_strikeoutCheckbox = sender;
    
    
if( [sender tag]==1)
{
        
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
    
        if (btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lbl_batRuns.text = [NSString stringWithFormat:@""];
        }
    
        else
        {
            lbl_batRuns.text = [NSString stringWithFormat:@"Ks"];
  
        }
                lbl_batRuns.textColor = [UIColor whiteColor];
        
        vw_OnBaseview.hidden=YES;
        
    }
    
    
    if([sender tag] == 2)
    {
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
               lbl_batRuns.textColor = [UIColor whiteColor];
        
        if (btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lbl_batRuns.text = [NSString stringWithFormat:@""];
        }
        
        else
        {
            lbl_batRuns.text = [NSString stringWithFormat:@"Kc"];
        }

        vw_OnBaseview.hidden=YES;
        
    }
    
    if ([sender tag]==3)
    {
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
       
        if (btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lbl_batRuns.text = [NSString stringWithFormat:@""];
        }
        
        else
        {
            lbl_batRuns.text = [NSString stringWithFormat:@"P"];
            
        }
        lbl_batRuns.textColor = [UIColor blueColor];
        vw_OnBaseview.hidden=YES;
        
    }
    
    if ([sender tag]==4)
    {
        
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        if (btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lbl_batRuns.text = [NSString stringWithFormat:@""];
        }
        
        else
        {
          lbl_batRuns.text = [NSString stringWithFormat:@"W"];
            
        }
                
        lbl_batRuns.textColor = [UIColor yellowColor];
        vw_OnBaseview.hidden=YES;
        
    }
    
    
    if ([sender tag] == 5)
    {
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        
        
        if (btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lbl_batRuns.text = [NSString stringWithFormat:@""];
                    }
        
        else
        {
            lbl_batRuns.text = [NSString stringWithFormat:@"Ks"];
        }

      lbl_batRuns.textColor = [UIColor whiteColor];
      vw_OnBaseview.hidden=YES;
        
    }
    
    
    if ([sender tag] == 6)
    {
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        [lbl_jerseyNumber setText:nil];
        [lbl_batterName setText:nil];
        [lbl_RorL setText:nil];

        if (btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lbl_batRuns.text = [NSString stringWithFormat:@""];
        }
        
        else
        {
            lbl_batRuns.text = [NSString stringWithFormat:@"Kc"];
        }
        
            lbl_batRuns.textColor = [UIColor whiteColor];
            vw_OnBaseview.hidden=YES;
        
    }
    
    
    if ([sender tag] ==7)
    {
               
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        

        if(btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lbl_batRuns.text = [NSString stringWithFormat:@""];
        }
        
        else
        {
           lbl_batRuns.text = [NSString stringWithFormat:@"Ks"];
        }
        
           lbl_batRuns.textColor = [UIColor whiteColor];
            vw_OnBaseview.hidden=YES;
    }
    
    if ([sender tag]==8)
    {
        
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        [lbl_jerseyNumber setText:nil];
        [lbl_batterName setText:nil];
        [lbl_RorL setText:nil];
        
        if(btn_strikeoutCheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lbl_batRuns.text = [NSString stringWithFormat:@""];
                    }
        
        else
        {
            lbl_batRuns.text = [NSString stringWithFormat:@"Ks"];
        }
        
        lbl_batRuns.textColor = [UIColor whiteColor];
        vw_OnBaseview.hidden=YES;
        
    }
    
    if ([sender tag]==9)
    {
        
        type = @"EoiAlert";
        bl_alertPopup=TRUE;
        
        alert1 = [[UIAlertView alloc] 
                  initWithTitle:@"Message!" 
                  message:@"Did This Inning Represent a Shutdown Opportunity For The Pitcher" 
                  delegate:self  
                  cancelButtonTitle:@"No" 
                  otherButtonTitles:@"Yes",nil];
        [alert1 show];
       
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        imgv_eoi = [[UIImageView alloc] 
                                        initWithImage:[UIImage imageNamed:@"Eoi_shape.png"]];
        [lbl_batRuns addSubview:imgv_eoi];
        lbl_batRuns.backgroundColor = [UIColor clearColor];
               
        vw_OnBaseview.hidden=YES;
        
        }
    
    
if ([sender tag]==10)
 {
    
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
      
     vw_OnBaseview.hidden=YES;
     
  }
    runsCount=0;
    
    
}

-(IBAction)OnbaseoptionHitFairMethod:(UIButton *)sender
{

    runsCount=0;
    NSLog(@"--> OnbaseoptionHitFairMethod: Called <--");
    
    
    type=@"Normal";
    
      
    if(in_circleoutcount==3)
    {
        NSLog(@"---int circle out value---");
        in_circleoutcount=0;
        
        }
    
    [tblw_alldatatable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
   [tblw_alldatatable reloadData]; 
  
 outComeAtbat_Tag=[sender tag];
 imgv_hitEoi.image = nil;
    
 if (btn_hitfaircheckbox) 
 {
     
   if (btn_hitfaircheckbox.tag == sender.tag )
    {
        NSLog(@"--btn-%i-sender--%i",[btn_hitfaircheckbox tag],[sender tag]);
        
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            [btn_hitfaircheckbox setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"btn_selected" ofType:@"png"]]forState:UIControlStateNormal];
         }
 
        else 
        {
    [btn_hitfaircheckbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
                    
       }
   }
     
  else
  {
    
[sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
            [btn_hitfaircheckbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
    }
 }
 
 else
{
        
[sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
    
}
    
btn_hitfaircheckbox = sender;
        
if ( [sender tag]==1)
{
        lbl_circleout.text=Nil;
        [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        self.vw_LocationOfHitView.frame = CGRectMake(350, 350, 213, 365);
     
        lbl_batRuns.text = [NSString stringWithFormat:@"1B"];
        lbl_batRuns.textColor = [UIColor whiteColor];
    
   


       if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
       {
             vw_LocationOfHitView.hidden=YES;
           runsCount--;
       }
    
        else
        vw_LocationOfHitView.hidden=NO;
    
 }

    if([sender tag] == 2)
    {
        
        lbl_circleout.text=Nil;
        [self.vw_defensivePositions removeFromSuperview];    
        bl_DefensivePosiPopups=TRUE;
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
         self.vw_LocationOfHitView.frame = CGRectMake(350, 370, 213, 365);

        lbl_batRuns.text = [NSString stringWithFormat:@"2B"];
        lbl_batRuns.textColor = [UIColor whiteColor];
        
        
        
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            vw_LocationOfHitView.hidden=YES;
            runsCount-=2;
           
        }
        
        else
                        
            vw_LocationOfHitView.hidden=NO;
         
    }
  
    if ([sender tag]==3)
    {
        lbl_circleout.text=Nil;
    [self.vw_defensivePositions removeFromSuperview];  
        bl_DefensivePosiPopups=TRUE;
      [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
         self.vw_LocationOfHitView.frame = CGRectMake(350, 390, 213, 365);
        
        lbl_batRuns.text = [NSString stringWithFormat:@"3B"];
        lbl_batRuns.textColor = [UIColor whiteColor];
        
        
        
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            vw_LocationOfHitView.hidden=YES;
            runsCount-=3;
            
        }
        
        else
            
            vw_LocationOfHitView.hidden=NO;
        
    }
    
    
    if ([sender tag]==4)
    {
        
        lbl_circleout.text=Nil;
        
        [self.vw_defensivePositions removeFromSuperview];  
        bl_DefensivePosiPopups=TRUE;
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
         self.vw_LocationOfHitView.frame = CGRectMake(350, 410, 213, 365);
        
        [btn_loadstrikesenderid setTitle:@"Bs" forState:normal];
        lbl_batRuns.text = [NSString stringWithFormat:@"Bs"];
        lbl_batRuns.textColor = [UIColor whiteColor];
        
       

        
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            vw_LocationOfHitView.hidden=YES;
            runsCount--;
            
        }
        
        else
            
            vw_LocationOfHitView.hidden=NO;
             
    }
    
    if ([sender tag] == 5)
    {
       
        lbl_circleout.text=Nil;
           
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
         self.vw_LocationOfHitView.frame = CGRectMake(350, 430, 213, 365);
            
        [self.vw_defensivePositions removeFromSuperview];     
         bl_DefensivePosiPopups=TRUE;
        lbl_batRuns.text = [NSString stringWithFormat:@"HR"];
        lbl_batRuns.textColor = [UIColor whiteColor];
               
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            vw_LocationOfHitView.hidden=YES;
            
            
        }
        
        else
            
            vw_LocationOfHitView.hidden=NO;        
           
    }
    
    if ([sender tag] == 6)
    {      
        lbl_circleout.text=Nil;
        
        vw_LocationOfHitView.hidden=YES;
        imgv_Fielderchoice.image =nil;
        
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        self.vw_defensivePositions.frame = CGRectMake(350, 550, 170, 296);
        lbl_batRuns.text = [NSString stringWithFormat:@"E"];
        lbl_batRuns.textColor = [UIColor whiteColor];
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            [ self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
        }
        
        else
        {
        
            [self.scrv_scoreboard addSubview:vw_defensivePositions];
            bl_DefensivePosiPopups=FALSE;
        
        }
        
    }
    
    if ([sender tag] ==7)
    {
        lbl_circleout.text=Nil;
                
        vw_LocationOfHitView.hidden=YES;
       
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        self.vw_defensivePositions.frame = CGRectMake(350, 550, 170, 296);
        lbl_batRuns.text = [NSString stringWithFormat:@"FC"];
        lbl_batRuns.textColor = [UIColor whiteColor];
        
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
        }
        
        else
        {
           
            [self.scrv_scoreboard addSubview:vw_defensivePositions]; 
            bl_DefensivePosiPopups=FALSE;
        }
        
    }
    
    if ([sender tag]==8)
    {
        
        vw_LocationOfHitView.hidden=YES;
        self.vw_defensivePositions.frame = CGRectMake(350, 400, 170, 296);
        
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        self.vw_defensivePositions.frame = CGRectMake(350, 650, 170, 296);
        in_circleoutcount++;
        NSString *str_circleout = [NSString stringWithFormat:@"%i",in_circleoutcount];
        lbl_circleout.text=str_circleout;
        lbl_batRuns.text = [NSString stringWithFormat:@""];
        lbl_batRuns.textColor = [UIColor whiteColor];
           
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            in_circleoutcount=0;
            lbl_circleout.text=Nil;
           

        }
        
        else
        {
            [self.scrv_scoreboard addSubview:vw_defensivePositions];
            bl_DefensivePosiPopups=FALSE;
        }
    }
    
    
if([sender tag]==9)
    {
        
        vw_LocationOfHitView.hidden=YES;  
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        self.vw_defensivePositions.frame = CGRectMake(350, 700, 170, 296);
        in_circleoutcount++;
       
        NSString *str_circleout = [NSString stringWithFormat:@"%i",in_circleoutcount];
        lbl_circleout.text=str_circleout;
        lbl_batRuns.text = [NSString stringWithFormat:@""];
        lbl_batRuns.textColor = [UIColor whiteColor];
        
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            in_circleoutcount=0;
            lbl_circleout.text=Nil;
            
        }
        
        else
        {
            [self.scrv_scoreboard addSubview:vw_defensivePositions];
            bl_DefensivePosiPopups=FALSE;  
        }
        }
    
if ([sender tag]==10)
   {
            
        vw_LocationOfHitView.hidden=YES;
        
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        in_circleoutcount++;
                NSString *str_circleout = [NSString stringWithFormat:@"%i",in_circleoutcount];
        lbl_circleout.text=str_circleout;
        self.vw_defensivePositions.frame = CGRectMake(350, 740, 170, 296);
        lbl_batRuns.text = [NSString stringWithFormat:@""];
        lbl_batRuns.textColor = [UIColor whiteColor];
        
 if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
 
 {
        in_circleoutcount=0;
        lbl_circleout.text=Nil;
        [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;
        
            
        }
        
        else
        {
            [self.scrv_scoreboard addSubview:vw_defensivePositions];
            bl_DefensivePosiPopups=FALSE;
        }

}
    

if([sender tag]==11)
{
        
        vw_LocationOfHitView.hidden=YES;
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
    
        in_circleoutcount++;
       
        NSString *str_circleout = [NSString stringWithFormat:@"%i",in_circleoutcount];
        lbl_circleout.text=str_circleout;
        self.vw_defensivePositions.frame = CGRectMake(350, 780, 170, 296);
        lbl_batRuns.text = [NSString stringWithFormat:@""];
        lbl_batRuns.textColor = [UIColor whiteColor];
    
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            
            in_circleoutcount=0;
            lbl_circleout.text=Nil;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
    
        else
        {
            [self.scrv_scoreboard addSubview:vw_defensivePositions]; 
            bl_DefensivePosiPopups=FALSE;
        }
        
    }
    
    if ([sender tag]==12)
    {
           
        vw_LocationOfHitView.hidden=YES;
        in_circleoutcount++;
        
        NSString *str_circleout = [NSString stringWithFormat:@"%i",in_circleoutcount];
        lbl_circleout.text=str_circleout;
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        self.vw_defensivePositions.frame = CGRectMake(350, 780, 170, 296);
        lbl_batRuns.text = [NSString stringWithFormat:@""];
        lbl_batRuns.textColor = [UIColor whiteColor];
        
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            
            in_circleoutcount=0;
            lbl_circleout.text=Nil;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
           
        }
        
        else
        {
            [self.scrv_scoreboard addSubview:vw_defensivePositions];
            bl_DefensivePosiPopups=FALSE;
        }
    
    }
    
if([sender tag]==13)
    
{
        vw_LocationOfHitView.hidden=YES;
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        in_circleoutcount++;
       
        NSString *str_circleout = [NSString stringWithFormat:@"%i",in_circleoutcount];
        lbl_circleout.text=str_circleout;
        self.vw_defensivePositions.frame = CGRectMake(350, 780, 170, 296);
        lbl_batRuns.text = [NSString stringWithFormat:@""];
        lbl_batRuns.textColor = [UIColor whiteColor];
        
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            in_circleoutcount=0;
            lbl_circleout.text=Nil;
            
        }
        
        else
        {
            [self.scrv_scoreboard addSubview:vw_defensivePositions];
            bl_DefensivePosiPopups=FALSE;
        }
        
     }
    
    if ([sender tag]==14)
    {
        
        vw_LocationOfHitView.hidden=YES;
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        self.vw_defensivePositions.frame = CGRectMake(350, 780, 170, 296);
        lbl_batRuns.text = [NSString stringWithFormat:@""];
        lbl_batRuns.textColor = [UIColor whiteColor];
        
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
        }
        
        else
        {
            [self.scrv_scoreboard addSubview:vw_defensivePositions];
            bl_DefensivePosiPopups=FALSE;
        }
        
    }

    if ([sender tag]==15)
    {
        
        vw_LocationOfHitView.hidden=YES;
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        self.vw_defensivePositions.frame = CGRectMake(350, 780, 170, 296);
        
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
        }
        
        else
        {
            [self.scrv_scoreboard addSubview:vw_defensivePositions];
            bl_DefensivePosiPopups=FALSE;
        }
        
}
    
    if ([sender tag]==16)
    {
        
        type = @"EoiAlert";
        bl_alertPopup=TRUE;
        
       alert1 = [[UIAlertView alloc] 
                               initWithTitle:@"Alert!" 
                               message:@"Did This Inning Represent a Shutdown Opportunity For The Pitcher" 
                               delegate:self  
                               cancelButtonTitle:@"No" 
                               otherButtonTitles:@"Yes",nil];
        [alert1 show];
        
        [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;
        vw_LocationOfHitView.hidden=YES;
        lbl_batRuns.text=nil;
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
             imgv_hitEoi = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Eoi_shape.png"]];
        
        [lbl_batRuns addSubview:imgv_hitEoi];
        lbl_batRuns.backgroundColor =[UIColor clearColor];
        
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            [alert1 dismissWithClickedButtonIndex:0 animated:YES];
            imgv_hitEoi.image =nil;
            alert1.hidden=YES;
                    
        }
        
        else
        {}
                    
    }
    
    if ([sender tag]==17)
    {
        vw_LocationOfHitView.hidden=YES;
        [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
        }
        
        else
        {}       
        
}
    
    if ([sender tag]==18)
    {
      
        vw_LocationOfHitView.hidden=YES;
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        self.vw_defensivePositions.frame = CGRectMake(350, 780, 170, 296);
        lbl_batRuns.text = [NSString stringWithFormat:@""];
        lbl_batRuns.textColor = [UIColor whiteColor];
        
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
        }
        
        else
        {
            [self.scrv_scoreboard addSubview:vw_defensivePositions];
            bl_DefensivePosiPopups=FALSE;
        }
    }
    
    if([sender tag]==19)
    {
        
        vw_LocationOfHitView.hidden=YES;
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
        self.vw_defensivePositions.frame = CGRectMake(350, 780, 170, 296);
        lbl_batRuns.text = [NSString stringWithFormat:@""];
        lbl_batRuns.textColor = [UIColor whiteColor];
    
        if(btn_hitfaircheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
        }
    
        else
        {
            
            [self.scrv_scoreboard addSubview:vw_defensivePositions];
            bl_DefensivePosiPopups=FALSE;
        }
  }
  
    NSLog(@"-->Tag Value : %d -->",sender.tag);
    
}

-(IBAction)HomeRunControllermethod:(id)sender

{
    
    NSLog(@"--> HomeRunControllermethod: Called! <--");
    
if (segmnt_controll.selectedSegmentIndex == 0)
{
          
        lbl_batRuns.text = [NSString stringWithFormat:@"HR"];
        lbl_batRuns.textColor = [UIColor whiteColor];
}
   

else
    {
        lbl_batRuns.text=nil;
        NSLog(@"--switched off---");
        
    }
    
}

-(IBAction)StrikepopupMethod:(UIButton *)sender
{
    
   
 
    if ([teamName_lbl.text length]<1 || [opponent_lbl.text length]<1 || [catcher_lbl.text length]<1 || [lbl_datepicker.text length]<1 ) 
    {
        
        UIAlertView *MsgAlert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Selection Of All  Options Above Are Mandatory!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [MsgAlert show];
        return;
        
    }
    
    if ([lbl_jerseyNumber.text length]<1 || [lbl_batterName.text length]<1 || [lbl_RorL.text length]<1) 
    {
        UIAlertView *MsgAlert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please Fill  Batter Details." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [MsgAlert show];
        
        return;
    }  

    
    
//if(btn_loadSenderidbtn.currentTitle==nil)
//    {
//          
//        UIAlertView *alrtv_ballpopup = [[UIAlertView alloc]initWithTitle:@"Alert Message!" message:@"Please select the ball type of pitch value" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//        
//     
//        [alrtv_ballpopup show];
//        [alrtv_ballpopup release];
//        
//    }
//    
//    else
//    {
    
//        for (int i=5; i<[sender tag]; i++) {
//            
//            UIButton *btn=[ArrayOfButtons objectAtIndex:i];
//            if (btn.currentTitle==nil) {
//                
//                NSLog(@"Current Title Of Previos Button : %@",btn.currentTitle);
//                UIAlertView *alertSequence=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please Select the previous Box." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//                [alertSequence show];
//                [alertSequence release];
//                return;
//            }
//            
//        }

        
    mainbaserunnerpopup=TRUE;
    imgv_hitEoi.image=nil;
    imgv_eoi.image=nil;
    nvgn_datepicker.hidden=YES;
    date_picker.hidden=YES;
    
    [self.vw_Mainbaserunnerview removeFromSuperview];
    [self.vw_defensivePositions removeFromSuperview]; 
    bl_DefensivePosiPopups=TRUE;
    vw_BallPopMainMenu.hidden=YES;
    vw_BallPopUpSubMenu.hidden=YES;
    vw_LocationOfHitView.hidden=YES;
    
    [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
    [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
   
     NSLog(@"--> Tag Value :%d",[sender tag]);
    
    if ([sender tag]==6)
   {
        btn_strike_out.hidden=YES;
               
    }
    
    if ([sender tag] ==8||[sender tag] == 9||[sender tag] == 10)
    {
        
        btn_strike_out.hidden=NO;
    }
    
    btn_loadstrikesenderid = sender;
    
       
        // For unselecting the radio button when clicking on Different Rectangular Box Button
        [btn_strikeTypeofPitcbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
        [btn_strikecheckboxOutPitch setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
        [btn_hitfaircheckbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
        [btn_strikeoutCheckbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal]; 
        
       
    if (vw_typeOFpitchselect.hidden==YES ) 
    {
        vw_typeOFpitchselect.hidden = NO;
        [btn_loadstrikesenderid setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
    }
    
    else 
        
    if(vw_typeOFpitchselect.hidden==NO ||vw_StrikeView.hidden==NO)
     {
        vw_StrikeView.hidden = YES;
        vw_typeOFpitchselect.hidden=YES;
             
     }
    
    
if (( vw_StrikeView.hidden == NO )|| vw_OnBaseview.hidden == NO || vw_onBaseOptionHitFair.hidden==NO)
{
    
        vw_onBaseOptionHitFair.hidden=YES;
        vw_StrikeView.hidden=YES;
        vw_OnBaseview.hidden=YES;
        vw_typeOFpitchselect.hidden = YES;

}

NSLog(@"-------below button-----");
   
    } 
//}

-(IBAction)BallpopupMethod:(UIButton *)sender
{
 
  
    NSLog(@"-latest button value--%@",btn_ballbox1.titleLabel.text);
    
    
    if ([teamName_lbl.text length]<1 || [opponent_lbl.text length]<1 || [catcher_lbl.text length]<1 || [lbl_datepicker.text length]<1 ) 
    {
        
        UIAlertView *MsgAlert=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"Selection Of All  Options Above Are Mandatory!" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [MsgAlert show];
        return;
                
    }
    
    if ([lbl_jerseyNumber.text length]<1 || [lbl_batterName.text length]<1 || [lbl_RorL.text length]<1) 
    {
        UIAlertView *MsgAlert=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please Fill  Batter Details." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [MsgAlert show];

        return;
    }  
//    
//    for (int i=0; i<[sender tag]; i++) {
//        
//        UIButton *btn=[ArrayOfButtons objectAtIndex:i];
//        if (btn.currentTitle==nil) {
//            
//            NSLog(@"Current Title Of Previos Button : %@",btn.currentTitle);
//            UIAlertView *alertSequence=[[UIAlertView alloc]initWithTitle:@"Alert!" message:@"Please Select the previous Box." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//            [alertSequence show];
//            [alertSequence release];
//            return;
//        }
//        
//    }
    
    imgv_Fielderchoice.image=nil;
    lbl_batRuns.text=nil;
    
    // For unselecting the radio button when clicking on Different Rectangular Box Button

    [btn_checkboxmainmenu setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
    [btn_submenucheckbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
    
 NSLog(@"--> BallpopupMethod: Called! <--");
    
     
    mainbaserunnerpopup=TRUE;
    imgv_hitEoi.image=nil;
    imgv_eoi.image=nil;
    
    [self.vw_Mainbaserunnerview removeFromSuperview];
    [self.vw_defensivePositions removeFromSuperview];   
    bl_DefensivePosiPopups=TRUE;
    [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
    [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
    
    
    vw_onBaseOptionHitFair.hidden=YES;
    vw_OnBaseview.hidden=YES;
    vw_StrikeView.hidden=YES;
    vw_typeOFpitchselect.hidden=YES;
    vw_LocationOfHitView.hidden=YES;
    
    btn_loadSenderidbtn= sender;
        
  
    NSLog(@"-latest button value--%@",btn_ballbox1.titleLabel.text);

    
    
NSLog(@"--> Tag Value : %d -->",[sender tag]);
    
    
    NSString *str=btn_loadSenderidbtn.currentTitle;
    NSLog(@"Current Title Of btn_loadSenderidbtn : %@",str);
    
       
    if (vw_BallPopUpSubMenu.hidden==NO)
    {
        
        vw_BallPopUpSubMenu.hidden=YES;
        vw_BallPopMainMenu.hidden=NO;
    
 }
    
if(ballpopup==TRUE && vw_BallPopMainMenu.hidden==YES )
{
        vw_BallPopMainMenu.hidden = NO;
        [btn_loadSenderidbtn setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
           
    } 
    
else
        
if(vw_BallPopMainMenu.hidden==NO || vw_BallPopUpSubMenu.hidden==NO) 
{
            vw_BallPopMainMenu.hidden=YES;
            vw_BallPopUpSubMenu.hidden=YES;
            [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
 
    }
    
else
    {
        vw_BallPopMainMenu.hidden=NO;
        
    }
    
    NSLog(@"-->>above button<<--");
    
}


-(IBAction)BallpopMainMenuMethod:(UIButton *)sender
{
   
    btn_loadstrikesenderid= nil;
    if(btn_loadSenderidbtn == btn_ballbox1)
    {
      
        [btn_strikebox8 setTitle:nil forState:UIControlStateNormal];
        
        if (bl_ballbox1==FALSE)
        {
           
        in_cummulativepitchcountValue++;
            bl_ballbox1=TRUE;
            
        }
        
    }
    
    if(btn_loadSenderidbtn == btn_ballbox2)
    {
        [btn_strikebox9 setTitle:nil forState:UIControlStateNormal];
        
        if (bl_ballbox2==FALSE)
        {
            
            in_cummulativepitchcountValue++;
            bl_ballbox2=TRUE;
            
        }
         
    }
if(btn_loadSenderidbtn == btn_ballbox3)
    {
        [btn_strikebox10 setTitle:nil forState:UIControlStateNormal];
        
        if (bl_ballbox3==FALSE)
        {
            
            in_cummulativepitchcountValue++;
            bl_ballbox3=TRUE;
            
        }
    }
    if(btn_loadSenderidbtn == btn_ballbox4)
    {
        [btn_strikebox11 setTitle:nil forState:UIControlStateNormal];
        
        if (bl_ballbox4==FALSE)
        {
            
            in_cummulativepitchcountValue++;
            bl_ballbox4=TRUE;
            
        }

    }
    if(btn_loadSenderidbtn == btn_ballbox5)
    {
        [btn_strikebox12 setTitle:nil forState:UIControlStateNormal];
        
        if (bl_ballbox5==FALSE)
        {
            
            in_cummulativepitchcountValue++;
            bl_ballbox5=TRUE;
            
            
        }

    }
    if(btn_loadSenderidbtn == btn_ballbox6)
    {
       [btn_strikebox13 setTitle:nil forState:UIControlStateNormal];
        if (bl_ballbox6==FALSE)
        {
            
            in_cummulativepitchcountValue++;
            bl_ballbox6=TRUE;
            
        }

        
    }
    if(btn_loadSenderidbtn == btn_ballbox7)
    {
        [btn_strikebox14 setTitle:nil forState:UIControlStateNormal];
        
        if (bl_ballbox7==FALSE)
        {
            
            in_cummulativepitchcountValue++;
            bl_ballbox7=TRUE;
            
            
        }

        
    }
              
if(btn_checkboxmainmenu) 
  {
        if (btn_checkboxmainmenu.tag == sender.tag)
        {
            if (btn_checkboxmainmenu.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
            {
                [btn_checkboxmainmenu setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"btn_selected" ofType:@"png"]]forState:UIControlStateNormal];
            }
            
            else 
                {
                    [btn_checkboxmainmenu setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
                    
                }
            
        }
      
        else
        {
        [sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
            [btn_checkboxmainmenu setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
        }
      
    }
    
    else
    {
    
        [sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
    }
    
    btn_checkboxmainmenu = sender;

if([sender tag]==1)
{
   
        
  [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
if(btn_checkboxmainmenu.currentBackgroundImage ==[UIImage imageNamed:@"radio_unselected"])
{  
    
        [btn_loadSenderidbtn setTitle:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];
        vw_BallPopUpSubMenu.hidden=YES;
    
    in_cummulativepitchcountValue--;
    bl_unselected=TRUE;
    }
        
else
{
    
    if (bl_unselected==TRUE) 
    {
        in_cummulativepitchcountValue++;
        bl_unselected=FALSE;
        
    }
    
[btn_loadSenderidbtn setTitle:@"1" forState:normal];
 vw_BallPopMainMenu.hidden=YES;
         vw_BallPopUpSubMenu.hidden=NO;
 
        }
        
        if (vw_BallPopUpSubMenu.hidden==NO)
        {
            
            [btn_loadSenderidbtn setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
            
 }
           
}
    
    if([sender tag] == 2)
    {
        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];

        if (btn_checkboxmainmenu.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"])
        {
            [btn_loadSenderidbtn setTitle:nil forState:UIControlStateNormal];
            [btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];
            vw_BallPopUpSubMenu.hidden=YES; 
            
            in_cummulativepitchcountValue--;
            bl_unselected=TRUE;                         
        }
        
        else
        {
        
            
            if (bl_unselected==TRUE) 
            {
                in_cummulativepitchcountValue++;
               
                bl_unselected=FALSE;
                
            }


        [btn_loadSenderidbtn setTitle:@"2" forState:normal];
        vw_BallPopMainMenu.hidden=YES;
        vw_BallPopUpSubMenu.hidden=NO;
        }
        
        if (vw_BallPopUpSubMenu.hidden==NO)
        {
            
            [btn_loadSenderidbtn setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
        }
        
    }
    
    if ([sender tag]==3)
    {
               
    [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
    [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
    if (btn_checkboxmainmenu.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"])
    {
            [btn_loadSenderidbtn setTitle:nil forState:UIControlStateNormal];
            [btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];
            vw_BallPopUpSubMenu.hidden=YES;
        //---cummulative pitchcount-----           
        in_cummulativepitchcountValue--;
        bl_unselected=TRUE;
        }
    else
        {
            if (bl_unselected==TRUE) 
            {
                in_cummulativepitchcountValue++;
                bl_unselected=FALSE;
                
            }

                      
            [btn_loadSenderidbtn setTitle:@"3" forState:normal];            
            vw_BallPopMainMenu.hidden=YES;
            vw_BallPopUpSubMenu.hidden=NO;
            
        }
        
        if (vw_BallPopUpSubMenu.hidden==NO)
        {
        
            [btn_loadSenderidbtn setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
        }
          
    }
    
    if ([sender tag]==4)
    {
    
        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
        if (btn_checkboxmainmenu.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"])
        {
            
            [btn_loadSenderidbtn setTitle:nil forState:UIControlStateNormal];
            [btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];  
            vw_BallPopUpSubMenu.hidden=YES;
            in_cummulativepitchcountValue--;
            bl_unselected=TRUE;

            
        }
        
        else
        {
               
            if (bl_unselected==TRUE) 
            {
                in_cummulativepitchcountValue++;
                bl_unselected=FALSE;
                
            }
  
         [btn_loadSenderidbtn setTitle:@"4" forState:normal]; 
         vw_BallPopMainMenu.hidden=YES;
         vw_BallPopUpSubMenu.hidden=NO;
             
            
        }
                
        if (vw_BallPopUpSubMenu.hidden==NO)
        {
            
            [btn_loadSenderidbtn setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
            
        }
        
    }
    
    if ([sender tag] == 5)
    {                   
        
        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
        if (btn_checkboxmainmenu.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"])
        {
            [btn_loadSenderidbtn setTitle:nil forState:UIControlStateNormal];
            [btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];            vw_BallPopUpSubMenu.hidden=YES;
            //---cummulative pitchcount-----           
            in_cummulativepitchcountValue--;
            bl_unselected=TRUE;
        
        }
        
        else
        {
            if (bl_unselected==TRUE) 
            {
                in_cummulativepitchcountValue++;
                bl_unselected=FALSE;
                
            }

            
            [btn_loadSenderidbtn setTitle:@"5" forState:normal];
            vw_BallPopMainMenu.hidden=YES;
            vw_BallPopUpSubMenu.hidden=NO;
                    }
        
        if (vw_BallPopUpSubMenu.hidden==NO)
        {
            
            [btn_loadSenderidbtn setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
        }
        
    }
    
    if ([sender tag] == 6)
    {

        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];

        if (btn_checkboxmainmenu.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"])
        {
            [btn_loadSenderidbtn setTitle:nil forState:UIControlStateNormal];
            [btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];
            vw_BallPopUpSubMenu.hidden=YES;
            //---cummulative pitchcount-----           
            in_cummulativepitchcountValue--;
            bl_unselected=TRUE;
        }
        
        else
        {
            if (bl_unselected==TRUE) 
            {
                in_cummulativepitchcountValue++;
                bl_unselected=FALSE;
                
            }

             
            [btn_loadSenderidbtn setTitle:@"6" forState:normal]; 
            vw_BallPopMainMenu.hidden=YES;
            vw_BallPopUpSubMenu.hidden=NO;
                   
        }
           
        if (vw_BallPopUpSubMenu.hidden==NO)
        {
            
            [btn_loadSenderidbtn setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
            
        }
        
    }
    
    if ([sender tag] ==7)
    {
        
        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
        if (btn_checkboxmainmenu.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"])
        {
            [btn_loadSenderidbtn setTitle:nil forState:UIControlStateNormal];
            [btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];             vw_BallPopUpSubMenu.hidden=YES;
            //---cummulative pitchcount-----           
            in_cummulativepitchcountValue--;
            bl_unselected=TRUE;
        }
        
        else
        {
            if (bl_unselected==TRUE) 
            {
                in_cummulativepitchcountValue++;
                bl_unselected=FALSE;
                
            }

             
            [btn_loadSenderidbtn setTitle:@"7" forState:normal];
            vw_BallPopMainMenu.hidden=YES;
            vw_BallPopUpSubMenu.hidden=NO;
                     }
                
        if (vw_BallPopUpSubMenu.hidden==NO)
        {
                        
            [btn_loadSenderidbtn setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
            
        }

    }
    
    if ([sender tag]==8)
    {
        
        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];

        if (btn_checkboxmainmenu.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"])
        {
            [btn_loadSenderidbtn setTitle:nil forState:UIControlStateNormal];
            [btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];
            vw_BallPopUpSubMenu.hidden=YES;
            //---cummulative pitchcount-----           
            in_cummulativepitchcountValue--;
            bl_unselected=TRUE;
        }
        
        else
        {          
            if (bl_unselected==TRUE) 
            {
                in_cummulativepitchcountValue++;
                bl_unselected=FALSE;
                
            }
   
            
            [btn_loadSenderidbtn setTitle:@"8" forState:normal];
            vw_BallPopMainMenu.hidden=YES;
            vw_BallPopUpSubMenu.hidden=NO;
                     }
          
        if (vw_BallPopUpSubMenu.hidden==NO)
        {
            
            [btn_loadSenderidbtn setBackgroundImage:[UIImage imageNamed:@"blue_bar.png"] forState:UIControlStateNormal];
            }

     }
           }
-(IBAction)BallpopSubMainMethod:(UIButton*)sender
{

    NSLog(@"--%s--",__func__);   
    if(btn_submenucheckbox) 
    {
        
        if (btn_submenucheckbox.tag == sender.tag)
        {
            if (btn_submenucheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
            {
                [btn_submenucheckbox setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"btn_selected" ofType:@"png"]]forState:UIControlStateNormal];
            }
            
            else 
            {
            
                [btn_submenucheckbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
            }
        }
        
        else
        {
    
            [sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
            [btn_submenucheckbox setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
        }
    }
    
    else
    {
        
        [sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
    }
    
    btn_submenucheckbox = sender;
    
    
if ( [sender tag]==1)
{        
        
        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];

        if (btn_submenucheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lbl_batRuns.text =[NSString stringWithFormat:@""];
            vw_BallPopUpSubMenu.hidden=NO;
            
        }
    
        else
        {
           lbl_batRuns.text =[NSString stringWithFormat:@"BB"]; 
            vw_BallPopUpSubMenu.hidden=YES;
        }
        
        lbl_batRuns.textColor = [UIColor whiteColor];
              
    }
    
    if([sender tag] ==2)
    {
        
        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];

        if (btn_submenucheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lbl_batRuns.text =[NSString stringWithFormat:@""];
             vw_BallPopUpSubMenu.hidden=NO;
        }
        
        else
        {
          lbl_batRuns.text =[NSString stringWithFormat:@"HBP"]; 
             vw_BallPopUpSubMenu.hidden=YES;
        }
        
        lbl_batRuns.textColor = [UIColor whiteColor];
        
    }
    
    
    if ([sender tag]==3)
    {
    
        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
        if (btn_submenucheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            
            lbl_batRuns.text =[NSString stringWithFormat:@""];
            vw_BallPopUpSubMenu.hidden=NO;
            
        }
        
        else
        {
            lbl_batRuns.text =[NSString stringWithFormat:@"P"];
             
            vw_BallPopUpSubMenu.hidden=YES;
        }
       
        lbl_batRuns.textColor = [UIColor blueColor];
        
    }
    
if([sender tag]==4)
    {
        
        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];

        if (btn_submenucheckbox.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            lbl_batRuns.text =[NSString stringWithFormat:@""];
             vw_BallPopUpSubMenu.hidden=NO;
            
        }
        
        else
        {
            
            lbl_batRuns.text =[NSString stringWithFormat:@"W"];
             vw_BallPopUpSubMenu.hidden=YES;
        }
        
        lbl_batRuns.textColor = [UIColor yellowColor];
      
    }
    
  if ([sender tag]==5)
   {
        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        [btn_loadSenderidbtn setTitle:@"" forState:normal]; 
        vw_BallPopUpSubMenu.hidden=YES;
       
   }
    
    lbl_pitchCount.text=[NSString stringWithFormat:@"%d", in_cummulativepitchcountValue ]; 
    
 
}

-(void)didRotateFromInterfaceOrientation: (UIInterfaceOrientation) fromInterfaceOrientation
{
    
    if(fromInterfaceOrientation != UIInterfaceOrientationPortrait &&
       fromInterfaceOrientation != UIInterfaceOrientationPortraitUpsideDown)
    {
        // portait
        self.scrv_scoreboard.frame = CGRectMake(0, 0, 768, 1024);
        self.nvgn_datepicker.frame = CGRectMake(360, 93, 325, 44);
        
        
   }
    
    else
    
    {    // Landscape
        self.scrv_scoreboard.frame = CGRectMake(0, 0, 1024, 768);
        self.nvgn_datepicker.frame = CGRectMake(360, 93, 325, 44);
   }
     
}


-(IBAction)BacktoBallTypeofpitchppopup:(id)sender

{
    
    NSLog(@"--> BacktoBallTypeofpitchppopup: Called! <--");
    
    vw_BallPopMainMenu.hidden=NO;
    vw_BallPopUpSubMenu.hidden=YES;
    
}


-(IBAction)BacktostrikeTypeofpitchpopup:(id)sender

{
    NSLog(@"--> BacktostrikeTypeofpitchpopup: Called! <--");
    vw_typeOFpitchselect.hidden=NO;
    vw_StrikeView.hidden=YES;
    
}


-(IBAction)BacktostrikebasepopupFrmstrikeout:(id)sender
{
    NSLog(@"--> BacktostrikebasepopupFrmstrikeout: Called! <--");
    vw_StrikeView.hidden=NO;
    vw_OnBaseview.hidden=YES;
    [self.vw_defensivePositions removeFromSuperview];
     bl_DefensivePosiPopups=TRUE;
    
}


-(IBAction)BacktostrikebasepopupFrmHitfair:(id)sender

{
    NSLog(@"--> BacktostrikebasepopupFrmHitfair: Called <--");
    
    vw_StrikeView.hidden=NO;
    vw_onBaseOptionHitFair.hidden=YES;
    vw_LocationOfHitView.hidden=YES;
    [self.vw_defensivePositions removeFromSuperview];
     bl_DefensivePosiPopups=TRUE;
    
}

////---------Base runner methods---------
//-(IBAction)mainBaseRunnerview:(UIButton *)sender
//{
//  
//     if (btn_loadSenderidbtn.currentTitle==nil ||btn_loadstrikesenderid.currentTitle==nil )
//     {
//     
//    UIAlertView *alrtv_ballpopup = [[UIAlertView alloc]initWithTitle:@"Alert Message!" message:@"Please Select The 'Ball and Strike' Type Of Pitch Values" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
//    [alrtv_ballpopup show];
//    [alrtv_ballpopup release];
//    }
//    
//else
//{
//        
//    date_picker.hidden = YES;
//    nvgn_datepicker.hidden = YES;
//    vw_BallPopMainMenu.hidden=YES;
//    vw_BallPopUpSubMenu.hidden=YES;
//    vw_onBaseOptionHitFair.hidden=YES;
//    vw_OnBaseview.hidden=YES;
//    vw_StrikeView.hidden=YES;
//    vw_typeOFpitchselect.hidden=YES;
//    vw_LocationOfHitView.hidden=YES;
//    
//    [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
//    [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
//    [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
//    [btn_loadstrikesenderid setImage:nil forState:UIControlStateNormal];
//NSLog(@"--> mainBaseRunnerview: Called! <--");
//        [self.vw_defensivePositions removeFromSuperview];
//         bl_DefensivePosiPopups=TRUE;
//    self.vw_Mainbaserunnerview.frame = CGRectMake(35, 264, 310, 785);
//    
//    if (mainbaserunnerpopup==TRUE)
//    {
//    [self.scrv_scoreboard addSubview:vw_Mainbaserunnerview];
//        mainbaserunnerpopup=FALSE;
//
//    }
//    
//    else
//    {
//        
//    [self.vw_Mainbaserunnerview removeFromSuperview];
//        mainbaserunnerpopup=TRUE;
//    }
//    }
//          
//}

-(IBAction)BaserunnerTagCallMethod:(UIButton *)sender
{
   NSLog(@"--> BaserunnerTagCallMethod: Called! <--");
    
    imgv_hitEoi.image=nil;
    [self.vw_defensivePositions removeFromSuperview];
    bl_DefensivePosiPopups=TRUE;
    [tblw_alldatatable scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:NO];
    mainbaserunnerpopup=TRUE;
    inTag_baseRunnerDEfensivepos = [sender tag];
    [lbl_batRuns setTextColor:[UIColor whiteColor]];
    type=@"Normal";

      [tblw_alldatatable reloadData];
    
    if(in_baseCircleout==3)
    {
        
        NSLog(@"---int circle out value---");
        in_baseCircleout=0;
        
    }

//--------radio button------
if(btn_baserunner) 
    {
        
    if(btn_baserunner.tag == sender.tag )
        {
            
            NSLog(@"--btn-%i-sender--%i",[btn_baserunner tag],[sender tag]);
            
            if(btn_baserunner.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
            {
                [btn_baserunner setBackgroundImage:[UIImage imageWithContentsOfFile:[[NSBundle mainBundle]pathForResource:@"btn_selected" ofType:@"png"]]forState:UIControlStateNormal];
            }
            
            else 
            {
                
                [btn_baserunner setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
                
            }
            
        }
        
        else
        {
            
            [sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
            [btn_baserunner setBackgroundImage:[UIImage imageNamed:@"radio_unselected"]forState:UIControlStateNormal];
        }
        
    }
    
else
{
[sender setBackgroundImage:[UIImage imageNamed:@"btn_selected"]forState:UIControlStateNormal];
        
    }
    
 btn_baserunner = sender;
    
    if ([sender tag]==1)
    {
         
        [btn_baseRunnerDispalyValue setTitle:@"SB2" forState:UIControlStateNormal];
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            
            [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
            
        }   
        
        [self.vw_Mainbaserunnerview removeFromSuperview];
    }
    
    if ([sender tag]==2)
    {
        
        [btn_baseRunnerDispalyValue setTitle:@"SB3" forState:UIControlStateNormal];
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            
            [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
            
        }   
        
        [self.vw_Mainbaserunnerview removeFromSuperview];
    }
    
    if ([sender tag]==3)
    {
        
        [btn_baseRunnerDispalyValue setTitle:@"SBH" forState:UIControlStateNormal];
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            
            [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
            
        }   
       [self.vw_Mainbaserunnerview removeFromSuperview]; 
        
    }
    
    if ([sender tag]==4)
    {
        
        [btn_baseRunnerDispalyValue setTitle:@"AB" forState:UIControlStateNormal];
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            
            [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
            
        }   
        
       [self.vw_Mainbaserunnerview removeFromSuperview]; 
    }
    
    if ([sender tag]==5)
    {
        
        [btn_baseRunnerDispalyValue setTitle:@"AW" forState:UIControlStateNormal];
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            
            [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
            
        }   
      [self.vw_Mainbaserunnerview removeFromSuperview];  
        
    }
    
if ([sender tag]==6)
{
        
        [btn_baseRunnerDispalyValue setTitle:@"AP" forState:UIControlStateNormal];
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            
            [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
            
        }   
       [self.vw_Mainbaserunnerview removeFromSuperview]; 
        
    }
    
    if ([sender tag]==7)
    {
        
        [btn_baseRunnerDispalyValue setTitle:@"AE" forState:UIControlStateNormal];
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            
            [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
            
        }   
       [self.vw_Mainbaserunnerview removeFromSuperview]; 
        
    }
    
    if ([sender tag]==8)
    {
        in_baseCircleout++;
        NSString *str_basecircleout = [NSString stringWithFormat:@"%i",in_baseCircleout];
        lbl_circleout.text=str_basecircleout;
        [self.scrv_scoreboard addSubview:vw_defensivePositions]; 
         bl_DefensivePosiPopups=FALSE;
        self.vw_defensivePositions.frame = CGRectMake(350, 560, 170, 296);
        
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            in_baseCircleout=0;
            lbl_circleout.text=Nil;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
        }
          
    }
    
if ([sender tag]==9)
{
        in_baseCircleout++;
        NSString *str_basecircleout = [NSString stringWithFormat:@"%i",in_baseCircleout];
        lbl_circleout.text=str_basecircleout;
        
        [self.scrv_scoreboard addSubview:vw_defensivePositions]; 
        bl_DefensivePosiPopups=FALSE;
        self.vw_defensivePositions.frame = CGRectMake(350, 580, 170, 296);
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            in_baseCircleout=0;
            lbl_circleout.text=Nil;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
         
    }
    
    if ([sender tag]==10)
    {
        in_baseCircleout++;
        NSString *str_basecircleout = [NSString stringWithFormat:@"%i",in_baseCircleout];
        lbl_circleout.text=str_basecircleout;

        [self.scrv_scoreboard addSubview:vw_defensivePositions]; 
        bl_DefensivePosiPopups=FALSE;
        self.vw_defensivePositions.frame = CGRectMake(350, 600, 170, 296);
        
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            
            in_baseCircleout=0;
            lbl_circleout.text=Nil;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
        }
    }
    
    if ([sender tag]==11)
    {
        
        in_baseCircleout++;
        NSString *str_basecircleout = [NSString stringWithFormat:@"%i",in_baseCircleout];
        lbl_circleout.text=str_basecircleout;
        
        [self.scrv_scoreboard addSubview:vw_defensivePositions]; 
        bl_DefensivePosiPopups=FALSE;
        self.vw_defensivePositions.frame = CGRectMake(350, 620, 170, 296);
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            in_baseCircleout=0;
            lbl_circleout.text=Nil;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
        }
    }
    
    if ([sender tag]==12)
    {
       lbl_batRuns.text=@"CS";
        [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal]; 
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            lbl_batRuns.text=nil;
            
        }   
        
        [self.vw_Mainbaserunnerview removeFromSuperview];
    
    }
    
if([sender tag]==13)
    {
        lbl_batRuns.text=@"POP";
        [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal]; 
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
           
             lbl_batRuns.text=nil;
        }   
        [self.vw_Mainbaserunnerview removeFromSuperview];  
        
    }
    
    if([sender tag]==14)
    {
        
        lbl_batRuns.text=@"POC";
        [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal]; 
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            
            lbl_batRuns.text=nil;
            
        }   
        [self.vw_Mainbaserunnerview removeFromSuperview];  
        
    }
    
    if([sender tag]==15)
    {

        lbl_batRuns.text=@"Itf";
        [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal]; 
       
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            
             lbl_batRuns.text=nil;
            
        }   
        [self.vw_Mainbaserunnerview removeFromSuperview];  
        
    }
    
if([sender tag]==16)
    {
       
        lbl_batRuns.text =@"Rbp";
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            
             lbl_batRuns.text =nil;
            
        }   
        [self.vw_Mainbaserunnerview removeFromSuperview];  
        
    }
    
if([sender tag]==17)
    {

        lbl_batRuns.text =@"Yes";
        [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal]; 
        
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            
            lbl_batRuns.text =nil;

        }   
        [self.vw_Mainbaserunnerview removeFromSuperview];  
        
    }
    
    if([sender tag]==18)
    {

        lbl_batRuns.text =@"No";
        [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
        
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
           lbl_batRuns.text =nil;
        }   
        [self.vw_Mainbaserunnerview removeFromSuperview];  
        
    }
    
    if([sender tag]==19)
    {
       
        lbl_batRuns.text =@"Yes";
        [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal]; 
        
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
           lbl_batRuns.text =nil;
            
        }   
        [self.vw_Mainbaserunnerview removeFromSuperview];  
        
    }
    
    if([sender tag]==20)
    {

        lbl_batRuns.text =@"No";
        [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal]; 
        
        if(btn_baserunner.currentBackgroundImage==[UIImage imageNamed:@"radio_unselected"])
        {
            
            lbl_batRuns.text =nil;
            
        }   
        [self.vw_Mainbaserunnerview removeFromSuperview];  
        
    }
    
if([sender tag]==21)
    {
          
        type = @"EoiAlert";
        bl_alertPopup=TRUE;
        
        alert1 = [[UIAlertView alloc] 
                  initWithTitle:@"Alert Message!" 
                  message:@"Did This Inning Represent a Shutdown Opportunity For The Pitcher" 
                  delegate:self  
                  cancelButtonTitle:@"No" 
                  otherButtonTitles:@"Yes",nil];
        [alert1 show];
               
        lbl_batRuns.text=nil;
        
        imgv_hitEoi = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Eoi_shape.png"]];
        [lbl_batRuns addSubview:imgv_hitEoi];
        
        if(btn_baserunner.currentBackgroundImage == [UIImage imageNamed:@"radio_unselected"]) 
        {
            [alert1 dismissWithClickedButtonIndex:0 animated:YES];
            
            imgv_hitEoi.image =nil;
            alert1.hidden=YES;
            
        }
        
}
    
    if ([sender tag]==22)
    {
        [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal]; 
        
        imgv_hitEoi.image=nil;
                       
    }
}



-(IBAction)DefensivePositionMethod:(id)sender

{
    
    NSLog(@"--> DefensivePositionMethod: Called! <--");
    
int_asgnButtontagvalue = [sender tag];

    
       [self.scrv_scoreboard addSubview:vw_defensivePositions];
       
    if ([sender tag]==1)
    {
        
    if(bl_DefensivePosiPopups==TRUE)
        {
            self.vw_defensivePositions.frame = CGRectMake(130, 620, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
    }
        
}
    
if([sender tag]==2)
    
    {
        
      if (bl_DefensivePosiPopups==TRUE)
        {
            self.vw_defensivePositions.frame = CGRectMake(250, 620, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }

    }
    
    
    if ([sender tag]==3)
    {
    
        if (bl_DefensivePosiPopups==TRUE)
        {
            self.vw_defensivePositions.frame = CGRectMake(250, 700, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }

    }
    
    if ([sender tag]==4)
    {
        
        if (bl_DefensivePosiPopups==TRUE)
           {
            self.vw_defensivePositions.frame = CGRectMake(330, 700, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        } 
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
        }
        
   }
    
    if ([sender tag]==5)
    {
        
        if (bl_DefensivePosiPopups==TRUE)
         {
            self.vw_defensivePositions.frame = CGRectMake(280, 750, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
    }
        
 }
    
if([sender tag]==6)
{

    if (bl_DefensivePosiPopups==TRUE)
    {
             self.vw_defensivePositions.frame = CGRectMake(330, 750, 170  , 295);
             bl_DefensivePosiPopups=FALSE;
            
        }
     
        else
        {
            
        [self.vw_defensivePositions removeFromSuperview];
         bl_DefensivePosiPopups=TRUE;
            
            
        }
    }
}
-(IBAction)ERAExitMethod:(id)sender
{
if(teamName_lbl.text.length>=1||opponent_lbl.text.length>=1||lbl_datepicker.text.length>=1||catcher_lbl.text.length>=1||pitcher_lbl.text.length>=1)
 {
        type=@"checkData";
        bl_check_data=FALSE;
      
        UIAlertView *alrt_checkdata = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Do you want to save this data" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES ", nil];
        [alrt_checkdata show];
        return;
 }
type=@"ExitApp";
alert1=[[UIAlertView alloc] 
                          initWithTitle:@"Message!" 
                          message:@"Are you sure you want to exit from ERA App" 
                          delegate:self  
                          cancelButtonTitle:@"Cancel" 
                          otherButtonTitles:@"Ok",nil];
    [alert1 show];
    NSLog(@"--> ERAExitMethod: Called! <--");
    
}

-(void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex 
{
    NSLog(@"---button index--%i--",buttonIndex);
if(buttonIndex==0)
{
    if (str_type1==@"EXitapp")
    {
        str_type1=@"changExit";
    }
if (type==@"R/LOption") 
    {
        lbl_RorL.text =@"L";
    }
if(type==@"checkData"&&bl_check_data==FALSE) 
{
    bl_check_data=TRUE;
    
  str_type1=@"EXitapp";
   
    alert1=[[UIAlertView alloc] 
            initWithTitle:@" Message!" 
            message:@"Are you sure you want to exit from ERA App" 
            delegate:self  
            cancelButtonTitle:@"Cancel" 
            otherButtonTitles:@"Ok",nil];
    [alert1 show];
} 
}
else
    
{
    
if(type == @"ExitApp") 
    
{
exit(0); 
}
if(type==@"checkData"&&bl_save_data==FALSE) 
{
[self SaveButtonMethod:nil];
    
}  
if(type==@"EoiAlert")
{
if(bl_alertPopup==TRUE)
{
alert1=[[UIAlertView alloc] 
                      initWithTitle:@"Alert Message!" 
                      message:@"“Was a Shutdown Accomplished?” " 
                      delegate:self  
                      cancelButtonTitle:@"No" 
                      otherButtonTitles:@"Yes",nil];
    [alert1 show];
     bl_alertPopup=FALSE;
    [teamName_lbl setText:nil];
    [opponent_lbl setText:nil];
    [lbl_datepicker setText:nil];
    [catcher_lbl setText:nil];
    //[pitcher_lbl setText:nil];
    [lbl_jerseyNumber setText:nil];
    [lbl_batterName setText:nil];
    [lbl_RorL setText:nil];
    [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
    [btn_loadSenderidbtn setTitle:nil forState:UIControlStateNormal];
    [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
    [btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];
    if (vw_onBaseOptionHitFair.hidden==NO)
    {
        vw_onBaseOptionHitFair.hidden=YES;
    }
}        
}
}
if(buttonIndex==1)
{
if(str_type1==@"EXitapp")
{
exit(0); 
}
if([textAlert.text length]==0)
{
    if(type==@"R/LOption")
{
      lbl_RorL.text=@"R";
}
    if ([type isEqualToString:@"jerseyNumber"])
    {
         lbl_jerseyNumber.text=nil;
    }
    
    if([type isEqualToString:@"BatterName"])
    {
   
    lbl_batterName.text=nil;
        
    }
        NSLog(@"----value stored----");
        [alert dismissWithClickedButtonIndex:0 animated:YES];
    
}
    
else
{
    
if ([type isEqualToString:@"jerseyNumber"])
    {
        
    NSLog(@"jerseyNumber");
        
        if ([self validateTextField:textAlert.text]==NO)
        {
        UIAlertView *alrt_textfield = [[UIAlertView alloc] initWithTitle:@"Alert Message!" message:@"Please Enter Numeric Value" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alrt_textfield show];
            return; 
            }
        
        else
        {
        NSString *str_jersyNum = [NSString stringWithFormat:@"%@",textAlert.text];
            lbl_jerseyNumber.text=str_jersyNum;
            
        }
           
}
        
if([type isEqualToString:@"BatterName"])
    {
            
        NSString *str_BatterName = [NSString stringWithFormat:@"%@",textAlert.text];
        
        lbl_batterName.text=str_BatterName;
            
    }

        
if(type==@"R/LOption")
        {
            
            lbl_RorL.text=@"R";
        }
    
    if([type isEqualToString:@"teamName"])
    {
    
    if(![teamNames_Array containsObject:textAlert.text])
            
            [teamNames_Array addObject:textAlert.text];   
 
       NSLog(@"---lenght-%i-----",[textAlert.text length]);
         
 }
        
if ([type isEqualToString: @"Opponent"])
{
        if(![opponents_Array containsObject:textAlert.text])
            [opponents_Array addObject:textAlert.text];
        
    } 
        
    if(type==@"Pitcher")
    {
        if(![pitchers_Array containsObject:textAlert.text])
            [pitchers_Array addObject:textAlert.text];
    }
        
    if([type isEqualToString:@"Catcher"])
    {
        if(![catchers_Array containsObject:textAlert.text])
            [catchers_Array addObject:textAlert.text];
        
    }

[tblw_alldatatable reloadData];
        
     }
    
  } 
    
}

-(IBAction)OnbasisPopups:(id)sender
{
    
NSLog(@"--> OnbasisPopups: Called! <--");
    
  int_asgnButtontagvalue = [sender tag];

    [self.scrv_scoreboard addSubview:vw_defensivePositions];
    if ([sender tag]==1)
    {
        
        if (bl_DefensivePosiPopups==TRUE)
        {
            self.vw_defensivePositions.frame = CGRectMake(150, 590, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
    }
}
    
if([sender tag]==2)
    {
        
        if (bl_DefensivePosiPopups==TRUE)
        {
            self.vw_defensivePositions.frame = CGRectMake(220, 590, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
        
        
    }
    
    if ([sender tag]==3)
    {
        
        
        if (bl_DefensivePosiPopups==TRUE)
        {
            
            self.vw_defensivePositions.frame = CGRectMake(280, 590, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
        
    }
    
    
if ([sender tag]==4)
    
{
    if (bl_DefensivePosiPopups==TRUE)
        {
            
            self.vw_defensivePositions.frame = CGRectMake(160, 640, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
          
    }

    
    if ([sender tag]==5)
    {
        
        if (bl_DefensivePosiPopups==TRUE)
        {
            
            self.vw_defensivePositions.frame = CGRectMake(220, 640, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
        
    }
    
    if ([sender tag]==6)
    {
       
        if (bl_DefensivePosiPopups==TRUE)
        {
            
            self.vw_defensivePositions.frame = CGRectMake(270, 640, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
        
    }

    if ([sender tag]==7)
    {
        
if (bl_DefensivePosiPopups==TRUE)
        {
            
            self.vw_defensivePositions.frame = CGRectMake(160, 690, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
            
    }

    
    if ([sender tag]==8)
    {
       
        
        if (bl_DefensivePosiPopups==TRUE)
        {
            
            self.vw_defensivePositions.frame = CGRectMake(230, 690, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
        
}

    if ([sender tag]==9)
    {
        
    if(bl_DefensivePosiPopups==TRUE)
        {
            
            self.vw_defensivePositions.frame = CGRectMake(270, 690, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
    else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
            
    }
    
    
    if ([sender tag]==10)
    {
          
        if (bl_DefensivePosiPopups==TRUE)
        {
            
            self.vw_defensivePositions.frame = CGRectMake(160, 735, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
        
    }
    
    
    if ([sender tag]==11)
    {
          
        if (bl_DefensivePosiPopups==TRUE)
        {
            
            self.vw_defensivePositions.frame = CGRectMake(220, 735, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
    }
    
if ([sender tag]==12)
   
{
    if (bl_DefensivePosiPopups==TRUE)
        {
            
            self.vw_defensivePositions.frame = CGRectMake(270, 735, 170  , 295);
            bl_DefensivePosiPopups=FALSE;
            
        }
    
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
   
        }

    }
    
}

-(BOOL)validateTextField:(NSString *)candidate 
{
    
    NSLog(@"validateTextField: Called");
    NSString *emailRegex=@"^(?:|0|[0-9]\\d*)(?:\\.\\d*)?$"; 
    NSLog(@"-all string value %@-",emailRegex);
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex]; 
    return [emailTest evaluateWithObject:candidate];
    
     
}

//------table view delegate---------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
  
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    if (type==@"teamName") 
    {
        
        [title_Label setText:@"Team Names"];
        [title_Label setTextAlignment:UITextAlignmentCenter];
        [title_Label setTextAlignment:UITextAlignmentCenter];  
        [title_Label setTextColor:[UIColor whiteColor]];
        [add_Button setHidden:NO];
       
        
        return [teamNames_Array count];
        
        
    }
    
else if (type==@"Opponent") {
        
        [title_Label setText:@"Opponents"];
        [title_Label setTextColor:[UIColor whiteColor]];
        [add_Button setHidden:NO];
        [title_Label setTextAlignment:UITextAlignmentCenter];
        [title_Label setTextColor:[UIColor whiteColor]];
        return [opponents_Array count];
        
    }
    
    
    else if(type==@"Pitcher")
        
    {
        
        [title_Label setText:@"Pitchers"];
        [title_Label setTextColor:[UIColor whiteColor]];
        [title_Label setTextColor:[UIColor whiteColor]];
        [title_Label setTextAlignment:UITextAlignmentCenter];
        [add_Button setHidden:NO];
        return [pitchers_Array count];
        
    }
    
    
    else if(type==@"Catcher")
        
    {
        
        [title_Label setText:@"Catchers"];
        [title_Label setTextColor:[UIColor whiteColor]];
        [title_Label setTextColor:[UIColor whiteColor]];
        [title_Label setTextAlignment:UITextAlignmentCenter];
        [add_Button setHidden:NO];
        return [catchers_Array count];
        
        
    }
else
    {
                
         [title_Label setText:@"Defensive Positions"];
         [title_Label setTextColor:[UIColor whiteColor]];
         [title_Label setTextColor:[UIColor whiteColor]];
         [title_Label setTextAlignment:UITextAlignmentCenter];
         [add_Button setHidden:YES];
        
        return [array_defnsiveposidata count];
        
    }
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier] ;
	//if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //	}
    
    UILabel *defensiveposn = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    
    if (type==@"teamName") 
    {
        
        
        defensiveposn.text=[teamNames_Array objectAtIndex:indexPath.row]; 
        
    }
    
    
    else if (type==@"Opponent") {
        
        defensiveposn.text=[opponents_Array objectAtIndex:indexPath.row]; 
        
    }
    
    
    else if(type==@"Pitcher")
        
    {
        
        defensiveposn.text=[pitchers_Array objectAtIndex:indexPath.row];
        
    }
    
    
    else if(type==@"Catcher")
        
    {
        
        defensiveposn.text=[catchers_Array objectAtIndex:indexPath.row];
        
    }
    
    
    else  {
        
        defensiveposn.text = [NSString stringWithFormat:[array_defnsiveposidata objectAtIndex:indexPath.row]]; 
        
    }
      
    defensiveposn.textAlignment = UITextAlignmentLeft;
	defensiveposn.textColor = [UIColor blackColor];
    defensiveposn.backgroundColor = [UIColor clearColor];
	defensiveposn.font = [UIFont fontWithName:@"Arial" size:12];
    [cell addSubview:defensiveposn];
    
    cell.selectionStyle=UITableViewCellSelectionStyleBlue;
    tableView.separatorColor=[UIColor clearColor];
       
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  
    /*
    if (type==@"teamName"|| type==@"Opponent" || type==@"Umpire" || type==@"Catcher" ) 
     {
        
        [lbl_jerseyNumber setText:nil];
        [lbl_batterName setText:nil];
        [lbl_RorL setText:nil];
        
    }
     */
    
    
    
    if (type==@"teamName")
    {
        
        teamName_lbl.text=[teamNames_Array objectAtIndex:indexPath.row];
        
        [self.vw_defensivePositions removeFromSuperview];
          bl_DefensivePosiPopups=TRUE;
        teamName_Scoreboard.text=[teamNames_Array objectAtIndex:indexPath.row];
       
        [imgv_hitEoi removeFromSuperview];
    }
    
    
    else if (type==@"Opponent") 
    {
             
        NSString *str=[NSString stringWithFormat:@"%@",[opponents_Array objectAtIndex:indexPath.row]];
        
        opponent_lbl.text=str;
        [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;
        opponent_Scoreboard.text=[opponents_Array objectAtIndex:indexPath.row];
    }
    
        
    
    else if (type==@"Catcher")
        
    {
        
        catcher_lbl.text=[catchers_Array objectAtIndex:indexPath.row];
        
        [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;              
        
    }
    
    else if (type==@"Pitcher")
        
    {
        
        pitcher_lbl.text=[pitchers_Array objectAtIndex:indexPath.row];
        
        [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;              
        
    }

    
    else
    {
    
    // Out Come Of At Bat ball hit position.....
    if(outComeAtbat_Tag==6)
    {
        
        switch (indexPath.row)
        {
                
            case 0:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Bpitcher.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 1:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"Bcatcher.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 2:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"B1baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            
            }
                
            case 3:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"B2baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 4:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"B3baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 5:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Bshprtstop.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                break;
            }
                
            case 6:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Bleftfield.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                break;
            }
                
            case 7:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"BcenterField.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                break;
            }
                
            case 8:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"Bleftfield.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                break;
            }
                
                default:
                break;
                
        }
        
        
    }
    
    
    if (outComeAtbat_Tag==7)
    {
        
        switch (indexPath.row)
        {
                
            case 0:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Bfpitcher.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                break;
            }
                
            case 1:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"Bfcatcher.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 2:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"Bf1baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 3:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"Bf2baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 4:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"Bf3baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 5:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Bfshortstop.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 6:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Bfleftfield.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                break;
            }
                
            case 7:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"Bfcenterfield.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                break;
            }
                
            case 8:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"Bfrightfield.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                break;
            }
                
            default:
                break;
                
        }

        
           }
    
if(outComeAtbat_Tag==8) 
 {
    NSString *str=[NSString stringWithFormat:@"GB-%d",indexPath.row+1];
        [lbl_batRuns setText:str];
        
       
        switch (indexPath.row)
        {
                
            case 0:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Gpitcher.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                break;
            }
                
                case 1:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"Gcatcher.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;

                break;
            }
                
            case 2:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"G1baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;

                 break;
            }
                
            case 3:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"G2baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;

                break;
            }
                
            case 4:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"G3baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;

                break;
            }
                
            case 5:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Gshortstop.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;

                break;
            }
                
            case 6:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Gleftfield.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                 bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;
                break;
            }
                
            case 7:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"GcenterField.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                 vw_onBaseOptionHitFair.hidden=YES;

                break;
            }
                
            case 8:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"GrightField.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                vw_onBaseOptionHitFair.hidden=YES;

                break;
            }
                
            default:
                break;
        }
        
        
        //[btn_loadSenderidbtn setTitle:nil forState:UIControlStateNormal];
        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        //[btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
}
    
    if (outComeAtbat_Tag==9)
        
    {
        
        NSString *str=[NSString stringWithFormat:@"DP-%d",indexPath.row+1];
        
        [lbl_batRuns setText:str];
        
        switch(indexPath.row)
        {
                
            case 0:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Gpitcher.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                vw_onBaseOptionHitFair.hidden=YES;
                break;
            }
                
            case 1:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"Gcatcher.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 2:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"G1baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 3:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"G2baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 4:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"G3baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 5:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Gshortstop.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 6:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Gleftfield.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                vw_onBaseOptionHitFair.hidden=YES;
                break;
            }
                
            case 7:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"GcenterField.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE; 
                vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 8:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"GrightField.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE; 
                vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
                default:
                break;
              
        }
        
               
        //[btn_loadSenderidbtn setTitle:nil forState:UIControlStateNormal];
        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        //[btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
        
    }
    
    if (outComeAtbat_Tag==10) 
    {
        
        NSString *str=[NSString stringWithFormat:@"TP-%d",indexPath.row+1];
        [lbl_batRuns setText:str];
        
               
        switch (indexPath.row)
        {
        case 0:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Gpitcher.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE; 
                vw_onBaseOptionHitFair.hidden=YES;
                break;
            }
                
            case 1:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"Gcatcher.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE; 
                vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 2:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"G1baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE; 
                vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 3:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"G2baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE; 
                vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 4:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"G3baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE; 
                vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 5:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Gshortstop.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE; 
                vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 6:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Gleftfield.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE; 
                vw_onBaseOptionHitFair.hidden=YES;
                
                
                break;
            }
                
            case 7:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"GcenterField.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE; 
                vw_onBaseOptionHitFair.hidden=YES;
                
                break;
            }
                
            case 8:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"GrightField.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE; 
                vw_onBaseOptionHitFair.hidden=YES;
                break;
            }
                
            default:
                break;

        }
     
        
        
        
        //[btn_loadSenderidbtn setTitle:nil forState:UIControlStateNormal];
        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        //[btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];

    }
    
if (outComeAtbat_Tag==11) 

{
    NSString *str=[NSString stringWithFormat:@"SacB-%d",indexPath.row+1];
    [lbl_batRuns setText:str];
    
   

    switch (indexPath.row)
     {
            
        case 0:
        {
              vw_onBaseOptionHitFair.hidden=YES;           
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            break;
        }
            
        case 1:
        {
           
             vw_onBaseOptionHitFair.hidden=YES;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }
            
        case 2:
        {
            
            
             vw_onBaseOptionHitFair.hidden=YES;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }
            
        case 3:
        {
            
           
             vw_onBaseOptionHitFair.hidden=YES;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }
            
        case 4:
        {
             vw_onBaseOptionHitFair.hidden=YES;
            
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }
            
        case 5:
        {
                      
            vw_onBaseOptionHitFair.hidden=YES; 
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }
            
        case 6:
        {
            vw_onBaseOptionHitFair.hidden=YES;
            
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }
            
        case 7:
        {
            
            vw_onBaseOptionHitFair.hidden=YES;         
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }
            
        case 8:
        {
            vw_onBaseOptionHitFair.hidden=YES;        
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            break;
        }
            
        default:
            break;
            
}
        //[btn_loadSenderidbtn setTitle:nil forState:UIControlStateNormal];
    [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
    //[btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];
    [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];

        
} 
        
if(outComeAtbat_Tag==12) 
        
    {
          
        NSString *str=[NSString stringWithFormat:@"FB-%d",indexPath.row+1];
        [lbl_batRuns setText:str];
        switch (indexPath.row)
        {
                
            case 0:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Fpitcher.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                 vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 1:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"Fcather.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                 vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 2:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"F1baseman.png"];
                 vw_onBaseOptionHitFair.hidden=YES;
                imgv_Fielderchoice.image = img_fchoice;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 3:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"F2baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                 vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 4:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"F3baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                 vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 5:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Fshortstop.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                 vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 6:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Fleftfield.png"];
                 vw_onBaseOptionHitFair.hidden=YES;
                imgv_Fielderchoice.image = img_fchoice;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 7:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"Fcenterfield.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                 vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 8:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"Frightfield.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                 vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            default:
                break;
        }
        
        
        
        //[btn_loadSenderidbtn setTitle:nil forState:UIControlStateNormal];
        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        //[btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];


    } 
    
    if (outComeAtbat_Tag==13) 
        
    {
        
        NSString *str=[NSString stringWithFormat:@"SacF-%d",indexPath.row+1];
        [lbl_batRuns setText:str];
        
       

        
        switch (indexPath.row)
        {
            case 0:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Spitcher.png"];
                imgv_Fielderchoice.image = img_fchoice;
                vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 1:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Scatcher.png"];
                imgv_Fielderchoice.image = img_fchoice;
                 vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 2:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"S1baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                 vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 3:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"S2baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                 vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 4:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"S3baseman.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                 vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 5:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"Sshortstop.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                 vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 6:
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"SleftField.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                 vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 7:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"ScenterField.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                 vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            case 8:
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"Srightfield.png"];
                
                imgv_Fielderchoice.image = img_fchoice;
                 vw_onBaseOptionHitFair.hidden=YES;
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                
                break;
            }
                
            default:
                break;
        }
        
        
        
        //[btn_loadSenderidbtn setTitle:nil forState:UIControlStateNormal];
        [btn_loadSenderidbtn setBackgroundImage:nil forState:UIControlStateNormal];
        //[btn_loadstrikesenderid setTitle:nil forState:UIControlStateNormal];
        [btn_loadstrikesenderid setBackgroundImage:nil forState:UIControlStateNormal];
    
    }
        
    if (outComeAtbat_Tag==14)
    {
        
    NSString *str=[NSString stringWithFormat:@"OD-%d",indexPath.row+1];
    [lbl_batRuns setText:str];
        
        
    }
        
if(outComeAtbat_Tag==18)
    
    {
        
switch (indexPath.row)
        
    {
            
    case 0:
        {
            NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
            lbl_hitfairLineDrive.text =strtext;
              [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
            break;
         }
            
    case 1:
        {
            NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
            lbl_hitfairLineDrive.text =strtext;
              [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
            break; 
        }
            
    case 2:
        {
         
            NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
            lbl_hitfairLineDrive.text =strtext;
            [self.vw_defensivePositions removeFromSuperview];        
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }
                
    case 3:
        {
            NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
            lbl_hitfairLineDrive.text =strtext;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }

    case 4:
    {
         NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
        lbl_hitfairLineDrive.text =strtext;
        [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;
        
        break;
    }

    case 5:
    {
        NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
        lbl_hitfairLineDrive.text =strtext;
        [self.vw_defensivePositions removeFromSuperview];        
        bl_DefensivePosiPopups=TRUE;
        
        break;
    }

    case 6:
    {
        NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
        lbl_hitfairLineDrive.text =strtext;
        [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;
        
        break;
    }

    case 7:
    {
        NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
        lbl_hitfairLineDrive.text =strtext;
        [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;
        
        break;
    }

    case 8:
    {
                
        NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
        lbl_hitfairLineDrive.text =strtext;
        [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;
        
        break;
    }
                
            default:
                break;
    }
        
}
        
        
if(outComeAtbat_Tag==19)

{
    
switch (indexPath.row)
    {
        case 0:
        {
            NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
            lbl_hitfairLineDriveto.text =strtext;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
             break;
        }
            
        case 1:
        {
            NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
            lbl_hitfairLineDriveto.text =strtext;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
            break; 
        }
            
        case 2:
        {
            NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
            lbl_hitfairLineDriveto.text =strtext;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }
            
        case 3:
        {
            NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
            lbl_hitfairLineDriveto.text =strtext;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }
            
        case 4:
        {
            NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
            lbl_hitfairLineDriveto.text =strtext;
            [self.vw_defensivePositions removeFromSuperview];            
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }
            
        case 5:
        {
            NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
            lbl_hitfairLineDriveto.text =strtext;
            [self.vw_defensivePositions removeFromSuperview]; 
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }
            
        case 6:
        {
             NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
            lbl_hitfairLineDriveto.text =strtext;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }
            
        case 7:
        {
            NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
            lbl_hitfairLineDriveto.text =strtext;
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }
            
        case 8:
        {
            
            NSString *strtext = [NSString stringWithFormat:@"%i",indexPath.row+1];
            lbl_hitfairLineDriveto.text =strtext;
            [self.vw_defensivePositions removeFromSuperview];            
            bl_DefensivePosiPopups=TRUE;
            
            break;
        }
            
        default:
            break;
    }
            
}
        
//-------BaseRunner defensive position functionality--------        
        
if (inTag_baseRunnerDEfensivepos==8)
{
        
    NSString *str=[NSString stringWithFormat:@"GB-%d",indexPath.row+1];
        [lbl_batRuns setText:str];
        [lbl_batRuns setTextColor:[UIColor whiteColor]];
    
    switch (indexPath.row)
            {
                    
            [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];     
            
                case 0:
                {
                    UIImage *img_fchoice = [UIImage imageNamed:@"Gpitcher.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [self.vw_Mainbaserunnerview removeFromSuperview];
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    break;
                }
                    
                case 1:
                {
                    
                    UIImage *img_fchoice = [UIImage imageNamed:@"Gcatcher.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [self.vw_Mainbaserunnerview removeFromSuperview];
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 2:
                {
                    
                    UIImage *img_fchoice = [UIImage imageNamed:@"G1baseman.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [self.vw_Mainbaserunnerview removeFromSuperview];
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 3:
                {
                    
                    UIImage *img_fchoice = [UIImage imageNamed:@"G2baseman.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [self.vw_Mainbaserunnerview removeFromSuperview];
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 4:
                {
                    
                    UIImage *img_fchoice = [UIImage imageNamed:@"G3baseman.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [self.vw_Mainbaserunnerview removeFromSuperview];
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 5:
                {
                    UIImage *img_fchoice = [UIImage imageNamed:@"Gshortstop.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [self.vw_Mainbaserunnerview removeFromSuperview];
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 6:
                {
                    UIImage *img_fchoice = [UIImage imageNamed:@"Gleftfield.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [self.vw_Mainbaserunnerview removeFromSuperview];
                    
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    break;
                }
                    
                case 7:
                {
                    
                    UIImage *img_fchoice = [UIImage imageNamed:@"GcenterField.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [self.vw_Mainbaserunnerview removeFromSuperview];
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 8:
                {
                    
                    UIImage *img_fchoice = [UIImage imageNamed:@"GrightField.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [self.vw_Mainbaserunnerview removeFromSuperview];
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                default:
                    break;
            }
        }
        
if(inTag_baseRunnerDEfensivepos==9)
    {
        
        NSString *str=[NSString stringWithFormat:@"UA-%d",indexPath.row+1];
        [lbl_batRuns setText:str];
        [lbl_batRuns setTextColor:[UIColor whiteColor]];
        
        switch (indexPath.row)
        {
                
            case 0:
                {
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                [self.vw_Mainbaserunnerview removeFromSuperview];
                [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                break;
            }
                
            case 1:
            {
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                [self.vw_Mainbaserunnerview removeFromSuperview];
                [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                
                break; 
            }
                
            case 2:
            {
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                [self.vw_Mainbaserunnerview removeFromSuperview];
                [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                
                break;
            }
                
            case 3:
            {
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                [self.vw_Mainbaserunnerview removeFromSuperview];
                [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                
                break;
            }
                
            case 4:
            {
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                [self.vw_Mainbaserunnerview removeFromSuperview];
                [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                
                break;
            }
                
            case 5:
            {
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                [self.vw_Mainbaserunnerview removeFromSuperview]; 
                [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                
                break;
            }
                
            case 6:
            {
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                [self.vw_Mainbaserunnerview removeFromSuperview];
                [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                
                break;
            }
                
            case 7:
            {
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                [self.vw_Mainbaserunnerview removeFromSuperview];
                [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                
                break;
            }
                
            case 8:
            {
                [self.vw_Mainbaserunnerview removeFromSuperview];
                [self.vw_defensivePositions removeFromSuperview];
                bl_DefensivePosiPopups=TRUE;
                [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                
                break;
            }
                
             default:
                break;
        }
  
            
}
        
if (inTag_baseRunnerDEfensivepos==10)
        {
            
            NSString *str=[NSString stringWithFormat:@"DP-%d",indexPath.row+1];
            
            [lbl_batRuns setText:str];
            
            switch (indexPath.row)
            {
                    
                case 0:
                {
                    UIImage *img_fchoice = [UIImage imageNamed:@"Gpitcher.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    break;
                }
                    
                case 1:
                {
                    
                    UIImage *img_fchoice = [UIImage imageNamed:@"Gcatcher.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 2:
                {
                    
                    UIImage *img_fchoice = [UIImage imageNamed:@"G1baseman.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 3:
                {
                    
                    UIImage *img_fchoice = [UIImage imageNamed:@"G2baseman.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 4:
                {
                    
                    UIImage *img_fchoice = [UIImage imageNamed:@"G3baseman.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 5:
                {
                    UIImage *img_fchoice = [UIImage imageNamed:@"Gshortstop.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 6:
                {
                    UIImage *img_fchoice = [UIImage imageNamed:@"Gleftfield.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    break;
                }
                    
                case 7:
                {
                    UIImage *img_fchoice = [UIImage imageNamed:@"GcenterField.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 8:
                {
                    UIImage *img_fchoice = [UIImage imageNamed:@"GrightField.png"];
                    
                    imgv_Fielderchoice.image = img_fchoice;
                    
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                default:
                    break;
            }
            
        }
        
        if (inTag_baseRunnerDEfensivepos==11)
        {
            NSString *str=[NSString stringWithFormat:@"F-%d",indexPath.row+1];
            [lbl_batRuns setText:str];
            
            switch (indexPath.row)
            {
                    
                case 0:
                {
                    UIImage *img_fchoice = [UIImage imageNamed:@"Fpitcher.png"];
                    imgv_Fielderchoice.image = img_fchoice;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 1:
                {
                    UIImage *img_fchoice = [UIImage imageNamed:@"Fcather.png"];
                    imgv_Fielderchoice.image = img_fchoice;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 2:
                {
                    UIImage *img_fchoice = [UIImage imageNamed:@"F1baseman.png"];
                    vw_onBaseOptionHitFair.hidden=YES;
                    imgv_Fielderchoice.image = img_fchoice;
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 3:
                {
                    UIImage *img_fchoice = [UIImage imageNamed:@"F2baseman.png"];
                    imgv_Fielderchoice.image = img_fchoice;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 4:
                {
                    UIImage *img_fchoice = [UIImage imageNamed:@"F3baseman.png"];
                    imgv_Fielderchoice.image = img_fchoice;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 5:
                {
                    UIImage *img_fchoice = [UIImage imageNamed:@"Fshortstop.png"];
                    imgv_Fielderchoice.image = img_fchoice;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 6:
                {
                    UIImage *img_fchoice = [UIImage imageNamed:@"Fleftfield.png"];
                    vw_onBaseOptionHitFair.hidden=YES;
                    imgv_Fielderchoice.image = img_fchoice;
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 7:
                {
                    
                    UIImage *img_fchoice = [UIImage imageNamed:@"Fcenterfield.png"];
                    imgv_Fielderchoice.image = img_fchoice;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                case 8:
                {
                    
                    UIImage *img_fchoice = [UIImage imageNamed:@"Frightfield.png"];
                    imgv_Fielderchoice.image = img_fchoice;
                    vw_onBaseOptionHitFair.hidden=YES;
                    [self.vw_defensivePositions removeFromSuperview];
                    bl_DefensivePosiPopups=TRUE;
                    [btn_baseRunnerDispalyValue setTitle:nil forState:UIControlStateNormal];
                    
                    break;
                }
                    
                default:
                    break;
            }
            
        }
        
        
if(int_asgnButtontagvalue==1)
   
{
        
 txtf_gb1.text = [NSString stringWithFormat:@"%i",indexPath.row+1];
 lbl_onbasepopups.text = [NSString stringWithFormat:@"%i",indexPath.row+1];
                
        NSString *str=[NSString stringWithFormat:@"GB-%d",indexPath.row+1];
        
        [lbl_batRuns setText:str];

                
        
        NSLog(@"---label value-%@",lbl_batRuns.text );
        
}
    if(int_asgnButtontagvalue==2)
    {
          txtf_gb2.text = [NSString stringWithFormat:@"%i",indexPath.row+1];
        lbl_onbasepopups1.text= [NSString stringWithFormat:@"%i",indexPath.row+1];
        
    }
    
    if(int_asgnButtontagvalue==3)
    {
        txtf_db1.text = [NSString stringWithFormat:@"%i",indexPath.row+1];
        lbl_onbasepopups3.text= [NSString stringWithFormat:@"%i",indexPath.row+1];
    }
    
    if(int_asgnButtontagvalue==4)
    {
        
        lbl_onbasepopups3.text= [NSString stringWithFormat:@"%i",indexPath.row+1];
        txtf_db2.text = [NSString stringWithFormat:@"%i",indexPath.row+1];
    }
        
    if(int_asgnButtontagvalue==5)
    
    {
        txtf_agb1.text = [NSString stringWithFormat:@"%i",indexPath.row+1];
    }
    
    if(int_asgnButtontagvalue==6)
    
    {
        txtf_agb2.text = [NSString stringWithFormat:@"%i",indexPath.row+1];
    }
        
 }
    
}


// LocationOfHitView Method ...

-(IBAction)locationOfHitView:(id)sender

{

    NSLog(@"--> locationOfHitView: Called <--");
    switch ([sender tag])
    {
       case 1:
        {
            
            if (outComeAtbat_Tag==5)
            
            {
                imgv_Fielderchoice.image=nil;
                vw_LocationOfHitView.hidden=YES;
                vw_onBaseOptionHitFair.hidden=YES;

            }
            
            else
            {
            
        UIImage *img_fchoice = [UIImage imageNamed:@"middleField.png"];
            
            imgv_Fielderchoice.image = img_fchoice;
       
            vw_LocationOfHitView.hidden=YES;
                vw_onBaseOptionHitFair.hidden=YES;
            
            }
            
            break;
        }
       
        case 2:
        {
            if (outComeAtbat_Tag==5)
            
            {
                imgv_Fielderchoice.image=nil;
                vw_LocationOfHitView.hidden=YES;
                vw_onBaseOptionHitFair.hidden=YES;

            }
            
            else
            {
                
            UIImage *img_fchoice = [UIImage imageNamed:@"1base.png"];
            imgv_Fielderchoice.image = img_fchoice;
            
            vw_LocationOfHitView.hidden=YES;
                vw_onBaseOptionHitFair.hidden=YES;

            }
            
            break;
        }
            
        case 3:
        {
            
            if (outComeAtbat_Tag==5)
            {
                imgv_Fielderchoice.image=nil;
                vw_LocationOfHitView.hidden=YES;
                vw_onBaseOptionHitFair.hidden=YES;

            }
            
            else
            {
            
                UIImage *img_fchoice = [UIImage imageNamed:@"2base.png"];
                imgv_Fielderchoice.image = img_fchoice; 
                vw_LocationOfHitView.hidden=YES;
                vw_onBaseOptionHitFair.hidden=YES;
            
            }
            
            break;
        }
            
        case 4:
        {
            
            if (outComeAtbat_Tag==5)
            {
                imgv_Fielderchoice.image=nil;
                vw_LocationOfHitView.hidden=YES;
                vw_onBaseOptionHitFair.hidden=YES;

            }
            
            else
            {
            
                UIImage *img_fchoice = [UIImage imageNamed:@"3base.png"];
            
                imgv_Fielderchoice.image = img_fchoice;
                vw_onBaseOptionHitFair.hidden=YES;

                vw_LocationOfHitView.hidden=YES;
            }
            
            break;
        }   
       
        case 5:
        {
            if (outComeAtbat_Tag==5)
           
            {
                imgv_Fielderchoice.image=nil;
                vw_LocationOfHitView.hidden=YES;
                vw_onBaseOptionHitFair.hidden=YES;
            }
            
            else
            {
            
                UIImage *img_fchoice = [UIImage imageNamed:@"shortstop.png"];
            
                imgv_Fielderchoice.image = img_fchoice; 
           
                vw_LocationOfHitView.hidden=YES;
                vw_onBaseOptionHitFair.hidden=YES;
            
            }
            
            break;
        }
            
        case 6:
        {
            
       if (outComeAtbat_Tag == 5) 
       {
                UIImage *img_fchoice = [UIImage imageNamed:@"HleftField.png"];
                imgv_Fielderchoice.image = img_fchoice; 
                
                vw_LocationOfHitView.hidden=YES;  
           vw_onBaseOptionHitFair.hidden=YES;

            }
            
            else
            {
                
                UIImage *img_fchoice = [UIImage imageNamed:@"left.png"];
                imgv_Fielderchoice.image = img_fchoice; 
                
                vw_LocationOfHitView.hidden=YES; 
                vw_onBaseOptionHitFair.hidden=YES;
                
            }
                       break;
        }
            
        case 7:
        {
            
            if (outComeAtbat_Tag==5)
            
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"HcenterField.png"];
                imgv_Fielderchoice.image = img_fchoice; 
                vw_onBaseOptionHitFair.hidden=YES;
                
                vw_LocationOfHitView.hidden=YES; 
            }
            
        else
           {
                UIImage *img_fchoice = [UIImage imageNamed:@"center.png"];
                imgv_Fielderchoice.image = img_fchoice; 
                vw_onBaseOptionHitFair.hidden=YES;
                
                vw_LocationOfHitView.hidden=YES; 
            }
            
            break;
        } 
            
        case 8:
        {
            
            if (outComeAtbat_Tag==5)
            {
                UIImage *img_fchoice = [UIImage imageNamed:@"HrightField.png"];
                imgv_Fielderchoice.image = img_fchoice; 
                vw_onBaseOptionHitFair.hidden=YES;
                vw_LocationOfHitView.hidden=YES;
            }
            
          else
          {
                UIImage *img_fchoice = [UIImage imageNamed:@"right.png"];
                imgv_Fielderchoice.image = img_fchoice; 
                vw_onBaseOptionHitFair.hidden=YES;
                
                vw_LocationOfHitView.hidden=YES;
            }
            break;
            
        }
        default:
            break;
    }
    
}


-(IBAction)ScoreBoardMethod:(id)sender

{
    NSLog(@"---> ScoreBoardMethod: called <---");
    
    [self.vw_defensivePositions removeFromSuperview];
    bl_DefensivePosiPopups=TRUE;
    nvgn_datepicker.hidden=YES;
    date_picker.hidden=YES;
    vw_BallPopMainMenu.hidden=YES;
    vw_BallPopUpSubMenu.hidden=YES;
    vw_onBaseOptionHitFair.hidden=YES;
    vw_OnBaseview.hidden=YES;
    vw_StrikeView.hidden=YES;
    vw_typeOFpitchselect.hidden=YES;
    vw_LocationOfHitView.hidden=YES;
    

if ([sender tag]==1)
{
     
    str_TeamName = @"Teamname";
        
        if (bl_DefensivePosiPopups==TRUE)
        {
            [self.scrv_scoreboard addSubview:vw_defensivePositions];
            self.vw_defensivePositions.frame = CGRectMake(140, 95, 170  , 400);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
        }

    }
    
    
    if ([sender tag]==2)
    {
        
        str_OpponentTeam =@"OppositTeam";
        
        if (bl_DefensivePosiPopups==TRUE)
        {
            [self.scrv_scoreboard addSubview:vw_defensivePositions];
            self.vw_defensivePositions.frame = CGRectMake(140, 125, 170  , 400);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
        
    }
    
    if ([sender tag]==3)
    {
        str_Catcher = @"Catcher";
        
        if (bl_DefensivePosiPopups ==TRUE)
        {
            [self.scrv_scoreboard addSubview:vw_defensivePositions];
            self.vw_defensivePositions.frame = CGRectMake(490, 105, 170  , 400);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
        }
        
    }
    
    if ([sender tag]==4)
    {
        str_Umpire  = @"Umpire";
        
        if (bl_DefensivePosiPopups ==TRUE)
        {
            [self.scrv_scoreboard addSubview:vw_defensivePositions];
            self.vw_defensivePositions.frame = CGRectMake(490, 130, 170  , 400);
            bl_DefensivePosiPopups=FALSE;
            
        }
        
        else
        {
            [self.vw_defensivePositions removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
   
        }
    
    }

}


//  Team Name Button Action.....

-(IBAction)teamName_ButtonAction: (id) sender

{
    NSLog(@"-->teamName_ButtonAction: Method Called<--");
    
    type=@"teamName";
    [tblw_alldatatable reloadData];
    [vw_defensivePositions setFrame:CGRectMake(167, 102, 170, 166)];
    
    nvgn_datepicker.hidden=YES;
    date_picker.hidden=YES;
    vw_BallPopMainMenu.hidden=YES;
    vw_BallPopUpSubMenu.hidden=YES;
    vw_onBaseOptionHitFair.hidden=YES;
    vw_OnBaseview.hidden=YES;
    vw_StrikeView.hidden=YES;
    vw_typeOFpitchselect.hidden=YES;
    vw_LocationOfHitView.hidden=YES;

if(bl_DefensivePosiPopups ==TRUE)
{
    
[self.scrv_scoreboard addSubview:vw_defensivePositions];
        
        bl_DefensivePosiPopups=FALSE;
    }

else
{

    [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;

   }

}

//Game Opponent Button Action ......

-(IBAction)opponent_ButtonAction:(id) sender

{  
    NSLog(@"--> opponent_ButtonAction: Method Called -->");  
   
    type=@"Opponent";
    
    [tblw_alldatatable reloadData];
    
    [vw_defensivePositions setFrame:CGRectMake(167, 130, 170, 166)];
    
    nvgn_datepicker.hidden=YES;
    date_picker.hidden=YES;
    vw_BallPopMainMenu.hidden=YES;
    vw_BallPopUpSubMenu.hidden=YES;
    vw_onBaseOptionHitFair.hidden=YES;
    vw_OnBaseview.hidden=YES;
    vw_StrikeView.hidden=YES;
    vw_typeOFpitchselect.hidden=YES;
    vw_LocationOfHitView.hidden=YES;
    
    if(bl_DefensivePosiPopups ==TRUE)
    {
        [self.scrv_scoreboard addSubview:vw_defensivePositions];
      
        bl_DefensivePosiPopups=FALSE;
    }
    
    else
    {
        
        [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;
        
    }
    
}


// Umpire_Button Action......

-(IBAction) umpire_ButtonAction:(id) sender

{            
    NSLog(@"--> umpire_ButtonAction: Called <--");

    type=@"Umpire";
      
    [tblw_alldatatable reloadData];

    [vw_defensivePositions setFrame:CGRectMake(522, 140, 170, 166)];
       
    nvgn_datepicker.hidden=YES;
    date_picker.hidden=YES;
    vw_BallPopMainMenu.hidden=YES;
    vw_BallPopUpSubMenu.hidden=YES;
    vw_onBaseOptionHitFair.hidden=YES;
    vw_OnBaseview.hidden=YES;
    vw_StrikeView.hidden=YES;
    vw_typeOFpitchselect.hidden=YES;
    vw_LocationOfHitView.hidden=YES;
    
    if(bl_DefensivePosiPopups ==TRUE)
    {
        [self.scrv_scoreboard addSubview:vw_defensivePositions];
      
        bl_DefensivePosiPopups=FALSE;
    }
    
    else
    {
        
        [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;
        
    }
    
}


//catcher_Button Action ......
-(IBAction) catcher_ButtonAction:(id) sender
{
    
    NSLog(@"--> catcher_ButtonAction: Called <--");

    type=@"Catcher";
    
    [tblw_alldatatable reloadData];
    
    [vw_defensivePositions setFrame:CGRectMake(522, 117, 170, 166)];
    
    nvgn_datepicker.hidden=YES;
    date_picker.hidden=YES;
    vw_BallPopMainMenu.hidden=YES;
    vw_BallPopUpSubMenu.hidden=YES;
    vw_onBaseOptionHitFair.hidden=YES;
    vw_OnBaseview.hidden=YES;
    vw_StrikeView.hidden=YES;
    vw_typeOFpitchselect.hidden=YES;
    vw_LocationOfHitView.hidden=YES;
    
    if(bl_DefensivePosiPopups ==TRUE)
    {
        [self.scrv_scoreboard addSubview:vw_defensivePositions];
        bl_DefensivePosiPopups=FALSE;
    }
    
    else
    {   
        [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;
        
    }
    
       
}

//pitcher_Button Action ......
-(IBAction) pitcher_ButtonAction:(id) sender
{
    
    NSLog(@"--> catcher_ButtonAction: Called <--");
    
    type=@"Pitcher";
    
    [tblw_alldatatable reloadData];
    
    [vw_defensivePositions setFrame:CGRectMake(522, 117, 170, 166)];
    
    nvgn_datepicker.hidden=YES;
    date_picker.hidden=YES;
    vw_BallPopMainMenu.hidden=YES;
    vw_BallPopUpSubMenu.hidden=YES;
    vw_onBaseOptionHitFair.hidden=YES;
    vw_OnBaseview.hidden=YES;
    vw_StrikeView.hidden=YES;
    vw_typeOFpitchselect.hidden=YES;
    vw_LocationOfHitView.hidden=YES;
    
    if(bl_DefensivePosiPopups ==TRUE)
    {
        [self.scrv_scoreboard addSubview:vw_defensivePositions];
        bl_DefensivePosiPopups=FALSE;
    }
    
    else
    {   
        [self.vw_defensivePositions removeFromSuperview];
        bl_DefensivePosiPopups=TRUE;
        
    }
    
    
}


-(IBAction)addNew:(id)sender
{
   

}


-(void)viewDidUnload

{
	[super viewDidUnload];

	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
	self.popoverController            =   nil;
    self.toolbar                      =   nil;
    
    self.scrv_scoreboard              =   nil;
    self.vw_BallPopMainMenu           =   nil;
    self.vw_BallPopUpSubMenu          =   nil;
    self.vw_defensivePositions        =   nil;
    self.vw_LocationOfHitView         =   nil;
    self.vw_Mainbaserunnerview        =   nil;
    self.vw_onBaseOptionHitFair       =   nil;
    self.vw_OnBaseview                =   nil;
    self.vw_StrikeView                =   nil;
    self.vw_typeOFpitchselect         =   nil;
    self.txtf_gb1                     =   nil;
    self.txtf_gb2                     =   nil;
    self.txtf_db1                     =   nil;
    self.txtf_db2                     =   nil;
    self.txtf_agb1                    =   nil;
    self.txtf_agb2                    =   nil;
    self.btn_typeofpitch              =   nil;
    self.nvgn_datepicker              =   nil;
    self.detailDescriptionLabel       =   nil;
    
       
}

#pragma mark - Memory management

- (void)didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}


@end

