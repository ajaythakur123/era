//
//  ERAAPPAppDelegate.m
//  ERAAPP
//
//  Created by Ambadas B on 4/24/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ERAAPPAppDelegate.h"
#import "TestViewController.h"
#import "RootViewController.h"
#import "ERAApplicationViewController.h"
@implementation ERAAPPAppDelegate
@synthesize dataBaseConnection;
@synthesize window=_window;
@synthesize tempnav;
@synthesize tempviewcontroller;
@synthesize splitViewController=_splitViewController;
@synthesize myHomeTeamName;

@synthesize rootViewController=_rootViewController;

@synthesize detailViewController=_detailViewController;

@synthesize viewController=_viewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    ERAApplicationViewController *test = [[ERAApplicationViewController alloc]init];
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:test];
//   
//    myHomeTeamName = [[NSMutableString alloc] init];
//    tempnav = [[UINavigationController alloc]initWithRootViewController:tempviewcontroller];
//    tempnav = [nav retain];
    
    self.window.rootViewController = nav;
    [self.window makeKeyAndVisible];
    return YES;
}
- (void) createEditableCopyOfDatabaseIfNeeded 
{
	NSLog(@"Creating editable copy of database");
	// First, test for existence.
	BOOL success;
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:@"ERA_appDatabase.db"];
	success = [fileManager fileExistsAtPath:writableDBPath];
	if (success)
	{
		NSLog(@"ALready exists");
		return;
	}
	// The writable database does not exist, so copy the default to the appropriate location.
	NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"ERA_appDatabase.db"];
	success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
	if (!success) 
	{
		NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
	}
}

- (void) openDatabaseConnection 
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *path = [documentsDirectory stringByAppendingPathComponent:@"ERA_appDatabase.db"];
	// Open the database. The database was prepared outside the application.
	if (sqlite3_open([path UTF8String], &dataBaseConnection) == SQLITE_OK)
	{
		NSLog(@"Database Successfully Opened :)");
	} 
	else 
	{
		NSLog(@"Error in opening database :(");
	}
}
- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*
     Called when the application is about to terminate.
     Save data if appropriate.
     See also applicationDidEnterBackground:.
     */
}



@end
