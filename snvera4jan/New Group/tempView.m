//
//  tempView.m
//  ERAPitchChart
//
//  Created by Krishna Kothapalli on 02/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "tempView.h"

@implementation tempView

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    NSLog(@"Touch Called!");
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if(touch.view == self)
    {
        
        // Post a notification to remove the dispalyView from ScrollView...
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"removeDisplayView" object:nil]; 
        
        NSLog(@"Notification Called :");
        
    }
  
}


@end
