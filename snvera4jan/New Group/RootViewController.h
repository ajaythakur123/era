//
//  RootViewController.h
//  ERAAPP

//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DetailViewController;
@class ViewController;

@interface RootViewController : UITableViewController
{

    
}

		
@property (nonatomic, strong) IBOutlet DetailViewController *detailViewController;
@property (nonatomic,strong) IBOutlet ViewController *viewController;

@property (nonatomic, strong) NSMutableArray *rootArray;

@end
