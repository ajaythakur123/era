//
//  TestView_2.h
//  ERAAPP
//
//  Created by Uday Kumar on 8/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <UIKit/UIKit.h>
@class RootViewController;
@class DetailViewController;
@class ViewController;
@interface TestView_2 : UIViewController<UISplitViewControllerDelegate>
{
   IBOutlet UIWindow *window;
}
@property (nonatomic, strong) IBOutlet UIWindow *window;
@property (nonatomic, strong) IBOutlet UISplitViewController *splitViewController;

@property (nonatomic, strong) IBOutlet RootViewController *rootViewController;

@property (nonatomic, strong) IBOutlet DetailViewController *detailViewController;
@property (nonatomic, strong) IBOutlet ViewController *viewController;

-(void)test;
@end
