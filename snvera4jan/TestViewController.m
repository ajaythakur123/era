//
//  TestViewController.m
//  ERAAPP
//
//  Created by Uday Kumar on 8/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TestViewController.h"
#import "ViewController.h"
#import "ERAAPPAppDelegate.h"

@implementation TestViewController
@synthesize splitViewController=_splitViewController;

@synthesize rootViewController=_rootViewController;

@synthesize detailViewController=_detailViewController;
@synthesize lbl_datepicker,vw_matchinfoData,nvgn_datepicker,vw_newgameoriantation,date_picker;
@synthesize homeTeamName,viewAddplayer,playerNameTextField,JerseyNumberTextField,btn_batterside,btnPitcher,btnNewGame,viewAddplayerCP;

@synthesize viewController=_viewController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma Mark new Game
-(IBAction)push:(id)sender
{

    
  
    
    
    if (lbl_datepicker.text.length<=0||lbl_pitcher.text.length<=0||lbl_opponentName.text.length==0)
    {
        
        
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please add Opponent and Pitcher" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alrt_message show];
        return;
    }
  
    
//    UIWindow *window =  [[UIApplication sharedApplication] keyWindow];
//    window.rootViewController = self.splitViewController;
    
    ViewController *viewController = [[ViewController alloc]initWithNibName:@"ViewController" bundle:nil];
    // [self presentedViewController:viewController animated:YES];
    viewController.teamName =lbl_opponentName.text;
    viewController.stringCatcher=lbl_catcher.text;
    viewController.stringPitcher=lbl_pitcher.text;
    viewController.stringDate =lbl_datepicker.text;
    viewController.stringClassMode=@"newGame";
    viewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self.navigationController presentModalViewController:viewController animated:YES];
    //  [self.navigationController popViewControllerAnimated:<#(BOOL)#>];
    //  [self.navigationController pushViewController:viewController animated:YES];
    

    
   
     
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    btnNewGame.enabled=YES;
    btnPitcher.enabled=YES;
    [self getTeamDb];
}
// Date Picker Button Action.....
-(IBAction)Datepickerbutton:(id)sender
{
    
    NSLog(@"--> Datepickerbutton: Called! <--");
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = NSDateFormatterMediumStyle;
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *dateString = [dateFormatter stringFromDate:date_picker.date];
    
    lbl_datepicker.text=dateString;
    // lbl_datepicker.backgroundColor = [UIColor clearColor];
    lbl_datepicker.textColor = [UIColor blackColor];
    [lbl_datepicker setFont:[UIFont fontWithName:@"Arial-Bold" size:12]];
    [lbl_datepicker setTextAlignment:UITextAlignmentLeft];
    date_picker.hidden=YES;
    nvgn_datepicker.hidden=YES;
    
    
}

-(IBAction)DatepickerCancel:(id)sender

{
    NSLog(@"--> DatepickerCancel: Called! <--");
    
    nvgn_datepicker.hidden=YES;
    date_picker.hidden=YES;
    
}

-(IBAction)DatepickerClicked:(id)sender
{
    self.nvgn_datepicker.frame=CGRectMake(227, 72, 218, 44);
    date_picker.hidden = NO;
    nvgn_datepicker.hidden =NO;
    [self.vw_matchinfoData removeFromSuperview];
}



- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    
    
    
    matchDic =[[NSMutableDictionary alloc]init];
    DBObj =[[detailsDB alloc]init];
    teamNames_Array=[[NSMutableArray alloc]init];
    matchDataArray=[[NSMutableArray alloc]init];
    array_pitcher=[[NSMutableArray alloc]init];
    array_catcher=[[NSMutableArray alloc]init];
  
    
    //------date picker------
    date_picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(313, 280, 218, 44)];
    [date_picker setDate:[NSDate date] animated:YES];
    date_picker.datePickerMode = UIDatePickerModeDate;
    [date_picker setHidden:YES];
    nvgn_datepicker.hidden =YES;
    [self.view addSubview:date_picker];
    bl_DefensivePosiPopups =TRUE;
    
    [self Datepickerbutton :nil]; 
    
    self.nvgn_datepicker.frame=CGRectMake(225, 72, 218, 44);
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


#pragma mark ibaction for all
-(IBAction)MatchinfoData:(UIButton *)sender
{
    
    
    
    
    if([sender tag]==1)
    {
        NSLog(@"----tag1---");
        NSLog(@"----%s-----",__func__);
        
        type=@"teamName";
        
        [tblw_alldatatable reloadData];
        if (self.interfaceOrientation == UIInterfaceOrientationPortrait||self.interfaceOrientation ==UIInterfaceOrientationPortraitUpsideDown) 
        {
            [vw_matchinfoData setFrame:CGRectMake(315, 250, 218, 200)];
            
        }
        else
        {
            [vw_matchinfoData setFrame:CGRectMake(370, 148, 218, 200)];
            
            
        }
        
        
        nvgn_datepicker.hidden=YES;
        date_picker.hidden=YES;
        if(bl_DefensivePosiPopups ==TRUE)
        {
            [self.view addSubview:vw_matchinfoData];
            
            bl_DefensivePosiPopups=FALSE;
        }
        else
        {
            [self.vw_matchinfoData removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
        
    }
    
    if ([sender tag]==2)
    {
        NSLog(@"----%s-----",__func__);
        type=@"opponentName";
         
        
        [tblw_alldatatable reloadData];
        if (self.interfaceOrientation == UIInterfaceOrientationPortrait||self.interfaceOrientation ==UIInterfaceOrientationPortraitUpsideDown) 
        {
            [vw_matchinfoData setFrame:CGRectMake(322, 340, 218, 200)]; 
            
            
        }
        else
        {
            [vw_matchinfoData setFrame:CGRectMake(377, 235, 218, 200)]; 
        }
        
        
        nvgn_datepicker.hidden=YES;
        date_picker.hidden=YES;
        if(bl_DefensivePosiPopups ==TRUE)
        {
            
            [self.view addSubview:vw_matchinfoData];
            
            bl_DefensivePosiPopups=FALSE;
        }
        
        else
        {
            
            [self.vw_matchinfoData removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
    }
    if ([sender tag]==3)
    {
        
//        if (lbl_teamName.text.length==0 )
//        {
//            
//            UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please select Team Name" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
//            [alrt_message show];
//            [alrt_message release];
//            return;
//            
//            
//        }
        
          array_pitcher =[DBObj selectHomeTeamPlayerstFromDb:@"P"];
//        
        NSLog(@"----tag3---");
        type=@"Pitcher";
        [tblw_alldatatable reloadData];
        if (self.interfaceOrientation == UIInterfaceOrientationPortrait||self.interfaceOrientation ==UIInterfaceOrientationPortraitUpsideDown) 
        {
            [vw_matchinfoData setFrame:CGRectMake(322, 535, 218, 200)];
            
        }
        else
        {
            [vw_matchinfoData setFrame:CGRectMake(377, 430, 218, 200)]; 
        }
        
        nvgn_datepicker.hidden=YES;
        date_picker.hidden=YES;
        if(bl_DefensivePosiPopups ==TRUE)
        {
            
            [self.view addSubview:vw_matchinfoData];
            
            bl_DefensivePosiPopups=FALSE;
        }
        
        else
        {
            
            [self.vw_matchinfoData removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
        
        
        
        
    }
    
    if([sender tag]==4)
    {
        
//        if (lbl_teamName.text.length==0)
//        {
//            
//            UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please select Team Name!" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
//            [alrt_message show];
//            [alrt_message release];
//            return;
//            
//            
//        }
       // NSUserDefaults *teamNamehome =[NSUserDefaults standardUserDefaults];
    
        
        array_catcher =[DBObj selectHomeTeamPlayerstFromDb:@"C"];
        
        
        NSLog(@"----tag4---");
        type=@"Catcher";
        [tblw_alldatatable reloadData];
        if (self.interfaceOrientation == UIInterfaceOrientationPortrait||self.interfaceOrientation ==UIInterfaceOrientationPortraitUpsideDown) 
        {
            [vw_matchinfoData setFrame:CGRectMake(322, 440, 218, 200)]; 
            
            
        }
        else
        {
            [vw_matchinfoData setFrame:CGRectMake(377, 335, 218, 200)]; 
        }
        
        nvgn_datepicker.hidden=YES;
        date_picker.hidden=YES;
        if(bl_DefensivePosiPopups ==TRUE)
        {
            
            [self.view addSubview:vw_matchinfoData];
            
            bl_DefensivePosiPopups=FALSE;
        }
        
        else
        {
            
            [self.vw_matchinfoData removeFromSuperview];
            bl_DefensivePosiPopups=TRUE;
            
        }
        
        
        
    } 
    
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    nvgn_datepicker.hidden=YES;
    [self.vw_matchinfoData removeFromSuperview];
    date_picker.hidden=YES;
    bl_DefensivePosiPopups=TRUE;
    
}

-(IBAction)Cancel
{
    btnNewGame.enabled=YES;
    btnPitcher.enabled=YES;
    [viewAddplayer removeFromSuperview];
}


-(void)CatcherPitcher:(id)sender
{
    
    
    
}
-(IBAction)BatterSideMethod:(id)sender
{
    
    typeBatter=@"batterside";
    UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please Select the batter side" delegate:self cancelButtonTitle:@"Left" otherButtonTitles:@"Right", nil];
    [alrt_message show];

    
}
-(IBAction)sumbmitPlayerToDabase{
    
    
    
    if([JerseyNumberTextField.text length]==0||[btn_batterside.titleLabel.text length]==0||[playerNameTextField.text length]==0)
    {
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please fill all the details" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alrt_message show];
        return;
    }
    
    if ([self validateTextField:JerseyNumberTextField.text]==NO)
    {
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number Should be numeric" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alrt_message show];
        return; 
    }
    
    
    

     
    
          if ( type==@"Catcher") 
      {
          
          
          for (int i=0; i<[array_catcher count]; i++) {
              
              NSString *jerseyrnumber =[[array_catcher objectAtIndex:i]objectForKey:@"JerseyNumber"];
              
              if ([JerseyNumberTextField.text isEqualToString:jerseyrnumber]) {
                  
                  UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number already exist" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                  [alrt_message show];
                  return;
                  
                  
              }
              
          }
          //
          
          NSMutableDictionary *dict = [[NSMutableDictionary alloc]init ];
          
          [dict setObject:@"Team" forKey:@"TeamName"];
          
          [dict setObject:playerNameTextField.text forKey:@"PlayerName"];
          [dict setObject:JerseyNumberTextField.text forKey:@"JerseyNumber"];
          [dict setObject:btn_batterside.titleLabel.text forKey:@"Handling"];
          [dict setObject:@"C"forKey:@"Status"];
          [array_pitcher addObject:dict];
              [DBObj insertHomePlayerIntoDb:dict];
          //[arrayPlayers addObject:textAlert.text];
          lbl_catcher.text=playerNameTextField.text;
          
         // [tblw_alldatatable reloadData];
          btnNewGame.enabled=YES;
          btnPitcher.enabled=YES;
          [viewAddplayer removeFromSuperview];
      }
     if (type==@"Pitcher") 
     {
         
         
         for (int i=0; i<[array_pitcher count]; i++) {
             
             NSString *jerseyrnumber =[[array_pitcher objectAtIndex:i]objectForKey:@"JerseyNumber"];
             
             if ([JerseyNumberTextField.text isEqualToString:jerseyrnumber]) {
                 
                 UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Jersey Number already exist" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                 [alrt_message show];
                 return;
                 
                 
             }
             
         }

         NSMutableDictionary *dict = [[NSMutableDictionary alloc]init ];
        // [dict setObject:textAlert.text forKey:@"PlayerName"];
          [dict setObject:@"Team" forKey:@"TeamName"];
         [dict setObject:playerNameTextField.text forKey:@"PlayerName"];
         [dict setObject:JerseyNumberTextField.text forKey:@"JerseyNumber"];
         [dict setObject:btn_batterside.titleLabel.text forKey:@"Handling"];
         [dict setObject:@"P"forKey:@"Status"];
         
           [DBObj insertHomePlayerIntoDb:dict];
         [array_pitcher addObject:dict];
         
         //[arrayPlayers addObject:textAlert.text];
         lbl_pitcher.text=playerNameTextField.text;
         
         //[tblw_alldatatable reloadData];
         btnNewGame.enabled=YES;
         btnPitcher.enabled=YES;
              [viewAddplayer removeFromSuperview];  
         
     }
          
    

    
}


#pragma mark add player

-(IBAction)addNew:(id)sender
{
    
    
    if ( type==@"Catcher")
    {
        
        btnNewGame.enabled=NO;
        btnPitcher.enabled=NO;
        viewAddplayer.frame=CGRectMake(200, 198, 379,269);
        [self.view addSubview:viewAddplayer];
        
        
         [self.vw_matchinfoData removeFromSuperview]; 
        playerNameTextField.text=@"";
        JerseyNumberTextField.text=@"";
        btn_batterside.titleLabel.text=nil;
        

    }
    
    if ( type==@"Pitcher")
    {
        btnNewGame.enabled=NO;
        btnPitcher.enabled=NO;
        viewAddplayer.frame=CGRectMake(200, 198, 379,269);
        [self.view addSubview:viewAddplayer];
        
         [self.vw_matchinfoData removeFromSuperview]; 
        
        playerNameTextField.text=@"";
        JerseyNumberTextField.text=@"";
        btn_batterside.titleLabel.text=nil;

    }

    if ( type==@"opponentName")
    {
        type=@"opponentName";
        UIAlertView *myAlertView = [[UIAlertView alloc]initWithTitle:@"Please addd Team Name" message:@"Please select Team Name" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
        
        myTextField = [[UITextField alloc] initWithFrame:CGRectMake(12.0, 45.0, 260.0, 25.0)];
        [myTextField setBackgroundColor:[UIColor whiteColor]];
        [myAlertView addSubview:myTextField];
        [myAlertView show];
        
    }

   
    
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==0)
    {
        NSLog(@"----0----");
        
        if(typeBatter==@"batterside")
        {
            [btn_batterside setTitle:@"L" forState:UIControlStateNormal];
            [btn_batterside setTintColor:[UIColor blackColor]];
            NSLog(@"---%@---",btn_batterside.titleLabel.text);
        }
        
    }
    if (buttonIndex==1)
    {
        NSLog(@"----1----");  
        
        if(typeBatter==@"batterside")
        {
            [btn_batterside setTitle:@"R" forState:UIControlStateNormal];
            [btn_batterside setTintColor:[UIColor blackColor]];
            NSLog(@"---%@---",btn_batterside.titleLabel.text);
            return;
        }

        
        if ( type==@"teamName")
        {
            if (textAlert.text.length==0)
            {
                return;
            }
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init ];
            [dict setObject:textAlert.text forKey:@"TeamName"];
            [dict setObject:@"Loc" forKey:@"Location"];
            
            
            
            
            [DBObj insertIntoDb:dict];
            [teamNames_Array addObject:dict];
            lbl_teamName.text=textAlert.text;
            [tblw_alldatatable reloadData];
        }
        if (type==@"opponentName") 
        {
            if (myTextField.text.length==0)
            {
                return;
            }
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init ];
            
            [dict setObject:myTextField.text forKey:@"TeamName"];
             [dict setObject:@"Loc" forKey:@"Location"];
            [teamNames_Array addObject:dict];
            lbl_opponentName.text=myTextField.text;
           //  [DBObj insertIntoDb:dict];
            [tblw_alldatatable reloadData];
            
            [DBObj insertIntoDb:dict];
        }
        
        if (type==@"Pitcher") 
        {
            if (textAlert.text.length==0)
            {
                return;
            }
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init ];
            [dict setObject:textAlert.text forKey:@"PlayerName"];
           
            [dict setObject:playerNameTextField.text forKey:@"PlayerName"];
            [dict setObject:JerseyNumberTextField.text forKey:@"JerseyNumber"];
            [dict setObject:btn_batterside.titleLabel.text forKey:@"Handling"];
            [dict setObject:@"P"forKey:@"Status"];
            [array_pitcher addObject:dict];
            
              //[arrayPlayers addObject:textAlert.text];
            lbl_pitcher.text=textAlert.text;
            
            [tblw_alldatatable reloadData];
        }
        
        if ( type==@"Catcher") 
        {
            if (textAlert.text.length==0)
            {
                return;
            }
            
            NSMutableDictionary *dict = [[NSMutableDictionary alloc]init ];
            [dict setObject:textAlert.text forKey:@"PlayerName"];
            
            [array_catcher addObject:dict];
           
            lbl_catcher.text=textAlert.text;
            
            [tblw_alldatatable reloadData];
        }
    }
    
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (type==@"teamName") 
    {
        
        [lbl_titleMatchdata setText:@"Team Names"];
        
        [lbl_titleMatchdata setTextAlignment:UITextAlignmentCenter];  
        [lbl_titleMatchdata setTextColor:[UIColor whiteColor]];
        //[add_Button setHidden:NO];
        
        
        return [teamNames_Array count];
        
        
    }
    
    if (type==@"opponentName") 
    {
        
        [lbl_titleMatchdata setText:@"Opponent Team"];
        [lbl_titleMatchdata setTextColor:[UIColor whiteColor]];
        //[add_Button setHidden:NO];
        [lbl_titleMatchdata setTextAlignment:UITextAlignmentCenter];
        [lbl_titleMatchdata setTextColor:[UIColor whiteColor]];
        return [teamNames_Array count];
        
    }
    
    if (type==@"Pitcher") 
    {
        
        [lbl_titleMatchdata setText:@"Pitcher"];
        [lbl_titleMatchdata setTextColor:[UIColor whiteColor]];
        //[add_Button setHidden:NO];
        [lbl_titleMatchdata setTextAlignment:UITextAlignmentCenter];
        [lbl_titleMatchdata setTextColor:[UIColor whiteColor]];
        return [array_pitcher count];
        
    }
    
    
    if (type==@"Catcher") 
    {
        
        [lbl_titleMatchdata setText:@"Catcher"];
        [lbl_titleMatchdata setTextColor:[UIColor whiteColor]];
        //[add_Button setHidden:NO];
        [lbl_titleMatchdata setTextAlignment:UITextAlignmentCenter];
        [lbl_titleMatchdata setTextColor:[UIColor whiteColor]];
        return [array_catcher count];
        
    }
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell;// = [tableView dequeueReusableCellWithIdentifier:CellIdentifier] ;
	//if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    //	}
    
    UILabel *defensiveposn = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 30)];
    
    if (type==@"teamName") 
    {
        NSLog(@"%@",teamNames_Array);
        
        defensiveposn.text=[[teamNames_Array objectAtIndex:indexPath.row]objectForKey:@"TeamName"]; 
        
    }
    
    
    else if (type==@"opponentName")
    {
        
        defensiveposn.text=[[teamNames_Array objectAtIndex:indexPath.row]objectForKey:@"TeamName"]; 
        
        
        
    }
    else if (type==@"Pitcher") 
    {
        
        defensiveposn.text=[[array_pitcher objectAtIndex:indexPath.row]objectForKey:@"PlayerName"];         
    }
    
    else if (type==@"Catcher")
    {
        
        defensiveposn.text=[[array_catcher objectAtIndex:indexPath.row]objectForKey:@"PlayerName"]; 
        
    }
    
    
    
    else  {
        
        //defensiveposn.text = [NSString stringWithFormat:[array_defnsiveposidata objectAtIndex:indexPath.row]]; 
        
    }
    
    defensiveposn.textAlignment = UITextAlignmentLeft;
	defensiveposn.textColor = [UIColor blackColor];
    defensiveposn.backgroundColor = [UIColor clearColor];
	defensiveposn.font = [UIFont fontWithName:@"Arial" size:12];
    [cell addSubview:defensiveposn];
    
    cell.selectionStyle=UITableViewCellSelectionStyleBlue;
    tableView.separatorColor=[UIColor clearColor];
    
    return cell;
    
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // [tableviewList removeFromSuperview];
    
    if (type==@"teamName") 
    {
        
        lbl_catcher.text=@"";
        lbl_pitcher.text=@"";
        
        
        lbl_teamName.text=[[teamNames_Array objectAtIndex:indexPath.row]objectForKey:@"TeamName"];
      // _viewController.teamName=[NSString stringWithFormat:@"%@",[[teamNames_Array objectAtIndex:indexPath.row]objectForKey:@"TeamName"]];
        if([lbl_teamName.text isEqualToString:lbl_opponentName.text]) 
        {
            
            UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Team name and oppent team should not be same" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alrt_message show];
            lbl_teamName.text=@"";
            
        }
        
        
    }
    
    
    else if (type==@"opponentName")
    {
        
        lbl_opponentName.text=[[teamNames_Array objectAtIndex:indexPath.row]objectForKey:@"TeamName"];
        
        if([lbl_teamName.text isEqualToString:lbl_opponentName.text]) 
        {
            
            UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Team name and oppent team should not be same" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alrt_message show];
            lbl_opponentName.text=@"";
        }
        
    }
    
    else if (type==@"Pitcher")
    {
        
        
        lbl_pitcher.text=[[array_pitcher objectAtIndex:indexPath.row]objectForKey:@"PlayerName"]; 
//        
//        if([lbl_pitcher.text isEqualToString: lbl_catcher.text]) 
//        {
//            
//            UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Pitcher name and Catcher name should not be same" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
//            [alrt_message show];
//            [alrt_message release];
//            lbl_pitcher.text=@"";
//            
//        }
        
        
    }
    
    
    else if (type==@"Catcher")
    {
        
        
        
        // type =@"PlayerData";
        
        
        lbl_catcher.text=[[array_catcher objectAtIndex:indexPath.row]objectForKey:@"PlayerName"]; 
//        
//        if([lbl_pitcher.text isEqualToString: lbl_catcher.text]) 
//        {
//            
//            UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Pitcher name and Catcher name should not be same" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
//            [alrt_message show];
//            [alrt_message release];
//            lbl_catcher.text=@"";
//            
//        }
        
        
        
    }
    
    
    [self.vw_matchinfoData removeFromSuperview]; 
    bl_DefensivePosiPopups=TRUE;
}

-(BOOL)validateTextField:(NSString *)candidate 
{
    
    NSLog(@"validateTextField: Called");
    NSString *emailRegex=@"^(?:|0|[0-9]\\d*)(?:\\.\\d*)?$"; 
    NSLog(@"-all string value %@-",emailRegex);
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex]; 
    return [emailTest evaluateWithObject:candidate];
    
    
}
-(void)getTeamDb
{
    
    
    
    teamNames_Array =[DBObj selectPlayerstFromDb];
    NSLog(@"----%@---",teamNames_Array);
     
    for (int i=0;i<[teamNames_Array count] ; i++) {
        NSUserDefaults *team =[NSUserDefaults standardUserDefaults];
        if([[team objectForKey:@"TeamName"] isEqualToString:[[teamNames_Array objectAtIndex:i]objectForKey:@"TeamName"]])
        {
            [teamNames_Array removeObjectAtIndex:i];
        }
    }
    
    
    
}

-(void)getTeam11Db
{
    
    
    
    teamNames_Array =[DBObj selectPlayerstFromDb];
    NSLog(@"----%@---",teamNames_Array);
    
    for (int i=0;i<[teamNames_Array count] ; i++) {
        NSUserDefaults *team =[NSUserDefaults standardUserDefaults];
        if([[team objectForKey:@"TeamName"] isEqualToString:[[teamNames_Array objectAtIndex:i]objectForKey:@"TeamName"]])
        {
            [teamNames_Array removeObjectAtIndex:i];
        }
    }
    
    
    
}

#pragma mark New game
-(IBAction)NewGame
{
    if (lbl_datepicker.text.length<=0||lbl_pitcher.text.length<=0||lbl_opponentName.text.length==0)
    {
        
        
        UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Please Add team Pitcher" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alrt_message show];
        return;
    }
//    
//    [matchDic setObject:lbl_teamName.text forKey:@"TeamName"];
//    [matchDic setObject:lbl_opponentName.text forKey:@"OppentTeamName"];
//    [matchDic setObject:lbl_datepicker.text forKey:@"Date"];
//    [matchDic setObject:lbl_catcher.text forKey:@"Catcher"];
//    [matchDic setObject:lbl_pitcher.text forKey:@"Pitcher"];
//    //[matchDataArray addObject:matchDic];
//    [DBObj insertIntoDbforMatches:matchDic];
//    UIAlertView *alrt_message = [[UIAlertView alloc]initWithTitle:@"Message!" message:@"Data Store Suceesfully" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
//    [alrt_message show];
//    [alrt_message release];
    
    
    //    LoadNewGameViewController *loadNewGameViewController = [[LoadNewGameViewController alloc]initWithNibName:@"LoadNewGameViewController" bundle:nil];
    
    UIWindow *window =  [[UIApplication sharedApplication] keyWindow];
    window.rootViewController = self.splitViewController;
    
    //    [self.navigationController pushViewController:loadNewGameViewController animated:YES];
    //    [loadNewGameViewController release];
    
}



-(void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{   
    if(interfaceOrientation != UIInterfaceOrientationPortrait &&
       interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown)
    {
        // landscape
        [self.vw_matchinfoData removeFromSuperview];
        self.nvgn_datepicker.frame=CGRectMake(220, 72, 218, 44);
        self.vw_newgameoriantation.frame=CGRectMake(150, 60,579, 676);
        self.date_picker.frame=CGRectMake(370, 175, 218, 44);
        
        
    }
    else
    {  
        // portait
        [self.vw_matchinfoData removeFromSuperview];
        self.nvgn_datepicker.frame=CGRectMake(230, 72, 218, 44);
        
        self.vw_newgameoriantation.frame=CGRectMake(94, 163, 579, 676);
        self.date_picker.frame=CGRectMake(319, 280, 218
                                          , 44);
        
    }
    
    
    
    // Return YES for supported orientations
	return YES;
}





@end
